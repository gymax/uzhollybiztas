/*****************************************************************************
 * FILE PURPOSE:
 *******************************************************************************
 *
 * FILE NAME: vtmsg.h
 * Update data: 2019/01/04
 *
 * dependency: lowpan6.h
 *
 ******************************************************************************/
#ifndef __VTMSG_H__
#define __VTMSG_H__
/* VTMSG_TLV_TYPE_NETWROK_EVENT */
enum VTMSG_TLV_TYPE_NETWORK_EVENT_TBL {
/* payload needed */
    VTMSG_NEVENT_PAN_JOINED   = 0x0,
    VTMSG_NEVENT_BOOTSTRAP_SUCCESS,
    VTMSG_NEVENT_RECV_BEACON_REQ,
    VTMSG_NEVENT_ROUTE_DISCOVERY_SUCCESS,
    VTMSG_NEVENT_PATH_DISCOVERY_SUCCESS,
    VTMSG_NEVENT_PATH_DISCOVERY_FAIL,
    VTMSG_NEVENT_SEARCH_NEIGHBOR_EUI64,
/* payload NULL */
    VTMSG_NEVENT_SEND_BEACON = 0x10,
    VTMSG_NEVENT_BOOTSTRAP_FAIL,
    VTMSG_NEVENT_ACK
};

typedef struct _vtmsg_msgHdr_ {
    unsigned char type;
    unsigned char flags;
    unsigned char lenHigh;
    unsigned char lenLow;
} vtmsg_msgHdr_s;

typedef struct {
    uint8_t sts;
    uint8_t msgType;
    uint8_t hdrFlags;
    int32_t pyldLen;
    uint8_t *pBuff;
} vtmsg_msg_s;

typedef struct _vtmsg_msgLongHdr_ {
    unsigned char type;
    unsigned char flags;
    unsigned char lenByte3;
    unsigned char lenByte2;
    unsigned char lenByte1;
    unsigned char lenByte0;
} vtmsg_msgLongHdr_s;

typedef enum {
    BBP_SIG_TYPE_PREAMBLE,
    BBP_SIG_TYPE_NOISE_FLOOR,
} bbp_sig_t;

enum VTUPG_ADDR_MODE_TABLE {
    VTUPG_ADDR_MODE_EUI64   = 0,
    VTUPG_ADDR_MODE_SHORTADDR,
};



/* below definitions use in host cli only */
#ifdef HOSTCLI_INC_USAGE
#define DIRECTION_MAX 2

#define MAX_BBP_MIB_LOG_ENTRY   4
struct bbp_mib_log_st {
    uint8_t index;
    uint8_t fir3_scale;
    uint8_t ot_event_cnt;
    uint8_t apdt_preamb_det_scale;
    uint16_t urgent_in_cnt;
    uint16_t tx_retransmit_cnt;
    uint32_t preamb_det;
    uint32_t syncm_det_success;
    uint32_t fch_crc_correct;
    uint32_t data_fch_correct;
    uint32_t data_correct;
};

struct bbp_mib_log_table {
    uint8_t current_index;
    uint8_t total_number;
    uint8_t reserved[2];
    struct bbp_mib_log_st bbp_log[MAX_BBP_MIB_LOG_ENTRY];
};

enum reboot_flag_t {
    RB_FLAG_CLI = 0,
    RB_FLAG_SDK_WD = 1,
    RB_FLAG_DMA = 2,
    RB_FLAG_SQ = 3,
    RB_FLAG_APP = 4,
    RB_FLAG_UPG = 5,
    RB_FLAG_CMD = 6,
    RB_FLAG_NOISE = 7,
    RB_FLAG_DMA_RX = 8,
    RB_FLAG_TX_FA = 9,
    RB_FLAG_DMA_SECURITY = 10,
    RB_FLAG_MAX,
};


// G3-2015 table 9-21 FCC
typedef struct NEIGHBOUR_TABLE_FCC {
    // first word
    uint32_t lqi                         : 8;       // link quality indicator of the link to the neighbour (reverse LQI) and the neighbour node (7.17.2.4) (8.9)
    uint32_t tone_map                    : 24;      // tone map used when transmitting to this neighbour (3 tones per tonemap group bit)

    // second word
    uint32_t tx_gain                     : 4;       // transmitter gain used when transmitting to this neighbour
    uint32_t modulation_type             : 3;       // modulation type used when transmitting to this neighbour (enum VTMAC_MODULATION_TYPE)
    uint32_t modulation_scheme           : 1;       // payload modulation scheme used when transmitting to this neighbour (enum VTMAC_MODULATION_SCHEME)
    uint32_t phase_differential          : 3;       // phase difference in multiples of 60 degrees between the mains phase of the local node
    uint32_t tx_res                      : 1;       // transmitter gain corresponding to one gain step (enum VTMAC_GAIN_STEP)
    uint32_t reserved                    : 4;
    uint32_t tx_coef_1_0                 : 2;       // (optional) number of gain steps requested for the tones represented by TM[0]
    uint32_t tx_coef_3_2                 : 2;       // (optional) number of gain steps requested for the tones represented by TM[1]
    uint32_t tx_coef_5_4                 : 2;       // (optional) number of gain steps requested for the tones represented by TM[2]
    uint32_t tx_coef_7_6                 : 2;       // (optional) number of gain steps requested for the tones represented by TM[3]
    uint32_t tx_coef_9_8                 : 2;       // (optional) number of gain steps requested for the tones represented by TM[4]
    uint32_t tx_coef_11_10               : 2;       // (optional) number of gain steps requested for the tones represented by TM[5]
    uint32_t tx_coef_13_12               : 2;       // (optional) number of gain steps requested for the tones represented by TM[6]
    uint32_t tx_coef_15_14               : 2;       // (optional) number of gain steps requested for the tones represented by TM[7]

    // third word
    uint32_t tx_coef_17_16               : 2;       // (optional) number of gain steps requested for the tones represented by TM[8]
    uint32_t tx_coef_19_18               : 2;       // (optional) number of gain steps requested for the tones represented by TM[9]
    uint32_t tx_coef_21_20               : 2;       // (optional) number of gain steps requested for the tones represented by TM[10]
    uint32_t tx_coef_23_22               : 2;       // (optional) number of gain steps requested for the tones represented by TM[11]
    uint32_t tx_coef_25_24               : 2;       // (optional) number of gain steps requested for the tones represented by TM[12]
    uint32_t tx_coef_27_26               : 2;       // (optional) number of gain steps requested for the tones represented by TM[13]
    uint32_t tx_coef_29_28               : 2;       // (optional) number of gain steps requested for the tones represented by TM[14]
    uint32_t tx_coef_31_30               : 2;       // (optional) number of gain steps requested for the tones represented by TM[15]
    uint32_t tx_coef_33_32               : 2;       // (optional) number of gain steps requested for the tones represented by TM[16]
    uint32_t tx_coef_35_34               : 2;       // (optional) number of gain steps requested for the tones represented by TM[17]
    uint32_t tx_coef_37_36               : 2;       // (optional) number of gain steps requested for the tones represented by TM[18]
    uint32_t tx_coef_39_38               : 2;       // (optional) number of gain steps requested for the tones represented by TM[19]
    uint32_t tx_coef_41_40               : 2;       // (optional) number of gain steps requested for the tones represented by TM[20]
    uint32_t tx_coef_43_42               : 2;       // (optional) number of gain steps requested for the tones represented by TM[21]
    uint32_t tx_coef_45_44               : 2;       // (optional) number of gain steps requested for the tones represented by TM[22]
    uint32_t tx_coef_47_46               : 2;       // (optional) number of gain steps requested for the tones represented by TM[23]
    // fourth word
    uint32_t curr_tone_map               : 24;
    uint32_t curr_modulation_type        : 3;
    uint32_t reserved1                   : 5;
} NEIGHBOUR_TABLE_FCC;
// G3-2015 table 9-20 CENELEC
typedef struct NEIGHBOUR_TABLE_CENELEC {
    // first word
    uint32_t lqi                         : 8;       // link quality indicator of the link to the neighbour (reverse LQI) and the neighbour node (7.17.2.4) (8.9)
    uint32_t tx_gain                     : 4;       // transmitter gain used when transmitting to this neighbour
    uint32_t phase_differential          : 3;       // phase difference in multiples of 60 degrees between the mains phase of the local node
    uint32_t tx_res                      : 1;       // transmitter gain corresponding to one gain step (enum VTMAC_GAIN_STEP)
    uint32_t tone_map                    : 6;       // tone map used when transmitting to this neighbour (6 tones per tonemap group bit) (0-masked 1-active, applies to payload only)
    uint32_t modulation_type             : 2;       // modulation type used when transmitting to this neighbour (enum VTMAC_MODULATION_TYPE)
    uint32_t modulation_scheme           : 1;       // payload modulation scheme used when transmitting to this neighbour (enum VTMAC_MODULATION_SCHEME)
    uint32_t reserved                    : 7;

    // second word
    uint32_t tx_coef_3_0                 : 4;       // (optional) number of gain steps requested for the tones represented by TM[0]
    uint32_t tx_coef_7_4                 : 4;       // (optional) number of gain steps requested for the tones represented by TM[1]
    uint32_t tx_coef_11_8                : 4;       // (optional) number of gain steps requested for the tones represented by TM[2]
    uint32_t tx_coef_15_12               : 4;       // (optional) number of gain steps requested for the tones represented by TM[3]
    uint32_t tx_coef_19_16               : 4;       // (optional) number of gain steps requested for the tones represented by TM[4]
    uint32_t tx_coef_23_20               : 4;       // (optional) number of gain steps requested for the tones represented by TM[5]
    uint32_t curr_tone_map               : 6;
    uint32_t curr_modulation_type        : 2;
} NEIGHBOUR_TABLE_CENELEC;
typedef struct Neighbor_Table_ST {
    // first word
#if (CONFIG_SW_MAC_ZC) || (CONFIG_SW_MAC_ST)
    uint32_t null            : 9;
    uint32_t st_state        : 1;
    uint32_t zc_state        : 2;
#else
    uint32_t null                : 12;
#endif
    uint32_t tmr_sec             : 8;
    uint32_t neighbor_sec        : 8;
    uint32_t bandplane           : 2;       // bandplane (enum VTMAC_BANDPLANE)
    uint32_t force_default_table : 1;
    uint32_t adaptive_request_tmr : 1;

    // second word
    uint8_t dest_addr[2];                           // the mac short address of this neighbour
    uint8_t tmr_valid_time;                         // remaining time in minutes until when the tone map response parameters are considered valid
    uint8_t neighbour_valid_time;                   // remaining time in minutes until when this entry is considered valid

    union {
        NEIGHBOUR_TABLE_CENELEC cen;
        NEIGHBOUR_TABLE_FCC fcc;
    } direction[DIRECTION_MAX];
} Neighbor_Table_ST;

typedef struct tplg_table_st {
    uint8_t dest_short_addr[2];
    uint8_t prev_hop_short_addr[2];
} TPLG_TABLE_ST;

typedef struct route_table_st {
    uint16_t valid_time;   // minute
    uint16_t route_cost;

    uint8_t dest_short_addr[2];
    uint8_t next_hop_short_addr[2];

    uint8_t hop_count       : 4;   // 0~14
    uint8_t weak_link_count : 4;   // 0~15
    uint8_t valid_sec;
    uint16_t rrep_sequence_number;
} ROUTE_TABLE_ST;

typedef struct lbs_alloc_short_addr_table {
    uint8_t valid;
    uint8_t null;
    uint8_t alloc_short_addr[2];
    uint8_t eui_64_bit_addr[8];
} S2E_TABLE_ST;

typedef struct dev_table_entry {
    uint8_t valid;
    uint8_t flag; // bit0-3: hop count
    uint8_t alloc_short_addr[2];
    uint8_t eui_64_bit_addr[8];
    uint8_t joined;
    uint8_t retry;
    ROUTE_TABLE_ST route_info;
} JOINED_DEV_TABLE_ST;



typedef struct vt_system_log_data_st {
    uint32_t system_tick;
    union {
        uint32_t value;

        struct {
            uint8_t log_type;
            uint8_t data[3];
        } format;
    };
} VT_SYSTEM_DATA_LOG;

#define MAX_REG_SIZE 1
#define MAX_SYSTEM_LOG_ENTRY     128

typedef struct sys_log_t {
    uint8_t name[3];
    uint8_t reg_idx;
    uint16_t reboot_flag_nv;
    uint16_t system_boot_up_counter;
    uint8_t dma_task_state;
    uint8_t rx_task_state;
    uint8_t tcip_task_state;
    uint8_t recovery_network_join_state;
    uint32_t reg_A0_80[MAX_REG_SIZE];
    uint32_t reg_A0_84[MAX_REG_SIZE];
    uint32_t reg_A0_C8[MAX_REG_SIZE];
    uint32_t reg_A0_CC[MAX_REG_SIZE];
    uint32_t reg_A1_B0[MAX_REG_SIZE];
    uint32_t reg_A1_C8[MAX_REG_SIZE];
    uint32_t reg_A024_9C00[MAX_REG_SIZE];
    uint32_t reg_A024_9C10[MAX_REG_SIZE];
} SYS_LOG;

enum VT_SYSTEM_LOG_TYPE {
    SYSTEM_LOG_TYPE_NONE                    = 0,
    SYSTEM_LOG_TYPE_SYSTEM_BOOT_UP          = 1,
    SYSTEM_LOG_TYPE_SYSTEM_REBOOT           = 2,
    SYSTEM_LOG_TYPE_DMA_RX_UNAVAIL          = 3,
    SYSTEM_LOG_TYPE_DMA_TX_STATE_ERROR      = 4,    // dma or bbp not ready, can't start tx
    SYSTEM_LOG_TYPE_DMA_TX_VCS_NOT_IDLE     = 5,
    SYSTEM_LOG_TYPE_DMA_TX_WAIT_BBP_DMA     = 6,    // csma fail or ack timeout
    SYSTEM_LOG_TYPE_DMA_TX_RETRY_FAIL       = 7,
    SYSTEM_LOG_TYPE_ROUTE_DISCOVERY_FAIL    = 8,
    SYSTEM_LOG_TYPE_BOOTSTRAP_MACP_MISMATCH = 9,    // only for lbs
    SYSTEM_LOG_TYPE_BOOTSTRAP_JOIN_SUCCESS  = 10,   // only for lbd
    SYSTEM_LOG_TYPE_PANID_CONFLICT          = 11,   // only for lbs
    SYSTEM_LOG_TYPE_EXCEPTION               = 12,
    SYSTEM_LOG_TYPE_DYING_GASP              = 13,
    SYSTEM_LOG_TYPE_IMMED_QUEUE_FULL        = 14,
    SYSTEM_LOG_TYPE_POOL_FREE_PTR_ERROR     = 15,
    SYSTEM_LOG_TYPE_DMA_SECURITY_ERROR      = 16,
    SYSTEM_LOG_TYPE_MAX                     = 17,
};

struct NEI_EUI64 {
    uint8_t eui64[8];
    uint16_t valid      : 1;
    uint16_t rx_eui_cnt : 15;
};

enum VTUPG_DB_STATE {
    UPG_STATE_READY = 0,
    UPG_STATE_INFO,
    UPG_STATE_DATA,
    UPG_STATE_DONE,
};

enum VTUPG_DB_INDEX {
    VTUPG_DATAPART_1ST   = 0,
    VTUPG_DATAPART_2ND,
    VTUPG_DATAPART_3RD,
    VTUPG_DATAPART_4TH,
};

enum VTUPG_MODE_TABLE {
    VTUPG_MODE_UNICAST   = 0,
    VTUPG_MODE_BCAST,
    VTUPG_MODE_SERIAL,
    VTUPG_MODE_UN_ONLY,
};



enum VTUPG_STATE_TABLE {
    UPG_NULL        = 0,
    UPG_HOST_STANDBY,
    UPG_HOST_SEND_WR,
    UPG_HOST_SEND_WE,
    UPG_HOST_SEND_UN,
    UPG_HOST_COLLECTION,
    UPG_HOST_UPGRADE,

    UPG_CLIENT_STANDBY,
    UPG_CLIENT_COLLECTION,
    UPG_CLIENT_REQUEST,
    UPG_CLIENT_READY,

    UPG_GET_UQ_INFO,
    UPG_GET_UQ_BLOCK,
    UPG_GET_UN_READY,
    UPG_GET_UN_CHKSUM_ERROR,
    UPG_GET_UN_STAGE_ERROR,
    UPG_GET_UN_DATA_ERROR,
    UPG_GET_UN_DATA_OK,
    UPG_GET_UN_READY_SERIAL,
    UPG_GET_UN_CANBE_FASTFORWARD,
    UPG_ADD_BLACKLIST,
};

enum VTUPG_PROGRESS_TABLE {
    UPG_PROGRESS_NONE     = 0x30,   //start from 0x30 to distingvtmsg.h from other state for VTMSG_TLV_TYPE_UPGRADE_EVENT
    UPG_PROGRESS_FILEINFO,
    UPG_PROGRESS_DATA,
    UPG_PROGRESS_OVER,
    UPG_PROGRESS_DATA_WE,
    UPG_PROGRESS_FILEINFO_WE,
};

enum VTUPG_DB_DATA_STATE {
    UPG_DATA_IDLE = 0,
    UPG_DATA_READY,
    UPG_DATA_SENDING,
};

enum VTUPG_CRC_METHOD {
    UPG_CRC16_HDLC = 0,
    UPG_XOR32,
};

typedef struct UPG_DB {
    uint8_t *pData;
    uint32_t size;
    uint8_t state;
    uint32_t *pBlockMap;
} upg_db_t;

enum VTMAC_BANDPLAN {
    VTMAC_BANDPLAN_G3_CENELEC      = 0,
    VTMAC_BANDPLAN_G3_FCC          = 1,
};
#define REDUCED_FUNCTION_DEVICE         0
#define FULL_FUNCTION_DEVICE            1
#define LOWPAN6_BOOTSTRAPPING_DEVICE    0
#define LOWPAN6_BOOTSTRAPPING_SERVER    1
#define NOT_UIU_DEVICE    0
#define UIU_DEVICE        1
#define DCU_IF_TOTAL    3

#define LF "\r\n"

#endif

/*
 * Message layout:
 *  | Message Hdr |  Message Body   |
 *
 *
 *  Message header layout:
 *    ---------------------------------
 *    Offset   Length   Description
 *    ---------------------------------
 *      0        1      Message Type
 *      1        1      Flags           (DIRECTION|SOL/UNSOL|SOURCE|DEVICE ID)
 *      2        1      Length High
 *      3        1      Length Low
 *    ---------------------------------
 *
 *  TLV format:
 *    ---------------------------------
 *    Offset   Length   Description
 *    ---------------------------------
 *       0       2       Type
 *       2       2       Length
 *       4       N       Value
 *    ---------------------------------
 */

#define VTMSG_STS_SUCCESS                0x0
#define VTMSG_STS_INV_PARAMS             0x1
#define VTMSG_STS_NOT_SUPPORTED          0x2
#define VTMSG_STS_BAD_MSG                0x3
#define VTMSG_STS_NOT_READY              0x4
#define VTMSG_STS_OOM                    0x5
#define VTMSG_STS_OPERATION_TIMED_OUT    0x6
#define VTMSG_STS_INTERNAL_FAILURE       0x7
#define VTMSG_STS_END_OF_ENTRY           0x8


#define VTMSG_MSG_HDR_TYPE_FIELD_LEN           1
#define VTMSG_MSG_HDR_TYPE_FIELD_OFF           0
#define VTMSG_MSG_HDR_FLAGS_FIELD_LEN          1
#define VTMSG_MSG_HDR_FLAGS_FIELD_OFF          1
#define VTMSG_MSG_HDR_PYLD_LEN_FIELD_LEN       2
#define VTMSG_MSG_HDR_PYLD_LEN_HIGH_FIELD_OFF  2
#define VTMSG_MSG_HDR_PYLD_LEN_LOW_FIELD_OFF   3

#define VTMSG_MSG_HDR_PYLD_LEN_FIELD_LEN_LONG      4
#define VTMSG_MSG_HDR_PYLD_LEN_BYTE3_FIELD_OFF     2
#define VTMSG_MSG_HDR_PYLD_LEN_BYTE2_FIELD_OFF     3
#define VTMSG_MSG_HDR_PYLD_LEN_BYTE1_FIELD_OFF     4
#define VTMSG_MSG_HDR_PYLD_LEN_BYTE0_FIELD_OFF     5

#define VTMSG_SEQ_LEN   1

// source + imcoming = send back to source
// source + outgoing
#define VTMSG_MSG_HDR_FLAG_DIRECTION_BIT   0x7 // Outgoing(1)/ Imcomig(0)
#define VTMSG_MSG_HDR_FLAG_SOL_BIT         0x6 // Solicited(1) /Unsolicited(0)
#define VTMSG_MSG_HDR_FLAG_SOURCE_BIT      0x5 // uart(0: Uart0 or Uart1) / spi(1)
#define VTMSG_MSG_HDR_FLAG_SDK_HOOK_BIT    0x4
#define VTMSG_MSG_HDR_FLAG_UART_SOURCE_BIT 0x3 // uart(0: Debug port) / uart(1: App port)
#define VTMSG_MSG_HDR_FLAG_DEV_ID_BIT1     0x1 // device id (0-3)
#define VTMSG_MSG_HDR_FLAG_DEV_ID_BIT0     0x0 //

#define VTMSG_SOLICITED_RESP     0x0
#define VTMSG_UNSOLICITED_MSG    0x1
#define VTMSG_IF_UART            0x0
#define VTMSG_IF_SPI             0x1
#define VTMSG_DIR_IN             0x0
#define VTMSG_DIR_OUT            0x1

/* TYPE 0x0*  SYSYEM CMD */
#define VTMSG_MSG_TYPE_SYS_CONFIGURATION              0x00
#define VTMSG_MSG_TYPE_GET_VERSION_INTERNAL           0x01
#define VTMSG_MSG_TYPE_GET_MEM_INFO                   0x02
#define VTMSG_MSG_TYPE_GET_LBS_INFO                   0x03
#define VTMSG_MSG_TYPE_SEND_JOIN_NETWORK_CMD          0x04
#define VTMSG_MSG_TYPE_APP_EXCHANGE_DATA              0x05
#define VTMSG_MSG_TYPE_MEM_OP_DUMP_WORD               0x07
#define VTMSG_MSG_TYPE_MEM_OP_WRITE_WORD              0x08
#define VTMSG_MSG_TYPE_REBOOT                         0x0b
#define VTMSG_MSG_TYPE_LOG_LEVEL                      0x0d
#define VTMSG_MSG_TYPE_SYS_EVENT                      0x0e
#define VTMSG_MSG_TYPE_GET_NEI_EUI_TABLE              0x0f

/* TYPE 0x10 - 0x15  SYSYEM CMD FOR CUSTOMER */
#define VTMSG_MSG_TYPE_START_NETWORK                  0x10
#define VTMSG_MSG_TYPE_GET_VERSION_INFO               0x11
#define VTMSG_MSG_TYPE_GET_JOINED_TABLE               0x12
#define VTMSG_MSG_TYPE_GET_XXXXXXXXXXXXXXXX           0x13
#define VTMSG_MSG_TYPE_GET_ZEROX_HIT_RECORD           0x14 //zc_hit_record

/* TYPE 0x15 command of BBP SIG */
#define VTMSG_MSG_TYPE_BBP_SIG                        0x15

/* TYPE 0x16 - 0x1f VANGO */
#define VTMSG_MSG_TYPE_UPGRADE_CONTROL                0x16
#define VTMSG_MSG_TYPE_ZEROX_SETTINGS                 0x17
#define VTMSG_MSG_TYPE_UPGRADE_EVENT                  0x18
#define VTMSG_MSG_TYPE_UPGRADE_DATA                   0x19
#define VTMSG_MSG_TYPE_UPGRADE_OP                     0x1a
#define VTMSG_MSG_TYPE_BBP_SETTINGS                   0x1b
#define VTMSG_MSG_TYPE_APP_SETTINGS                   0x1c
#define VTMSG_MSG_TYPE_BBP_RAW_DUMP                   0x1d
#define VTMSG_MSG_TYPE_NETWROK_DIAGNOSTIC             0x1e
#define VTMSG_MSG_TYPE_RUNTIME_SYS_LOG                0x1f


/* TYPE 0x2*- 0x4* network ralated  */
#define VTMSG_MSG_TYPE_NETWORK_INFO                   0x20
#define VTMSG_MSG_TYPE_NETWORK_CNTR                   0x21
#define VTMSG_MSG_TYPE_SAVE_NWK_STATE_REQ             0x22
#define VTMSG_MSG_TYPE_CONFIG_DC                      0x23
#define VTMSG_MSG_TYPE_MAC_FRAME_COUNTER_UPDATE       0x24
#define VTMSG_MSG_TYPE_LOAD_NG_SEQ_NR_UPDATE          0x25

#define VTMSG_MSG_TYPE_SHOW_ACTIVE_NODES              0x2a
#define VTMSG_MSG_TYPE_DETACH_NODE                    0x2b
#define VTMSG_MSG_TYPE_REQUEST_ROUTE                  0x2c
#define VTMSG_MSG_TYPE_REQUEST_PATH                   0x2d
#define VTMSG_MSG_TYPE_ROUTE_DISCOVERY_RESULT         0x2e
#define VTMSG_MSG_TYPE_PATH_DISCOVERY_RESULT          0x2f

#define VTMSG_MSG_TYPE_GET_ADP_IB                     0x30
#define VTMSG_MSG_TYPE_SET_ADP_IB                     0x31
#define VTMSG_MSG_TYPE_GET_MAC_IB                     0x32
#define VTMSG_MSG_TYPE_SET_MAC_IB                     0x33
#define VTMSG_MSG_TYPE_MAC_IB_ATTR                    0x34

#define VTMSG_MSG_TYPE_GET_PAN_ID                     0x35
#define VTMSG_MSG_TYPE_SET_PAN_ID                     0x36

#define VTMSG_MSG_TYPE_NETWORK_EVENT                  0x3a
#define VTMSG_MSG_TYPE_ADD_TO_BLACK_LIST              0x3b
#define VTMSG_MSG_TYPE_REMOVE_FROM_BLACK_LIST         0x3c
#define VTMSG_MSG_TYPE_SHOW_BLACK_LIST                0x3d
#define VTMSG_MSG_TYPE_CSMA_SETTINGS                  0x3e


#define VTMSG_MSG_TYPE_SHOW_COMP_CNTXT_TBL            0x40
#define VTMSG_MSG_TYPE_ADD_TO_COMP_CNTXT_TBL          0x41
#define VTMSG_MSG_TYPE_DEL_FROM_COMP_CNTXT_TBL        0x42
#define VTMSG_MSG_TYPE_GET_TOPOLOGY_TABLE             0x43
#define VTMSG_MSG_TYPE_START_SEARCH_RT_TABLE          0x44
#define VTMSG_MSG_TYPE_REKEY_NWK                      0x45
#define VTMSG_MSG_TYPE_GET_REKEY_STS                  0x46
#define VTMSG_MSG_TYPE_GET_PSK_FOR_EUI                0x47
#define VTMSG_MSG_TYPE_GET_PSK_SET                    0x48
#define VTMSG_MSG_TYPE_SET_PSK_SET                    0x49

#define VTMSG_MSG_TYPE_SEND_MAC_DATA                  0x4a
#define VTMSG_MSG_TYPE_SET_PHY_TX_PARAMS              0x4b
#define VTMSG_MSG_TYPE_NETWORK_START_REQUEST          0x4c
#define VTMSG_MSG_TYPE_GET_ROUTING_TABLE              0x4d
#define VTMSG_MSG_TYPE_GET_NEIGHBOR_TABLE             0x4e
#define VTMSG_MSG_TYPE_GET_RX_DEVICE_TABLE            0x4f

/* TYPE 0x50 remote header */
#define VTMSG_MSG_TYPE_REMOTE_COMMAND                 0x50
#define VTMSG_ATTR_MSG_OUTGOING_LEN                   (4 + 1 + 8) // MSG_hdr(4) + addr_len(1) + addr(8)

/* TYPE 0x58 vtupg transparent */
#define VTMSG_MSG_TYPE_VTUPG                          0x58 //this type dosen't have 4 byte hdr


/* TYPE 0x60 command of DCU */
#define VTMSG_MSG_TYPE_DCU_SUPPORT                    0x60
#define VTMSG_MSG_TYPE_DCU_GET_JOINED_TBL             0x61





#define VTMSG_TLV_TYPE_FIELD_LEN   2
#define VTMSG_TLV_LEN_FIELD_LEN    2
#define VTMSG_TLV_HDR_LEN  (VTMSG_TLV_TYPE_FIELD_LEN + VTMSG_TLV_LEN_FIELD_LEN)
#define VTMSG_TLV_TYPE_FIELD_OFF   0
#define VTMSG_TLV_LEN_FIELD_OFF    2



#define VTMSG_TLV_LEN_FIELD_LEN_LONG    4
#define VTMSG_TLV_HDR_LEN_LONG  (VTMSG_TLV_TYPE_FIELD_LEN + VTMSG_TLV_LEN_FIELD_LEN_LONG)

/* TLV TYPE STS  */
#define VTMSG_TLV_TYPE_CMD_RESULT         0x0100
#define VTMSG_TLV_TYPE_CMD_ARG            0x0101
#define VTMSG_TLV_TYPE_CMD_INDEX          0x0102
#define VTMSG_TLV_TYPE_MEM_ADDR_AND_LEN   0x0110
#define VTMSG_TLV_TYPE_MEM_ADDR_AND_DATA  0x0111
#define VTMSG_TLV_TYPE_MEM_OP_DW          0x0112
#define VTMSG_TLV_TYPE_MEM_OP_WW          0x0113
#define VTMSG_TLV_TYPE_REBOOT_REMAP       0x0120
#define VTMSG_ATTR_MEM_ADDR_LEN_LEN   (VTMSG_TLV_HDR_LEN + 5) // addr + max 256
#define VTMSG_ATTR_MEM_ADDR_DATA_LEN  (VTMSG_TLV_HDR_LEN + 8) //
#define VTMSG_ATTR_CMD_RESULT_LEN     (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_ATTR_CMD_INDEX_LEN      (VTMSG_TLV_HDR_LEN + 2) // LE ordering
#define VTMSG_ATTR_REBOOT_REMAP_LEN   (VTMSG_TLV_HDR_LEN + 1)

/* TLV TYPE EVENT  */
#define VTMSG_TLV_TYPE_EVENT_NETWORK_JOIN     0x0201
#define VTMSG_ATTR_NETWORK_JOIN_LEN           (VTMSG_TLV_HDR_LEN + 1)


/* TLV TYPE REMOTE UPGRADE  */
#define VTMSG_TLV_TYPE_UPGRADE_START          0x0300
#define VTMSG_TLV_TYPE_UPGRADE_STOP           0x0301
#define VTMSG_TLV_TYPE_UPGRADE_FILEINFO       0x0302
#define VTMSG_TLV_TYPE_UPGRADE_EVENT          0x0303
#define VTMSG_TLV_TYPE_UPGRADE_BLOCKIDX       0x0304
#define VTMSG_TLV_TYPE_UPGRADE_MAP            0x0305
#define VTMSG_TLV_TYPE_UPGRADE_DATA           0x0306
#define VTMSG_TLV_TYPE_UPGRADE_OP_WE          0x0307
#define VTMSG_TLV_TYPE_UPGRADE_OP_WR          0x0308
#define VTMSG_TLV_TYPE_UPGRADE_OP_UN          0x0309
#define VTMSG_TLV_TYPE_UPGRADE_REPLY          0x030a
#define VTMSG_ATTR_UPGRADE_BLOCKIDX_LEN       (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_UPGRADE_FILEINFO_LEN       (VTMSG_TLV_HDR_LEN + 20)
#define VTMSG_ATTR_UPGRADE_REPLY_LEN          (VTMSG_TLV_HDR_LEN + 5)


/* TLV TYPE VANGO SETTINGS  */
#define VTMSG_TLV_TYPE_BBP_SETTINGS_IDX             0x0400
#define VTMSG_TLV_TYPE_BBP_SETTINGS_VALUE           0x0401
#define VTMSG_TLV_TYPE_NWK_DIGNO_PING               0x0410
#define VTMSG_TLV_TYPE_NWK_SEARCH_NEIGHBOR_EUI64    0x0411
#define VTMSG_ATTR_NWK_DIGNO_PING_LEN               (VTMSG_TLV_HDR_LEN + 2) // seq + dummy lenght
#define VTMSG_ATTR_BBP_SETTINGS_IDX_LEN             (VTMSG_TLV_HDR_LEN + 1)


/* TLV TYPE SYS  */
#define VTMSG_TLV_TYPE_BOOTSTRAP_ONOFF        0x0600
#define VTMSG_TLV_TYPE_VERSION_INFO           0x0630
#define VTMSG_TLV_TYPE_VERSION_INTERNAL       0x0631
#define VTMSG_TLV_TYPE_START_NETWORK          0x0640
#define VTMSG_TLV_TYPE_SYS_UPTIME             0x0650
#define VTMSG_TLV_TYPE_SYSTEM_LOG             0x0651
#define VTMSG_TLV_TYPE_SYSTEM_DATA_LOG        0x0652
#define VTMSG_TLV_RUNTIME_SYS_LOG             0x0660
#define VTMSG_TLV_TYPE_BBP_SIG_TYPE           0x0661
#define VTMSG_TLV_TYPE_BBP_SIG0_BAND          0x0662
#define VTMSG_TLV_TYPE_BBP_SIG0_RX_GAIN       0x0663
#define VTMSG_TLV_TYPE_BBP_SIG0_RX_PWR        0x0664
#define VTMSG_TLV_TYPE_BBP_SIG_DATA           0x0665
#define VTMSG_TLV_TYPE_BBP_SIG1_PMB_MISC      0x0666
#define VTMSG_TLV_TYPE_ZEROX_HIT_RECORD       0x0670
#define VTMSG_ATTR_BBP_SIG_TYPE_LEN           (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_VERSION_INFO_TLV_LEN            (VTMSG_TLV_HDR_LEN + 10)
#define VTMSG_VERSION_INTERNAL_TLV_LEN        (VTMSG_TLV_HDR_LEN + 29)
#define VTMSG_ATTR_START_NETWORK_LEN          (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_ATTR_SYS_UPTIME_LEN             (VTMSG_TLV_HDR_LEN + 4)
#define VTMSG_RUNTIME_SYS_LOG_TLV_LEN         (VTMSG_TLV_HDR_LEN + sizeof(SYS_LOG))
#define VTMSG_ATTR_SYSTEM_LOG_LEN             (VTMSG_TLV_HDR_LEN + sizeof(uint16_t))
#define VTMSG_ATTR_SYSTEM_DATA_LOG_LEN        (VTMSG_TLV_HDR_LEN + sizeof(VT_SYSTEM_DATA_LOG))
#define VTMSG_BBP_SIG_TLV_LEN                 (VTMSG_TLV_HDR_LEN + 4)
#define VTMSG_ATTR_ZEROX_HIT_RECORD_LEN       (VTMSG_TLV_HDR_LEN + sizeof(zc_hit_records_t))


/* TLV TYPE LOCAL  */
#define VTMSG_TLV_TYPE_DEVICE_TYPE                0x0700
#define VTMSG_TLV_TYPE_BANDPLAN                  0x0701
#define VTMSG_TLV_TYPE_SHORT_ADDR                 0x0702
#define VTMSG_TLV_TYPE_IPV6_ADDR                  0x0703
#define VTMSG_TLV_TYPE_EUI64_ADDR                 0x0704
#define VTMSG_TLV_TYPE_PAN_JOIN                   0x0705
#define VTMSG_TLV_TYPE_COMMON_CONFIG_IDX          0x0706
#define VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE        0x0707
#define VTMSG_TLV_TYPE_DCU_SUPPROT_FLAGS          0x0710
#define VTMSG_ATTR_DEVICE_TYPE_LEN                (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_BANDPLAN_LEN                  (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_SHORT_ADDR_LEN                 (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_IPV6_ADDR_LEN                  (VTMSG_TLV_HDR_LEN + 16)
#define VTMSG_ATTR_EUI64_ADDR_LEN                 (VTMSG_TLV_HDR_LEN + 8)
#define VTMSG_ATTR_PAN_JOINED_LEN                 VTMSG_ATTR_SHORT_ADDR_LEN + VTMSG_ATTR_EUI64_ADDR_LEN + VTMSG_ATTR_IPV6_ADDR_LEN
#define VTMSG_ATTR_COMMON_CONFIG_IDX_LEN          (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN        (VTMSG_TLV_HDR_LEN + 4)
#define VTMSG_ATTR_DCU_SUPPORT_FLAGS_LEN          (VTMSG_TLV_HDR_LEN + 1)

/* TLV TYPE NETWORK  */
#define VTMSG_TLV_TYPE_PAN_ID                     0x0800
#define VTMSG_TLV_TYPE_BC_LOG_TBL_ENTRY           0x0801
#define VTMSG_TLV_TYPE_MESH_BC_PKT_SEQ_NR         0x0802
#define VTMSG_TLV_TYPE_BC_LOG_TBL_ENTRY_REM_TTL   0x0803
#define VTMSG_TLV_TYPE_ROUTING_TBL_ENTRY          0x0804
#define VTMSG_TLV_TYPE_ROUTING_ENTRY_AGE          0x0805
#define VTMSG_TLV_TYPE_DEST_SHORT_ADDR            0x0806
#define VTMSG_TLV_TYPE_NEXT_HOP_SHORT_ADDR        0x0807
#define VTMSG_TLV_TYPE_GROUP_ADDR                 0x0808
#define VTMSG_TLV_TYPE_TX_MODULATION              0x0809
#define VTMSG_TLV_TYPE_TX_GAIN                    0x080a
#define VTMSG_TLV_TYPE_TX_RES                     0x080b
#define VTMSG_TLV_TYPE_TX_COEFF                   0x080c
#define VTMSG_TLV_TYPE_LQI                        0x080d
#define VTMSG_TLV_TYPE_NEIGHBOR_TBL_ENTRY_AGE     0x080e
#define VTMSG_TLV_TYPE_NEIGHBOR_TBL_TONE_MAP      0x080f
#define VTMSG_TLV_TYPE_NEIGHBOR_TBL_ENTRY         0x0810
#define VTMSG_TLV_TYPE_GROUP_TBL_ENTRY            0x0811
#define VTMSG_TLV_TYPE_MAC_ATTR_ID                0x0812
#define VTMSG_TLV_TYPE_ROUTE_REQ_RESP             0x0813
#define VTMSG_TLV_TYPE_PATH_RESULT                0x0814
#define VTMSG_TLV_TYPE_NEIGHBOR_TBL_PHASE_INFO    0x0815
#define VTMSG_TLV_TYPE_ROUTE_REQ_RESULT           0x0818
#define VTMSG_TLV_TYPE_HOP_COUNT                  0x0821
#define VTMSG_TLV_TYPE_ROUTING_WEAK_LINK          0x0822
#define VTMSG_TLV_TYPE_RT_BLACK_TBL_ENTRY         0x0823
#define VTMSG_TLV_TYPE_RT_BLACKLIST_TIME          0x0824
#define VTMSG_TLV_TYPE_PREFIX_TBL_ENTRY           0x0825
#define VTMSG_TLV_TYPE_PREFIX_TBL_LEN             0x0826
#define VTMSG_TLV_TYPE_PREFIX_TBL_CONTENT         0x0827
#define VTMSG_TLV_TYPE_LOAD_ROUTE_COST            0x0830
#define VTMSG_TLV_TYPE_NETWORK_EVENT              0x0900
#define VTMSG_TLV_TYPE_TOPOLOGY_TBL_ENTRY         0x0901
#define VTMSG_TLV_TYPE_NEI_EUI_TBL_ENTRY          0x0902
#define VTMSG_TLV_TYPE_S2E_TBL_ENTRY              0x0903
#define VTMSG_TLV_TYPE_LBS_TBL_ENTRY              0x0904
#define VTMSG_TLV_TYPE_SEND_JOIN_PAN              0x0905
#define VTMSG_LBS_TBL_LEN                         (VTMSG_TLV_HDR_LEN + 18)
#define VTMSG_SEND_JOIN_PAN_LEN                   (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_TLV_TYPE_PSK_ENTRY                  0x0a00

#define VTMSG_TLV_TYPE_APP_DATA_EXCHANGE_CMD_INDEX_LEN        2
#define VTMSG_TLV_TYPE_APP_DATA_EXCHANGE_BUF_SIZE_LEN           2

#define VTMSG_TLV_TYPE_APP_DATA_EXCHANGE          0x0b00
#define VTMSG_ATTR_APP_DATA_EXCHANGE_LEN    (VTMSG_TLV_HDR_LEN + VTMSG_TLV_TYPE_APP_DATA_EXCHANGE_CMD_INDEX_LEN + VTMSG_TLV_TYPE_APP_DATA_EXCHANGE_BUF_SIZE_LEN)




#define MAX_PATH_RESULT                               (48)
#define VTMSG_NODE_EXT_ADDR_LEN                       (8)
#define VTMSG_NODE_PSK_LEN                            (16)
#define VTMSG_MAC_ATTR_VAL_MAX_LEN                    (16)
#define VTMSG_ATTR_PAN_ID_LEN                 (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_ROUTING_ENTRY_AGE_LEN      (VTMSG_TLV_HDR_LEN + 3) // minute:2byte sec:1byte
#define VTMSG_ATTR_DEST_SHORT_ADDR_LEN        (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_NEXT_HOP_SHORT_ADDR_LEN    (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_LOAD_ROUTE_COST_LEN        (VTMSG_TLV_HDR_LEN + 2)
#define VTMSG_ATTR_LQI_LEN                    (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_ATTR_HOP_COUNT_LEN              (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_ATTR_ROUTING_WEAK_LINK_LEN      (VTMSG_TLV_HDR_LEN + 1)
#define VTMSG_ATTR_ROUTING_TBL_ENTRY_LEN      (VTMSG_TLV_HDR_LEN + sizeof(struct route_table_st))
#define VTMSG_ATTR_NEIGHBOR_TBL_ENTRY_LEN     (VTMSG_TLV_HDR_LEN + 4 * 10) // sizeof struct Neighbor_Table_ST
#define VTMSG_ATTR_NEI_EUI_TBL_ENTRY_LEN      (VTMSG_TLV_HDR_LEN + 10) // sizeof struct NEI_EUI64
#define VTMSG_ATTR_S2E_TBL_ENTRY_LEN          (VTMSG_TLV_HDR_LEN + sizeof(struct lbs_alloc_short_addr_table))
#define VTMSG_ATTR_TOPOLOGY_TBL_ENTRY_LEN     (VTMSG_TLV_HDR_LEN + 4) //
#define VTMSG_ATTR_PSK_ENTRY_LEN              (VTMSG_TLV_HDR_LEN + VTMSG_NODE_PSK_LEN + 1)
#define VTMSG_ATTR_PATH_RESULT_LEN            (VTMSG_TLV_HDR_LEN + MAX_PATH_RESULT)


#endif   // __VTMSG_H__
