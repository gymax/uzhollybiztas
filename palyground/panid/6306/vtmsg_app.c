#include "sdk.h"
#include "vtupg.h"
#include "vtmsg.h"
#include "vtmsg_if.h"
#include "vtmsg_app.h"
#include "vtclx_reboot.h"
#include "vtmac_zerox.h"
#include "dcu_api_msg.h"
#include "dcu_if_tx.h"
#include "dcu_api.h"
#include "vtmm.h"
#include "netif/vtremote.h"
#include "vtclv_vtmac_bbp.h"
#include "netif/lowpan6_psk.h"
#if (CONFIG_SYSTEM_LOG_ENABLE)
#include "vt_sys_log.h"
#endif

#if (CONFIG_VTMSG)
extern void *__info_h0;
extern void *__info_hb0;
#if (CONFIG_REMOTE_FW_UPGRADE)
extern vtupg_ctrl_blk_t vtupg_cb;
extern upg_database_t UPG_DB;
#endif

#if (CONFIG_SDK2)
extern vtsdk_ctrl_blk_t vtsdk_cb;
#endif


uint8_t msg_vtmsg_hint[VTMSG_CMD_OFFSET] = { 0x40, 0x56, 0x20 };
uint8_t vtmsg_msg_flags = 0;
vtmsg_ctrl_blk_t vtmsg_msg_destination;
static uint8_t vtmsg_seq = 0;
static struct LBS_PAN_DESCRIPTOR lbs_beacon_packet_table[3];
bool WaitJoinPANCmd = false;
extern uint8_t lowpan_current_lqi;

uint8_t *vtmsg_build_msghdr(uint8_t *pHdr, uint8_t type, uint16_t len)
{
    unsigned char flags = 0;
    pHdr[VTMSG_MSG_HDR_TYPE_FIELD_OFF] = type;
    flags = vtmsg_msg_flags; //basically flags is the same as requested
    pHdr[VTMSG_MSG_HDR_FLAGS_FIELD_OFF] = flags;
    pHdr[VTMSG_MSG_HDR_PYLD_LEN_HIGH_FIELD_OFF] = (len >> 8) & 0xff;
    pHdr[VTMSG_MSG_HDR_PYLD_LEN_LOW_FIELD_OFF] = len & 0xff;
    return(pHdr + sizeof(vtmsg_msgHdr_s));
}


int32_t vtmsg_check_type(uint8_t *pBuff, uint16_t tlvType)
{
    uint16_t type = pBuff[0];
    int32_t len = pBuff[2];

    type = (type << 8) | pBuff[1];
    len = (len << 8) | pBuff[3];


    if (type == tlvType)
        return len;
    else
        return len ? -len : -1; //-1 for len is 0
}


int32_t vtmsg_get_tlv(uint8_t *pBuff,
                      int32_t buffLen,
                      uint16_t tlvType, int32_t *pTlvValLen, uint8_t **ppTlvValBuff)
{
    if (buffLen == 0)
        return 0;

    // Get the tlv type
    while (buffLen >= VTMSG_TLV_HDR_LEN) {
        int32_t tlvLen;

        if ((tlvLen = vtmsg_check_type(pBuff, tlvType)) >= 0) {
            *pTlvValLen = tlvLen;
            *ppTlvValBuff = (pBuff + VTMSG_TLV_HDR_LEN);
            return 1;
        } else {
            pBuff += (VTMSG_TLV_HDR_LEN - tlvLen);
            buffLen -= (VTMSG_TLV_HDR_LEN - tlvLen);
        }
    }

    return 0;
}

int32_t vtmsg_check_seq_exist(uint8_t *pTlvBuff, int32_t buffLen)
{
    if (buffLen == 0)
        return 0;

    while (buffLen >= VTMSG_TLV_HDR_LEN) {
        int32_t tlvLen = pTlvBuff[3] + (pTlvBuff[2] << 8);
        pTlvBuff += (VTMSG_TLV_HDR_LEN + tlvLen);
        buffLen -= (VTMSG_TLV_HDR_LEN + tlvLen);
    }

    return (buffLen == 1) ? 1 : 0;
}


uint8_t *vtmsg_build_tlvhdr(uint8_t *pTlv, uint16_t type, uint16_t len)
{
    len -= VTMSG_TLV_HDR_LEN;
    pTlv[VTMSG_TLV_TYPE_FIELD_OFF] = ((type >> 8) & 0xff);
    pTlv[VTMSG_TLV_TYPE_FIELD_OFF + 1] = type & 0xff;
    pTlv[VTMSG_TLV_LEN_FIELD_OFF] = (len >> 8) & 0xff;
    pTlv[VTMSG_TLV_LEN_FIELD_OFF + 1] = len & 0xff;
    return(pTlv + VTMSG_TLV_TYPE_FIELD_LEN + VTMSG_TLV_LEN_FIELD_LEN);
}


void vtmsg_msg_send(uint8_t *mbuf, uint32_t len)
{
    vtmsg_msgHdr_s *msgHdr = (vtmsg_msgHdr_s *)mbuf;

    if ((msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_DIRECTION_BIT)) != 0) { //outgoing
        u8_t *pbuf;
        pbuf = vtmm_pool_alloc(len + VTMSG_CMD_OFFSET + VTMSG_SEQ_LEN, NULL);
        if (msgHdr->type != VTMSG_MSG_TYPE_VTUPG) { //vtupg format is different
            msgHdr->lenLow += VTMSG_SEQ_LEN;
        }
        memcpy(pbuf, msg_vtmsg_hint, VTMSG_CMD_OFFSET);
        memcpy(pbuf + VTMSG_CMD_OFFSET, mbuf, len);
        len += VTMSG_CMD_OFFSET + VTMSG_SEQ_LEN;
        pbuf[len - 1] = vtmsg_seq;
        if (vtmsg_msg_destination.final_dest_addr_len == 2) {
            lowpan6_raw_send(NULL, vtmsg_msg_destination.final_dest_addr, pbuf, len);
        } else if (vtmsg_msg_destination.final_dest_addr_len == 8) {
            lowpan6_mac_eui64_send(vtmsg_msg_destination.final_dest_addr, pbuf, len);
        }
        vtmm_pool_free(pbuf);
    } else if ((msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_SOURCE_BIT)) != 0) { //imcoming & to spi
    #if (CONFIG_DCUIF_ENABLE)
        uint8_t *pbuf;
        DCUIF_MSG_FORMAT hdr;
        hdr.dispatch = SPI_HOSTCLI;
        pbuf = vtmm_pool_alloc(len + DCUIF_HDR_LEN, NULL);
        memcpy(pbuf, &hdr, DCUIF_HDR_LEN);
        memcpy(pbuf + DCUIF_HDR_LEN, mbuf, len);
        dcu_send_packet(pbuf, len + DCUIF_HDR_LEN, FROM_USER_REQ, NULL, 0);
        vtmm_pool_free(pbuf);
    #endif
    } else if (msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_SDK_HOOK_BIT)) {
        vtsdk_vtmsg_hook(mbuf, len);
        if (msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_UART_SOURCE_BIT)) {
            slipif_uart1_send(mbuf, len);
        } else {
            slipif_uart0_send(mbuf, len);
        }
    } else { //imcoming & to uart
        if (msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_UART_SOURCE_BIT)) {
            slipif_uart1_send(mbuf, len);
        } else {
            slipif_uart0_send(mbuf, len);
        }
    }
}

/*
 *  VTMSG_STS helper, send message in place please return after called
 */
static void vtmsg_status_only(uint8_t sts, uint8_t msg_type)
{
    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN] = {};
    uint8_t *pBuffCurr;
    pBuffCurr = vtmsg_build_msghdr(buff,
                                   msg_type,
                                   VTMSG_ATTR_CMD_RESULT_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_CMD_RESULT,
                                   VTMSG_ATTR_CMD_RESULT_LEN);
    pBuffCurr[0] = sts;
    vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN);
}

//uint8_t *pBuff[] = {0x01,0x00,0x00,0x0a,0x06,0x30,0x00,0x06,0x01,0x00,0x41,0x00,0x00,0x00};
////
#define INFO_HDR_SOC_B0_PLATFORM 5
#define INFO_HDR_SOC_B0_TAINTED (1 << 0)
#define INFO_HDR_SOC_B0_FPGA (1 << 7)

void vtmsg_get_version(uint8_t *pTlv, uint32_t len)
{
    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_VERSION_INFO_TLV_LEN] = {};
    uint8_t *pBuffCurr;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    pBuffCurr = vtmsg_build_msghdr(buff,
                                   VTMSG_MSG_TYPE_GET_VERSION_INFO,
                                   VTMSG_VERSION_INFO_TLV_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_VERSION_INFO,
                                   VTMSG_VERSION_INFO_TLV_LEN);
    pBuffCurr[0] = *(((uint8_t *)&__info_h0) + 1);
    pBuffCurr[1] = *(((uint8_t *)&__info_h0) + 2);
    pBuffCurr[2] = *(((uint8_t *)&__info_h0) + 3);
    pBuffCurr[3] = *(((uint8_t *)&__info_hb0) + 2);
    pBuffCurr[4] = *(((uint8_t *)&__info_hb0) + 1);
    pBuffCurr[5] = *(((uint8_t *)&__info_hb0) + 0);
    memcpy(pBuffCurr + 6, (((uint32_t *)&__info_h0) + 3), 4); //build time

    //vtmsg_msg_send(pBuffHead, sizeof(vtmsg_msgHdr_s) + VTMSG_TLV_HDR_LEN + 6);
    vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_VERSION_INFO_TLV_LEN);
}


void vtmsg_get_version_internal(uint8_t *pTlv, uint32_t len)
{
    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_VERSION_INTERNAL_TLV_LEN] = {};
    uint8_t *pBuffCurr;
    uint8_t version_d_flag = *(((uint8_t *)&__info_h0) + 3);

    //VPRINTF(VTLL_USER, "%s"LF,__func__);
    pBuffCurr = vtmsg_build_msghdr(buff,
                                   VTMSG_MSG_TYPE_GET_VERSION_INTERNAL,
                                   VTMSG_VERSION_INTERNAL_TLV_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_VERSION_INTERNAL,
                                   VTMSG_VERSION_INTERNAL_TLV_LEN);

    pBuffCurr[0] = *(((uint8_t *)&__info_h0) + 1);
    pBuffCurr[1] = *(((uint8_t *)&__info_h0) + 2);
    pBuffCurr[2] = *(((uint8_t *)&__info_h0) + 3);
    memcpy(pBuffCurr + 3, (((uint32_t *)&__info_h0) + 2), 4); //short hash
    memcpy(pBuffCurr + 7, (((uint32_t *)&__info_h0) + 3), 4); //build time
    pBuffCurr[11] = version_d_flag & INFO_HDR_SOC_B0_TAINTED;

    pBuffCurr[12] = *(((uint8_t *)&__info_hb0) + 2);
    pBuffCurr[13] = *(((uint8_t *)&__info_hb0) + 1);
    pBuffCurr[14] = *(((uint8_t *)&__info_hb0) + 0);
    memcpy(pBuffCurr + 15, (((uint32_t *)&__info_hb0) + 1), 4); //short hash
    memcpy(pBuffCurr + 19, (((uint32_t *)&__info_hb0) + 3), 4); //hw setting
    memcpy(pBuffCurr + 23, (((uint32_t *)&__info_hb0) + 2), 4); //build time


    //vtmsg_send(pBuffHead, sizeof(vtmsg_msgHdr_s) + VTMSG_TLV_HDR_LEN + 6);
    vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_VERSION_INTERNAL_TLV_LEN);
}


void vtmsg_get_networkInfo(uint8_t *pTlv, uint32_t len)
{
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t lenOut;
    TickType_t time;

    time = xTaskGetTickCount() / 1000;

    pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) +
                            VTMSG_ATTR_BANDPLAN_LEN +
                            VTMSG_ATTR_EUI64_ADDR_LEN +
                            VTMSG_ATTR_DEVICE_TYPE_LEN +
                            VTMSG_ATTR_SYS_UPTIME_LEN +
                            VTMSG_ATTR_PAN_JOINED_LEN,
                            NULL);


    memset(pBuff, 0, sizeof(vtmsg_msgHdr_s) +
           VTMSG_ATTR_BANDPLAN_LEN +
           VTMSG_ATTR_EUI64_ADDR_LEN +
           VTMSG_ATTR_DEVICE_TYPE_LEN +
           VTMSG_ATTR_SYS_UPTIME_LEN +
           VTMSG_ATTR_PAN_JOINED_LEN);

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    pBuffCurr = pBuff + sizeof(vtmsg_msgHdr_s);

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_BANDPLAN,
                                   VTMSG_ATTR_BANDPLAN_LEN);

    pBuffCurr[0] = DB->mac_st.f_bandplan;
    pBuffCurr[1] = DB->mac_st.f_vt_bandplan;

    pBuffCurr += 2;

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_EUI64_ADDR,
                                   VTMSG_ATTR_EUI64_ADDR_LEN);

    memcpy(pBuffCurr, DB->mac_st.attrib->aExtendedAddress, 8);

    pBuffCurr += 8;

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_DEVICE_TYPE,
                                   VTMSG_ATTR_DEVICE_TYPE_LEN);

    pBuffCurr[0] = DB->local_status.is_lbs;
    pBuffCurr[1] = DB->local_status.device_state;

    pBuffCurr += 2;

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_SYS_UPTIME,
                                   VTMSG_ATTR_SYS_UPTIME_LEN);

    memcpy(pBuffCurr, &time, 4);

    pBuffCurr += 4;


    if (DB->local_status.already_join_pan == 0) {
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_PAN_JOIN,
                                       VTMSG_TLV_HDR_LEN);

        lenOut = sizeof(vtmsg_msgHdr_s) +
                 VTMSG_ATTR_BANDPLAN_LEN +
                 VTMSG_ATTR_EUI64_ADDR_LEN +
                 VTMSG_ATTR_DEVICE_TYPE_LEN +
                 VTMSG_ATTR_SYS_UPTIME_LEN +
                 VTMSG_TLV_HDR_LEN;
    } else {
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_PAN_JOIN,
                                       VTMSG_TLV_HDR_LEN + VTMSG_ATTR_PAN_JOINED_LEN);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_PAN_ID,
                                       VTMSG_ATTR_PAN_ID_LEN);

        memcpy(pBuffCurr, DB->mac_st.pib->macPANId, 2);

        pBuffCurr += 2;


        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_SHORT_ADDR,
                                       VTMSG_ATTR_SHORT_ADDR_LEN);

        memcpy(pBuffCurr, DB->lowpan6_adp_ib.short_address, 2);

        pBuffCurr += 2;


        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_IPV6_ADDR,
                                       VTMSG_ATTR_IPV6_ADDR_LEN);

        memcpy(pBuffCurr, DB->netif.ip6_addr[0].addr, 16);

        lenOut = sizeof(vtmsg_msgHdr_s) +
                 VTMSG_ATTR_BANDPLAN_LEN +
                 VTMSG_ATTR_EUI64_ADDR_LEN +
                 VTMSG_ATTR_DEVICE_TYPE_LEN +
                 VTMSG_ATTR_SYS_UPTIME_LEN +
                 VTMSG_ATTR_PAN_JOINED_LEN;
    }

    vtmsg_build_msghdr(pBuff, VTMSG_MSG_TYPE_NETWORK_INFO,
                       lenOut - sizeof(vtmsg_msgHdr_s));

    vtmsg_msg_send(pBuff, lenOut);
    vtmm_pool_free(pBuff);
}


void vtmsg_nwk_diagnostic(uint8_t *pTlv, uint32_t len)
{
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_NWK_DIGNO_PING, &tlvLen0, &pTlvPld0)) {
            uint8_t previous_hop_addr[2] = {};
            if (DB->rx_pkt_mac_info.receive_src.addr_len == 2) {
                lowpan6_addr_cpy(previous_hop_addr, DB->rx_pkt_mac_info.receive_src.addr, DB->rx_pkt_mac_info.receive_src.addr_len);
            }
            vtmsg_nwk_event(VTMSG_NEVENT_ACK, previous_hop_addr);
        } else if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_NWK_SEARCH_NEIGHBOR_EUI64, &tlvLen0, &pTlvPld0)) {
            vtmsg_nwk_event(VTMSG_NEVENT_SEARCH_NEIGHBOR_EUI64, DB->mac_st.attrib->aExtendedAddress);
        }
    }
}


void vtmsg_upg_data(uint8_t *pTlv, uint32_t len)
{
    //for request
    #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
    int32_t tlvLen0;
    uint16_t blockidx;
    uint8_t *pTlvPld0;
    #endif

    VPRINTF(VTLL_USER, "#");
    if (len > 0) {
        #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_BLOCKIDX, &tlvLen0, &pTlvPld0)) {
            if (tlvLen0 == sizeof(uint16_t)) {
                memcpy(&blockidx, pTlvPld0, sizeof(uint16_t));
            } else {
                vtmsg_status_only(VTMSG_STS_INV_PARAMS, VTMSG_MSG_TYPE_UPGRADE_DATA);
                return;
            }
            pTlv += VTMSG_ATTR_UPGRADE_BLOCKIDX_LEN;
            len -= VTMSG_ATTR_UPGRADE_BLOCKIDX_LEN;
            if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_DATA, &tlvLen0, &pTlvPld0)) {
                if (tlvLen0 == (vtupg_cb.info.block_size)) {
                    //memcpy(&blockidx, rReq, sizeof(uint16_t));
                    uint8_t bcast_addr[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
                    vtmsg_msg_destination.final_dest_addr_len = vtupg_cb.info.final_dest_addr_len;
                    memcpy(vtmsg_msg_destination.final_dest_addr, bcast_addr, vtupg_cb.info.final_dest_addr_len);
                    vtupg_host_send_wr_data(pTlvPld0, blockidx);
                    vtmsg_status_only(VTMSG_STS_SUCCESS, VTMSG_MSG_TYPE_UPGRADE_DATA);
                } else {
                    vtmsg_status_only(VTMSG_STS_BAD_MSG, VTMSG_MSG_TYPE_UPGRADE_DATA);
                }
            }
        }
        #else
        vtmsg_status_only(VTMSG_STS_NOT_SUPPORTED, VTMSG_MSG_TYPE_UPGRADE_DATA);
        #endif
    }
}


#if (CONFIG_REMOTE_FW_UPGRADE)
extern uint32_t bcast_blocknum;
void vtmsg_upg_event(uint8_t event, uint8_t *msg)
{
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t tvlLen = 0;
    pBuff = vtmm_pool_alloc(250, NULL);

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (event == UPG_HOST_SEND_WR) {
        tvlLen = VTMSG_TLV_HDR_LEN + ((bcast_blocknum + 7) / 8); //round up

        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_UPGRADE_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_UPGRADE_MAP,
                                       tvlLen);
        memcpy(pBuffCurr, UPG_DB.datamap, ((bcast_blocknum + 7) / 8));
    } else if (event == UPG_HOST_SEND_WE) {
        tvlLen = VTMSG_TLV_HDR_LEN; //round up

        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_UPGRADE_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_UPGRADE_MAP,
                                       tvlLen);
    } else if (event == UPG_ADD_BLACKLIST) {
        tvlLen = VTMSG_ATTR_DEST_SHORT_ADDR_LEN;

        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_UPGRADE_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_DEST_SHORT_ADDR,
                                       tvlLen);
        memcpy(pBuffCurr, msg, 2);
    } else if (event == UPG_GET_UN_READY) {
        tvlLen = VTMSG_ATTR_UPGRADE_REPLY_LEN;

        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_UPGRADE_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_UPGRADE_REPLY,
                                       tvlLen);
        memcpy(pBuffCurr, msg, 5);
    } else if (event == UPG_GET_UN_DATA_OK) {
        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_UPGRADE_OP,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_RESULT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;

        pBuff[1] &= ~(1 << VTMSG_MSG_HDR_FLAG_DIRECTION_BIT); // vtmsg_msgHdr_s.flags
        vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN);
        vtmm_pool_free(pBuff);
        return;
    } else {
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_UPGRADE_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
    }

    vtmsg_build_msghdr(pBuff,
                       VTMSG_MSG_TYPE_UPGRADE_EVENT,
                       VTMSG_ATTR_CMD_RESULT_LEN + tvlLen);

    pBuff[1] &= ~(1 << VTMSG_MSG_HDR_FLAG_DIRECTION_BIT); // vtmsg_msgHdr_s.flags
    vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + tvlLen);
    vtmm_pool_free(pBuff);
}
#endif


void vtmsg_nwk_event(uint8_t event, uint8_t *addr)
{
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t tvlLen = 0;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (event == VTMSG_NEVENT_BOOTSTRAP_SUCCESS ||
        event == VTMSG_NEVENT_ROUTE_DISCOVERY_SUCCESS) {
        tvlLen = VTMSG_ATTR_DEST_SHORT_ADDR_LEN;

        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + tvlLen, NULL);
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_NETWORK_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_DEST_SHORT_ADDR,
                                       tvlLen);
        memcpy(pBuffCurr, addr, 2);
    } else if (event == VTMSG_NEVENT_PATH_DISCOVERY_SUCCESS ||
               event == VTMSG_NEVENT_PATH_DISCOVERY_FAIL) {
        tvlLen = VTMSG_ATTR_PATH_RESULT_LEN;

        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + tvlLen, NULL);
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_NETWORK_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_PATH_RESULT,
                                       tvlLen);
        memcpy(pBuffCurr, addr, VTMSG_ATTR_PATH_RESULT_LEN);
    } else if (event == VTMSG_NEVENT_RECV_BEACON_REQ ||
               event == VTMSG_NEVENT_SEARCH_NEIGHBOR_EUI64) {
        tvlLen = VTMSG_ATTR_EUI64_ADDR_LEN;

        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + tvlLen, NULL);
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_NETWORK_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_EUI64_ADDR,
                                       tvlLen);
        memcpy(pBuffCurr, addr, 8);
    } else if (event == VTMSG_NEVENT_PAN_JOINED) {
        tvlLen = VTMSG_ATTR_NETWORK_JOIN_LEN;

        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + tvlLen, NULL);
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_NETWORK_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_EVENT_NETWORK_JOIN,
                                       tvlLen);
        memcpy(pBuffCurr, addr, 1);
    } else if (event == VTMSG_NEVENT_ACK) {
        tvlLen = VTMSG_ATTR_DEST_SHORT_ADDR_LEN + VTMSG_ATTR_LQI_LEN;

        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + tvlLen, NULL);
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_NETWORK_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
        pBuffCurr += sizeof(event);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_DEST_SHORT_ADDR,
                                       VTMSG_ATTR_DEST_SHORT_ADDR_LEN);
        memcpy(pBuffCurr, addr, 2);
        pBuffCurr += 2;
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_LQI,
                                       VTMSG_ATTR_LQI_LEN);
        pBuffCurr[0] = lowpan_current_lqi;
    } else {
        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN, NULL);
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_NETWORK_EVENT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = event;
    }


    vtmsg_build_msghdr(pBuff,
                       VTMSG_MSG_TYPE_NETWORK_EVENT,
                       VTMSG_ATTR_CMD_RESULT_LEN + tvlLen);

    vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + tvlLen);
    vtmm_pool_free(pBuff);
}


void vtmsg_upg_control(uint8_t *pTlv, uint32_t len)
{
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint8_t result = VTMSG_STS_BAD_MSG;
    #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
    uint8_t start = false;
    #endif

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_START, &tlvLen0, &pTlvPld0)) {
            #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
            if (vtupg_cb.is_running == false)
                result = VTMSG_STS_SUCCESS;
            else
                result = VTMSG_STS_NOT_READY;
            #else
            result = VTMSG_STS_NOT_SUPPORTED;
            #endif
        } else if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_FILEINFO, &tlvLen0, &pTlvPld0)) {
            #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
            //if(vtupg_cb.is_running == false){ always can retry
            memcpy(&vtupg_cb.info.target_offset, pTlvPld0, 4);
            memcpy(&vtupg_cb.info.total_size, pTlvPld0 + sizeof(uint32_t), 4);
            memcpy(&vtupg_cb.info.block_size, pTlvPld0 + (2 * sizeof(uint32_t)), 1);
            memcpy(&vtupg_cb.info.remark, pTlvPld0 + (2 * sizeof(uint32_t) + sizeof(uint8_t)), 1);
            memcpy(&vtupg_cb.info.mode, pTlvPld0 + (2 * sizeof(uint32_t) + 2 * sizeof(uint8_t)), 1);
            memcpy(&vtupg_cb.info.final_dest_addr, pTlvPld0 + (2 * sizeof(uint32_t) + 3 * sizeof(uint8_t)), 8);
            if (pTlvPld0[19] == VTUPG_ADDR_MODE_EUI64) {
                vtupg_cb.info.final_dest_addr_len = 8;
            } else {
                vtupg_cb.info.final_dest_addr_len = 2;
            }
            result = VTMSG_STS_SUCCESS;
            VPRINTF(VTLL_USER, "LMA %08x ,size 0x%x bytes, block size %d bytes remark:%d, mode:%d, dest:0x%02x%02x"LF,
                    vtupg_cb.info.target_offset,
                    vtupg_cb.info.total_size,
                    vtupg_cb.info.block_size,
                    vtupg_cb.info.remark,
                    vtupg_cb.info.mode,
                    vtupg_cb.info.final_dest_addr[0], vtupg_cb.info.final_dest_addr[1]);
            VPRINTF(VTLL_USER, "start firmware upgrade..."LF);
            start = true;
            //} else {
            //    result = VTMSG_STS_NOT_READY;
            //}
            #else
            result = VTMSG_STS_NOT_SUPPORTED;
            #endif
        } else if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_STOP, &tlvLen0, &pTlvPld0)) {
            #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
            vtupg_cb.is_running = false;
            result = VTMSG_STS_SUCCESS;
            #else
            result = VTMSG_STS_NOT_SUPPORTED;
            #endif
        } else {
            result = VTMSG_STS_BAD_MSG;
        }
    }


    vtmsg_status_only(result, VTMSG_MSG_TYPE_UPGRADE_CONTROL);
    #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
    if (start) {
        vtmsg_msg_destination.final_dest_addr_len = vtupg_cb.info.final_dest_addr_len;
        memcpy(vtmsg_msg_destination.final_dest_addr, vtupg_cb.info.final_dest_addr, vtupg_cb.info.final_dest_addr_len);
        vtupg_upgrade_start();
    }
    #endif
}


void vtmsg_upg_operation(uint8_t *pTlv, uint32_t len)
{
    //for request
    #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    #endif
    uint8_t result = VTMSG_STS_BAD_MSG;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        #if (CONFIG_REMOTE_FW_UPGRADE) && (!CONFIG_FW_UPGRADE_CLIENT_ONLY)
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_OP_WE, &tlvLen0, &pTlvPld0)) {
            int32_t tlvLen1;
            uint8_t *pTlvPld1;

            if (vtmsg_get_tlv(pTlvPld0, tlvLen0, VTMSG_TLV_TYPE_EUI64_ADDR, &tlvLen1, &pTlvPld1)) {
                vtupg_host_operation_handler(UPG_HOST_SEND_WE, pTlvPld1);
                result = VTMSG_STS_SUCCESS;
            }
        } else if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_OP_WR, &tlvLen0, &pTlvPld0)) {
            vtupg_host_operation_handler(UPG_HOST_SEND_WR, NULL);
            result = VTMSG_STS_SUCCESS;
        } else if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_UPGRADE_OP_UN, &tlvLen0, &pTlvPld0)) {
            int32_t tlvLen1;
            uint8_t *pTlvPld1;

            if (vtmsg_get_tlv(pTlvPld0, tlvLen0, VTMSG_TLV_TYPE_EUI64_ADDR, &tlvLen1, &pTlvPld1)) {
                vtupg_host_operation_handler(UPG_HOST_SEND_UN, pTlvPld1);
                result = VTMSG_STS_SUCCESS;
            }
        }
        #else
        result = VTMSG_STS_NOT_SUPPORTED;
        #endif
    }

    vtmsg_status_only(result, VTMSG_MSG_TYPE_UPGRADE_OP);
}

extern void lwip_sys_init(uint8_t *eui64, uint32_t node_id, uint32_t bandplan);
void vtmsg_start_network(uint8_t *pTlv, uint32_t len)
{
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint8_t result = VTMSG_STS_BAD_MSG;
    extern void set_bootstrap_start(uint8_t start);

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_START_NETWORK, &tlvLen0, &pTlvPld0)) {
            lwip_sys_init(NULL, pTlvPld0[0], 1);
            if (pTlvPld0[0] == 1)
                set_bootstrap_start(1);
            result = VTMSG_STS_SUCCESS;
        } else {
            result = VTMSG_STS_BAD_MSG;
        }
    }

    vtmsg_status_only(result, VTMSG_MSG_TYPE_START_NETWORK);
}


void vtmsg_get_topology_table(uint8_t *pTlv, uint32_t len)
{
    //for response
    uint8_t *pBuff = NULL, result = VTMSG_STS_INV_PARAMS;
    uint32_t lenOut;

    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            uint16_t index = 0;
            TPLG_TABLE_ST entry;
            memset(&entry, 0x0, sizeof(TPLG_TABLE_ST));
            memcpy(&index, pTlvPld0, 2);

            extern bool to_dcu_get_tplg_table(uint16_t index, TPLG_TABLE_ST *entry);
            if (DB->local_status.is_lbs == LOWPAN6_BOOTSTRAPPING_SERVER) {
#if CONFIG_DCUIF_ENABLE
                uint8_t *pBuffCurr;
                result = to_dcu_get_tplg_table(index, &entry);
                VPRINTF(VTLL_USER, "%s index = %d, table.result = %d"LF, __func__, index, result);

                if (result == 1) {
                    lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN +
                             VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_TOPOLOGY_TBL_ENTRY_LEN;

                    pBuff = vtmm_pool_alloc(lenOut, NULL);

                    pBuffCurr = vtmsg_build_msghdr(pBuff,
                                                   VTMSG_MSG_TYPE_GET_TOPOLOGY_TABLE,
                                                   VTMSG_ATTR_CMD_RESULT_LEN +
                                                   VTMSG_ATTR_CMD_INDEX_LEN +
                                                   VTMSG_ATTR_TOPOLOGY_TBL_ENTRY_LEN);
                    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                                   VTMSG_TLV_TYPE_CMD_RESULT,
                                                   VTMSG_ATTR_CMD_RESULT_LEN);
                    pBuffCurr[0] = VTMSG_STS_SUCCESS;
                    result = VTMSG_STS_SUCCESS;
                    pBuffCurr += 1;

                    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                                   VTMSG_TLV_TYPE_CMD_INDEX,
                                                   VTMSG_ATTR_CMD_INDEX_LEN);
                    memcpy(pBuffCurr, &index, sizeof(uint16_t));
                    pBuffCurr += 2;

                    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                                   VTMSG_TLV_TYPE_TOPOLOGY_TBL_ENTRY,
                                                   VTMSG_ATTR_TOPOLOGY_TBL_ENTRY_LEN);

                    memcpy(pBuffCurr, &entry, VTMSG_ATTR_TOPOLOGY_TBL_ENTRY_LEN);
                } else {
                    result = VTMSG_STS_END_OF_ENTRY;
                }
#else
                result = VTMSG_STS_NOT_SUPPORTED;
#endif
            } else {
                result = VTMSG_STS_NOT_SUPPORTED;
            }
        }
    }

    if (result != VTMSG_STS_SUCCESS) {
        vtmsg_status_only(result, VTMSG_MSG_TYPE_GET_TOPOLOGY_TABLE);
    } else {
        vtmsg_msg_send(pBuff, lenOut);
        vtmm_pool_free(pBuff);
    }
}


void vtmsg_request_route(uint8_t *pTlv, uint32_t len)
{
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint8_t addr[2];

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_SHORT_ADDR, &tlvLen0, &pTlvPld0)) {
            memcpy(addr, pTlvPld0, 2);
            vtmsg_status_only(VTMSG_STS_SUCCESS, VTMSG_MSG_TYPE_REQUEST_ROUTE);
            start_route_discovery(addr, false, 0);
        } else {
            vtmsg_status_only(VTMSG_STS_INV_PARAMS, VTMSG_MSG_TYPE_REQUEST_ROUTE);
        }
    }
}

void vtmsg_request_path(uint8_t *pTlv, uint32_t len)
{
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint8_t addr[2];

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_SHORT_ADDR, &tlvLen0, &pTlvPld0)) {
            memcpy(addr, pTlvPld0, 2);
            vtmsg_status_only(VTMSG_STS_SUCCESS, VTMSG_MSG_TYPE_REQUEST_PATH);
            start_path_discovery(addr, DB->lowpan6_adp_ib.adpMetricType);
        } else {
            vtmsg_status_only(VTMSG_STS_INV_PARAMS, VTMSG_MSG_TYPE_REQUEST_PATH);
        }
    }
}

void vtmsg_set_pan_id(uint8_t *pTlv, uint32_t len)
{
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint8_t addr[2];

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_PAN_ID, &tlvLen0, &pTlvPld0)) {
            memcpy(addr, pTlvPld0, 2);
            vtmsg_status_only(VTMSG_STS_SUCCESS, VTMSG_MSG_TYPE_SET_PAN_ID);
            memcpy(DB->mac_st.pib->macPANId, addr, 2);
            netif_create_ip6_linklocal_address(&DB->netif, 1);
        } else {
            vtmsg_status_only(VTMSG_STS_INV_PARAMS, VTMSG_MSG_TYPE_SET_PAN_ID);
        }
    }
}

void vtmsg_get_nt_table(uint8_t *pTlv, uint32_t len)
{
#if 0
    //for response
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t lenOut;
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t index = 0, curr;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            index = *pTlvPld0;
        } else {
            //TODO send error event
            return;
        }
    }
    //find routing table entry
    for (curr = index; curr < MAX_NEIGHBOR_TABLE_SIZE; curr++) {
        if (DB->mac_neighbor_table[curr].neighbour_valid_time ||
            DB->mac_neighbor_table[curr].neighbor_sec) {
            break;
        }
    }
    if (curr < MAX_NEIGHBOR_TABLE_SIZE) {
        lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN +
                 VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_NEIGHBOR_TBL_ENTRY_LEN;

        pBuff = vtmm_pool_alloc(lenOut, NULL);

        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_GET_NEIGHBOR_TABLE,
                                       VTMSG_ATTR_CMD_RESULT_LEN +
                                       VTMSG_ATTR_CMD_INDEX_LEN +
                                       VTMSG_ATTR_NEIGHBOR_TBL_ENTRY_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_RESULT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = VTMSG_STS_SUCCESS;
        pBuffCurr += 1;

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_INDEX,
                                       VTMSG_ATTR_CMD_INDEX_LEN);
        memcpy(pBuffCurr, &curr, sizeof(uint16_t));
        pBuffCurr += 2;

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_NEIGHBOR_TBL_ENTRY,
                                       VTMSG_ATTR_NEIGHBOR_TBL_ENTRY_LEN);
        memcpy(pBuffCurr, &DB->mac_neighbor_table[curr], sizeof(struct Neighbor_Table_ST));
    } else {
        lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN;
        pBuff = vtmm_pool_alloc(lenOut, NULL);
        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_GET_NEIGHBOR_TABLE,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_RESULT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = VTMSG_STS_END_OF_ENTRY;
    }
    vtmsg_msg_send(pBuff, lenOut);
    vtmm_pool_free(pBuff);
#endif
}


static uint32_t prepare_rt_entry(uint8_t **pBuff, uint16_t idx)
{
    uint8_t *pBuffCurr;
    uint32_t lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN +
                      VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_ROUTING_TBL_ENTRY_LEN;

    *pBuff = vtmm_pool_alloc(lenOut, NULL);

    pBuffCurr = vtmsg_build_msghdr(*pBuff,
                                   VTMSG_MSG_TYPE_GET_ROUTING_TABLE,
                                   VTMSG_ATTR_CMD_RESULT_LEN +
                                   VTMSG_ATTR_CMD_INDEX_LEN +
                                   VTMSG_ATTR_ROUTING_TBL_ENTRY_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_CMD_RESULT,
                                   VTMSG_ATTR_CMD_RESULT_LEN);
    pBuffCurr[0] = VTMSG_STS_SUCCESS;
    pBuffCurr += 1;

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_CMD_INDEX,
                                   VTMSG_ATTR_CMD_INDEX_LEN);
    memcpy(pBuffCurr, &idx, sizeof(uint16_t));
    pBuffCurr += 2;

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_ROUTING_TBL_ENTRY,
                                   VTMSG_ATTR_ROUTING_TBL_ENTRY_LEN);

    memcpy(pBuffCurr, &DB->route_table[idx], sizeof(struct route_table_st));

    return lenOut;
}

void vtmsg_get_rt_table(uint8_t *pTlv, uint32_t len)
{
    //for response
    uint8_t *pBuff = NULL;
    uint32_t lenOut = 0;
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t index = 0, curr;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        //table traversal
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            memcpy(&index, pTlvPld0, 2);
            //find routing table entry
            for (curr = index; curr < MAX_ROUTING_TABLE_SIZE; curr++) {
                if (DB->route_table[curr].valid_time || DB->route_table[curr].valid_sec) {
                    break;
                }
            }
            if (curr < MAX_ROUTING_TABLE_SIZE) {
                lenOut = prepare_rt_entry(&pBuff, curr);
            } else {
                vtmsg_status_only(VTMSG_STS_END_OF_ENTRY, VTMSG_MSG_TYPE_GET_ROUTING_TABLE);
                return;
            }
        } else if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_SHORT_ADDR, &tlvLen0, &pTlvPld0)) {
            uint8_t addr[2];
            memcpy(addr, pTlvPld0, 2);
            //find routing table entry
            for (index = 0; index < MAX_ROUTING_TABLE_SIZE; index++) {
                if (memcmp(DB->route_table[index].dest_short_addr, addr, 2) == 0) {
                    break;
                }
            }
            if (index < MAX_ROUTING_TABLE_SIZE) {
                lenOut = prepare_rt_entry(&pBuff, index);
            } else {
                vtmsg_status_only(VTMSG_STS_BAD_MSG, VTMSG_MSG_TYPE_GET_ROUTING_TABLE);
                return;
            }
        }
    }
    vtmsg_msg_send(pBuff, lenOut);
    vtmm_pool_free(pBuff);
}


void vtmsg_mem_dw(uint8_t *pTlv, uint32_t len)
{
    //response
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t *pBuffCurr_w;
    uint32_t addr, tvlLen = 0;
    //request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint32_t *pAddr;
    uint32_t idx = 0;
    uint8_t memLen = 0;

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_MEM_ADDR_AND_LEN, &tlvLen0, &pTlvPld0)) {
            addr = pTlvPld0[0] + (pTlvPld0[1] << 8) + (pTlvPld0[2] << 16) + (pTlvPld0[3] << 24);
            pAddr = (uint32_t *)addr;
            memLen = *(pTlvPld0 + 4);
        } else {
            //TODO send error event
            return;
        }
    }

    tvlLen = VTMSG_TLV_HDR_LEN + memLen * 4; // x4 for word

    pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + tvlLen, NULL);
    pBuffCurr = vtmsg_build_msghdr(pBuff,
                                   VTMSG_MSG_TYPE_MEM_OP_DUMP_WORD,
                                   tvlLen);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_MEM_OP_DW,
                                   tvlLen);
    pBuffCurr_w = (uint32_t *)pBuffCurr;
    while (memLen--) {
        pBuffCurr_w[idx] = pAddr[idx];
        idx++;
    }

    vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + tvlLen);
    vtmm_pool_free(pBuff);
}


void vtmsg_mem_ww(uint8_t *pTlv, uint32_t len)
{
    //response
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t *pBuffCurr_w;
    uint32_t input32b, tvlLen = 0;
    //request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint32_t *pAddr = NULL;
    uint32_t wordWrite;

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_MEM_ADDR_AND_DATA, &tlvLen0, &pTlvPld0)) {
            input32b = pTlvPld0[0] + (pTlvPld0[1] << 8) + (pTlvPld0[2] << 16) + (pTlvPld0[3] << 24);
            pAddr = (uint32_t *)input32b;
            input32b = pTlvPld0[4] + (pTlvPld0[5] << 8) + (pTlvPld0[6] << 16) + (pTlvPld0[7] << 24);
            wordWrite = input32b;
            *((volatile uint32_t *)pAddr) = wordWrite;
        } else {
            //TODO send error event
            return;
        }
    }

    tvlLen = VTMSG_TLV_HDR_LEN + 4;

    pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + tvlLen, NULL);
    pBuffCurr = vtmsg_build_msghdr(pBuff,
                                   VTMSG_MSG_TYPE_MEM_OP_WRITE_WORD,
                                   tvlLen);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_MEM_OP_WW,
                                   tvlLen);
    pBuffCurr_w = (uint32_t *)pBuffCurr;
    *pBuffCurr_w = *pAddr;

    vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + tvlLen);
    vtmm_pool_free(pBuff);
}

void vtmsg_bbp_raw_dump(uint8_t *pTlv, uint32_t len)
{
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t nword, tvlLen = 0;
    //request
    uint32_t input32b;
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    //bbp pkt
    struct vtdmac_packet *pkt = NULL;
    uint32_t data_out[10];
    uint32_t data_in[32];
    uint32_t len_in = 0;
    VPRINTF(VTLL_USER, "%s"LF, __func__);

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_BBP_SETTINGS_VALUE, &tlvLen0, &pTlvPld0)) {
            for (nword = 0; (nword * 4) < tlvLen0; nword++) {
                input32b = pTlvPld0[nword * 4 + 0] + (pTlvPld0[nword * 4 + 1] << 8) + (pTlvPld0[nword * 4 + 2] << 16) + (pTlvPld0[nword * 4 + 3] << 24);
                data_out[nword] = input32b;
            }
            if (data_out[0] != 0xffffffff) { //all 0xff is a special case for bbp log
                pkt = vtmac_bbp_raw_pkt_alloc(0, 0, data_out, nword * 4, false);
            }
        }
    }
    if (pkt != NULL) {
        if (vtmac_pkt_submit(pkt, true, false) < 0) {
            VPRINTF(VTLL_USER, "submit fail"LF);
            vtdmac_pkt_put(pkt, true);
        }
        len_in = vtmac_bbp_raw_rx_wait_data((uint8_t *)data_in, sizeof(data_in), 500);
        tvlLen = VTMSG_TLV_HDR_LEN + len_in;

        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + tvlLen, NULL);
        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_BBP_RAW_DUMP,
                                       tvlLen);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_BBP_SETTINGS_VALUE,
                                       tvlLen);
        memcpy(pBuffCurr, data_in, len_in);

        vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + tvlLen);
        vtmm_pool_free(pBuff);
    }
#if (CONFIG_BBP_MIB_LOG_ENABLE && CONFIG_CONSOLE_ENABLE)
    else if (data_out[0] == 0xffffffff) {
        extern struct bbp_mib_log_table bbp_mib_log;
        tvlLen = VTMSG_TLV_HDR_LEN + (sizeof(struct bbp_mib_log_st) * MAX_BBP_MIB_LOG_ENTRY);
        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + tvlLen, NULL);
        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_BBP_RAW_DUMP,
                                       tvlLen);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_BBP_SETTINGS_VALUE,
                                       tvlLen);
        memcpy(pBuffCurr, bbp_mib_log.bbp_log, (sizeof(struct bbp_mib_log_st) * MAX_BBP_MIB_LOG_ENTRY));
        vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + tvlLen);
        vtmm_pool_free(pBuff);
    }
#endif /* (CONFIG_BBP_MIB_LOG_ENABLE) */
}


void common_config_handler(uint8_t *pTlv, uint32_t len, uint32_t type, void *_st_ptr)
{
    //response
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t tvlLen = 0;
    uint32_t i = 0;
    //request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    struct {
        uint32_t *pvalue;
    } *st_param = _st_ptr;

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pTlvPld0)) {
            i = pTlvPld0[0];

            pTlv += VTMSG_ATTR_COMMON_CONFIG_IDX_LEN;
            len -= VTMSG_ATTR_COMMON_CONFIG_IDX_LEN;
            if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pTlvPld0)) {
                memcpy(st_param[i].pvalue, pTlvPld0, 4);
            }
        }
    }

    tvlLen = (VTMSG_ATTR_COMMON_CONFIG_IDX_LEN + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);

    pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + tvlLen, NULL);

    pBuffCurr = vtmsg_build_msghdr(pBuff,
                                   type,
                                   tvlLen);

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                   VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
    pBuffCurr[0] = i;
    pBuffCurr++;
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE,
                                   VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);
    memcpy(pBuffCurr, st_param[i].pvalue, 4);
    pBuffCurr += 4;

    vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + tvlLen);
    vtmm_pool_free(pBuff);
}


void vtmsg_bbp_settings(uint8_t *pTlv, uint32_t len)
{
    // TODO: change bbp_nv.h, fix later
    struct {
        uint32_t *pvalue;
    } _st_param[] = {
        { (uint32_t *)&(vtdmac_bbp_configs.band_freq_range.init_tone) },
        { (uint32_t *)&(vtdmac_bbp_configs.band_freq_range.total_tones) },
        { (uint32_t *)&(vtdmac_bbp_configs.band_freq_range.tones_per_grp) },
        { (uint32_t *)&(vtdmac_bbp_configs.modulation) },
        { &(vtdmac_bbp_configs.tdp_fir3_scale_max) },
        { &(vtdmac_bbp_configs.tdp_fir3_scale_min) },
        { &(vtdmac_bbp_configs.tdp_fir3_addr3db_thr) },
        { &(vtdmac_bbp_configs.tdp_fir3_start_scale) },
        { &(vtdmac_bbp_configs.ack_tx_additional_gain) },
        { &(vtdmac_bbp_configs.pga_mode) },
        { &(vtdmac_bbp_configs.urgentIn_en) },
        { &(vtdmac_bbp_configs.pmb_syncp_num) },
        { &(vtdmac_bbp_configs.pmb_match_symbol) },
        { &(vtdmac_bbp_configs.pmb_det_scale_fcc) },
        { &(vtdmac_bbp_configs.pmb_det_scale_cen_a) },
        { &(vtdmac_bbp_configs.pmb_det_scale_vango) },
        { &(vtdmac_bbp_configs.pmb_noise_threshold) },
        { &(vtdmac_bbp_configs.apdt_en) },
        { &(vtdmac_bbp_configs.modulation_scheme) },
        { &(vtdmac_bbp_configs.vango_band_pmb_syncp_num) },
    };

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    common_config_handler(pTlv, len, VTMSG_MSG_TYPE_BBP_SETTINGS, _st_param);
}

void vtmsg_csma_settings(uint8_t *pTlv, uint32_t len)
{
    VTMAC_CSMA *csma_var = &DB->mac_st.csma;
    VTMAC_PIB *pib = DB->mac_st.pib;
    VTMAC_ATTRIB *attrib = DB->mac_st.attrib;
    struct {
        uint32_t *pvalue;
    } _st_param[] = {
        { &(pib->macMaxBE) },
        { &(pib->macMinBE) },
        { &(pib->macA) },
        { &(pib->macK) },
        { &(pib->macMinCWAttempts) },
        { &(pib->macMaxCSMABackoffs) },
        { &(pib->macCSMAFairnessLimit) },
        { &(pib->macHighPriorityWindowSize) },
        { &(csma_var->NB) },
        { &(csma_var->NBF) },
        { &(csma_var->CW) },
        { &(csma_var->minCWCount) },
        { (uint32_t *)&(csma_var->QualtiyOfService) },
        { &(attrib->aSlotTime) },
    };

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    common_config_handler(pTlv, len, VTMSG_MSG_TYPE_CSMA_SETTINGS, _st_param);
}

void vtmsg_app_settings(uint8_t *pTlv, uint32_t len)
{
#if (CONFIG_SDK_ENABLE)
    extern vtsdk_nv_para_t sdk_para;
    struct {
        uint32_t *pvalue;
    } _st_param[] = {
        { &sdk_para.lbs_keepalive_duration },
#if 0
        { &sdk_para.announce_heartbeat_time },
#endif
    };

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    common_config_handler(pTlv, len, VTMSG_MSG_TYPE_APP_SETTINGS, _st_param);
#else
    vtmsg_status_only(VTMSG_STS_NOT_SUPPORTED, VTMSG_MSG_TYPE_APP_SETTINGS);
#endif
}



void vtmsg_packet_cntr(uint8_t *pTlv, uint32_t len)
{
    int32_t tlvLen0, i;
    uint8_t *pTlvPld0;

    struct {
        uint32_t *pvalue;
    } _st_param[] = {
        { &(vtdmac_global.pkt_mist.submitted) },
        { &(vtdmac_global.pkt_mist.tx) },
        { &(vtdmac_global.pkt_mist.rx) },
        { &(vtdmac_global.pkt_mist.rx_ctrl_normal_rx) },
        { &(vtdmac_global.pkt_mist.rx_ctrl_bypass_rx) },
        { &(vtdmac_global.pkt_mist.rx_ctrl_raw_rx) },
        { &(vtdmac_global.pkt_mist.rx_fcs_ok) },
        { &(vtdmac_global.pkt_mist.rx_fcs_fail) },
        { &(vtdmac_global.pkt_mist.rx_hcs_ok) },
        { &(vtdmac_global.pkt_mist.rx_hcs_fail) },
        { &(vtdmac_global.pkt_mist.rx_mic_ok) },
        { &(vtdmac_global.pkt_mist.rx_mic_fail) },
        { &(vtdmac_global.pkt_mist.rx_ctrl_ack) },
        { &(vtdmac_global.pkt_mist.rx_ctrl_nack) },
        { &(vtdmac_global.pkt_mist.detect_pmb) },
        { &(vtdmac_global.pkt_mist.rx_cmp) },
        { &(vtdmac_global.pkt_mist.urgent_in_cnt) },
        { &(vtdmac_global.pkt_mist.tx_cmp) },
        { &(vtdmac_global.pkt_mist.tx_ack) },
        { &(vtdmac_global.pkt_mist.tx_nack) },
        { &(vtdmac_global.pkt_mist.tx_ack_timeout) },
        { &(vtdmac_global.pkt_mist.tx_ack_timeout_ignore) },
        { &(vtdmac_global.pkt_mist.tx_csma_success) },
        { &(vtdmac_global.pkt_mist.tx_csma_fail) },
        { &(vtdmac_global.pkt_mist.tx_retry_fail) },
        { &(vtdmac_global.pkt_mist.tx_sw_ack_timeout) },
        { &(vtdmac_global.pkt_mist.tx_sw_tx_cmp_timeout) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_esc_route) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_esc_bootstrap) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_esc_vango) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_drop) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_drop_multicast) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_forward_unicast) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_forward_unicast_fail) },
        { &(vtdmac_global.pkt_mist.lowpan6_rx_forward_broadcast) },
        { &(vtdmac_global.pkt_mist.icmp_rx_echo_request) },
        { &(vtdmac_global.pkt_mist.icmp_rx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_request) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
#if (CONFIG_SDK_ENABLE)
        {&(sdk_packet_counter.sdk_uart_tx_packet) },
        { &(sdk_packet_counter.sdk_uart_rx_packet) },
        { &(sdk_packet_counter.sdk_plc_tx_packet) },
        { &(sdk_packet_counter.sdk_plc_rx_packet) },
        { &(sdk_packet_counter.sdk_uart_tx_packet) },
        { &(sdk_packet_counter.sdk_uart_rx_packet) },
        { &(sdk_packet_counter.sdk_plc_tx_packet) },
        { &(sdk_packet_counter.sdk_plc_rx_packet) },
#else
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
        { &(vtdmac_global.pkt_mist.icmp_tx_echo_reply) },
#endif
    };

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pTlvPld0)) {
            i = pTlvPld0[0];
            if (i == 0xff) { // 0xff is for "clear"
                memset(&vtdmac_global.pkt_mist, 0, sizeof(vtdmac_global.pkt_mist));
                #if (CONFIG_SDK_ENABLE)
                memset(&sdk_packet_counter, 0, sizeof(sdk_packet_counter));
                #endif
                return;
            } else {
                common_config_handler(pTlv, len, VTMSG_MSG_TYPE_NETWORK_CNTR, _st_param);
            }
        }
    }
}

#if (CONFIG_SW_MAC_ZC_TEST)
extern uint32_t zc_test_force_vcs_busy;
extern uint32_t zc_test_force_csma_fail;
extern uint32_t zc_test_force_reply_nack;
extern uint32_t zc_test_force_no_reply;
extern uint32_t zc_test_force_reply_ack;
extern uint32_t p_csma_success_to;
extern uint32_t p_ack_to;
extern uint32_t p_tx_cmp_to_from_ackto;
extern uint32_t p_tx_cmp_to_from_csma;
extern uint32_t p_tx_cmp_to_from_ack;
extern uint32_t p_tx_cmp_to;
#endif
/*- vtmsg_get_zerox_hi_record */
void vtmsg_get_zerox_hi_record(uint8_t *pTlv, uint32_t len)
{
#if (CONFIG_DCUIF_ENABLE && CONFIG_PHASE_CORRECTION)
    extern zc_hit_records_t _zc_hit_record[2];
#else
    extern zc_hit_records_t _zc_hit_record[1];
#endif

    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_ZEROX_HIT_RECORD_LEN] = {};
    uint8_t *pBuffCurr;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    pBuffCurr = vtmsg_build_msghdr(buff,
                                   VTMSG_MSG_TYPE_GET_ZEROX_HIT_RECORD,
                                   VTMSG_ATTR_ZEROX_HIT_RECORD_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_ZEROX_HIT_RECORD,
                                   VTMSG_ATTR_ZEROX_HIT_RECORD_LEN);

    memcpy(pBuffCurr, &_zc_hit_record[ZC_PRI_INDEX], sizeof(zc_hit_records_t)); //build time

    vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_ZEROX_HIT_RECORD_LEN);
}

void vtmsg_zerox_settings(uint8_t *pTlv, uint32_t len)
{
#if (CONFIG_SW_MAC_ZC)
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    struct {
        uint32_t *pvalue;
    } _st_param[] = {
        { &zc_enable_nv },
        { &zc_gpio_in_nv },
        { &zc_gpio_out_nv },
        { &zc_degree_nv },
        { &zc_check_no_ac_nv },
        { &zc_bbp_proc_delay_nv },
        { (uint32_t *)&zc_window_nv }, //skipping the differ in signedness issue
#if (CONFIG_SW_MAC_ZC_TEST)
        {&zc_test_force_vcs_busy },
        { &zc_test_force_csma_fail },
        { &zc_test_force_reply_ack },
        { &zc_test_force_reply_nack },
        { &zc_test_force_no_reply },
        { &p_csma_success_to },
        { &p_ack_to },
        { &p_tx_cmp_to_from_ackto },
        { &p_tx_cmp_to_from_csma },
        { &p_tx_cmp_to_from_ack },
        { &p_tx_cmp_to },
#else
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
        { &zc_gpio_out_nv },
#endif
    };

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    common_config_handler(pTlv, len, VTMSG_MSG_TYPE_ZEROX_SETTINGS, _st_param);

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pTlvPld0)) {
            vtmm_flush_nvram();
#if (CONFIG_SW_MAC_ZC_TEST)
            extern void vtmac_change_zc_timer(void);
            vtmac_change_zc_timer();
#endif
            vtmac_zc_init();
        }
    }
#else
    vtmsg_status_only(VTMSG_STS_NOT_SUPPORTED, VTMSG_MSG_TYPE_ZEROX_SETTINGS);
#endif
}



void vtmsg_reboot(uint8_t *pTlv, uint32_t len)
{
    //request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    VPRINTF(VTLL_USER, "%s"LF, __func__);

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_REBOOT_REMAP, &tlvLen0, &pTlvPld0)) {
            vtclx_reboot(pTlvPld0[0], RB_FLAG_CLI);
        }
    }
}

void vtmsg_get_system_log_table(uint8_t *pTlv, uint32_t len)
{
    //for response
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t lenOut = 0;
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t curr = 0;

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            //curr = *pTlvPld0;
            memcpy(&curr, pTlvPld0, 2);
            if (curr == 0xeeee) { // 0xeeee is for "erase"
                memset((uint8_t *)&sys_log, 0x0, sizeof(sys_log));
                #if (CONFIG_SYSTEM_LOG_ENABLE)
                clean_system_log();
                #endif /* (CONFIG_SYSTEM_LOG_ENABLE) */
                //return;
            }
        } else {
            //TODO send error event
            return;
        }
    }

    if (curr < MAX_SYSTEM_LOG_ENTRY && curr < system_log.total_log_count) {
        lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_TLV_HDR_LEN + sizeof(VT_SYSTEM_DATA_LOG) + VTMSG_TLV_HDR_LEN + sizeof(SYS_LOG);

        pBuff = vtmm_pool_alloc(lenOut, NULL);
        // build result TLV header
        pBuffCurr = vtmsg_build_tlvhdr(pBuff + sizeof(vtmsg_msgHdr_s),
                                       VTMSG_TLV_TYPE_CMD_RESULT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = VTMSG_STS_SUCCESS;
        pBuffCurr += 1;

        // build cmd index TLV header
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_INDEX,
                                       VTMSG_ATTR_CMD_INDEX_LEN);
        memcpy(pBuffCurr, &curr, sizeof(uint16_t));
        pBuffCurr += 2;


        // build system log data TLV header
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_SYSTEM_DATA_LOG,
                                       VTMSG_TLV_HDR_LEN + sizeof(VT_SYSTEM_DATA_LOG));

        memcpy(pBuffCurr, (uint8_t *)&system_log.log[curr], sizeof(VT_SYSTEM_DATA_LOG));
        pBuffCurr += sizeof(VT_SYSTEM_DATA_LOG);
        if (curr == 0) { //put runtime in syslog 1st entry
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                           VTMSG_TLV_RUNTIME_SYS_LOG,
                                           VTMSG_TLV_HDR_LEN + sizeof(SYS_LOG));

            memcpy(pBuffCurr, (uint8_t *)&sys_log, sizeof(SYS_LOG));
            //lenOut += VTMSG_TLV_HDR_LEN + sizeof(SYS_LOG);
        } else {
            lenOut -= VTMSG_TLV_HDR_LEN + sizeof(SYS_LOG);
        }
        // build msg header
        vtmsg_build_msghdr(pBuff,
                           VTMSG_MSG_TYPE_RUNTIME_SYS_LOG,
                           lenOut - sizeof(vtmsg_msgHdr_s));
    } else {
        lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN + VTMSG_ATTR_CMD_INDEX_LEN;

        pBuff = vtmm_pool_alloc(lenOut, NULL);

        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_RUNTIME_SYS_LOG,
                                       VTMSG_ATTR_CMD_RESULT_LEN + VTMSG_ATTR_CMD_INDEX_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_RESULT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = VTMSG_STS_END_OF_ENTRY;
        pBuffCurr += 1;
        //return total count if VTMSG_STS_END_OF_ENTRY
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_INDEX,
                                       VTMSG_ATTR_CMD_INDEX_LEN);
        memcpy(pBuffCurr, &system_log.total_log_count, sizeof(uint16_t));
    }


    vtmsg_msg_send(pBuff, lenOut);
    vtmm_pool_free(pBuff);
}


#define BBP2SOC_ADDR_MAPPING(addr) \
    if ((addr >> 16) == 0x6001) addr = 0xA0240000 + (addr & 0xFFFF); \
    else if ((addr >> 16) == 0x6002) addr = 0xA0220000 + (addr & 0xFFFF);
void vtmsg_bbp_sig_data(uint32_t reg_addr, uint16_t index)
{
    uint32_t data_addr = 0;
    uint32_t value;
    uint8_t *pBuffCurr;
    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_BBP_SIG_TLV_LEN] = {};

    data_addr = *((uint32_t *)reg_addr);

    BBP2SOC_ADDR_MAPPING(data_addr);
    value = (int16_t)((*(volatile int32_t *)((data_addr + index * 2) - (uint32_t)(data_addr + index * 2) % 4)) >> (uint32_t)(((data_addr + index * 2) % 4) * 8));
    pBuffCurr = vtmsg_build_msghdr(buff,
                                   VTMSG_MSG_TYPE_BBP_SIG,
                                   VTMSG_BBP_SIG_TLV_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_BBP_SIG_DATA,
                                   VTMSG_BBP_SIG_TLV_LEN);
    memcpy(pBuffCurr, (uint8_t *)&value, VTMSG_BBP_SIG_TLV_LEN - VTMSG_TLV_HDR_LEN);  //copy the bbp ram data
    vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_BBP_SIG_TLV_LEN);
}



void vtmsg_bbp_sig0(uint8_t *pTlv, uint32_t len)
{
    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_BBP_SIG_TLV_LEN * 3] = {};
    extern Neighbor_Table_ST to_all_neighbour_table;
    uint32_t band_plan = to_all_neighbour_table.bandplan;
    vtdmac_bbp_agc_gain_t rx_gain;
    vtdmac_bbp_rx_pwr_t rx_pwr;
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t index = 0;
    uint8_t *pBuffCurr;
    uint32_t reg_addr;
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            memcpy(&index, pTlvPld0, 2);
        } else {
            //TODO send error event
            return;
        }
    }
    reg_addr = 0xA0220010;
    if (index == 0xffff) {
        vtdmac_bbp_get_rx_gain_power(&rx_gain, &rx_pwr);

        vtmac_bbp_reset(0);

        pBuffCurr = vtmsg_build_msghdr(buff,
                                       VTMSG_MSG_TYPE_BBP_SIG,
                                       VTMSG_BBP_SIG_TLV_LEN * 3);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_BBP_SIG0_BAND,
                                       VTMSG_BBP_SIG_TLV_LEN);
        memcpy(pBuffCurr, (uint8_t *)&band_plan, VTMSG_BBP_SIG_TLV_LEN - VTMSG_TLV_HDR_LEN);

        pBuffCurr += (VTMSG_BBP_SIG_TLV_LEN - VTMSG_TLV_HDR_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_BBP_SIG0_RX_GAIN,
                                       VTMSG_BBP_SIG_TLV_LEN);

        memcpy(pBuffCurr, (uint8_t *)&(rx_gain.synp_pass_gain), VTMSG_BBP_SIG_TLV_LEN - VTMSG_TLV_HDR_LEN);

        pBuffCurr += (VTMSG_BBP_SIG_TLV_LEN - VTMSG_TLV_HDR_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_BBP_SIG0_RX_PWR,
                                       VTMSG_BBP_SIG_TLV_LEN);
        memcpy(pBuffCurr, (uint8_t *)&(rx_pwr.synp_pass_pwr), VTMSG_BBP_SIG_TLV_LEN - VTMSG_TLV_HDR_LEN);


        vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_BBP_SIG_TLV_LEN * 3); //VTMSG_TLV_TYPE_BBP_SIG0_BAND +  VTMSG_TLV_TYPE_BBP_SIG0_RX_GAIN + VTMSG_TLV_TYPE_BBP_SIG0_RX_PWR
    } else {
        vtmsg_bbp_sig_data(reg_addr, index);
    }
}


void vtmsg_bbp_sig1(uint8_t *pTlv, uint32_t len)
{
    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_BBP_SIG_TLV_LEN * 2] = {};
    uint16_t analog_RX_gain, digital_RX_gain;
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t index = 0;
    uint8_t *pBuffCurr;
    uint32_t reg_addr;

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            memcpy(&index, pTlvPld0, 2);
        } else {
            //TODO send error event
            return;
        }
    }
    reg_addr = 0xA0220014;
    if (index == 0xffff) {
        vtdmac_bbp_get_pmb_detect_idle_gain_misc(&analog_RX_gain, &digital_RX_gain);
        vtdmac_bbp_issue_noise_flow_cmd(1000, 0, 0, 1, 0, 0);
        vtmac_bbp_reset(0);

        pBuffCurr = vtmsg_build_msghdr(buff,
                                       VTMSG_MSG_TYPE_BBP_SIG,
                                       VTMSG_BBP_SIG_TLV_LEN);

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_BBP_SIG1_PMB_MISC,
                                       VTMSG_BBP_SIG_TLV_LEN);
        memcpy(pBuffCurr, (uint8_t *)&analog_RX_gain, sizeof(uint16_t));
        pBuffCurr += sizeof(uint16_t);
        memcpy(pBuffCurr, (uint8_t *)&digital_RX_gain, sizeof(uint16_t));

        vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_BBP_SIG_TLV_LEN);
    } else {
        vtmsg_bbp_sig_data(reg_addr, index);
    }
}


void vtmsg_bbp_sig(uint8_t *pTlv, uint32_t len)
{
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    uint8_t bbp_sig_type = BBP_SIG_TYPE_PREAMBLE;
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_BBP_SIG_TYPE, &tlvLen0, &pTlvPld0)) {
            bbp_sig_type = *(uint8_t *)pTlvPld0;
        } else {
            //TODO send error event
            return;
        }
    }

    switch (bbp_sig_type) {
    case BBP_SIG_TYPE_PREAMBLE:
        vtmsg_bbp_sig0(pTlv, len);
        break;
    case BBP_SIG_TYPE_NOISE_FLOOR:
        vtmsg_bbp_sig1(pTlv, len);
        break;
    }
}



#if 0
void vtmsg_get_eni_eui_table(uint8_t *pTlv, uint32_t len)
{
#if (NEI_EUI_ENABLE)
    //for response
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t lenOut;
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t index = 0, curr;
    extern struct NEI_EUI64 nei_eui64_list[NEI_EUI_TABLE_SIZE];

    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            memcpy(&index, pTlvPld0, 2);
        } else {
            //TODO send error event
            return;
        }
    }
    //find routing table entry
    for (curr = index; curr < NEI_EUI_TABLE_SIZE; curr++) {
        if (nei_eui64_list[curr].valid) {
            break;
        }
    }
    if (curr < NEI_EUI_TABLE_SIZE) {
        lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN +
                 VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_NEI_EUI_TBL_ENTRY_LEN;

        pBuff = vtmm_pool_alloc(lenOut, NULL);

        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_GET_NEI_EUI_TABLE,
                                       VTMSG_ATTR_CMD_RESULT_LEN +
                                       VTMSG_ATTR_CMD_INDEX_LEN +
                                       VTMSG_ATTR_NEI_EUI_TBL_ENTRY_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_RESULT,
                                       VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = VTMSG_STS_SUCCESS;
        pBuffCurr += 1;

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_CMD_INDEX,
                                       VTMSG_ATTR_CMD_INDEX_LEN);
        memcpy(pBuffCurr, &curr, sizeof(uint16_t));
        pBuffCurr += 2;

        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                       VTMSG_TLV_TYPE_NEI_EUI_TBL_ENTRY,
                                       VTMSG_ATTR_NEI_EUI_TBL_ENTRY_LEN);

        memcpy(pBuffCurr, (uint8_t *)&nei_eui64_list[curr], VTMSG_ATTR_NEI_EUI_TBL_ENTRY_LEN - VTMSG_TLV_HDR_LEN);
    } else {
        vtmsg_status_only(VTMSG_STS_END_OF_ENTRY, VTMSG_MSG_TYPE_GET_NEI_EUI_TABLE);
        return;
    }

    vtmsg_msg_send(pBuff, lenOut);
    vtmm_pool_free(pBuff);
#endif /*(NEI_EUI_ENABLE)*/
}
#else
void vtmsg_get_eni_eui_table(uint8_t *pTlv, uint32_t len)
{
    vtmsg_status_only(VTMSG_STS_NOT_SUPPORTED, VTMSG_MSG_TYPE_GET_NEI_EUI_TABLE);
}
#endif

extern uint8_t vtmac_disable_bootstrap;
void vtmsg_configuration(uint8_t *pTlv, uint32_t len)
{
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    //VPRINTF(VTLL_USER, "%s"LF,__func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_BOOTSTRAP_ONOFF, &tlvLen0, &pTlvPld0)) {
            if (pTlvPld0[0]) {
                vtmac_disable_bootstrap = 1;
            } else {
                vtmac_disable_bootstrap = 0;
            }
            vtmsg_status_only(VTMSG_STS_SUCCESS, VTMSG_MSG_TYPE_SYS_CONFIGURATION);
        }
    }
}


void vtmsg_get_joined_table(uint8_t *pTlv, uint32_t len)
{
    //for response
    uint8_t *pBuff = NULL;
    uint8_t *pBuffCurr;
    uint32_t lenOut;
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t index = 0;


    VPRINTF(VTLL_USER, "%s"LF, __func__);
    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pTlvPld0)) {
            memcpy(&index, pTlvPld0, 2);
        } else {
            //TODO send error event
            return;
        }
    }
    if (DB->local_status.is_lbs == LOWPAN6_BOOTSTRAPPING_SERVER) {
        S2E_TABLE_ST entry;
#if CONFIG_DCUIF_ENABLE
        extern void to_dcu_get_s2e_table_entry(uint16_t index, S2E_TABLE_ST *entry);
        to_dcu_get_s2e_table_entry(index, &entry);
#else
#if (!CONFIG_SDK2)
        uint16_t curr = 0, valid_cnt = 0;
        for (curr = 0; curr < MAX_ROUTING_TABLE_SIZE; curr++) {
            if (DB->lbs_alloc_addr_table[curr].valid) {
                valid_cnt++;
                if (valid_cnt == (index + 1)) {
                    entry = DB->lbs_alloc_addr_table[curr];
                    break;
                }
            }
        }
        if (curr >= MAX_ROUTING_TABLE_SIZE) {
            entry.valid = 0;
        }
#else
        entry.valid = 0;
        if (vtsdk_cb.param.get_lbs_joined_table_callback != NULL)
            vtsdk_cb.param.get_lbs_joined_table_callback(index, (vtsdk_lbs_s2e_table_entry *)&entry);
#endif
#endif

        VPRINTF(VTLL_USER, "%s index = %d, table.valid = %d"LF, __func__, index, entry.valid);

        if (entry.valid) {
            lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN +
                     VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_S2E_TBL_ENTRY_LEN;

            pBuff = vtmm_pool_alloc(lenOut, NULL);

            pBuffCurr = vtmsg_build_msghdr(pBuff,
                                           VTMSG_MSG_TYPE_GET_JOINED_TABLE,
                                           VTMSG_ATTR_CMD_RESULT_LEN +
                                           VTMSG_ATTR_CMD_INDEX_LEN +
                                           VTMSG_ATTR_S2E_TBL_ENTRY_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                           VTMSG_TLV_TYPE_CMD_RESULT,
                                           VTMSG_ATTR_CMD_RESULT_LEN);
            pBuffCurr[0] = VTMSG_STS_SUCCESS;
            pBuffCurr += 1;

            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                           VTMSG_TLV_TYPE_CMD_INDEX,
                                           VTMSG_ATTR_CMD_INDEX_LEN);
            memcpy(pBuffCurr, &index, sizeof(uint16_t));
            pBuffCurr += 2;

            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                           VTMSG_TLV_TYPE_S2E_TBL_ENTRY,
                                           VTMSG_ATTR_S2E_TBL_ENTRY_LEN);

            memcpy(pBuffCurr, &entry, sizeof(S2E_TABLE_ST));
            vtmsg_msg_send(pBuff, lenOut);
            vtmm_pool_free(pBuff);
        } else {
            vtmsg_status_only(VTMSG_STS_END_OF_ENTRY, VTMSG_MSG_TYPE_GET_JOINED_TABLE);
        }
    } else {
        vtmsg_status_only(VTMSG_STS_NOT_SUPPORTED, VTMSG_MSG_TYPE_GET_JOINED_TABLE);
    }
}


void vtmsg_get_lbs_info(uint8_t *pTlv, uint32_t len)
{
    uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_LBS_TBL_LEN] = {};
    uint8_t *pBuffCurr;
    uint8_t i = 0;
    uint16_t route_cost = 0;

    //VPRINTF(VTLL_USER, "%s"LF,__func__);
    pBuffCurr = vtmsg_build_msghdr(buff,
                                   VTMSG_MSG_TYPE_GET_LBS_INFO,
                                   VTMSG_LBS_TBL_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                   VTMSG_TLV_TYPE_LBS_TBL_ENTRY,
                                   VTMSG_LBS_TBL_LEN);

    for (i = 0; i < 3; i++) {
        pBuffCurr[0 + i * 6] = lbs_beacon_packet_table[i].PAN_id & 0xFF;
        pBuffCurr[1 + i * 6] = lbs_beacon_packet_table[i].PAN_id >> 8;
        pBuffCurr[2 + i * 6] = lbs_beacon_packet_table[i].LBA_address[0];
        pBuffCurr[3 + i * 6] = lbs_beacon_packet_table[i].LBA_address[1];
        route_cost = lbs_beacon_packet_table[i].link_cost + lbs_beacon_packet_table[i].RC_COORD;
        memcpy(&pBuffCurr[4 + i * 6], &route_cost, 2);
    }

    vtmsg_msg_send(buff, sizeof(vtmsg_msgHdr_s) + VTMSG_LBS_TBL_LEN);
    vtmm_pool_free(buff);
}


uint16_t lbs_panid;
void vtmsg_send_join_network(uint8_t *pTlv, uint32_t len)
{
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_SEND_JOIN_PAN, &tlvLen0, &pTlvPld0)) {
            lbs_panid = lbs_beacon_packet_table[pTlvPld0[0]].PAN_id;
            SetWaitJoinPANCmd(1);
            vtmsg_status_only(VTMSG_STS_SUCCESS, VTMSG_MSG_TYPE_SEND_JOIN_NETWORK_CMD);
        }
    }
}



uint32_t add_lbs_info(LBS_status_t status, uint8_t PAN_count, LBS_pan_descriptor_t *PAN_descriptor)
{
    if (WaitJoinPANCmd != 1) {
        memset(lbs_beacon_packet_table, 0x0, sizeof(lbs_beacon_packet_table));
        memcpy(lbs_beacon_packet_table, PAN_descriptor, sizeof(LBS_pan_descriptor_t) * PAN_count);
        return 0xFFFFFFFF;
    } else {
        WaitJoinPANCmd = 0;
        return lbs_panid;
    }
}


void SetWaitJoinPANCmd(bool value)
{
    WaitJoinPANCmd = value;
}


void vtmsg_mac_data_send(uint8_t *pbuf, uint32_t len)
{
    if (vtmsg_msg_destination.final_dest_addr_len == 2) {
        lowpan6_raw_send(NULL, vtmsg_msg_destination.final_dest_addr, pbuf, len);
    } else if (DB->rx_pkt_mac_info.src.addr_len == 8) {
        lowpan6_mac_eui64_send(vtmsg_msg_destination.final_dest_addr, pbuf, len);
    }
}

void vtmsg_psk_set(uint8_t *pTlv, uint32_t len)
{
    //for request
    int32_t tlvLen0;
    uint8_t *pTlvPld0;

    if (len == 0) {
        //for response
        uint8_t *pBuff = NULL;
        uint8_t *pBuffCurr;
        uint32_t lenOut;
        uint8_t i;
        pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + (VTMSG_ATTR_PSK_ENTRY_LEN * LBS_PSK_KEY_COUNT), NULL);
        pBuffCurr = vtmsg_build_msghdr(pBuff,
                                       VTMSG_MSG_TYPE_GET_PSK_SET,
                                       (VTMSG_ATTR_PSK_ENTRY_LEN * LBS_PSK_KEY_COUNT));
        for (i = 0; i < LBS_PSK_KEY_COUNT; i++) {
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                           VTMSG_TLV_TYPE_PSK_ENTRY,
                                           VTMSG_ATTR_PSK_ENTRY_LEN);
            pBuffCurr[0] = i;
            memcpy(&pBuffCurr[1], DB->psk.keyPSK[i], LOWPAN6_KEY_LENGTH);
            pBuffCurr += 1 + LOWPAN6_KEY_LENGTH;
        }
        lenOut = sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_PSK_ENTRY_LEN * LBS_PSK_KEY_COUNT;
        vtmsg_msg_send(pBuff, lenOut);
        vtmm_pool_free(pBuff);
    } else {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_PSK_ENTRY, &tlvLen0, &pTlvPld0)) {
            if (pTlvPld0[0] < LBS_PSK_KEY_COUNT) {
                memcpy(DB->psk.keyPSK[pTlvPld0[0]], &pTlvPld0[1], LOWPAN6_KEY_LENGTH);
                vtmsg_status_only(VTMSG_STS_SUCCESS, VTMSG_MSG_TYPE_GET_PSK_SET);
            } else {
                vtmsg_status_only(VTMSG_STS_INV_PARAMS, VTMSG_MSG_TYPE_GET_PSK_SET);
            }
        }
    }
}

void vtmsg_app_exangedata(uint8_t *pTlv, uint32_t len)
{
#if defined(CONFIG_APP_EXCHANGEDATA_ENABLE) && (CONFIG_APP_EXCHANGEDATA_ENABLE == 1)
    //uint8_t buff [sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN] = {};
    uint8_t *pBuffCurr;
    uint8_t *pBuff = NULL;
    int32_t tlvLen0;
    uint8_t *pTlvPld0;
    uint16_t cmd_index;
    uint16_t data_size;
    uint8_t *data;
    //uint8_t i;


    //VPRINTF(VTLL_USER, "%s"LF, __func__);
    extern uint32_t vtsdk_app_exchangedatapath(uint16_t cmd_index, uint16_t *bufsize, uint8_t **buf);

    if (len > 0) {
        if (vtmsg_get_tlv(pTlv, len, VTMSG_TLV_TYPE_APP_DATA_EXCHANGE, &tlvLen0, &pTlvPld0)) {
            memcpy(&cmd_index, &pTlvPld0[0], 2);

            vtsdk_app_exchangedatapath(cmd_index, &data_size, &data);

            if (data_size > 0) {
                pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_APP_DATA_EXCHANGE_LEN + data_size, NULL);

                //for (i = 0; i < data_size; i++)
                //    VPRINTF(VTLL_USER, "%s %02X"LF, __func__, data[i]);

                pBuffCurr = vtmsg_build_msghdr(pBuff,
                                               VTMSG_MSG_TYPE_APP_EXCHANGE_DATA,
                                               VTMSG_ATTR_APP_DATA_EXCHANGE_LEN + data_size);
                pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                               VTMSG_TLV_TYPE_APP_DATA_EXCHANGE,
                                               VTMSG_ATTR_APP_DATA_EXCHANGE_LEN + data_size);

                memcpy(&pBuffCurr[0], &cmd_index, sizeof(uint16_t));
                pBuffCurr += sizeof(uint16_t);
                memcpy(pBuffCurr, &data_size, sizeof(uint16_t));
                pBuffCurr += sizeof(uint16_t);

                if (data != NULL)
                    memcpy(pBuffCurr, data, data_size);

                //for (i = 0; i < sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_APP_DATA_EXCHANGE_LEN + data_size; i++)
                //    VPRINTF(VTLL_USER, "%s %02X"LF, __func__, pBuff[i]);

                vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_APP_DATA_EXCHANGE_LEN + data_size);
                vtmm_pool_free(pBuff);
            } else {
                pBuff = vtmm_pool_alloc(sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_APP_DATA_EXCHANGE_LEN, NULL);

                pBuffCurr = vtmsg_build_msghdr(pBuff,
                                               VTMSG_MSG_TYPE_APP_EXCHANGE_DATA,
                                               VTMSG_ATTR_APP_DATA_EXCHANGE_LEN);
                pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                               VTMSG_TLV_TYPE_APP_DATA_EXCHANGE,
                                               VTMSG_ATTR_APP_DATA_EXCHANGE_LEN);

                memcpy(&pBuffCurr[0], &cmd_index, sizeof(uint16_t));
                pBuffCurr += sizeof(uint16_t);
                memcpy(pBuffCurr, &data_size, sizeof(uint16_t));
                pBuffCurr += sizeof(uint16_t);
                //for (i = 0; i < sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_APP_DATA_EXCHANGE_LEN; i++)
                //    VPRINTF(VTLL_USER, "%s %02X"LF, __func__, pBuff[i]);

                vtmsg_msg_send(pBuff, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_APP_DATA_EXCHANGE_LEN);
                vtmm_pool_free(pBuff);
            }
        }
    }
#else
    vtmsg_status_only(VTMSG_STS_NOT_SUPPORTED, VTMSG_MSG_TYPE_APP_EXCHANGE_DATA);
#endif
}

#if (__SOC_PLATFORM__ == __MAGPIE_E__)
#define VTMSG_LITE 0
#else
#if (CONFIG_REMOTE_FW_UPGRADE)
#define VTMSG_LITE 1
#else
#define VTMSG_LITE 0
#endif
#endif
/*
 *  index is defined in vtmsg.h
 * */
#if VTMSG_LITE
VTMSG_PROC vtmsg_app_table[80] =
{
/* TYPE 0x0*  SYSYEM CMD */
    NULL,//vtmsg_configuration,      //0x00    VTMSG_MSG_TYPE_SYS_CONFIGURATION
    vtmsg_get_version_internal,//0x01   VTMSG_MSG_TYPE_GET_VERSION_INTERNAL
    NULL,                   //0x02    VTMSG_MSG_TYPE_GET_MEM_INFO
    NULL,                   //0x03
    NULL,                   //0x04
    NULL,                   //0x05
    NULL,                   //0x06
    vtmsg_mem_dw,             //0x07    VTMSG_MSG_TYPE_MEM_OP_WRITE_BYTE
    vtmsg_mem_ww,             //0x08    VTMSG_MSG_TYPE_MEM_OP_WRITE_WORD
    NULL,                   //0x09
    NULL,                   //0x0A
    vtmsg_reboot,             //0x0B
    NULL,                   //0x0C
    NULL,                   //0x0D
    NULL,                   //0x0E
    NULL,//vtmsg_get_eni_eui_table,  //0x0F
/* TYPE 0x1*  SYSYEM CMD FOR CUSTOMER */
    NULL,//vtmsg_start_network,      //0x10    VTMSG_MSG_TYPE_START_NETWORK
    vtmsg_get_version,        //0x11    VTMSG_MSG_TYPE_GET_VERSION_INFO
    NULL,//vtmsg_get_joined_table,   //0x12
    NULL,                   //0x13
    NULL,                   //0x14
    NULL,//vtmsg_bbp_sig,            //0x15    VTMSG_MSG_TYPE_BBP_SIG
/* TYPE 0x16 - 0x1f VANGO */
    NULL,//vtmsg_upg_control,        //0x16
    NULL,//vtmsg_zerox_settings,     //0x17
    NULL,                   //0x18    vtmsg_upg_event,  outgoing only
    NULL,//vtmsg_upg_data,           //0x19
    NULL,//vtmsg_upg_operation,      //0x1A
    NULL,//vtmsg_bbp_settings,       //0x1B    VTMSG_MSG_TYPE_BBP_SETTINGS
    NULL,//vtmsg_app_settings,       //0x1C    VTMSG_MSG_TYPE_APP_SETTINGS
    vtmsg_bbp_raw_dump,       //0x1D    VTMSG_MSG_TYPE_BBP_RAW_DUMP
    NULL,//vtmsg_nwk_diagnostic,     //0x1E    VTMSG_MSG_TYPE_NETWROK_DIAGNOSTIC
    vtmsg_get_system_log_table,        //0x1F
/* TYPE 0x2*- 0x4* G3 RELATED  */
    NULL,//vtmsg_get_networkInfo,    //0x20    VTMSG_MSG_TYPE_NETWORK_INFO
    NULL,//vtmsg_packet_cntr,        //0x21    VTMSG_MSG_TYPE_NETWORK_CNTR
    NULL,                   //0x22    VTMSG_MSG_TYPE_SAVE_NWK_STATE_REQ
    NULL,                   //0x23    VTMSG_MSG_TYPE_CONFIG_DC
    NULL,                   //0x24    VTMSG_MSG_TYPE_MAC_FRAME_COUNTER_UPDATE
    NULL,                   //0x25    VTMSG_MSG_TYPE_LOAD_NG_SEQ_NR_UPDATE
    NULL,                   //0x26
    NULL,                   //0x27
    NULL,                   //0x28
    NULL,                   //0x29
    NULL,                   //0x2A    VTMSG_MSG_TYPE_SHOW_ACTIVE_NODES
    NULL,                   //0x2B    VTMSG_MSG_TYPE_DETACH_NODE
    NULL,                   //0x2C    VTMSG_MSG_TYPE_REQUEST_ROUTE
    NULL,                   //0x2D    VTMSG_MSG_TYPE_REQUEST_PATH
    NULL,                   //0x2E    VTMSG_MSG_TYPE_ROUTE_DISCOVERY_RESULT
    NULL,                   //0x2F    VTMSG_MSG_TYPE_PATH_DISCOVERY_RESULT
    NULL,                   //0x30    VTMSG_MSG_TYPE_GET_ADP_IB
    NULL,                   //0x31    VTMSG_MSG_TYPE_SET_ADP_IB
    NULL,                   //0x32    VTMSG_MSG_TYPE_GET_MAC_IB
    NULL,                   //0x33    VTMSG_MSG_TYPE_SET_MAC_IB
    NULL,                   //0x34    VTMSG_MSG_TYPE_MAC_IB_ATTR
    NULL,                   //0x35
    NULL,                   //0x36
    NULL,                   //0x37
    NULL,                   //0x38
    NULL,                   //0x39
    NULL,                   //0x3A    VTMSG_MSG_TYPE_NETWORK_EVENT , outgoing only
    NULL,                   //0x3B    VTMSG_MSG_TYPE_ADD_TO_BLACK_LIST
    NULL,                   //0x3C    VTMSG_MSG_TYPE_REMOVE_FROM_BLACK_LIST
    NULL,                   //0x3D    VTMSG_MSG_TYPE_SHOW_BLACK_LIST
    NULL,//vtmsg_csma_settings,      //0x3E    VTMSG_MSG_TYPE_CSMA_SETTINGS
    NULL,                   //0x3F
    NULL,                   //0x40    VTMSG_MSG_TYPE_SHOW_COMP_CNTXT_TBL
    NULL,                   //0x41    VTMSG_MSG_TYPE_ADD_TO_COMP_CNTXT_TBL
    NULL,                   //0x42    VTMSG_MSG_TYPE_DEL_FROM_COMP_CNTXT_TBL
    NULL,//vtmsg_get_topology_table, //0x43    VTMSG_MSG_TYPE_GET_TOPOLOGY_TABLE
    NULL,                   //0x44    VTMSG_MSG_TYPE_START_SEARCH_RT_TABLE
    NULL,                   //0x45    VTMSG_MSG_TYPE_REKEY_NWK
    NULL,                   //0x46    VTMSG_MSG_TYPE_GET_REKEY_STS
    NULL,                   //0x47    VTMSG_MSG_TYPE_GET_PSK_FOR_EUI
    NULL,                   //0x48    VTMSG_MSG_TYPE_ABORT_REKEY_NWK
    NULL,                   //0x49    VTMSG_MSG_TYPE_UPDATE_PSK_FOR_EUI
    NULL,                   //0x4A    VTMSG_MSG_TYPE_SET_PHY_LEVEL
    NULL,                   //0x4B    VTMSG_MSG_TYPE_SET_PHY_TX_PARAMS
    NULL,                   //0x4C    VTMSG_MSG_TYPE_NETWORK_START_REQUEST
    NULL,//vtmsg_get_rt_table,       //0x4D    VTMSG_MSG_TYPE_GET_ROUTING_TABLE
    NULL,//vtmsg_get_nt_table,       //0x4E    VTMSG_MSG_TYPE_GET_NEIGHBOR_TABLE_MULTI
    NULL,                   //0x4F    VTMSG_MSG_TYPE_GET_RX_DEVICE_TABLE_MULT
};
#else
VTMSG_PROC vtmsg_app_table[80] =
{
/* TYPE 0x0*  SYSYEM CMD */
    vtmsg_configuration,        //0x00    VTMSG_MSG_TYPE_SYS_CONFIGURATION
    vtmsg_get_version_internal, //0x01   VTMSG_MSG_TYPE_GET_VERSION_INTERNAL
    NULL,                       //0x02    VTMSG_MSG_TYPE_GET_MEM_INFO
    vtmsg_get_lbs_info,         //0x03
    vtmsg_send_join_network,    //0x04
    vtmsg_app_exangedata,       //0x05
    NULL,                       //0x06
    vtmsg_mem_dw,             //0x07    VTMSG_MSG_TYPE_MEM_OP_WRITE_BYTE
    vtmsg_mem_ww,             //0x08    VTMSG_MSG_TYPE_MEM_OP_WRITE_WORD
    NULL,                   //0x09
    NULL,                   //0x0A
    vtmsg_reboot,             //0x0B
    NULL,                   //0x0C
    NULL,                   //0x0D
    NULL,                   //0x0E
    vtmsg_get_eni_eui_table,  //0x0F
/* TYPE 0x1*  SYSYEM CMD FOR CUSTOMER */
    vtmsg_start_network,      //0x10    VTMSG_MSG_TYPE_START_NETWORK
    vtmsg_get_version,        //0x11    VTMSG_MSG_TYPE_GET_VERSION_INFO
    vtmsg_get_joined_table,   //0x12    VTMSG_MSG_TYPE_GET_JOINED_TABLE
    NULL,                     //0x13
    vtmsg_get_zerox_hi_record, ///0x14   VTMSG_MSG_TYPE_GET_ZEROX_HIT_RECORD
    vtmsg_bbp_sig,            //0x15    VTMSG_MSG_TYPE_BBP_SIG
/* TYPE 0x16 - 0x1f VANGO */
    vtmsg_upg_control,        //0x16
    vtmsg_zerox_settings,     //0x17
    NULL,                   //0x18    vtmsg_upg_event,  outgoing only
    vtmsg_upg_data,           //0x19
    vtmsg_upg_operation,      //0x1A
    vtmsg_bbp_settings,       //0x1B    VTMSG_MSG_TYPE_BBP_SETTINGS
    vtmsg_app_settings,       //0x1C    VTMSG_MSG_TYPE_APP_SETTINGS
    vtmsg_bbp_raw_dump,       //0x1D    VTMSG_MSG_TYPE_BBP_RAW_DUMP
    vtmsg_nwk_diagnostic,     //0x1E    VTMSG_MSG_TYPE_NETWROK_DIAGNOSTIC
    vtmsg_get_system_log_table,        //0x1F    VTMSG_MSG_TYPE_GET_SYS_LOG
/* TYPE 0x2*- 0x4* G3 RELATED  */
    vtmsg_get_networkInfo,    //0x20    VTMSG_MSG_TYPE_NETWORK_INFO
    vtmsg_packet_cntr,        //0x21    VTMSG_MSG_TYPE_NETWORK_CNTR
    NULL,                   //0x22    VTMSG_MSG_TYPE_SAVE_NWK_STATE_REQ
    NULL,                   //0x23    VTMSG_MSG_TYPE_CONFIG_DC
    NULL,                   //0x24    VTMSG_MSG_TYPE_MAC_FRAME_COUNTER_UPDATE
    NULL,                   //0x25    VTMSG_MSG_TYPE_LOAD_NG_SEQ_NR_UPDATE
    NULL,                   //0x26
    NULL,                   //0x27
    NULL,                   //0x28
    NULL,                   //0x29
    NULL,                   //0x2A    VTMSG_MSG_TYPE_SHOW_ACTIVE_NODES
    NULL,                   //0x2B    VTMSG_MSG_TYPE_DETACH_NODE
    vtmsg_request_route,    //0x2C    VTMSG_MSG_TYPE_REQUEST_ROUTE
    vtmsg_request_path,     //0x2D    VTMSG_MSG_TYPE_REQUEST_PATH
    NULL,                   //0x2E    VTMSG_MSG_TYPE_ROUTE_DISCOVERY_RESULT
    NULL,                   //0x2F    VTMSG_MSG_TYPE_PATH_DISCOVERY_RESULT
    NULL,                   //0x30    VTMSG_MSG_TYPE_GET_ADP_IB
    NULL,                   //0x31    VTMSG_MSG_TYPE_SET_ADP_IB
    NULL,                   //0x32    VTMSG_MSG_TYPE_GET_MAC_IB
    NULL,                   //0x33    VTMSG_MSG_TYPE_SET_MAC_IB
    NULL,                   //0x34    VTMSG_MSG_TYPE_MAC_IB_ATTR
    NULL,                   //0x35    get pan id
    vtmsg_set_pan_id,       //0x36    set pan id
    NULL,                   //0x37
    NULL,                   //0x38
    NULL,                   //0x39
    NULL,                   //0x3A    VTMSG_MSG_TYPE_NETWORK_EVENT , outgoing only
    NULL,                   //0x3B    VTMSG_MSG_TYPE_ADD_TO_BLACK_LIST
    NULL,                   //0x3C    VTMSG_MSG_TYPE_REMOVE_FROM_BLACK_LIST
    NULL,                   //0x3D    VTMSG_MSG_TYPE_SHOW_BLACK_LIST
    vtmsg_csma_settings,      //0x3E    VTMSG_MSG_TYPE_CSMA_SETTINGS
    NULL,                   // 0x3F
    NULL,                   //0x40    VTMSG_MSG_TYPE_SHOW_COMP_CNTXT_TBL
    NULL,                   //0x41    VTMSG_MSG_TYPE_ADD_TO_COMP_CNTXT_TBL
    NULL,                   //0x42    VTMSG_MSG_TYPE_DEL_FROM_COMP_CNTXT_TBL
    vtmsg_get_topology_table, //0x43    VTMSG_MSG_TYPE_GET_TOPOLOGY_TABLE
    NULL,                   //0x44    VTMSG_MSG_TYPE_START_SEARCH_RT_TABLE
    NULL,                   //0x45    VTMSG_MSG_TYPE_REKEY_NWK
    NULL,                   //0x46    VTMSG_MSG_TYPE_GET_REKEY_STS
    NULL,                   //0x47    VTMSG_MSG_TYPE_GET_PSK_FOR_EUI
    vtmsg_psk_set,          //0x48    VTMSG_MSG_TYPE_GET_PSK_SET
    vtmsg_psk_set,          //0x49    VTMSG_MSG_TYPE_SET_PSK_SET
    vtmsg_mac_data_send,    //0x4A    VTMSG_MSG_TYPE_SEND_MAC_DATA
    NULL,                   //0x4B    VTMSG_MSG_TYPE_SET_PHY_TX_PARAMS
    NULL,                   //0x4C    VTMSG_MSG_TYPE_NETWORK_START_REQUEST
    vtmsg_get_rt_table,     //0x4D    VTMSG_MSG_TYPE_GET_ROUTING_TABLE
    vtmsg_get_nt_table,     //0x4E    VTMSG_MSG_TYPE_GET_NEIGHBOR_TABLE_MULTI
    NULL,                   //0x4F    VTMSG_MSG_TYPE_GET_RX_DEVICE_TABLE_MULT
};
#endif


extern void vtsdk_reset_sw_watch_dog(void);
void vtmsg_input_plc(uint8_t *pbuf, uint32_t len)
{
    //check msg hdr
    vtmsg_msgHdr_s *msgHdr = (vtmsg_msgHdr_s *)pbuf;
    uint32_t pyldLen;
    VTMSG_PROC pFunc;
#if (CONFIG_REMOTE_FW_UPGRADE)
    static uint8_t no_seq = 0; //workaround
#endif
    vtsdk_reset_sw_watch_dog();
    vtmsg_msg_flags = msgHdr->flags;
    vtmsg_msg_flags ^= 1 << VTMSG_MSG_HDR_FLAG_SOL_BIT; //change sol for sending

    vtmsg_msg_destination.final_dest_addr_len = DB->rx_pkt_mac_info.src.addr_len;
    if (vtmsg_msg_destination.final_dest_addr_len == 2) {
        lowpan6_addr_cpy(vtmsg_msg_destination.final_dest_addr, DB->rx_pkt_mac_info.src.addr, DB->rx_pkt_mac_info.src.addr_len);
    } else {
        memcpy(vtmsg_msg_destination.final_dest_addr, DB->rx_pkt_mac_info.src.addr, vtmsg_msg_destination.final_dest_addr_len);
    }
#if (CONFIG_REMOTE_FW_UPGRADE)
    //check VTUPG before length
    if (msgHdr->type == VTMSG_MSG_TYPE_VTUPG) {
        if (vtupg_cb.role == HOST) {
            if (pbuf[len - 1] == vtmsg_seq || no_seq == 1) {
                vtupg_pkt_input(pbuf);
            }
        } else {
            vtmsg_seq = pbuf[len - 1];//VTMSG_SEQ, same as request
            vtupg_pkt_input(pbuf);
        }
        return;
    }
#endif
    pyldLen = (msgHdr->lenHigh << 8) + (msgHdr->lenLow);

    //SDK_NPRINTF(pbuf, len);
    if (pyldLen != (len - sizeof(vtmsg_msgHdr_s))) {
        SDK_PRINTF(VTLL_SDK, "payload error!!"LF);
        return;
    }

    if (((msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_DIRECTION_BIT)) != 0) &&
        ((msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_SOL_BIT)) != 0)) {
        vtmsg_seq = pbuf[len - 1];//VTMSG_SEQ, same as request
        pbuf += sizeof(vtmsg_msgHdr_s);
        pFunc = vtmsg_app_table[msgHdr->type];
        if (pFunc != NULL && (msgHdr->type) < (sizeof(vtmsg_app_table) / sizeof(vtmsg_app_table[0]))) {
            pFunc(pbuf, pyldLen);
        }
    } else { //imcoming
        if (vtmsg_check_seq_exist(pbuf + sizeof(vtmsg_msgHdr_s), pyldLen)) {
            //for imcoming from plc, check seq the seq should the same as saved one
            if (pbuf[len - 1] == vtmsg_seq) {
                msgHdr->flags &= ~(1 << VTMSG_MSG_HDR_FLAG_DIRECTION_BIT);
                vtmsg_msg_send(pbuf, len);
            }
        } else {
#if (CONFIG_REMOTE_FW_UPGRADE)
            no_seq = 1;
#endif
            msgHdr->flags &= ~(1 << VTMSG_MSG_HDR_FLAG_DIRECTION_BIT);
            vtmsg_msg_send(pbuf, len);
        }
    }
}

void vtmsg_input_serial(uint8_t *pbuf, uint32_t len)
{
    //check msg hdr
    vtmsg_msgHdr_s *msgHdr = (vtmsg_msgHdr_s *)pbuf;

    vtmsg_msg_flags = msgHdr->flags;

    //VPRINTF(VTLL_USER, "%s %02x"LF, __func__, vtmsg_msg_flags);

    vtmsg_seq++;//VTMSG_SEQ, incremental

    if ((msgHdr->flags & (1 << VTMSG_MSG_HDR_FLAG_DIRECTION_BIT)) != 0) {
        msgHdr = (vtmsg_msgHdr_s *)(pbuf + VTMSG_ATTR_MSG_OUTGOING_LEN);
        vtmsg_msg_destination.final_dest_addr_len = pbuf[sizeof(vtmsg_msgHdr_s)];
        memcpy(vtmsg_msg_destination.final_dest_addr, &pbuf[sizeof(vtmsg_msgHdr_s) + 1], vtmsg_msg_destination.final_dest_addr_len);
        if ((msgHdr->type) != VTMSG_MSG_TYPE_SEND_MAC_DATA) {
            vtmsg_msg_send(pbuf + VTMSG_ATTR_MSG_OUTGOING_LEN, len - VTMSG_ATTR_MSG_OUTGOING_LEN);
        } else {
            vtmsg_mac_data_send(pbuf + VTMSG_ATTR_MSG_OUTGOING_LEN + sizeof(vtmsg_msgHdr_s), len - VTMSG_ATTR_MSG_OUTGOING_LEN - sizeof(vtmsg_msgHdr_s));
        }
    } else { //imcoming
        uint32_t pyldLen;
        VTMSG_PROC pFunc;

#if (CONFIG_REMOTE_FW_UPGRADE)
        if ((msgHdr->type) == VTMSG_MSG_TYPE_VTUPG) {
            vtupg_pkt_input(pbuf);
            return;
        }
#endif

        //SDK_NPRINTF(pbuf, len);

        pyldLen = (msgHdr->lenHigh << 8) + (msgHdr->lenLow);
        if (pyldLen != (len - sizeof(vtmsg_msgHdr_s))) {
            SDK_PRINTF(VTLL_SDK, "payload error!!"LF);
            return;
        }

        pFunc = vtmsg_app_table[msgHdr->type];
        if (pFunc != NULL && (msgHdr->type) < (sizeof(vtmsg_app_table) / sizeof(vtmsg_app_table[1]))) {
            pbuf += sizeof(vtmsg_msgHdr_s);
            pFunc(pbuf, pyldLen);
        } else {
            vtmsg_status_only(VTMSG_STS_NOT_SUPPORTED, msgHdr->type);
        }
    }
}
#endif //(CONFIG_VTMSG)
