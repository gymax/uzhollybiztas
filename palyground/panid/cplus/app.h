#ifndef __APP_H__
#define __APP_H__

#define MAX_ROUTING_TABLE_SIZE 1024
#define MAX_NEIGHBOR_EUI_TABLE_SIZE 1024

int32_t config_upgConfig(int32_t);
int32_t set_target_dest(int32_t);
int32_t get_networkInfo(int32_t);
int32_t get_joined_table(int32_t);
int32_t lbslist(int32_t);
int32_t joinpan(int32_t);
int32_t get_version(int32_t);
int32_t get_version_internal(int32_t);
int32_t do_raw_vtmsg_type(int32_t argCnt);
void* app_main(void*);
int32_t start_network(int32_t argCnt);
int32_t get_log(int32_t argCnt);
int32_t get_routingTable(int32_t argCnt);
int32_t get_attached(int32_t argCnt);
int32_t route_request(int32_t argCnt);
int32_t config_psk(int32_t);
int32_t get_runtime_sys_log(int32_t argCnt);
int32_t check_network_event(uint8_t *, uint32_t);
int32_t host_ping(int32_t argCnt);
int32_t get_tlg_from_dcu(int32_t argCnt);
int32_t get_tlg_from_lbd(int32_t argCnt);
int32_t get_routingTable_fromDCU(int32_t argCnt);
int32_t field_analysis_tool_operation(int32_t argCnt);
int32_t sched_setting(int32_t argCnt);
int32_t sched_job(int32_t argCnt);
int32_t nevent_ack(int32_t argCnt);
int32_t save_topology(int32_t argCnt);
int32_t bbp_nv_config(int32_t argCnt);
int32_t target_reboot(int32_t argCnt);
int32_t trytry(int32_t argCnt);
int32_t neighbor_eui(int32_t argCnt);
int32_t vtcli_remoteupg(int32_t argCnt);
int32_t set_dbg_print(int32_t argCnt);

int32_t set_pan_id(int32_t argCnt);


#endif
