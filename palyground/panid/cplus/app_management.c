#include "app_top.h"
#include "app_topology.h"

uint32_t reply_sig = 0;

struct {
    char str[32];
} bbpnv_param[] = {
    { "VGFREQINITTONE"},
    { "VGFREQTTLTONE"},
    { "VGFREQTONEPERGRP"},
    { "MODULATION"},
    { "TDPFIR3SCALEMAX"},
    { "TDPFIR3SCALEMIN"},
    { "TDPFIR3_ADDR3DB_THR"},
    { "TDPFIR3_START_SCALE"},
    { "ACK_TX_ADDITIONAL_GAIN"},
    { "ENPGAMODE"},
    { "URGENTIN_EN"},
    { "PRMB_SYNCP_SYM_NUM"},
    { "PRMB_MATCH_SYM"},
    { "PRMB_DET_SCALE_FCC"},
    { "PRMB_DET_SCALE_CEN_A"},
    { "PRMB_DET_SCALE_VANGO"},
    { "PRMB_DET_NOISE_THR"},
    { "APDT_EN"}
};

struct {
    char str[32];
} csma_param[] = {
    { "macMaxBE" },
    { "macMinBE" },
    { "macA" },
    { "macK" },
    { "macMinCWAttempts" },
    { "macMaxCSMABackoffs" },
    { "macCSMAFairnessLimit" },
    { "macHPWindowSize" },
    { "NB" },
    { "NBF" },
    { "CW" },
    { "minCWCount" },
    { "QualtiyOfService" },
    { "aSlotTime" },
};

struct {
    char str[32];
} appnv_param[] = {
    { "lbs_keepalive_duration" },
    { "self_join_time" },
    { "self_join_check_time" },
    { "sdk_dup_send_cnt" },
    { "announce_heartbeat_time" },
};

struct {
    char str[32];
    char help[16];
} zerox_param[] = {
    { "enable", "<0/1>" },
    { "gpio_in", "<0~31>" },
    { "gpio_out", "<0~31>" },
    { "degree", "<1~99%>" },
    { "check_no_ac", "<0/1>" },
    { "bbp_proc_delay", "<0~5000 us>" },
    { "window", "<-7~7>" },
    { "vcs_busy", "" },
    { "csma_fail", "" },
    { "reply_ack", "" },
    { "reply_nack", "" },
    { "no_reply", "" },
    { "p_csma_success_to", "" },
    { "p_ack_to", "" },
    { "p_tx_cmp_to_from_ackto", "" },
    { "p_tx_cmp_to_from_csma", "" },
    { "p_tx_cmp_to_from_ack", "" },
    { "p_tx_cmp_to", "" },
};

enum NETWORK_PACKET_CNTR_TABLE {
    tx_submitted = 0,
    tx,
    rx,
    rx_ctrl_normal_rx,
    rx_ctrl_bypass_rx,
    rx_ctrl_raw_rx,
    rx_fcs_ok,
    rx_fcs_fail,
    rx_hcs_ok,
    rx_hcs_fail,
    rx_mic_ok,
    rx_mic_fail,
    rx_ctrl_ack,
    rx_ctrl_nack,
    detect_pmb,
    rx_cmp,
    urgent_in,
    tx_cmp,
    tx_ack,
    tx_nack,
    tx_ack_timeout,
    tx_ack_timeout_ignore,
    tx_csma_success,
    tx_csma_fail,
    tx_retry_fail,
    tx_sw_ack_timeout,
    tx_sw_tx_cmp_timeout,
    lowpan6_rx,
    lowpan6_rx_esc_route,
    lowpan6_rx_esc_bootstrap,
    lowpan6_rx_esc_vango,
    lowpan6_rx_drop,
    lowpan6_rx_drop_multicast,
    lowpan6_rx_forward_unicast,
    lowpan6_rx_forward_unicast_fail,
    lowpan6_rx_forward_broadcast,
    icmp_rx_echo_request,
    icmp_rx_echo_reply,
    icmp_tx_echo_request,
    icmp_tx_echo_reply,
    sdk_uart_tx_packet,
    sdk_uart_rx_packet,
    sdk_plc_tx_packet,
    sdk_plc_rx_packet,
    _COUNT_NETWORK_PACKET_CNTR_TABLE,
};



#if 0 
int32_t check_dcu_support(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_DCU_SUPPORT, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_DCU_SUPPORT) {
        int32_t tlvLen0;
        uint8_t *pBuff0;
        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_DCU_SUPPROT_FLAGS, &tlvLen0, &pBuff0)) {
            uint8_t i, count = 1, mapgie_found = 0;
            printf("DCU\nList available MAGPIE under DCU:\n");
            for (i = 0; i < DCU_IF_TOTAL; i++) {
                if (pBuff0[0] & 1 << i) {
                    printf("%d.  MAGPIE [%d]\n", count, i);
                    mapgie_found++;
                    count++;
                }
            }

            if (mapgie_found > 0) {
                char lineBuff[16];
                uint8_t idx;
                //printf("Please select index of MAGPIE: (0-2)\nEnter 3 or others is DCU mode\n");
                //fgets((char *)lineBuff, 16, stdin);
                //sscanf(lineBuff, "%u", &idx);
                idx = 0;
                if (idx >= 0 && idx < 3) {
                    printf("MAGPIE [%u] selected!\n", idx);
                    g_vtmsg_msg_devid = idx;
                } else {
                    printf("DCU mode!\n", idx);
                    g_vtmsg_msg_devid = 3;
                    g_cmd_level = COMMAND_DCU;
                }
                g_vtmsg_msg_source = VTMSG_IF_SPI;
            } else {
                printf("\tnot found\n");
            }
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}
#endif

int32_t set_target_dest(int32_t argCnt)
{
    if (argCnt == 0) {
        printf("Current vtmsg destination is ");

        if (!DB->ctrl.msg_outgoing) 
            printf("local");
        else if (DB->dest_addr.len == 2)
            printf("0x%02x%02x\n", DB->dest_addr.addr[0], DB->dest_addr.addr[1]);
        else if (DB->dest_addr.len == 8)
            printf("%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
                    DB->dest_addr.addr[7], DB->dest_addr.addr[6],
                    DB->dest_addr.addr[5], DB->dest_addr.addr[4],
                    DB->dest_addr.addr[3], DB->dest_addr.addr[2],
                    DB->dest_addr.addr[1], DB->dest_addr.addr[0]);

        printf("\nUsage: target [short addr 0x??]/\"local\"\n");
        return RTN_CODE_OK;
    } else if (argCnt == 1) {
        if (strstr((char *)cli_argv[1], "local") || strcmp((char *)cli_argv[1], "0") == 0) {
            vtmsg_dest_setting(NULL, NULL, FALSE);
            printf("\nChange to local\n");
        } else if (cli_argv[1][1]== 'x' || cli_argv[1][1]== 'X') {
            uint32_t input;
            uint8_t addr[2];
            uint8_t len;
            len = 2;
            sscanf((char *)&cli_argv[1][2], "%x", &input);
            addr[0] = (uint8_t)((input & 0x0000ff00) >> 8);
            addr[1] = (uint8_t)input & 0x000000ff;
            vtmsg_dest_setting(addr, &len, TRUE);
        } else if (strlen(cli_argv[1]) == 16) {
            uint8_t addr[8];
            uint8_t len;
            len = 8;
            sscanf(cli_argv[1], "%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx",
                    (uint8_t *)&addr[7], (uint8_t *)&addr[6],
                    (uint8_t *)&addr[5], (uint8_t *)&addr[4],
                    (uint8_t *)&addr[3], (uint8_t *)&addr[2],
                    (uint8_t *)&addr[1], (uint8_t *)&addr[0]);
            vtmsg_dest_setting(addr, &len, TRUE);
        }
    }
    return RTN_CODE_OK;
}


void add_payload_to_json(json_t* obj, const char* name, const uint8_t* marr, uint32_t dim)
{
    uint32_t i;
    json_t* jarr = json_array();

    for( i=0; i<dim; ++i ) {
        json_t* jval = json_integer(marr[i]);
        json_array_append_new( jarr, jval );
    }
    json_object_set_new(obj, name, jarr);
    return;
}


struct {
    char desc[32];
} bbpnv_web[] = {
    { "readonly" },//{ "VGFREQINITTONE"},
    { "readonly" },//{ "VGFREQTTLTONE"},
    { "readonly" },//{ "VGFREQTONEPERGRP"},
    { "readonly" },//{ "MODULATION"},
    { "select;68.134." },//{ "TDPFIR3SCALEMAX"},
    { "readonly" },//{ "TDPFIR3SCALEMIN"},
    { "readonly" },//{ "TDPFIR3_ADDR3DB_THR"},
    { "select;68.134." },//{ "TDPFIR3_START_SCALE"},
    { "readonly" },//{ "ACK_TX_ADDITIONAL_GAIN"},
    { "readonly" },//{ "ENPGAMODE"},
    { "readonly" },//{ "URGENTIN_EN"},
    { "readonly" },//{ "PRMB_SYNCP_SYM_NUM"},
    { "readonly" },//{ "PRMB_MATCH_SYM"},
    { "readonly" },//{ "PRMB_DET_SCALE_FCC"},
    { "readonly" },//{ "PRMB_DET_SCALE_CEN_A"},
    { "readonly" },//{ "PRMB_DET_SCALE_VANGO"},
    { "readonly" },//{ "PRMB_DET_NOISE_THR"},
    { "readonly" }//{ "APDT_EN"}
};


int32_t bbp_nv_config(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                    + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    int32_t rtn, value, i;
    uint32_t para = sizeof(bbpnv_param) / sizeof(bbpnv_param[0]) + 1;
    json_t* jarr = json_array();

    for (i = 0; i < sizeof(bbpnv_param) / sizeof(bbpnv_param[0]); i++) {
        if (argCnt == 2 && strcmp(cli_argv[1], bbpnv_param[i].str) == 0) {
            sscanf(cli_argv[2], "%d", &value);
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_BBP_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                                + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = para = i;
            pBuffCurr++;
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE,
                                            VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);

            memcpy(pBuffCurr, &value, 4);

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(buffReq));
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else if (argCnt == 0) {
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_BBP_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = i;

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);

            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else {
            continue;
        }

        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_BBP_SETTINGS) {
            pBuffCurr = pBuff;
            int32_t tlvLen0;
            uint8_t *pBuff0;
            if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pBuff0)) {
                uint8_t idx = pBuff0[0];
                if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pBuff0)) {
                    json_t* jentry = json_object();
                    memcpy(&value, pBuff0, 4);
                    printf("[%02d] %s: %d\n", idx, bbpnv_param[idx].str, value);
                    json_object_set_new(jentry, "type", json_sprintf("%s", bbpnv_param[idx].str));
                    json_object_set_new(jentry, "value", json_sprintf("%d", value));
                    json_object_set_new(jentry, "desc", json_sprintf("%s", bbpnv_web[idx].desc));
                    json_array_append(jarr, jentry);
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                    break;
                }
            } else {
                rtn = RTN_CODE_TLV_MISSING;
                break;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
        free_dev_resp(pBuff);
    }

    if (argCnt == 2 && para > sizeof(bbpnv_param) / sizeof(bbpnv_param[0])) {
        printf("BBP_NV para not found!!\n");
        return RTN_CODE_OK;
    }

    if (rtn == RTN_CODE_OK) {
        json_object_set_new(HTTP_JSON_OUTPUT, "bbp_nv", jarr);
    }

    return rtn;
}

int32_t target_reboot(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_REBOOT_REMAP_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint32_t remap;
    int32_t rtn;

    if (argCnt == 0) {
        remap = 2;
    } else if (argCnt == 1) {
        sscanf(cli_argv[1], "%u", &remap);
    } else {
        printf("reboot [0-2] (remap)\n");
        return RTN_CODE_OK;
    }
    printf("rebooting... \n");

    vtmsg_build_msghdr((uint8_t *)&buffReq, VTMSG_MSG_TYPE_REBOOT, VTMSG_ATTR_REBOOT_REMAP_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_REBOOT_REMAP,
                                    VTMSG_ATTR_REBOOT_REMAP_LEN);


    pBuffCurr[0] = (uint8_t)remap;

    rtn = dev_send((uint8_t *)&buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_REBOOT_REMAP_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    return rtn;
}

void _ver_time(uint32_t version_time)
{
    uint16_t year;
    uint8_t month, day, hour, minute, sec;

    year = 2000 + ((version_time >> 26) & 0x3F);
    month = (version_time) >> 22 & 0xF;
    day = (version_time) >> 17 & 0x1F;
    hour = (version_time) >> 12 & 0x1F;
    minute = (version_time) >> 6 & 0x3F;
    sec = version_time & 0x3F;

    printf("%04d/%02d/%02d %02d:%02d:%02d",
           year, month, day, hour, minute, sec);
    json_object_set_new(HTTP_JSON_OUTPUT, "build_time", 
                        json_sprintf("%04d/%02d/%02d %02d:%02d:%02d", year, month, day, hour, minute, sec));
}

int32_t get_version(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_GET_VERSION_INFO, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_GET_VERSION_INFO);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_VERSION_INFO) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_VERSION_INFO, &tlvLen0, &pBuff0)) {
            printf("\nMagpie PLC Platform - Copyright (c) 2015 Vango Technologies, Inc.\n");
            printf("\nversion: v6.%d.%d.%d_%d.%d.%d   ", pBuff0[0],
                                                         pBuff0[1],
                                                         pBuff0[2],
                                                         pBuff0[3],
                                                         pBuff0[4],
                                                         pBuff0[5]);
            _ver_time(*(uint32_t *)(pBuff0 + 6));
            /*json output*/
            json_object_set_new(HTTP_JSON_OUTPUT, "version", 
                                json_sprintf("v6.%d.%d.%d_%d.%d.%d", pBuff0[0],
                                                                     pBuff0[1],
                                                                     pBuff0[2],
                                                                     pBuff0[3],
                                                                     pBuff0[4],
                                                                     pBuff0[5]));
            add_payload_to_json(HTTP_JSON_OUTPUT, "raw", pBuff0, 10);
            printf("\n");
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}

int32_t get_version_internal(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_GET_VERSION_INTERNAL, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_VERSION_INTERNAL) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_VERSION_INTERNAL, &tlvLen0, &pBuff0)) {
            uint32_t version_bbp_flag;
            printf("\nMagpie PLC Platform - Copyright (c) 2015 Vango Technologies, Inc.\n");
            printf("\nSW version: v6.%d.%d.%d @%08x\n   ", pBuff0[0],
                    pBuff0[1],
                    pBuff0[2],
                    *(uint32_t *)(pBuff0 + 3));
            _ver_time(*(uint32_t *)(pBuff0 + 7));
            printf("%s", pBuff0[11] & 1 ? "- tainted\n" : "\n");
            printf("\nFW version: v6.%d.%d.%d @%08x - %08x\n   ", pBuff0[14],
                    pBuff0[13],
                    pBuff0[12],
                    *(uint32_t *)(pBuff0 + 15),
                    *(uint32_t *)(pBuff0 + 19));
            _ver_time(*(uint32_t *)(pBuff0 + 23));
            printf("\n\n");
            version_bbp_flag = *(uint32_t *)(pBuff0 + 19);
            printf("Chip ver %s%s\n",
                    ((version_bbp_flag & 0xf) == 1) ? "A1" : "",
                    ((version_bbp_flag & 0xf) == 2) ? "A2" : "");
            printf("LD %s%s%s%s\n",
                    ((version_bbp_flag & 0xf0) == 0x00) ? "TestChip B" : "",
                    ((version_bbp_flag & 0xf0) == 0x10) ? "MP A01" : "",
                    ((version_bbp_flag & 0xf0) == 0x20) ? "MP A02" : "",
                    ((version_bbp_flag & 0xf0) == 0x30) ? "MP B01" : "");
            printf("Band support: %s%s%s\n",
                    (version_bbp_flag & 0x100) ? "CEN-A " : "",
                    (version_bbp_flag & 0x200) ? "FCC " : "",
                    (version_bbp_flag & 0x400) ? "Vango " : "");
            printf("Others: %s%s\n",
                    (version_bbp_flag & 0x1000) ? "Data coherent mode, " : "",
                    (version_bbp_flag & 0x2000) ? "Two RS blocks, " : "");
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}

int32_t get_networkInfo(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn = RTN_CODE_OK;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_NETWORK_INFO, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_NETWORK_INFO);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_NETWORK_INFO) {
        int32_t tlvLen0;
        uint8_t *pBuff0;


        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BANDPLAN, &tlvLen0, &pBuff0)) {
            printf("\n\nBandplan   : ");
            if (pBuff0[0] == VTMAC_BANDPLAN_G3_CENELEC) {
                printf("CEN-A\n");
                json_object_set_new(HTTP_JSON_OUTPUT, "bandplan", json_sprintf("CEN-A"));
            } else {
                printf("%s", pBuff0[1] == 1 ? "VANGO" : "FCC\n");
                json_object_set_new(HTTP_JSON_OUTPUT, "bandplan", json_sprintf("%s", pBuff0[1] == 1 ? "VANGO" : "FCC"));
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }


        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_EUI64_ADDR, &tlvLen0, &pBuff0)) {
            printf("EUI-64      : ");
            printf("%02x%02x:%02x%02x:%02x%02x:%02x%02x\n",
                   pBuff0[7], pBuff0[6],
                   pBuff0[5], pBuff0[4],
                   pBuff0[3], pBuff0[2],
                   pBuff0[1], pBuff0[0]);
            json_object_set_new(HTTP_JSON_OUTPUT, "eui64", json_sprintf("%02x%02x:%02x%02x:%02x%02x:%02x%02x\n",
                   pBuff0[7], pBuff0[6],
                   pBuff0[5], pBuff0[4],
                   pBuff0[3], pBuff0[2],
                   pBuff0[1], pBuff0[0]));
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }


        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_DEVICE_TYPE, &tlvLen0, &pBuff0)) {
            printf("Device      : %s (%s)  ",
                   pBuff0[0] == LOWPAN6_BOOTSTRAPPING_SERVER ? "LBS" : "LBD",
                   pBuff0[1] == REDUCED_FUNCTION_DEVICE ? "RFD" : "FFD");
            json_object_set_new(HTTP_JSON_OUTPUT, "device", json_sprintf("%s(%s)",
                   pBuff0[0] == LOWPAN6_BOOTSTRAPPING_SERVER ? "LBS" : "LBD",
                   pBuff0[1] == REDUCED_FUNCTION_DEVICE ? "RFD" : "FFD"));

            DB->ctrl.role = pBuff0[0];
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_SYS_UPTIME, &tlvLen0, &pBuff0)) {
            printf("(uptime %d sec)\n", *((uint32_t *)pBuff0));
            json_object_set_new(HTTP_JSON_OUTPUT, "uptime", json_sprintf("%d", *((uint32_t *)pBuff0)));
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_PAN_JOIN, &tlvLen0, &pBuff0)) {
            if (tlvLen0 == 0) {
                printf("Not yet joined a pan\n");
                json_object_set_new(HTTP_JSON_OUTPUT, "panid", json_sprintf("null"));
                json_object_set_new(HTTP_JSON_OUTPUT, "short_addr", json_sprintf("null"));
            } else if (tlvLen0 == VTMSG_ATTR_PAN_JOINED_LEN) {
                int32_t tlvLen1;
                uint8_t *pBuff1;

                printf("Already joined a pan\n");

                if (vtmsg_get_tlv(pBuff0, tlvLen0, VTMSG_TLV_TYPE_PAN_ID, &tlvLen1, &pBuff1)) {
                    printf("PAN ID      :  0x%02x%02x\n", pBuff1[1], pBuff1[0]);
                    json_object_set_new(HTTP_JSON_OUTPUT, "panid", json_sprintf("0x%02x%02x", pBuff1[1], pBuff1[0]));
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                }


                if (vtmsg_get_tlv(pBuff0, tlvLen0, VTMSG_TLV_TYPE_SHORT_ADDR, &tlvLen1, &pBuff1)) {
                    printf("Short Addr  :  0x%02x%02x\n", pBuff1[0], pBuff1[1]);
                    json_object_set_new(HTTP_JSON_OUTPUT, "short_addr", json_sprintf("0x%02x%02x", pBuff1[1], pBuff1[0]));
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                }


                if (vtmsg_get_tlv(pBuff0, tlvLen0, VTMSG_TLV_TYPE_IPV6_ADDR, &tlvLen1, &pBuff1)) {
                    uint32_t *addr = NULL;
                    addr = (uint32_t *)pBuff1;

                    printf("IPv6 addr   :  %08lx %08lx %08lx %08lx\n", PP_HTONL(addr[0]), PP_HTONL(addr[1]),
                           PP_HTONL(addr[2]), PP_HTONL(addr[3]));
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                }
            } else {
                rtn = RTN_CODE_GENERAL_FAILURE;
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}

int32_t get_joined_entry_print(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
    int32_t tlvLen0;
    uint8_t *pBuff0;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        memcpy(pEntryIdx, pBuff0, sizeof(uint16_t));
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_S2E_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            struct lbs_alloc_short_addr_table entry;
            int j;
            memset(&entry, 0, sizeof(struct lbs_alloc_short_addr_table));
            memcpy(&entry, pBuff0, 14);//FIXME s2e table length issue
            printf("[%02d]", *pEntryIdx);
            if (entry.valid == 1) {
                int i;
                printf("0x%02x%02x    ", entry.alloc_short_addr[0], entry.alloc_short_addr[1]);
                for (j = 0; j < 8; j++)
                    printf("0x%02x ", entry.eui_64_bit_addr[j]);

                printf("\n");
                for (i = 0; i < MAX_ROUTING_TABLE_SIZE; i++) {
                    if (DB->joined_dev_table[i].valid != 1) {
                        memcpy(&DB->joined_dev_table[i], &entry, sizeof(struct lbs_alloc_short_addr_table));
                        DB->joined_dev_table[i].valid = 1;
                        break;
                    }
                }
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }

    return rtn;
}


int32_t get_joined_table_entry(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_JOINED_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
            VTMSG_TLV_TYPE_CMD_INDEX,
            VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_GET_JOINED_TABLE);

    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        printf("LBS Short Address to EUI64 address Table \n");
        printf("=====================================================\n");
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_JOINED_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_joined_entry_print(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            if(vtmsg_tlv_sts_handler(pBuff, pyldLen)  == VTMSG_STS_NOT_SUPPORTED)
                rtn = RTN_CODE_NOT_SUPPORT;
            else
                rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t get_joined_table(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;
    memset(DB->joined_dev_table, 0, sizeof(DB->joined_dev_table));
    json_t* jarr = json_array();
   

    do {
        rtn = get_joined_table_entry(&entryIdx);
                
        if(rtn == 0 && entryIdx < 0xFFFF && entryIdx < MAX_ROUTING_TABLE_SIZE)
        {
            json_t* jentry = json_object();
            json_object_set_new(jentry, "short_addr", json_sprintf("0x%02x%02x", DB->joined_dev_table[entryIdx].alloc_short_addr[0], DB->joined_dev_table[entryIdx].alloc_short_addr[1]));
            json_object_set_new(jentry, "eui64", json_sprintf("%02x%02x%02x%02x%02x%02x%02x%02x", DB->joined_dev_table[entryIdx].eui_64_bit_addr[0], DB->joined_dev_table[entryIdx].eui_64_bit_addr[1], DB->joined_dev_table[entryIdx].eui_64_bit_addr[2], DB->joined_dev_table[entryIdx].eui_64_bit_addr[3], DB->joined_dev_table[entryIdx].eui_64_bit_addr[4], DB->joined_dev_table[entryIdx].eui_64_bit_addr[5], DB->joined_dev_table[entryIdx].eui_64_bit_addr[6], DB->joined_dev_table[entryIdx].eui_64_bit_addr[7]));
            json_array_append(jarr, jentry);         
        }

        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
        
    } while (rtn == 0 && entryIdx < 0xFFFF && entryIdx < MAX_ROUTING_TABLE_SIZE);

    if(rtn == 0)
         json_object_set_new(HTTP_JSON_OUTPUT, "s2e_table", jarr);
    return rtn;
}

           
           
int32_t get_routingTable_entry_print(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
    int32_t tlvLen0;
    uint8_t *pBuff0;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        memcpy(pEntryIdx, pBuff0, sizeof(uint16_t));
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_ROUTING_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            uint32_t i,j;
            ROUTE_TABLE_ST entry;
            memcpy(&entry, pBuff0, tlvLen0);
            /*printf("[%03d](%05dm%02ds):  0x%02X%02X   0x%02X%02X     0x%04X       %2d            %2d\n",
             *       *pEntryIdx, entry.valid_time, entry.valid_sec,
             *       entry.dest_short_addr[0], entry.dest_short_addr[1],
             *       entry.next_hop_short_addr[0], entry.next_hop_short_addr[1],
             *       entry.route_cost, entry.hop_count, entry.weak_link_count);
             */
             
            /*maintain Routing Table*/
            for (i = 0; i < MAX_ROUTING_TABLE_SIZE; i++) {
                if (DB->joined_dev_table[i].valid != 0 && memcmp(DB->joined_dev_table[i].alloc_short_addr ,entry.dest_short_addr, 2) == 0) {
                    memcpy(&DB->joined_dev_table[i].route_info, &entry, sizeof(ROUTE_TABLE_ST));
		
					printf("[%03d](%05dm%02ds):  0x%02X%02X   0x%02X%02X  0x%04X  %2d   %2d      ",
							*pEntryIdx, entry.valid_time, entry.valid_sec,
							entry.dest_short_addr[0], entry.dest_short_addr[1],
							entry.next_hop_short_addr[0], entry.next_hop_short_addr[1],
							entry.route_cost, entry.hop_count, entry.weak_link_count);
					for (j = 0; j < 8; j++)
						printf("%02x", DB->joined_dev_table[i].eui_64_bit_addr[j]);
					printf("\n");
                    break;
                }
            }

        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }

    return rtn;
}

int32_t get_routingTable_entry(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_ROUTING_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_GET_ROUTING_TABLE);

    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        //printf("Routing Table : [000]-[099]\n");
        //printf("                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count\n");
        printf("Routing Table : [000]-[999]\n");
		printf("                    Addr   Next_Hop  Cost   Hop  Weak        EUI64\n");

    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_ROUTING_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_routingTable_entry_print(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}

int32_t get_routingTable(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;

    do {
        rtn = get_routingTable_entry(&entryIdx);
        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
    } while (rtn == 0 && entryIdx < 0xFFFF);
    return rtn;
}

int32_t get_attached(int32_t argCnt)
{
    uint32_t i;//, j;
    json_t* jarr = json_array();
    get_joined_table(0);
    get_routingTable(0);
    printf("Routing Table : [000]-[999]\n");
    printf("                    Addr   Next_Hop  Cost   Hop  Weak        EUI64\n");


    for (i = 0; i < MAX_ROUTING_TABLE_SIZE; i++) {
        if (DB->joined_dev_table[i].valid != 0) {
            /*printf("[%03d](%05dm%02ds):  0x%02X%02X   0x%02X%02X  0x%04X  %2d   %2d      ",
             *       i, DB->joined_dev_table[i].route_info.valid_time, DB->joined_dev_table[i].route_info.valid_sec,
             *       DB->joined_dev_table[i].alloc_short_addr[0], DB->joined_dev_table[i].alloc_short_addr[1],
             *       DB->joined_dev_table[i].route_info.next_hop_short_addr[0], DB->joined_dev_table[i].route_info.next_hop_short_addr[1],
             *       DB->joined_dev_table[i].route_info.route_cost, 
             *       DB->joined_dev_table[i].route_info.hop_count, 
             *       DB->joined_dev_table[i].route_info.weak_link_count);
			 *
             *for (j = 0; j < 8; j++)
             *    printf("%02x", DB->joined_dev_table[i].eui_64_bit_addr[j]);
			 *
             *printf("\n");
			 */
            json_t* jentry = json_object();
            json_object_set_new(jentry, "short_addr", json_sprintf("0x%02x%02x", DB->joined_dev_table[i].alloc_short_addr[0], DB->joined_dev_table[i].alloc_short_addr[1]));
            json_object_set_new(jentry, "eui64", json_sprintf("%02x%02x%02x%02x%02x%02x%02x%02x", DB->joined_dev_table[i].eui_64_bit_addr[0], DB->joined_dev_table[i].eui_64_bit_addr[1], DB->joined_dev_table[i].eui_64_bit_addr[2], DB->joined_dev_table[i].eui_64_bit_addr[3], DB->joined_dev_table[i].eui_64_bit_addr[4], DB->joined_dev_table[i].eui_64_bit_addr[5], DB->joined_dev_table[i].eui_64_bit_addr[6], DB->joined_dev_table[i].eui_64_bit_addr[7]));
            json_object_set_new(jentry, "next_hop", json_sprintf("0x%02x%02x", DB->joined_dev_table[i].route_info.next_hop_short_addr[0], DB->joined_dev_table[i].route_info.next_hop_short_addr[1]));
            json_object_set_new(jentry, "route_cost", json_sprintf("0x%04x", DB->joined_dev_table[i].route_info.route_cost));
            json_object_set_new(jentry, "hop_count", json_sprintf("%d", DB->joined_dev_table[i].route_info.hop_count));
            json_object_set_new(jentry, "weak_link", json_sprintf("%d", DB->joined_dev_table[i].route_info.weak_link_count));
            json_object_set_new(jentry, "route_time", json_sprintf("%d", (DB->joined_dev_table[i].route_info.valid_time*60 + DB->joined_dev_table[i].route_info.valid_sec)));
            json_array_append(jarr, jentry);
        }
    }
    
    json_object_set_new(HTTP_JSON_OUTPUT, "att_list", jarr);
    return 0;
}

enum settings_configuration {
    BOOTSTRAP_ONOFF = (1 << 0),
};
int32_t do_configuration(int32_t argCnt)
{
    int32_t rtn = 0, para1 = 0, current_argCnt = 1;
    uint32_t settings = 0;
    if (argCnt >= 2) {
        while (current_argCnt <= argCnt) {
            char *cmd = (char *)cli_argv[current_argCnt];
            if (strncmp(cmd, "bsoff", 2) == 0) {
                settings |= BOOTSTRAP_ONOFF;
                current_argCnt++;
                if (current_argCnt <= argCnt) sscanf((char *)cli_argv[current_argCnt], "%u", &para1);
            }
            current_argCnt++;
        }
    } else {
        printf("device configuration\n");
        printf("Usage: config [action] [para...]\n");
        printf("\tAction:\n");
        printf("\tbsoff [1/0], disable/enable bootstrapping\n");
        return RTN_CODE_OK;
    }

    if (settings & BOOTSTRAP_ONOFF) {
        uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN] = {};
        uint8_t msgType, hdrFlags;
        uint8_t *pBuffCurr = NULL;
        uint8_t *pBuff = NULL;
        int32_t pyldLen = 0;

        pBuffCurr = vtmsg_build_msghdr(buffReq,
                VTMSG_MSG_TYPE_SYS_CONFIGURATION,
                VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                VTMSG_TLV_TYPE_BOOTSTRAP_ONOFF,
                VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = para1;

        rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN);

        if (rtn < 0) {
            rtn = RTN_CODE_MESSAGE_SEND_FAILED;
            return rtn;
        }

        rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_SYS_CONFIGURATION);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_SYS_CONFIGURATION) {
            int32_t tlvLen0;
            uint8_t *pBuff0;

            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_CMD_RESULT, &tlvLen0, &pBuff0)) {
            } else {
                rtn = RTN_CODE_TLV_MISSING;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }

        free_dev_resp(pBuff);
    }

    return rtn;
}

// topology 
#if 1
int32_t lbslist(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;
    uint8_t i = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_GET_LBS_INFO, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags,VTMSG_MSG_TYPE_GET_LBS_INFO);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_LBS_INFO) {
        int32_t tlvLen0;
        uint8_t *pBuff0;
        json_t* jarr = json_array();

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_LBS_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            printf("\nlbs table\n");
            printf("\nIndex    PAN ID    Short Addr    Route Cost\n");	
            for(i= 0; i< 3; i++) {
                json_t* jobj = json_object();
                printf(" [%d]     ", i);
                printf("0x%02X%02X      ", pBuff0[1 + i*6], pBuff0[0+ i*6] );
                printf("0x%02X%02X     ", pBuff0[3 + i*6], pBuff0[2+ i*6] );
                printf("%5d      ", ((pBuff0[5 + i*6]<< 8) + pBuff0[4+ i*6]));
                printf("\n");
                json_object_set_new(jobj, "index", json_sprintf("%d", i));
                json_object_set_new(jobj, "panid", json_sprintf("0x%02X%02X", pBuff0[1 + i*6], pBuff0[0+ i*6]));
                json_object_set_new(jobj, "short_addr", json_sprintf("0x%02X%02X", pBuff0[3 + i*6], pBuff0[2+ i*6]));
                json_object_set_new(jobj, "route_cost", json_sprintf("%d", ((pBuff0[5 + i*6]<< 8) + pBuff0[4+ i*6])));
                json_array_append_new(jarr, jobj);
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
        json_object_set_new(HTTP_JSON_OUTPUT, "lbslist", jarr);
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}


int32_t joinpan(int32_t argCnt)
{
    uint8_t msgType, hdrFlags, idx;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_SEND_JOIN_PAN_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 1) {
        sscanf((char *)cli_argv[1], "%hhu", &idx);
    } else {
        printf("Please input index\n");
        return RTN_CODE_OK;
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_SEND_JOIN_NETWORK_CMD, VTMSG_SEND_JOIN_PAN_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
            VTMSG_TLV_TYPE_SEND_JOIN_PAN,
            VTMSG_SEND_JOIN_PAN_LEN);

    pBuffCurr[0] = idx;

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_SEND_JOIN_PAN_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags,VTMSG_MSG_TYPE_SEND_JOIN_NETWORK_CMD);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_SEND_JOIN_NETWORK_CMD) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } 
        else 
        {
            rtn = RTN_CODE_OK;
            printf("send join pan command\n");
            json_object_set_new(HTTP_JSON_OUTPUT, "Status", json_sprintf("Send out"));
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}
#endif

int32_t do_raw_vtmsg_type(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn = RTN_CODE_OK;
    int32_t pyldLen = 0;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;

    for(rtn = 0; rtn <= argCnt; rtn++)
        printf("%d, %s | ", rtn, cli_argv[rtn]);

    if (argCnt >= 1) {
        if (cli_argv[1][1] == 'x' || cli_argv[1][1] == 'X') {
        uint8_t input_type;
        uint16_t input_index;
        
        if (argCnt >= 2) {
            sscanf(&cli_argv[1][2], "%hhx", &input_type);
            vtmsg_build_msghdr(buffReq, (uint8_t)input_type & 0x000000ff, VTMSG_ATTR_CMD_INDEX_LEN);

            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                            VTMSG_TLV_TYPE_CMD_INDEX,
                            VTMSG_ATTR_CMD_INDEX_LEN);
            
            sscanf(cli_argv[2], "%hd", &input_index);
            memcpy(pBuffCurr, (uint16_t *)&input_index, sizeof(uint16_t));

            rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);
    
        } else {

            sscanf(&cli_argv[1][2], "%hhx", &input_type);
            vtmsg_build_msghdr((uint8_t *)&msgHdr, input_type, 0x0);

            rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
        }

        //rtn = dev_sendOut((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
        if (rtn < 0) {
            rtn = RTN_CODE_MESSAGE_SEND_FAILED;
            return rtn;
        }

        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0) {
            printf("\nMagpie PLC Platform - Copyright (c) 2018 Vango Technologies, Inc.\n");
            printf("\nvtmsg type: 0x%X   ", msgType);
            
            /*json output*/
            json_object_set_new(HTTP_JSON_OUTPUT, "type", json_sprintf("0x%X", msgType));
            add_payload_to_json(HTTP_JSON_OUTPUT, "raw", pBuff, pyldLen);

            printTlv(pBuff, pyldLen);

            
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }

        free_dev_resp(pBuff);

        return rtn;
        }
    }
    return rtn;
}

typedef void (*LOG_PROC)(VT_SYSTEM_DATA_LOG);
#define MAX_SYSTEM_LOG_ENTRY     128
void print_system_log_boot_up(VT_SYSTEM_DATA_LOG log_data)
{
    uint8_t unknow = 1, i;
    uint32_t mask;
    uint32_t bootup_state = (log_data.format.data[0] << 16) |
                            (log_data.format.data[1] << 8) |
                            log_data.format.data[2];
    // reference FTSCU_REG_BTUP_STS
    char *bootup_name[18] = {"(SDR Clock Enable pin high)",   // 0
                             "(Dram Reset Disable)",          // 1
                             "(Hold External GPIO)",          // 2
                             "(Power-Mode 5 Reset)",          // 3
                             "(Power-Mode 4 Reset)",          // 4
                             "(Power-Mode 3 Reset)",          // 5
                             "(Power-Mode 3 Reset)",          // 6
                             "(Power-Mode 3 Reset)",          // 7
                             "(Hardware Reset)",              // 8
                             "(Watchdog Reset)",              // 9
                             "(Dormant Reset)",               // 10
                             "(Power-OFF Reset)",             // 11
                             "(EMI Fail)",                    // 12
                             "(Brown-Out)",                   // 13
                             "(Power Fault)",                 // 14
                             "(Reserved)",                    // 15
                             "(Power Button Reset)",          // 16
                             "(RTC Alarm)"};                  // 17

    for (i = 0; i < 18; i++) {
        mask = 0x00000001 << i;
        if (bootup_state & mask) {
            g_print( "%s ", bootup_name[i]);
            unknow = 0;
        }
    }

    if (unknow) {
        g_print( "(Unknow)");
    }
    g_print( LF);
}

void print_system_log_reboot(VT_SYSTEM_DATA_LOG log_data)
{
    uint16_t reboot_flag = (log_data.format.data[0] << 8) |
                                 log_data.format.data[1];
    // reference enum reboot_flag_t
    char *reboot_name[11] = {"(HostCli)",                  // 0
                            "(SDK_WD)",                    // 1
                            "(DMA_Tx)",                    // 2
                            "(UnJoin, Queue Full > 255)",  // 3
                            "(APP)",                       // 4
                            "(Upgrade)",                   // 5
                            "(Command Line)",              // 6
                            "(Capture Noise)",             // 7
                            "(DMA_RX)",                    // 8
                            "(BBP_FA_Capture_Noise)",      // 9
                            "(DMA_SECURITY)"};             // 10
    if (reboot_flag < 11) {
        g_print( "%s ", reboot_name[reboot_flag]);
    } else {
        g_print( "error reboot_flag 0x%X ", reboot_flag);
    }
    g_print( LF);
}

void print_system_log_tx_state_error(VT_SYSTEM_DATA_LOG log_data)
{
    uint16_t dma_state = (log_data.format.data[0] << 4) |
                         (log_data.format.data[1] >> 4);
    uint16_t dsp_state = ((log_data.format.data[1] & 0x0F) << 8) |
                         log_data.format.data[2];
    g_print( " DMA(100C8):0x%03X, DSP(C8):0x%03X"LF, dma_state, dsp_state);
}

void print_system_log_tx_wait_bbp_dma(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " DMA(100B0):VCS(%1X) CSMA(%1X) TAF(%1X)%s"LF,
                 (log_data.format.data[0] & 0xF),
                 ((log_data.format.data[0] >> 4) & 0xF),
                 (log_data.format.data[1] & 0xF),
                 log_data.format.data[2] ? ", DSP in tx" : "");
}

void print_system_log_tx_retry_fail(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (0x%02X%02X)"LF,
                 log_data.format.data[0],
                 log_data.format.data[1]);
}

void print_system_log_macp_mismatch(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (LBD %02X%02X%02X%02X - ??%02X%02X%02X)"LF,
                 (log_data.system_tick >> 24) & 0xFF,
                 (log_data.system_tick >> 16) & 0xFF,
                 (log_data.system_tick >> 8) & 0xFF,
                 (log_data.system_tick & 0xFF),
                 log_data.format.data[0],
                 log_data.format.data[1],
                 log_data.format.data[2]);
}

void print_system_log_join_success(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (PANID 0x%02X%02X)"LF,
                 log_data.format.data[0],
                 log_data.format.data[1]);
}

void print_system_log_panid_conflict(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (OLD:0x%02X%02X -> NEW:0x%02X%02X)"LF,
                 log_data.format.data[0],
                 log_data.format.data[1],
                 (log_data.system_tick >> 8) & 0xFF,
                 (log_data.system_tick & 0xFF));
}

void print_system_log_exception(VT_SYSTEM_DATA_LOG log_data)
{
    switch(log_data.format.data[0]) {
        case 1:
            g_print( " und");
            break;
        case 2:
            g_print( " swi");
            break;
        case 3:
            g_print( " pfa");
            break;
        case 4:
            g_print( " dat");
            break;
        case 6:
            g_print( " irq");
            break;
        case 7:
            g_print( " fiq");
            break;
        default:
            g_print( " ???");
            break;
    }
    g_print( "-exception, pc(0x%08X), lr(0x%08X)"LF,
                 log_data.system_tick - log_data.format.data[1],
                 log_data.system_tick);
}

void print_system_log_dying_gasp(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (Power Off)"LF);
}

void print_system_log_immed_queue_full(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (func 0x%08X)"LF, log_data.system_tick);
}

void print_system_log_pool_free_error(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (0x%08X)"LF, log_data.system_tick);
}

void print_system_log_dma_security_error(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( " (%s)"LF, log_data.format.data[0] ? "encryption" : "decryption");
}

char *system_log_name[SYSTEM_LOG_TYPE_MAX] = {
    "NONE",                     // 0
    "SYSTEM_BOOT_UP",           // 1
    "SYSTEM_REBOOT",            // 2
    "DMA_RX_UNAVAIL",           // 3
    "DMA_TX_STATE_ERROR",       // 4
    "DMA_TX_VCS_NOT_IDLE",      // 5
    "DMA_TX_WAIT_BBP_DMA",      // 6
    "DMA_TX_RETRY_FAIL",        // 7
    "ROUTE_DISCOVERY_FAIL",     // 8
    "BOOTSTRAP_MACP_MISMATCH",  // 9
    "BOOTSTRAP_JOIN_SUCCESS",   // 10
    "PANID_CONFLICT",           // 11
    "EXCEPTION",                // 12
    "DYING_GASP",               // 13
    "IMMED_QUEUE_FULL",         // 14
    "POOL_FREE_POINTER_ERROR",  // 15
};

LOG_PROC system_log_func[SYSTEM_LOG_TYPE_MAX] = {
    NULL,                               // 0 SYSTEM_LOG_TYPE_NONE
    print_system_log_boot_up,           // 1 SYSTEM_LOG_TYPE_SYSTEM_BOOT_UP
    print_system_log_reboot,            // 2 SYSTEM_LOG_TYPE_SYSTEM_REBOOT
    NULL,                               // 3 SYSTEM_LOG_TYPE_DMA_RX_UNAVAIL
    print_system_log_tx_state_error,    // 4 SYSTEM_LOG_TYPE_DMA_TX_STATE_ERROR
    NULL,                               // 5 SYSTEM_LOG_TYPE_DMA_TX_VCS_NOT_IDLE
    print_system_log_tx_wait_bbp_dma,   // 6 SYSTEM_LOG_TYPE_DMA_TX_WAIT_BBP_DMA
    print_system_log_tx_retry_fail,     // 7 SYSTEM_LOG_TYPE_DMA_TX_RETRY_FAIL
    print_system_log_tx_retry_fail,     // 8 SYSTEM_LOG_TYPE_ROUTE_DISCOVERY_FAIL (use same printf function)
    print_system_log_macp_mismatch,     // 9 SYSTEM_LOG_TYPE_BOOTSTRAP_MACP_MISMATCH
    print_system_log_join_success,      // 10 SYSTEM_LOG_TYPE_BOOTSTRAP_JOIN_SUCCESS
    print_system_log_panid_conflict,    // 11 SYSTEM_LOG_TYPE_PANID_CONFLICT
    print_system_log_exception,         // 12 SYSTEM_LOG_TYPE_EXCEPTION
    print_system_log_dying_gasp,        // 13 SYSTEM_LOG_TYPE_DYING_GASP
    print_system_log_immed_queue_full,  // 14 SYSTEM_LOG_TYPE_IMMED_QUEUE_FULL
    print_system_log_pool_free_error,   // 15 SYSTEM_LOG_TYPE_POOL_FREE_PTR_ERROR
    print_system_log_dma_security_error,// 16 SYSTEM_LOG_TYPE_DMA_SECURITY_ERROR
};

void print_log_status(VT_SYSTEM_DATA_LOG log_data)
{
    g_print( "%s ", system_log_name[log_data.format.log_type]);
    if (system_log_func[log_data.format.log_type])
        system_log_func[log_data.format.log_type](log_data);
    else {
        g_print( "0x%02X%02X%02X"LF,
                log_data.format.data[0],
                log_data.format.data[1],
                log_data.format.data[2]);
    }
}

int printf_runtime_sys_log(SYS_LOG sys_log)
{
    struct reg_pri_t {
        char str[9];
        uint32_t *value;
    } reg_val[] = {
        { "A0000080", sys_log.reg_A0_80 },
        { "A0000084", sys_log.reg_A0_84 },
        { "A00000C8", sys_log.reg_A0_C8 },
        { "A00000CC", sys_log.reg_A0_CC },
        { "A00100B0", sys_log.reg_A1_B0 },
        { "A00100C8", sys_log.reg_A1_C8 },
        { "A0249C00", sys_log.reg_A024_9C00 },
        { "A0249C10", sys_log.reg_A024_9C10 },
    };
    int i, j;

    g_print( "Reboot Counter %u"LF, sys_log.system_boot_up_counter);
    json_object_set_new(HTTP_JSON_OUTPUT, "reboot", json_sprintf("%u", sys_log.system_boot_up_counter));
    g_print( "Reboot flag %04x"LF, sys_log.reboot_flag_nv);
    g_print( "CLI|SDK|DMA| SQ|APP|UPG|CMD|CNF|RXQ|TFA|SEC|"LF);
    for (i = 0; i < RB_FLAG_MAX; i++) {
        g_print( "  %01d|", (sys_log.reboot_flag_nv & (1 << i)) >> i);
    }
    g_print( LF "DMA : %u"LF, sys_log.dma_task_state);
    g_print( "RX  : %u"LF, sys_log.rx_task_state);
    g_print( "TCIP: %u"LF, sys_log.tcip_task_state);
    g_print( "Current_idx = %d"LF, sys_log.reg_idx);
    for (j = 0; j < MAX_REG_SIZE; j++) {
        g_print( "Index--[%d]--"LF, j);
        for (i = 0; i < sizeof(reg_val) / sizeof(reg_val[0]); i++) {
            g_print( "REG %s: %08X"LF, reg_val[i].str, *(reg_val[i].value + j));
        }
    }

    return 0;
}

void print_system_log(VT_SYSTEM_DATA_LOG log_data)
{
    if ((log_data.format.log_type == SYSTEM_LOG_TYPE_BOOTSTRAP_MACP_MISMATCH) ||
            (log_data.format.log_type == SYSTEM_LOG_TYPE_PANID_CONFLICT) ||
            (log_data.format.log_type == SYSTEM_LOG_TYPE_EXCEPTION) ||
            (log_data.format.log_type == SYSTEM_LOG_TYPE_IMMED_QUEUE_FULL)) {
        g_print("               ");
    } else {
        g_print("(%3dD %02d:%02d:%02d)",
                (log_data.system_tick / 86400000),
                (log_data.system_tick / 3600000) % 24,
                (log_data.system_tick / 60000) % 60,
                (log_data.system_tick / 1000) % 60);
    }
    print_log_status(log_data);
}

int32_t get_log(int32_t argCnt)
{
    //vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn = RTN_CODE_OK;
    int32_t pyldLen = 0;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint16_t iloop=0;


    for (iloop = 0; iloop < 128; iloop++) {
        vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_RUNTIME_SYS_LOG, VTMSG_ATTR_CMD_INDEX_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                    VTMSG_TLV_TYPE_CMD_INDEX,
                    VTMSG_ATTR_CMD_INDEX_LEN);
        /* send out packet, and iloop max. is 128 */
        memcpy(pBuffCurr, (uint16_t *)&iloop, sizeof(uint16_t));
        rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);
        if (rtn < 0) {
            rtn = RTN_CODE_MESSAGE_SEND_FAILED;
            return rtn;
        }
        /* receive packet */
        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0) {
            if(pBuff[VTMSG_TLV_HDR_LEN] != VTMSG_STS_SUCCESS) {
                break;
            }
            VT_SYSTEM_DATA_LOG log_data;
            SYS_LOG sys_log;
            int32_t tlvLen0;
            uint8_t *pBuff0;

            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_RUNTIME_SYS_LOG, &tlvLen0, &pBuff0)) {
                memcpy(&sys_log, pBuff0, tlvLen0);
                printf_runtime_sys_log(sys_log);
            }

            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_SYSTEM_DATA_LOG, &tlvLen0, &pBuff0)) {
                memcpy(&log_data, pBuff0, tlvLen0);
                if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
                    uint16_t idx;
                    memcpy(&idx, pBuff0, sizeof(uint16_t));
                    g_print("[%d]:", idx);
                }
                /*console print*/
                print_system_log(log_data);
            } else {
                rtn = RTN_CODE_TLV_MISSING;
            }

        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
    }

    free_dev_resp(pBuff);

    return rtn;
}

// topology
int32_t start_network(int32_t argCnt)
{
    uint8_t msgType, hdrFlags, role;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_START_NETWORK_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 1) {
        sscanf(cli_argv[1], "%hhu", &role);
    } else {
        printf("Usage:\n");
        printf("nwkstart <role>    0:LBS 1:LBD\n");
        return RTN_CODE_OK;
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_START_NETWORK, VTMSG_ATTR_START_NETWORK_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_START_NETWORK,
                                    VTMSG_ATTR_START_NETWORK_LEN);

    pBuffCurr[0] = role;

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_START_NETWORK_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_START_NETWORK);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_START_NETWORK) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } else {
            if (role == 1) {
                printf("System Init, LBD\n");
                json_object_set_new(HTTP_JSON_OUTPUT, "System Init", json_sprintf("LBD"));
            } else if (role == 0) {
                printf("System Init, LBS\n");
                json_object_set_new(HTTP_JSON_OUTPUT, "System Init", json_sprintf("LBS"));
            } else {
                printf("Unknown role?\n");
            }
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t route_request(int32_t argCnt)
{
    uint8_t msgType, hdrFlags;
    uint8_t addr[2];
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_SHORT_ADDR_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 0){
        printf("Route discovery requset usage:\n");
        printf("route a [short addr 0x??]\n");
        return get_routingTable(0);
    } else if (cli_argv[2][1]== 'x' || cli_argv[2][1]== 'X') {
        if (cli_argv[1][0]== 'a') {
            uint32_t input;
            sscanf((char *)&cli_argv[1][2], "%x", &input);
            addr[0] = (uint8_t)((input & 0x0000ff00) >> 8);
            addr[1] = (uint8_t)input & 0x000000ff;
        }
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_REQUEST_ROUTE, VTMSG_ATTR_SHORT_ADDR_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_SHORT_ADDR,
                                    VTMSG_ATTR_SHORT_ADDR_LEN);

    memcpy(pBuffCurr, addr, sizeof(addr));

    rtn = dev_send(buffReq, sizeof(buffReq));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_REQUEST_ROUTE);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_REQUEST_ROUTE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } else {
            rtn = RTN_CODE_OK;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t config_psk(int32_t argCnt)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_PSK_ENTRY_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn = RTN_CODE_OK;
    int32_t pyldLen = 0, i;

    if (argCnt == 3) {
        if (cli_argv[1][0]== 's' && atoi(cli_argv[2]) < 2) {
            pBuffCurr = vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_SET_PSK_SET, VTMSG_ATTR_PSK_ENTRY_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                           VTMSG_TLV_TYPE_PSK_ENTRY,
                                           VTMSG_ATTR_PSK_ENTRY_LEN);

            pBuffCurr[0] = atoi(cli_argv[2]);
            sscanf(cli_argv[3], "%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx", 
                                &pBuffCurr[1], &pBuffCurr[2], &pBuffCurr[3], &pBuffCurr[4],
                                &pBuffCurr[5], &pBuffCurr[6], &pBuffCurr[7], &pBuffCurr[8],
                                &pBuffCurr[9], &pBuffCurr[10], &pBuffCurr[11], &pBuffCurr[12],
                                &pBuffCurr[13], &pBuffCurr[14], &pBuffCurr[15], &pBuffCurr[16]);

            rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_PSK_ENTRY_LEN);
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }

            rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_SET_PSK_SET);
            if (rtn < 0) {
                rtn = RTN_CODE_NO_RESPONSE;
                return rtn;
            }
            if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_SET_PSK_SET) {
                if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
                    rtn = RTN_CODE_CMD_FAILED;
                } else {
                    rtn = RTN_CODE_OK;
                }
            } else {
                rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
            }
            free_dev_resp(pBuff);
        }
    } else {
        printf("config psk usage:\n");
        printf("psk s [idx] [128bit KEY]\n");    

        vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_PSK_SET, 0x0);

        rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s));
        if (rtn < 0) {
            rtn = RTN_CODE_MESSAGE_SEND_FAILED;
            return rtn;
        }

        rtn = dev_get_resp_type(&msgType, &pBuff, &pyldLen, &hdrFlags, VTMSG_MSG_TYPE_GET_PSK_SET);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_PSK_SET) {
            int32_t tlvLen0;
            uint8_t *pBuff0;

            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_PSK_ENTRY, &tlvLen0, &pBuff0)) {
                int32_t tlvLen1;
                uint8_t *pBuff1;
                printf("PSK%d:  ", pBuff0[0]);
                for (i = 0; i < 16; i++) {
                    printf("%02x", pBuff0[i + 1]);
                }
                printf("\n");

                /*json output*/
                json_object_set_new(HTTP_JSON_OUTPUT, "psk0", 
                                    json_sprintf("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", 
                                    pBuff0[1], pBuff0[2], pBuff0[3], pBuff0[4], pBuff0[5], pBuff0[6], pBuff0[7], pBuff0[8],
                                    pBuff0[9], pBuff0[10], pBuff0[11], pBuff0[12], pBuff0[13], pBuff0[14], pBuff0[15], pBuff0[16]));
                pBuff0 += 1 + 16;
                pyldLen -= VTMSG_ATTR_PSK_ENTRY_LEN;
                if (vtmsg_get_tlv(pBuff0, pyldLen, VTMSG_TLV_TYPE_PSK_ENTRY, &tlvLen1, &pBuff1)) {
                    printf("PSK%d:  ", pBuff1[0]);
                    for (i = 0; i < 16; i++) {
                        printf("%02x", pBuff1[i + 1]);
                    }
                    printf("\n");

                    /*json output*/
                    json_object_set_new(HTTP_JSON_OUTPUT, "psk1", 
                                        json_sprintf("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", 
                                        pBuff1[1], pBuff1[2], pBuff1[3], pBuff1[4], pBuff1[5], pBuff1[6], pBuff1[7], pBuff1[8],
                                        pBuff1[9], pBuff1[10], pBuff1[11], pBuff1[12], pBuff1[13], pBuff1[14], pBuff1[15], pBuff1[16]));
                }
            } else {
                rtn = RTN_CODE_TLV_MISSING;
            }

        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
        free_dev_resp(pBuff);
    }

    return rtn;
}

int32_t check_network_event(uint8_t *pBuff, uint32_t pyldLen)
{
    int32_t tlvLen0;
    uint8_t *pBuff0;

    g_info("\n----------------------------------------------------------------------");
    g_info("\nNETWORK EVENT Received: ");

    if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_NETWORK_EVENT, &tlvLen0, &pBuff0)) {
        switch (*pBuff0) {
        case VTMSG_NEVENT_PAN_JOINED:
        {
            printf("Join pan success\n");
            json_object_set_new(HTTP_JSON_OUTPUT, "Network status", json_sprintf("Joined"));
            break;
        }
        case VTMSG_NEVENT_BOOTSTRAP_SUCCESS:
        {
            break;
        }
        case VTMSG_NEVENT_RECV_BEACON_REQ:
        {
            break;
        }
        case VTMSG_NEVENT_SEARCH_NEIGHBOR_EUI64:{
            int32_t tlvLen1;
            int16_t i;
            uint8_t *addr = NULL;
            
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_EUI64_ADDR, &tlvLen1, &addr)) {
                g_print("eui64: %02x%02x%02x%02x%02x%02x%02x%02x\n", 
                        addr[7], addr[6], addr[5], addr[4], 
                        addr[3], addr[2], addr[1], addr[0]);
               
                for (i=0; i<MAX_NEIGHBOR_EUI_TABLE_SIZE; i++) {
                    /* duplicate eui64 */
                    if (DB->neighbor_eui_table[i].valid && 
                        memcmp(DB->neighbor_eui_table[i].eui64, addr, 8) == 0) {
                        break;
                    
                    /* add new neighbor */                            
                    } else if (!DB->neighbor_eui_table[i].valid) {
                        memcpy(DB->neighbor_eui_table[i].eui64, addr, 8);
                        DB->neighbor_eui_table[i].valid = TRUE;
                        break;
                    }
                }                                              
            }
            break;
        }
        case VTMSG_NEVENT_SEND_BEACON:
        {
            break;
        }
        case VTMSG_NEVENT_BOOTSTRAP_FAIL:
        {
            break;
        }
        case VTMSG_NEVENT_ACK:
        {
            int32_t tlvLen1;
            uint8_t *addr = NULL, *lqi = NULL;
            printf("\n");
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_DEST_SHORT_ADDR, &tlvLen1, &addr)) {
                printf("find prev hop %02x%02x\n", addr[0], addr[1]);
            }
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_LQI, &tlvLen1, &lqi)) {
                printf("find lqi %02x\n", lqi[0]);
            }
            if (DB->ctrl.saving_resp) {
                if (addr != NULL && lqi != NULL) {
                    int32_t i = 0;
                    tplg_node_info tplg = {};
                    memcpy(tplg.short_addrs, DB->dest_addr.addr, 2);
                    memcpy(tplg.prev_short_addrs, addr, 2);
                    memcpy(&tplg.prev_lqi, lqi, 1);
                    i = get_joined_idx(tplg.short_addrs);
                    if (i >= 0) {
                        memcpy(tplg.eui64, DB->joined_dev_table[i].eui_64_bit_addr, 8);
                    } else {
                        //try again
                        printf("[tplg] eui64 doesn't exist. get s2e...");
                        get_joined_table(0);
                        i = get_joined_idx(tplg.short_addrs);
                        if (i > 0) {
                            memcpy(tplg.eui64, DB->joined_dev_table[i].eui_64_bit_addr, 8);
                        }
                    }
                    tplg_update_entry(tplg);
                }
            }

            reply_sig |= 1 << VTMSG_NEVENT_ACK;
        }
        default:
            break;
        }
    } else {
        g_info("\nBad message !!");
    }
    g_info("\n-----------------------------------------------------------------------\n");
    return RTN_CODE_OK;
}

int32_t trytry(int32_t argCnt)
{
    uint32_t i;
    for (i = 0; i < MAX_ROUTING_TABLE_SIZE; i++) {
        if (DB->joined_dev_table[i].valid != 0) {
            printf("[%d]: 0x%02x%02x\n", 
                    i,
                    DB->joined_dev_table[i].alloc_short_addr[0],
                    DB->joined_dev_table[i].alloc_short_addr[1]);
        }
    }
    return 0;
}


int32_t set_pan_id(int32_t argCnt)
{
	uint8_t msgType, hdrFlags;
	uint8_t addr[2];
	uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_PAN_ID_LEN] = {};
	uint8_t *pBuffCurr = NULL;
	uint8_t *pBuff = NULL;
	int32_t rtn;
	int32_t pyldLen = 0;


	if (argCnt == 0){
		printf("Panid usage:\n");
		printf("panid [panid 0x??]\n");
		return RTN_CODE_OK;
	} else if (argCnt == 1) {
		if ((cli_argv[1][1]== 'x' || cli_argv[1][1]== 'X')) {
			uint32_t input;
			sscanf((char *)&cli_argv[1][2], "%x", &input);
			addr[1] = (uint8_t)((input & 0x0000ff00) >> 8);
			addr[0] = (uint8_t)input & 0x000000ff;
			printf("%x %x\n",addr[1], addr[0]);
		} else {
			printf("Panid usage:\n");
			printf("panid [panid 0x??]\n");
			return RTN_CODE_OK;
		}
	}

	
	//return RTN_CODE_OK;
	vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_SET_PAN_ID, VTMSG_ATTR_PAN_ID_LEN);
	pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
			VTMSG_TLV_TYPE_PAN_ID,
			VTMSG_ATTR_PAN_ID_LEN);

	memcpy(pBuffCurr, addr, sizeof(addr));

	rtn = dev_send(buffReq, sizeof(buffReq));
	if (rtn < 0) {
		rtn = RTN_CODE_MESSAGE_SEND_FAILED;
		return rtn;
	}

	rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
	if (rtn < 0) {
		rtn = RTN_CODE_NO_RESPONSE;
		return rtn;
	}

	if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_SET_PAN_ID) {
		if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
			rtn = RTN_CODE_CMD_FAILED;
		} else {
			rtn = RTN_CODE_OK;
		}
	} else {
		rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
	}

	free_dev_resp(pBuff);
	return rtn;	
}


