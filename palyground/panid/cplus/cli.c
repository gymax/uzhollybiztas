#include "app_top.h"
#include "cli.h"
#include "dev.h"
#include "utility.h"

uint32_t cli_argc;
const char *cli_argv[CONFIG_CONSOLE_ARGC_MAX];
uint32_t cli_cmd_level = COMMAND_SUPER | COMMAND_NORMAL | COMMAND_LOCAL;

cliCmd cliCmdsList[] = {
    //{ (char *)"ver", get_version, COMMAND_NORMAL },
    { (char *)"info", dev_show_info, COMMAND_LOCAL},
    { (char *)"target", set_target_dest, COMMAND_LOCAL},
    { (char *)"ver", get_version, COMMAND_NORMAL },
    { (char *)"rev", get_version_internal, COMMAND_NORMAL },
    { (char *)"ifconfig", get_networkInfo, COMMAND_NORMAL },
#if 1
    { (char *)"lbslist", lbslist, COMMAND_NORMAL },
    { (char *)"joinpan", joinpan, COMMAND_NORMAL },
#endif
    { (char *)"raw", do_raw_vtmsg_type, COMMAND_NORMAL },
    { (char *)"s2e", get_joined_table, COMMAND_NORMAL | COMMAND_DCU },
    { (char *)"upg-config", config_upgConfig, COMMAND_LOCAL | COMMAND_DCU },
    { (char *)"nwkstart", start_network, COMMAND_NORMAL }, 
    { (char *)"log", get_log, COMMAND_NORMAL },     
    { (char *)"route", route_request, COMMAND_NORMAL },     
    { (char *)"att", get_attached, COMMAND_NORMAL },
    { (char *)"psk", config_psk, COMMAND_NORMAL },
    { (char *)"syslog", get_runtime_sys_log, COMMAND_NORMAL },
    { (char *)"hping", host_ping, COMMAND_NORMAL },    
    //{ (char *)"tlg-dcu", get_tlg_from_dcu, COMMAND_NORMAL },
    //{ (char *)"tlg-lbd", get_tlg_from_lbd, COMMAND_NORMAL },
    { (char *)"nevent-ack", nevent_ack, COMMAND_NORMAL },
    { (char *)"save-tplg", save_topology, COMMAND_NORMAL },
    { (char *)"try", trytry, COMMAND_NORMAL },
    { (char *)"fat", field_analysis_tool_operation, COMMAND_NORMAL },
    { (char *)"sche-setting", sched_setting, COMMAND_NORMAL },
    { (char *)"sche-job", sched_job, COMMAND_NORMAL },
    { (char *)"bbp-nv", bbp_nv_config, COMMAND_NORMAL },
    { (char *)"reboot", target_reboot, COMMAND_NORMAL },
    { (char *)"nei", neighbor_eui, COMMAND_NORMAL },
    { (char *)"upg", vtcli_remoteupg, COMMAND_NORMAL },
    { (char *)"reset", dev_reset, COMMAND_NORMAL },
    { (char *)"dbg-print", set_dbg_print, COMMAND_NORMAL },
    { (char *)"panid", set_pan_id, COMMAND_NORMAL },
    { NULL, NULL }
};

int32_t cli_process_cmd(int32_t argCnt)
{
    int32_t idx = 0, rtn;

    g_info("%s\n", __func__);

#if 0
    for (idx = 0; idx < argCnt; idx++){
        printf("[COM%d] %s\n", idx, cli_argv[idx]);
    }
    idx = 0;
#endif

    while (cliCmdsList[idx].cmdStr != NULL) {
        if (strcmp((const char *)cli_argv[0], (const char *)cliCmdsList[idx].cmdStr) == 0
            && (cli_cmd_level & cliCmdsList[idx].cmd_level)) {
            rtn = (*cliCmdsList[idx].cmdFn)(argCnt - 1);
            break;
        } else {
            idx++;
        }
    }

    cli_argc = 0;

    print_rtn_error(rtn, __func__);

    if (cliCmdsList[idx].cmdStr == NULL) {
        g_print("Unknown Command !!\n");
        rtn = -1;
    }

    return rtn;
}


int32_t cli_parse_input(char *str)
{
    int32_t rtn = 0, argStart = 1, idx = 0;
    char *pInput = str;

    g_debug("%s\n", __func__);

    while (*pInput == ' ' || *pInput == '\t') {
        pInput++;
    }

    if (pInput[0] == 0x5b || pInput[0] == '\n') {
        g_print("vt# ");
        return rtn; 
    }

    if (pInput[0] == 'q' || pInput[0] == 'Q') {
        //app_exit();
        return rtn;
    }

    if (pInput[0] == '?') {
        idx = 0;
        int32_t printed = 0;

        while (cliCmdsList[idx].cmdStr != NULL) {
            if (cli_cmd_level & cliCmdsList[idx].cmd_level) {
                g_print("%-18s", cliCmdsList[idx].cmdStr);
                printed++;
            }
            idx++;
            if (printed % 4 == 0)
                g_print("\n");
        }

        g_print("\nvt# ");
        return rtn;
    }

    idx = 0;

    g_mutex_lock(dev_ready);
    while (pInput[idx] != '\0') {
        if (pInput[idx] != 0x20 && pInput[idx] != '\t' && pInput[idx] != '\n') {
            if (argStart) {
                if (cli_argc >= CONFIG_CONSOLE_ARGC_MAX)
                    cli_argc = CONFIG_CONSOLE_ARGC_MAX;
                cli_argv[cli_argc] = &pInput[idx];
                cli_argc++;
                argStart = 0;
            }
        } else {
            argStart = 1;
            pInput[idx] = '\0';
        }
        idx++;
    }

    if (cli_argc == 0) {
        g_mutex_unlock(dev_ready);
        return -1;
    }

    rtn = cli_process_cmd(cli_argc);
    g_mutex_unlock(dev_ready);

    g_print("vt# ");

    return rtn;
}


json_t *http_parse_input(char *str)
{
    int32_t argStart = 1, idx = 0;
    char *pInput = str;
    json_t *json_body = json_object();

    g_debug("%s\n", __func__);

    while (*pInput == ' ' || *pInput == '\t') {
        pInput++;
    }

    g_mutex_lock(dev_ready);
    json_object_clear(HTTP_JSON_OUTPUT);
    while (pInput[idx] != '\0') {
        if (pInput[idx] != 0x20 && pInput[idx] != '\t' && pInput[idx] != '\n') {
            if (argStart) {
                if (cli_argc >= CONFIG_CONSOLE_ARGC_MAX)
                    cli_argc = CONFIG_CONSOLE_ARGC_MAX;
                cli_argv[cli_argc] = &pInput[idx];
                cli_argc++;
                argStart = 0;
            }
        } else {
            argStart = 1;
            pInput[idx] = '\0';
        }
        idx++;
    }

    if (cli_argc == 0) {
        json_object_set_new(HTTP_JSON_OUTPUT, "error", json_string("ture"));
    } else {
        if (cli_process_cmd(cli_argc) == 0) {
            json_object_set_new(HTTP_JSON_OUTPUT, "error", json_string("false"));
        } else {
            json_object_set_new(HTTP_JSON_OUTPUT, "error", json_string("ture"));
        }
    }
    json_body = json_deep_copy(HTTP_JSON_OUTPUT);
    g_mutex_unlock(dev_ready);
    //json_dumpf(json_body, stdout, JSON_INDENT(2));
    return json_body;
}
