#include "app_top.h"
#include "http.h"
#include "cli.h"

#ifndef _WIN32
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#define U_DISABLE_CURL
#define U_DISABLE_WEBSOCKET
#include <ulfius.h>

#define PORT 5656
#define CONTROLLERPLUS_ENDPOINT "/app"
#define NETWORK_ENDPOINT "/plc"
#define REMOTE_UPGRADE_ENDPOINT "/upg"
#define SCHEDULED_ENDPOINT "/scheduled"


/**
 * callback functions declaration
 */

/***************network endpoint*****************/
int callback_get_plc_resource(const struct _u_request * request, struct _u_response * response, void * user_data);
int callback_post_plc_resource(const struct _u_request * request, struct _u_response * response, void * user_data);
int callback_get_app_resource(const struct _u_request * request, struct _u_response * response, void * user_data);
int callback_get_upg_resource(const struct _u_request * request, struct _u_response * response, void * user_data);
int callback_post_upg_resource(const struct _u_request * request, struct _u_response * response, void * user_data);
int callback_get_scheduled_resource(const struct _u_request * request, struct _u_response * response, void * user_data);
int callback_post_scheduled_resource(const struct _u_request * request, struct _u_response * response, void * user_data);
int callback_del_scheduled_resource(const struct _u_request * request, struct _u_response * response, void * user_data);

int callback_default (const struct _u_request * request, struct _u_response * response, void * user_data);

/**
 * decode a json array into a string then run command
 */
void do_each_post_command_json(json_t *json, const char *command) {
    const char *key;
    char * line = NULL;
    int len;
    json_t *value;
    json_dumpf(json, stdout, JSON_INDENT(2));
    if (json_is_object(json)) {
        json_object_foreach(json, key, value) {
            if (json_is_string(value)) {
                len = snprintf(NULL, 0, "%s %s %s", command, key, json_string_value(value));
                line = o_malloc((len+1)*sizeof(char));
                snprintf(line, (len+1), "%s %s %s", command, key, json_string_value(value));
                json_t *jbody = http_parse_input(line);
                json_decref(jbody); //alway decref after userd
                o_free(line);
            } else if (json_is_array(value)) {
                size_t index;
                json_t *vvalue;
                json_array_foreach(value, index, vvalue) {
                    len = snprintf(NULL, 0, "%s %s %s", command, key, json_string_value(vvalue));
                    line = o_malloc((len+1)*sizeof(char));
                    snprintf(line, (len+1), "%s %s %s", command, key, json_string_value(vvalue));
                    g_print("%s\n", line);
                    json_t *jbody = http_parse_input(line);
                    json_decref(jbody); //alway decref after userd
                    o_free(line);
                }
            }
        }
    }
}


/**
 * decode a u_map into a string then run command
 */
void do_each_post_command(const struct _u_map * map, const char* command) {
    char * line = NULL;
    const char **keys, * value;
    int len, i;
    if (map != NULL) {
        keys = u_map_enum_keys(map);
        for (i=0; keys[i] != NULL; i++) {
            value = u_map_get(map, keys[i]);
            len = snprintf(NULL, 0, "%s %s %s", command, keys[i], value);
            line = o_malloc((len+1)*sizeof(char));
            snprintf(line, (len+1), "%s %s %s", command, keys[i], value);
            json_t *jbody = http_parse_input(line);
            json_decref(jbody); //alway decref after userd
            o_free(line);
        }
    }
}

/**
 * decode a u_map into a string
 */
char * print_map(const struct _u_map * map) {
    char * line, * to_return = NULL;
    const char **keys, * value;
    int len, i;
    if (map != NULL) {
        keys = u_map_enum_keys(map);
        for (i=0; keys[i] != NULL; i++) {
            value = u_map_get(map, keys[i]);
            len = snprintf(NULL, 0, "key is %s, value is %s", keys[i], value);
            line = o_malloc((len+1)*sizeof(char));
            snprintf(line, (len+1), "key is %s, value is %s", keys[i], value);
            if (to_return != NULL) {
                len = strlen(to_return) + strlen(line) + 1;
                to_return = o_realloc(to_return, (len+1)*sizeof(char));
                if (strlen(to_return) > 0) {
                    strcat(to_return, "\n");
                }
            } else {
                to_return = o_malloc((strlen(line) + 1)*sizeof(char));
                to_return[0] = 0;
            }
            strcat(to_return, line);
            o_free(line);
        }
        return to_return;
    } else {
        return NULL;
    }
}

char * read_file(const char * filename) {
    char * buffer = NULL;
    long length;
    FILE * f = fopen (filename, "rb");
    if (filename != NULL) {

        if (f) {
            fseek (f, 0, SEEK_END);
            length = ftell (f);
            fseek (f, 0, SEEK_SET);
            buffer = o_malloc (length + 1);
            if (buffer) {
                fread (buffer, 1, length, f);
            }
            buffer[length] = '\0';
            fclose (f);
        }
        return buffer;
    } else {
        return NULL;
    }
}

void* http_main(void *data) {
    int ret;

    // Set the framework port number
    struct _u_instance instance;
    struct _u_map mime_types;
    u_map_init(&mime_types);

    y_init_logs("controller-plus http service", Y_LOG_MODE_CONSOLE, Y_LOG_LEVEL_DEBUG, NULL, "Starting...");
    y_log_message(Y_LOG_LEVEL_INFO, "Listen to prot:%d",PORT);

    if (ulfius_init_instance(&instance, PORT, NULL, NULL) != U_OK) {
        y_log_message(Y_LOG_LEVEL_ERROR, "Error ulfius_init_instance, abort");
        return NULL;
    }

    u_map_put(instance.default_headers, "Access-Control-Allow-Origin", "*");
    u_map_put(instance.default_headers, "Cache-Control", "no-cache");
    u_map_put(&mime_types, "*", "application/octet-stream");

    // Maximum body size sent by the client is 8 Kb
    instance.max_post_body_size = (1024 * 8);

    // Endpoint list declaration
    ulfius_add_endpoint_by_val(&instance, "GET", NETWORK_ENDPOINT, "/:resource/:element", 0, &callback_get_plc_resource, NULL);
    ulfius_add_endpoint_by_val(&instance, "POST", NETWORK_ENDPOINT, "/:resource/:element", 0, &callback_post_plc_resource, NULL);
    ulfius_add_endpoint_by_val(&instance, "GET", CONTROLLERPLUS_ENDPOINT, "/:resource", 0, &callback_get_app_resource, NULL);
    ulfius_add_endpoint_by_val(&instance, "GET", REMOTE_UPGRADE_ENDPOINT, "/:resource/:element", 0, &callback_get_upg_resource, NULL);
    ulfius_add_endpoint_by_val(&instance, "POST", REMOTE_UPGRADE_ENDPOINT, "/:resource", 0, &callback_post_upg_resource, NULL);
    ulfius_add_endpoint_by_val(&instance, "GET", SCHEDULED_ENDPOINT, "/:resource", 0, &callback_get_scheduled_resource, NULL);
    ulfius_add_endpoint_by_val(&instance, "POST", SCHEDULED_ENDPOINT, "/:resource", 0, &callback_post_scheduled_resource, NULL);
    ulfius_add_endpoint_by_val(&instance, "DELETE", SCHEDULED_ENDPOINT, "/:resource", 0, &callback_del_scheduled_resource, NULL);

    // default_endpoint declaration
    ulfius_set_default_endpoint(&instance, &callback_default, NULL);

    // Start the framework

    // Open an http connection
    ret = ulfius_start_framework(&instance);

    if (ret == U_OK) {
        while(1) {
            sleep(1);
        }
    } else {
        y_log_message(Y_LOG_LEVEL_DEBUG, "Error starting framework");
    }
    y_log_message(Y_LOG_LEVEL_DEBUG, "End framework");

    y_close_logs();

    ulfius_stop_framework(&instance);
    ulfius_clean_instance(&instance);

    return NULL;
}

/***************network endpoint*****************/
int callback_get_plc_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    char * url_params = print_map(request->map_url);
    char * command = NULL;
    const char * resource = u_map_get(request->map_url, "resource");
    const char * element = u_map_get(request->map_url, "element");
    uint32_t error = 0;
    g_info("%s\n", url_params);
        
    if (strcmp(resource, "joinpan") == 0 || strcmp(resource, "nwkstart") == 0) {
        command = msprintf("%s %s", resource, element);
    } else if(strcmp(resource, "raw") == 0) {
        if (strstr(element, "0x") || strstr(element, "0X")) {
            cli_argv[1] = element;
            set_target_dest(1);
        } else {
            cli_argv[1] = "local";
            set_target_dest(1);
        }
        const char * vtmag_type = u_map_get(request->map_url, "type");
        const char * vtmag_idx = u_map_get(request->map_url, "idx");
        if(vtmag_type == NULL) {
            command = msprintf("%s", resource);
        } else {
            if(vtmag_idx == NULL)
                command = msprintf("%s %s", resource, vtmag_type);
            else
               command = msprintf("%s %s %s", resource, vtmag_type, vtmag_idx); 
        }
    } else if(strcmp(resource, "log") == 0) {
        if (strstr(element, "0x") || strstr(element, "0X")) {
            cli_argv[1] = element;
            set_target_dest(1);
        } else {
            cli_argv[1] = "local";
            set_target_dest(1);
        }
        const char * vtmag_type = u_map_get(request->map_url, "short_addr");
        if(vtmag_type == NULL) {
            command = msprintf("%s local", resource);
        } else {
            command = msprintf("%s %s", resource, vtmag_type); 
        }
        
    } else if(strcmp(resource, "panid") == 0) {
    	if (strstr(element, "0x") || strstr(element, "0X")) {
            command = msprintf("%s %s", resource, element); 
	    printf("%s", command);
        }
    } else {
        if (strstr(element, "0x") || strstr(element, "0X")) {
            cli_argv[1] = element;
            set_target_dest(1);
        } else if (strlen(element) == 16) {
            cli_argv[1] = element;
            set_target_dest(1);
        } else if (strcmp(element, "0") == 0 || strcmp(element, "local") == 0) {
            cli_argv[1] = "local";
            set_target_dest(1);
        } else {
            error = 1; 
        }
        command = msprintf("%s", resource);
    }

    if (error) {
        ulfius_set_string_body_response(response, 403, "Unknown access");
    } else {
        json_t *jbody = http_parse_input(command);
        ulfius_set_json_body_response(response, 200, jbody);
        json_decref(jbody); //always decref after userd
    }
    o_free(url_params);
    o_free(command);
    return U_CALLBACK_CONTINUE;
}

int callback_post_plc_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    json_t * json_post = ulfius_get_json_body_request(request, NULL);
    char * command = NULL;
    const char * resource = u_map_get(request->map_url, "resource");
    const char * element = u_map_get(request->map_url, "element");
    uint32_t error = 0;

    //determine target
    if (strstr(element, "0x") || strstr(element, "0X")) {
        cli_argv[1] = element;
        set_target_dest(1);
    } else if (strlen(element) == 16) {
        cli_argv[1] = element;
        set_target_dest(1);
    } else if (strcmp(element, "0") == 0 || strcmp(element, "local") == 0) {
        cli_argv[1] = "local";
        set_target_dest(1);
    } else {
        error = 1; 
    }

    command = msprintf("%s", resource);

    if (error) {
        ulfius_set_string_body_response(response, 403, "Unknown access");
    } else {
        if (strcmp(resource, "bbp-nv") == 0) {           
            do_each_post_command_json(json_post, command);
        }
        g_print("post command : %s\n", command);
        json_t *jbody = http_parse_input(command);
        ulfius_set_json_body_response(response, 200, jbody);
        json_decref(jbody); //alway decref after userd
    }

    o_free(command);
    return U_CALLBACK_CONTINUE;
}

int callback_get_app_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    char * url_params = print_map(request->map_url);
    char * command = NULL;
    const char * resource = u_map_get(request->map_url, "resource");
    g_info("%s\n", url_params);
    command = msprintf("%s", resource);
    json_t *jbody = http_parse_input(command);
    ulfius_set_json_body_response(response, 200, jbody);
    json_decref(jbody); //alway decref after userd
    o_free(url_params);
    o_free(command);
    return U_CALLBACK_CONTINUE;
}

int callback_get_upg_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    char * url_params = print_map(request->map_url);
    char * command = NULL;
    const char * resource = u_map_get(request->map_url, "resource");
    const char * element = u_map_get(request->map_url, "element");
    g_info("%s\n", url_params);
    if (strstr(element, "node")) {
        command = msprintf("%s %s", resource, element);
    /*  */
    } else if (strcmp(resource, "nei") == 0) {
        const char *idx = u_map_get(request->map_url, "idx");
        const char *eui = u_map_get(request->map_url, "eui");
        const char *time = u_map_get(request->map_url, "time");
        if (strstr(element, "search")) { 
            command = msprintf("%s %s %s", resource, element, time); 
        } else if (strstr(element, "add") || strstr(element, "del")) {
            command = msprintf("%s %s %s %s", resource, element, idx, eui);
        } else {
            command = msprintf("%s %s", resource, element);
        }
        
        g_print("%s\n", command);

    } else {
        command = msprintf("%s", resource);
    }
    json_t *jbody = http_parse_input(command);
    ulfius_set_json_body_response(response, 200, jbody);
    json_decref(jbody); //alway decref after userd
    o_free(url_params);
    o_free(command);
    return U_CALLBACK_CONTINUE;
}

int callback_post_upg_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    char * post_params = print_map(request->map_post_body);
    json_t * json_post = ulfius_get_json_body_request(request, NULL); 
    char * command = NULL;
    const char * resource = u_map_get(request->map_url, "resource");
    
    g_info("%s\n", post_params);
    command = msprintf("%s", resource);
    if (strcmp(resource, "nei") == 0) {
        do_each_post_command_json(json_post, command);
        g_print("post: %s\n", command);
    } else {
        do_each_post_command(request->map_post_body, command);
    }
    json_t *jbody = http_parse_input(command);
    ulfius_set_json_body_response(response, 200, jbody);
    json_decref(jbody); //alway decref after userd
    o_free(post_params);
    o_free(command);
    return U_CALLBACK_CONTINUE;
}

int callback_get_scheduled_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    char *post_params = print_map(request->map_post_body);
    char *command = NULL;
    const char * resource = u_map_get(request->map_url, "resource");
    g_info("%s\n", post_params);
    command = msprintf("sche-%s", resource);
    do_each_post_command(request->map_post_body, command);
    json_t *jbody = http_parse_input(command);
    ulfius_set_json_body_response(response, 200, jbody);
    json_decref(jbody); //alway decref after userd
    o_free(post_params);
    o_free(command);
    return U_CALLBACK_CONTINUE;
}

int callback_post_scheduled_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    json_t * json_sche_post = ulfius_get_json_body_request(request, NULL);
    const char * resource = u_map_get(request->map_url, "resource");
    char *command = NULL;
    g_print("[post] resource is \"%s\"\n", resource);
    command = msprintf("sche-%s", resource);
    if (strcmp(resource, "setting") == 0 || strcmp(resource, "job") == 0) {
        do_each_post_command_json(json_sche_post, command);
    }
    json_t *jbody = http_parse_input(command);
    ulfius_set_json_body_response(response, 200, jbody);
    json_decref(jbody); //alway decref after userd
    o_free(command);
    return U_CALLBACK_CONTINUE;
}

int callback_del_scheduled_resource(const struct _u_request * request, struct _u_response * response, void * user_data) {
    char * url_params = print_map(request->map_url);
    char * command = NULL;
    const char * resource = u_map_get(request->map_url, "resource");
    g_info("%s\n", url_params);
    if (strcmp(resource, "job") == 0) {
        const char * keyword = u_map_get(request->map_url, "keyword");
        command = msprintf("sche-%s del %s", resource, keyword);
        json_t *jbody = http_parse_input(command);
        ulfius_set_json_body_response(response, 200, jbody);
        json_decref(jbody); //alway decref after userd
    } else {
        ulfius_set_empty_body_response(response, 200);
    }
    o_free(url_params);
    o_free(command);    
    return U_CALLBACK_CONTINUE;
}

/**
 * Default callback function called if no endpoint has a match
 */
int callback_default (const struct _u_request * request, struct _u_response * response, void * user_data) {
    ulfius_set_string_body_response(response, 404, "Endpoint not found");
    return U_CALLBACK_CONTINUE;
}
