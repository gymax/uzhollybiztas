#ifndef __APP_H__
#define __APP_H__

int32_t get_version(int32_t);
int32_t get_version_internal(int32_t);
int32_t get_networkInfo(int32_t);
int32_t get_routingTable(int32_t);
int32_t get_neighborTable(int32_t);
int32_t get_topologyTable(int32_t);
int32_t mem_dumpword(int32_t);
int32_t mem_writeword(int32_t);
int32_t start_upgrade(int32_t);
int32_t start_network(int32_t);
int32_t check_dcu_support(int32_t);
int32_t check_upgrade_event(uint8_t *, uint32_t);
int32_t check_network_event(uint8_t *, uint32_t);
int32_t upg_check_ver(uint8_t flag);
int32_t config_upgConfig(int32_t);
int32_t target_reboot(int32_t);
int32_t bbp_nv_config(int32_t);
int32_t app_nv_config(int32_t);
int32_t zerox_config(int32_t);
int32_t csma_config(int32_t);
int32_t packet_cntr(int32_t);
int32_t host_ping(int32_t);
int32_t bbp_raw_command(int32_t);
int32_t get_runtime_sys_log(int32_t argCnt);
int32_t get_log(int32_t argCnt);
int32_t get_bbp_sig(int32_t argCnt);
int32_t start_tplg(int32_t argCnt);
int32_t get_eni_eui_Table(int32_t argCnt);
int32_t get_joined_table(int32_t argCnt);
int32_t get_joined_table_clou(int32_t argCnt);
int32_t get_joined_table_clou_failed(int32_t argCnt);
int32_t network_diagnosis(int32_t argCnt);
int32_t tools_settimer(int32_t argCnt);
int32_t traverse_send_command(int32_t argCnt);
int32_t do_configuration(int32_t argCnt);
int32_t route_request(int32_t argCnt);
int32_t path_request(int32_t argCnt);
int32_t reset_dev(int32_t argCnt);
int32_t network_check_ver(int32_t argCnt);

int32_t set_pan_id(int32_t argCnt);

int32_t app_rework_handle(char *cmd, char *str1);

int32_t upg_procedure(void);
void stop_upgrade(void);

void print_rtn_error(int32_t, const char *);
int32_t check_connection(void);

int32_t cli_processCmd(int32_t);

// topology 
int32_t lbslist(int32_t argCnt);
int32_t joinpan(int32_t argCnt);
// topology


#define MAX_FILEREAD_SIZE   512*1024
#define UPG_DB_LIST_SIZE    4
#define UPG_MIN_BLOCK_SIZE  32
#define MAX_POLLING_TABLE_SIZE 512

#define MAX_WE_TIMEOUT_RETRY 2

enum VTUPG_TARGET_ROLE_TABLE {
    VTUPG_TARGET_UIU        = 0,
    VTUPG_TARGET_METER,
};

typedef struct UPG_POLLING_ENTRY {
    uint8_t addr[8];
    uint8_t valid;  // bit 0: avaliable, bit 1:unicast polling
    uint8_t role;
    uint8_t short_addr[2];
} polling_entry;

typedef struct UPG_CONFIG {
    uint32_t blockSize;
    uint32_t delay;
    uint32_t offset_table[UPG_DB_LIST_SIZE];
    uint32_t mode;    // 0:unicast, 1:broadcast
    uint8_t dest_addr[8];
    uint8_t addr_mode; // 0:EUI64, 1:short addr
    uint8_t uart_port_num;
    uint8_t reserve0;
    uint8_t reserve1;
    polling_entry polling_table[MAX_POLLING_TABLE_SIZE];
    //uint32_t    maxRetry; TODO
} upg_config_t;

typedef struct dest_short_addr {
    uint8_t sAddr[2];
}dest_short_addr;

typedef struct current_all_routing_table {
    uint8_t currentNodeCount;
    dest_short_addr destAddr[100];
}current_all_routing_table;

typedef struct DIAGNO_PEER_ENTRY {
    uint8_t eui64[8];
    uint8_t short_addr[2];
    uint8_t valid;
    uint32_t condi;
    uint32_t timestamp;
    uint32_t flags;
    uint32_t success;
} diagnosis_peer_entry;

extern uint32_t g_vtmsg_msg_outgoing;
extern uint32_t msg_outgoing_addr[8];
extern uint32_t msg_outgoing_addr_len;
extern int hostcliSqlDbFlag;
extern unsigned int hostcli_maints_cell_count;
extern uint32_t cli_argc;
extern uint8_t *cli_argv[CONFIG_CONSOLE_ARGC_MAX];

#endif //__APP_H__
