#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <sys/timeb.h>
#include "main.h"
#include "utility.h"
#include "app.h"
#include "vtmsg.h"
#include "log.h"
#include "bbp.h"

#if OS_WIN32
#include <windows.h>
#include <tchar.h>
#define __func__ __FUNCTION__
#else
#include <unistd.h> /*  misc. UNIX functions      */
#include <syslog.h>
#endif

uint32_t reply_sig;

JOINED_DEV_TABLE_ST joined_dev_table[MAX_POLLING_TABLE_SIZE];

struct {
    char str[32];
} bbpnv_param[] = {
    { "VGFREQINITTONE"},
    { "VGFREQTTLTONE"},
    { "VGFREQTONEPERGRP"},
    { "MODULATION"},
    { "TDPFIR3SCALEMAX"},
    { "TDPFIR3SCALEMIN"},
    { "TDPFIR3_ADDR3DB_THR"},
    { "TDPFIR3_START_SCALE"},
    { "ACK_TX_ADDITIONAL_GAIN"},
    { "ENPGAMODE"},
    { "URGENTIN_EN"},
    { "PRMB_SYNCP_SYM_NUM"},
    { "PRMB_MATCH_SYM"},
    { "PRMB_DET_SCALE_FCC"},
    { "PRMB_DET_SCALE_CEN_A"},
    { "PRMB_DET_SCALE_VANGO"},
    { "PRMB_DET_NOISE_THR"},
    { "APDT_EN"}
};

struct {
    char str[32];
} csma_param[] = {
    { "macMaxBE" },
    { "macMinBE" },
    { "macA" },
    { "macK" },
    { "macMinCWAttempts" },
    { "macMaxCSMABackoffs" },
    { "macCSMAFairnessLimit" },
    { "macHPWindowSize" },
    { "NB" },
    { "NBF" },
    { "CW" },
    { "minCWCount" },
    { "QualtiyOfService" },
    { "aSlotTime" },
};

struct {
    char str[32];
} appnv_param[] = {
    { "lbs_keepalive_duration" },
    { "self_join_time" },
    { "self_join_check_time" },
    { "sdk_dup_send_cnt" },
    { "announce_heartbeat_time" },
};

struct {
    char str[32];
    char help[16];
} zerox_param[] = {
    { "enable", "<0/1>" },
    { "gpio_in", "<0~31>" },
    { "gpio_out", "<0~31>" },
    { "degree", "<1~99%>" },
    { "check_no_ac", "<0/1>" },
    { "bbp_proc_delay", "<0~5000 us>" },
    { "window", "<-7~7>" },
    { "vcs_busy", "" },
    { "csma_fail", "" },
    { "reply_ack", "" },
    { "reply_nack", "" },
    { "no_reply", "" },
    { "p_csma_success_to", "" },
    { "p_ack_to", "" },
    { "p_tx_cmp_to_from_ackto", "" },
    { "p_tx_cmp_to_from_csma", "" },
    { "p_tx_cmp_to_from_ack", "" },
    { "p_tx_cmp_to", "" },
};

enum NETWORK_PACKET_CNTR_TABLE {
    tx_submitted = 0,
    tx,
    rx,
    rx_ctrl_normal_rx,
    rx_ctrl_bypass_rx,
    rx_ctrl_raw_rx,
    rx_fcs_ok,
    rx_fcs_fail,
    rx_hcs_ok,
    rx_hcs_fail,
    rx_mic_ok,
    rx_mic_fail,
    rx_ctrl_ack,
    rx_ctrl_nack,
    detect_pmb,
    rx_cmp,
    urgent_in,
    tx_cmp,
    tx_ack,
    tx_nack,
    tx_ack_timeout,
    tx_ack_timeout_ignore,
    tx_csma_success,
    tx_csma_fail,
    tx_retry_fail,
    tx_sw_ack_timeout,
    tx_sw_tx_cmp_timeout,
    lowpan6_rx,
    lowpan6_rx_esc_route,
    lowpan6_rx_esc_bootstrap,
    lowpan6_rx_esc_vango,
    lowpan6_rx_drop,
    lowpan6_rx_drop_multicast,
    lowpan6_rx_forward_unicast,
    lowpan6_rx_forward_unicast_fail,
    lowpan6_rx_forward_broadcast,
    icmp_rx_echo_request,
    icmp_rx_echo_reply,
    icmp_tx_echo_request,
    icmp_tx_echo_reply,
    sdk_uart_tx_packet,
    sdk_uart_rx_packet,
    sdk_plc_tx_packet,
    sdk_plc_rx_packet,
    _COUNT_NETWORK_PACKET_CNTR_TABLE,
};


int32_t check_connection()
{
    //return RTN_CODE_OK;
    int i;
    printf("Connecting...\r\n");
    //send out 4x dummy 0xc0 for DCU cli mode switch
    dev_send(NULL, 0);
    dev_send(NULL, 0);
    dev_send(NULL, 0);
    dev_send(NULL, 0);
    if (g_vtmsg_msg_outgoing) {
        printf("MAGPIE remote address ");
        if (msg_outgoing_addr_len == 2) {
            printf("0x%02x%02x...", msg_outgoing_addr[0], msg_outgoing_addr[1]);
        } else if (msg_outgoing_addr_len == 8) {
            printf("%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x...",
                   msg_outgoing_addr[7], msg_outgoing_addr[6],
                   msg_outgoing_addr[5], msg_outgoing_addr[4],
                   msg_outgoing_addr[3], msg_outgoing_addr[2],
                   msg_outgoing_addr[1], msg_outgoing_addr[0]);
        }
    } else {
        if (check_dcu_support(0) != RTN_CODE_OK) { //magpie
            printf("MAGPIE\n");
            printf("Check remote upgrade server ability...\n");
            stop_upgrade();
            return get_version(0);
        }
    }
    return RTN_CODE_OK;
}

extern upg_config_t upgConfig;
void app_safety_exit()
{
    save_app_config();
    //if (upg_running){
    //    printf("upgrade still running!!\nstop frimware upgrade...");
    //    stop_upgrade();
    //}
}

static void print_bbp_log_info(uint8_t* payload)
{
    int i;
    struct bbp_mib_log_st bbp_log[MAX_BBP_MIB_LOG_ENTRY] = {};
    memcpy(bbp_log, payload, sizeof(struct bbp_mib_log_st)*MAX_BBP_MIB_LOG_ENTRY);
    printf("idx|fir3_scale|ur_in|ot_event|pmb_scale|pmb_det|syncm_det|fch_crc_ok|data_fch_ok|data_ok|tx_re\n");
    for(i = 0; i < MAX_BBP_MIB_LOG_ENTRY; i++) {
    printf("%u\t%u\t%u\t%u\t%u  \t%u\t%u\t%u\t%u\t%u\t%u\n",
                bbp_log[i].index,
                bbp_log[i].fir3_scale,
                bbp_log[i].urgent_in_cnt,
                bbp_log[i].ot_event_cnt,
                bbp_log[i].apdt_preamb_det_scale,
                bbp_log[i].preamb_det,
                bbp_log[i].syncm_det_success,
                bbp_log[i].fch_crc_correct,
                bbp_log[i].data_fch_correct,
                bbp_log[i].data_correct,
                bbp_log[i].tx_retransmit_cnt);
    }

}

int32_t bbp_raw_command(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_TLV_HDR_LEN + 4 * 10] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    uint32_t nr_words;
    uint32_t words_out[10];
    int32_t rtn;

    if (argCnt == 0) {
        printf("bbpraw [word...]\n");
        return RTN_CODE_OK;
    } else if(argCnt == 1 && strstr(cli_argv[1],"l") != 0) {
        cli_argv[1] = "ffffffff";
    }

    vtmsg_build_msghdr((uint8_t *)&buffReq, VTMSG_MSG_TYPE_BBP_RAW_DUMP, VTMSG_TLV_HDR_LEN + argCnt * 4);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_BBP_SETTINGS_VALUE,
                                    VTMSG_TLV_HDR_LEN + argCnt * 4);


    for (nr_words = 0; nr_words < argCnt; nr_words++) {
        sscanf(cli_argv[nr_words + 1], "%08x", &words_out[nr_words]);
        memcpy(pBuffCurr, &words_out[nr_words], 4);
        pBuffCurr += 4;
    }

    rtn = dev_send((uint8_t *)&buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_TLV_HDR_LEN + argCnt * 4);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_BBP_RAW_DUMP) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BBP_SETTINGS_VALUE, &tlvLen0, &pBuff0)) {
            uint32_t i;
            if (words_out[0] != 0xffffffff) {
                printf("BBP RAW RX:\n");
                for (i = 0; i < tlvLen0; i++) {
                    if (i % 4 == 0) printf(" ");
                    printf("%02x", pBuff0[i]);
                }
                printf("\n");
                printf_bbp_raw_rx_info((uint32_t *)pBuff0, tlvLen0);
            } else {
                print_bbp_log_info(pBuff0);
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}


int32_t check_network_event(uint8_t *pBuff, uint32_t pyldLen)
{
    int32_t tlvLen0;
    uint8_t *pBuff0;

    DEBUG_MORE("\n----------------------------------------------------------------------");
    DEBUG_MORE("\nNETWORK EVENT Received: ");

    if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_NETWORK_EVENT, &tlvLen0, &pBuff0)) {
        int32_t tlvLen1;
        uint8_t *pBuff1;
        switch (*pBuff0) {
        case VTMSG_NEVENT_PAN_JOINED:
        {
            break;
        }
        case VTMSG_NEVENT_BOOTSTRAP_SUCCESS:
        {
            break;
        }
        case VTMSG_NEVENT_RECV_BEACON_REQ:
        {
            break;
        }
        case VTMSG_NEVENT_SEND_BEACON:
        {
            break;
        }
        case VTMSG_NEVENT_BOOTSTRAP_FAIL:
        {
            break;
        }
        case VTMSG_NEVENT_ROUTE_DISCOVERY_SUCCESS:
        {
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_DEST_SHORT_ADDR, &tlvLen1, &pBuff1)) {
                printf("\nRoute discovery to 0x%02x%02x success!!\n", pBuff1[0], pBuff1[1]);
            }
            break;
        }
        case VTMSG_NEVENT_PATH_DISCOVERY_SUCCESS:
        {
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_PATH_RESULT, &tlvLen1, &pBuff1)) {
                uint8_t i = 0;
                printf("\nPath discovery success!!\n");
                for (i = 0; i < 30;) {
                    printf("0x%02x%02x(%d)", pBuff1[i], pBuff1[i + 1], pBuff1[i + 2]);
                    i += 3;
                    if (pBuff1[i] != 0xff) {
                        printf(" -> ");
                    } else {
                        printf("\n");
                        break;
                    }
                }
            }
            break;
        }
        case VTMSG_NEVENT_PATH_DISCOVERY_FAIL:
        {
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_PATH_RESULT, &tlvLen1, &pBuff1)) {
                uint8_t i = 0;
                printf("\nPath discovery fail!!\n");
                for (i = 0; i < 30;) {
                    printf("0x%02x%02x(%d)", pBuff1[i], pBuff1[i + 1], pBuff1[i + 2]);
                    i += 3;
                    if (pBuff1[i] != 0xff) {
                        printf(" -> ");
                    } else {
                        break;
                    }
                }
            }
            break;
        }
        case VTMSG_NEVENT_ACK:
        {
            reply_sig |= 1 << VTMSG_NEVENT_ACK;
        }
        default:
            break;
        }
    } else {
        DEBUG_MORE("\nBad message !!");
    }
    DEBUG_MORE("\n-----------------------------------------------------------------------\n");
    return RTN_CODE_OK;
}


int32_t get_networkInfo(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn = RTN_CODE_OK;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_NETWORK_INFO, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_NETWORK_INFO) {
        int32_t tlvLen0;
        uint8_t *pBuff0;


        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BANDPLAN, &tlvLen0, &pBuff0)) {
            printf("\n\nBandplan   : ");
            if (pBuff0[0] == VTMAC_BANDPLAN_G3_CENELEC) {
                printf("CEN-A\n");
            } else {
                printf("%s", pBuff0[1] == 1 ? "VANGO" : "FCC\n");
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }


        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_EUI64_ADDR, &tlvLen0, &pBuff0)) {
            printf("EUI-64      : ");
            printf("%02x%02x:%02x%02x:%02x%02x:%02x%02x\n",
                   pBuff0[7], pBuff0[6],
                   pBuff0[5], pBuff0[4],
                   pBuff0[3], pBuff0[2],
                   pBuff0[1], pBuff0[0]);
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }


        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_DEVICE_TYPE, &tlvLen0, &pBuff0)) {
            printf("Device      : %s (%s)  ",
                   pBuff0[0] == LOWPAN6_BOOTSTRAPPING_SERVER ? "LBS" : "LBD",
                   pBuff0[1] == REDUCED_FUNCTION_DEVICE ? "RFD" : "FFD");
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_SYS_UPTIME, &tlvLen0, &pBuff0)) {
            printf("(uptime %d sec)\n", *((uint32_t *)pBuff0));
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_PAN_JOIN, &tlvLen0, &pBuff0)) {
            if (tlvLen0 == 0) {
                printf("Not yet joined a pan\n");
            } else if (tlvLen0 == VTMSG_ATTR_PAN_JOINED_LEN) {
                uint32_t tlvLen1;
                uint8_t *pBuff1;

                printf("Already joined a pan\n");

                if (vtmsg_get_tlv(pBuff0, tlvLen0, VTMSG_TLV_TYPE_PAN_ID, &tlvLen1, &pBuff1)) {
                    printf("PAN ID      :  0x%02x%02x\n", pBuff1[1], pBuff1[0]);
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                }


                if (vtmsg_get_tlv(pBuff0, tlvLen0, VTMSG_TLV_TYPE_SHORT_ADDR, &tlvLen1, &pBuff1)) {
                    printf("Short Addr  :  0x%02x%02x\n", pBuff1[0], pBuff1[1]);
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                }


                if (vtmsg_get_tlv(pBuff0, tlvLen0, VTMSG_TLV_TYPE_IPV6_ADDR, &tlvLen1, &pBuff1)) {
                    uint32_t *addr = NULL;
                    addr = (uint32_t *)pBuff1;

                    printf("IPv6 addr   :  %08lx %08lx %08lx %08lx\n", PP_HTONL(addr[0]), PP_HTONL(addr[1]),
                           PP_HTONL(addr[2]), PP_HTONL(addr[3]));
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                }
            } else {
                rtn = RTN_CODE_GENERAL_FAILURE;
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t get_neighborTable_entry_print(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
#if 0   // FIXME, a broken api
    int32_t tlvLen0;
    uint8_t *pBuff0;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        *pEntryIdx = (uint16_t)*pBuff0;
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_NEIGHBOR_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            Neighbor_Table_ST entry;
            memcpy(&entry, pBuff0, tlvLen0);
            printf("[%03d] 0x%02X%02X ->%d %d\r\n",
                   *pEntryIdx,
                   entry.dest_addr[1],
                   entry.dest_addr[0],
                   sizeof(Neighbor_Table_ST),
                   sizeof(ROUTE_TABLE_ST));
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }
#endif
    return rtn;
}

extern current_all_routing_table g_Current_Route;
int32_t get_routingTable_entry_print(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
    int32_t tlvLen0;
    uint8_t *pBuff0;
    uint32_t i;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        memcpy(pEntryIdx, pBuff0, sizeof(uint16_t));
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_ROUTING_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            ROUTE_TABLE_ST entry;
            memcpy(&entry, pBuff0, tlvLen0);
            printf("[%03d](%05dm%02ds):  0x%02x%02x   0x%02x%02x  0x%04x  %2d  %2d",
                   *pEntryIdx, entry.valid_time, entry.valid_sec,
                   entry.dest_short_addr[0], entry.dest_short_addr[1],
                   entry.next_hop_short_addr[0], entry.next_hop_short_addr[1],
                   entry.route_cost, entry.hop_count, entry.weak_link_count);
            /*maintain Routing Table*/

            /*add hop count into joined table*/
            for (i = 0; i < MAX_POLLING_TABLE_SIZE; i++) {
                if (memcmp(joined_dev_table[i].alloc_short_addr, entry.dest_short_addr, 2) == 0) {
                    joined_dev_table[i].route_info.hop_count = entry.hop_count;
                    //FIXME simply s2e table
                    //memcpy(&joined_dev_table[i].route_info, &entry, sizeof(ROUTE_TABLE_ST));
                    //printf("  %02x%02x%02x%02x%02x%02x",
                    //         joined_dev_table[i].eui_64_bit_addr[7],
                    //         joined_dev_table[i].eui_64_bit_addr[6],
                    //         joined_dev_table[i].eui_64_bit_addr[5],
                    //         joined_dev_table[i].eui_64_bit_addr[2],
                    //         joined_dev_table[i].eui_64_bit_addr[1],
                    //         joined_dev_table[i].eui_64_bit_addr[0]);
                    break;
                }
            }
            printf("\n");
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }

    return rtn;
}


int32_t get_neighborTable_entry(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_NEIGHBOR_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        printf("neighbor Table : [000]-[099]\r\n");
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_NEIGHBOR_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_neighborTable_entry_print(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}

int32_t get_routingTable_entry(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_ROUTING_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);

    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        printf("Routing Table : [000]-[099]\n");
        printf("                    Dest   Next_Hop  Cost  Hop_Count  Weak  EUI64\n");
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_ROUTING_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_routingTable_entry_print(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}
int32_t get_routingTable_from_joined_table(int32_t argCnt)
{
    int32_t rtn = 0;
    uint8_t i;
    g_Current_Route.currentNodeCount = 0;
    for (i = 0; i < 100; i++) {
        if (joined_dev_table[i].valid == 1) {
            printf("sAddr:%d,%02X%02X\r\n",
                   i,
                   joined_dev_table[i].alloc_short_addr[0],
                   joined_dev_table[i].alloc_short_addr[1]);
            g_Current_Route.destAddr[g_Current_Route.currentNodeCount].sAddr[0] = joined_dev_table[i].alloc_short_addr[0];
            g_Current_Route.destAddr[g_Current_Route.currentNodeCount].sAddr[1] = joined_dev_table[i].alloc_short_addr[1];
            g_Current_Route.currentNodeCount++;
        }
    }
    return rtn;
}
int32_t get_routingTable(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;

    g_Current_Route.currentNodeCount = 0;

    do {
        rtn = get_routingTable_entry(&entryIdx);
        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
    } while (rtn == 0 && entryIdx < 0xFFFF);
    return rtn;
}



int32_t get_neighborTable(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;

    do {
        rtn = get_neighborTable_entry(&entryIdx);
        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
    } while (rtn == 0 && entryIdx < 0xFFFF);

    return rtn;
}


int32_t start_network(int32_t argCnt)
{
    uint8_t msgType, hdrFlags, role;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_START_NETWORK_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 1) {
        sscanf(cli_argv[1], "%hhu", &role);
    } else {
        printf("Usage:\n");
        printf("nwkstart <role>    0:LBS 1:LBD\n");
        return RTN_CODE_OK;
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_START_NETWORK, VTMSG_ATTR_START_NETWORK_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_START_NETWORK,
                                    VTMSG_ATTR_START_NETWORK_LEN);

    pBuffCurr[0] = role;

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_START_NETWORK_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_START_NETWORK) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } else {
            if (role == 1) {
                printf("System Init, LBD\n");
            } else if (role == 0) {
                printf("System Init, LBS\n");
            } else {
                printf("Unknown role?\n");
            }
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


void _memory_help()
{
    printf("  dw  addr <nr_words>\t\t\tmem-dump\n");
    printf("  ww  addr word\t\tmem-write\n");
}

void _word_dump(const char *indent, uint32_t addr, uint32_t *data, uint32_t nr_words)
{
    uint32_t nr_lines, i;

    if (nr_words <= 0)
        return;

    nr_lines = nr_words / 4;

    while (nr_lines--) {
        printf("%s%08X | %08X %08X - %08X %08X\n", indent, addr, data[0], data[1], data[2], data[3]);
        addr += 16;
        data += 4;
    }

    nr_words = nr_words & 3;

    if (nr_words) {
        printf("%s%08X |", indent, addr);

        for (i = 0; i < nr_words; ++i) {
            if (i == 2)
                printf(" -");
            printf(" %08X", data[i]);
        }

        printf("\n");
    }
}



int32_t mem_dumpword(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_MEM_ADDR_LEN_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    uint32_t addr, nr_words;
    int32_t rtn;

    if (argCnt == 2 || argCnt == 1) {
        sscanf(cli_argv[1], "%08x", &addr);
        if (argCnt == 1) {
            nr_words = 4;
        } else {
            if (cli_argv[2][1] == 'x')
                sscanf(&cli_argv[2][2], "%x", &nr_words);
            else
                sscanf(cli_argv[2], "%x", &nr_words);
            if (nr_words > 0x20) {
                printf("out of range (1~20)");
                return RTN_CODE_OK;
            }
        }
    } else {
        _memory_help();
        return RTN_CODE_OK;
    }

    vtmsg_build_msghdr((uint8_t *)&buffReq, VTMSG_MSG_TYPE_MEM_OP_DUMP_WORD, VTMSG_ATTR_MEM_ADDR_LEN_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_MEM_ADDR_AND_LEN,
                                    VTMSG_ATTR_MEM_ADDR_LEN_LEN);


    memcpy(pBuffCurr, &addr, 4);
    pBuffCurr[4] = nr_words;

    rtn = dev_send((uint8_t *)&buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_MEM_ADDR_LEN_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_MEM_OP_DUMP_WORD) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_MEM_OP_DW, &tlvLen0, &pBuff0)) {
            _word_dump("", addr, (uint32_t *)pBuff0, tlvLen0 / 4);
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}


int32_t mem_writeword(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_MEM_ADDR_DATA_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    uint32_t addr, data;
    int32_t rtn;

    if (argCnt == 2) {
        sscanf(cli_argv[1], "%08x", &addr);
        sscanf(cli_argv[2], "%08x", &data);
    } else {
        _memory_help();
        return RTN_CODE_OK;
    }

    vtmsg_build_msghdr((uint8_t *)&buffReq, VTMSG_MSG_TYPE_MEM_OP_WRITE_WORD, VTMSG_ATTR_MEM_ADDR_DATA_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_MEM_ADDR_AND_DATA,
                                    VTMSG_ATTR_MEM_ADDR_DATA_LEN);


    memcpy(pBuffCurr, &addr, 4);
    memcpy(pBuffCurr + 4, &data, 4);

    rtn = dev_send((uint8_t *)&buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_MEM_ADDR_DATA_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_MEM_OP_WRITE_WORD) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_MEM_OP_WW, &tlvLen0, &pBuff0)) {
            _word_dump("", addr, (uint32_t *)pBuff0, tlvLen0 / 4);
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}


int32_t target_reboot(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_REBOOT_REMAP_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint32_t remap;
    int32_t rtn;

    if (argCnt == 0) {
        remap = 2;
    } else if (argCnt == 1) {
        sscanf(cli_argv[1], "%u", &remap);
    } else {
        printf("reboot [0-2] (remap)\n");
        return RTN_CODE_OK;
    }
    printf("rebooting... \n");

    vtmsg_build_msghdr((uint8_t *)&buffReq, VTMSG_MSG_TYPE_REBOOT, VTMSG_ATTR_REBOOT_REMAP_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_REBOOT_REMAP,
                                    VTMSG_ATTR_REBOOT_REMAP_LEN);


    pBuffCurr[0] = (uint8_t)remap;

    rtn = dev_send((uint8_t *)&buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_REBOOT_REMAP_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    return rtn;
}


void _ver_time(uint32_t version_time)
{
    uint16_t year;
    uint8_t month, day, hour, minute, sec;

    year = 2000 + ((version_time >> 26) & 0x3F);
    month = (version_time) >> 22 & 0xF;
    day = (version_time) >> 17 & 0x1F;
    hour = (version_time) >> 12 & 0x1F;
    minute = (version_time) >> 6 & 0x3F;
    sec = version_time & 0x3F;

    printf("%04d/%02d/%02d %02d:%02d:%02d",
           year, month, day, hour, minute, sec);
}


int32_t get_version_internal(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_GET_VERSION_INTERNAL, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_VERSION_INTERNAL) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_VERSION_INTERNAL, &tlvLen0, &pBuff0)) {
            uint32_t version_bbp_flag;
            printf("\nMagpie PLC Platform - Copyright (c) 2015 Vango Technologies, Inc.\n");
            printf("\nSW version: v6.%d.%d.%d @%08x\n   ", pBuff0[0],
                   pBuff0[1],
                   pBuff0[2],
                   *(uint32_t *)(pBuff0 + 3));
            _ver_time(*(uint32_t *)(pBuff0 + 7));
            printf("%s", pBuff0[11] & 1 ? "- tainted\n" : "\n");
            printf("\nFW version: v6.%d.%d.%d @%08x - %08x\n   ", pBuff0[14],
                   pBuff0[13],
                   pBuff0[12],
                   *(uint32_t *)(pBuff0 + 15),
                   *(uint32_t *)(pBuff0 + 19));
            _ver_time(*(uint32_t *)(pBuff0 + 23));
            printf("\n\n");
            version_bbp_flag = *(uint32_t *)(pBuff0 + 19);
            printf("Chip ver %s%s\n",
                   ((version_bbp_flag & 0xf) == 1) ? "A1" : "",
                   ((version_bbp_flag & 0xf) == 2) ? "A2" : "");
            printf("LD %s%s%s%s\n",
                   ((version_bbp_flag & 0xf0) == 0x00) ? "TestChip B" : "",
                   ((version_bbp_flag & 0xf0) == 0x10) ? "MP A01" : "",
                   ((version_bbp_flag & 0xf0) == 0x20) ? "MP A02" : "",
                   ((version_bbp_flag & 0xf0) == 0x30) ? "MP B01" : "");
            printf("Band support: %s%s%s\n",
                   (version_bbp_flag & 0x100) ? "CEN-A " : "",
                   (version_bbp_flag & 0x200) ? "FCC " : "",
                   (version_bbp_flag & 0x400) ? "Vango " : "");
            printf("Others: %s%s\n",
                   (version_bbp_flag & 0x1000) ? "Data coherent mode, " : "",
                   (version_bbp_flag & 0x2000) ? "Two RS blocks, " : "");
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}


int32_t bbp_nv_config(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                    + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    int32_t rtn, value, i;
    uint32_t para = sizeof(bbpnv_param) / sizeof(bbpnv_param[0]) + 1;

    for (i = 0; i < sizeof(bbpnv_param) / sizeof(bbpnv_param[0]); i++) {
        if (argCnt == 2 && strcmp(cli_argv[1], bbpnv_param[i].str) == 0) {
            sscanf(cli_argv[2], "%d", &value);
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_BBP_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                                + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = para = i;
            pBuffCurr++;
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE,
                                            VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);

            memcpy(pBuffCurr, &value, 4);

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(buffReq));
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else if (argCnt == 0) {
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_BBP_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = i;

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);

            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else {
            continue;
        }

        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_BBP_SETTINGS) {
            pBuffCurr = pBuff;
            int32_t tlvLen0;
            uint8_t *pBuff0;
            if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pBuff0)) {
                uint8_t idx = pBuff0[0];
                if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pBuff0)) {
                    memcpy(&value, pBuff0, 4);
                    printf("[%02d] %s: %d\n", idx, bbpnv_param[idx].str, value);
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                    break;
                }
            } else {
                rtn = RTN_CODE_TLV_MISSING;
                break;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
        free_dev_resp(pBuff);
    }

    if (argCnt == 2 && para > sizeof(bbpnv_param) / sizeof(bbpnv_param[0])) {
        printf("BBP_NV para not found!!\n");
        return RTN_CODE_OK;
    }

    return rtn;
}


int32_t app_nv_config(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                    + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    int32_t rtn, value, i;
    uint32_t para = sizeof(appnv_param) / sizeof(appnv_param[0]) + 1;

    for (i = 0; i < sizeof(appnv_param) / sizeof(appnv_param[0]); i++) {
        if (argCnt == 2 && strcmp(cli_argv[1], appnv_param[i].str) == 0) {
            sscanf(cli_argv[2], "%d", &value);
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_APP_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                                + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = para = i;
            pBuffCurr++;
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE,
                                            VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);

            memcpy(pBuffCurr, &value, 4);

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(buffReq));
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else if (argCnt == 0) {
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_APP_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = i;

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);

            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else {
            continue;
        }

        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_APP_SETTINGS) {
            pBuffCurr = pBuff;
            int32_t tlvLen0;
            uint8_t *pBuff0;
            if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pBuff0)) {
                uint8_t idx = pBuff0[0];
                if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pBuff0)) {
                    memcpy(&value, pBuff0, 4);
                    printf("[%02d] %s: %d\n", idx, appnv_param[idx].str, value);
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                    break;
                }
            } else {
                rtn = RTN_CODE_TLV_MISSING;
                break;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
        free_dev_resp(pBuff);
    }

    if (argCnt == 2 && para > sizeof(appnv_param) / sizeof(appnv_param[0])) {
        printf("APP NV para not found!!\n");
        return RTN_CODE_OK;
    }

    return rtn;
}


int32_t csma_config(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                    + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    int32_t rtn, value, i;
    uint32_t para = sizeof(csma_param) / sizeof(csma_param[0]) + 1;

    for (i = 0; i < sizeof(csma_param) / sizeof(csma_param[0]); i++) {
        if (argCnt == 2 && strcmp(cli_argv[1], csma_param[i].str) == 0) {
            sscanf(cli_argv[2], "%d", &value);
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_CSMA_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                                + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = para = i;
            pBuffCurr++;
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE,
                                            VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);

            memcpy(pBuffCurr, &value, 4);

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(buffReq));
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else if (argCnt == 0) {
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_CSMA_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = i;

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);

            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else {
            continue;
        }

        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_CSMA_SETTINGS) {
            pBuffCurr = pBuff;
            int32_t tlvLen0;
            uint8_t *pBuff0;
            if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pBuff0)) {
                uint8_t idx = pBuff0[0];
                if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pBuff0)) {
                    memcpy(&value, pBuff0, 4);
                    printf("[%02d] %s: %d\n", idx, csma_param[idx].str, value);
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                    break;
                }
            } else {
                rtn = RTN_CODE_TLV_MISSING;
                break;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
        free_dev_resp(pBuff);
    }

    if (argCnt == 2 && para > sizeof(csma_param) / sizeof(csma_param[0])) {
        printf("CSMA para not found!!\n");
        return RTN_CODE_OK;
    }

    return rtn;
}


int32_t zerox_config(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                    + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    int32_t rtn, value, i;
    uint32_t para = sizeof(zerox_param) / sizeof(zerox_param[0]) + 1;

    for (i = 0; i < sizeof(zerox_param) / sizeof(zerox_param[0]); i++) {
        if (argCnt == 2 && strcmp(cli_argv[1], zerox_param[i].str) == 0) {
            sscanf(cli_argv[2], "%d", &value);
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_ZEROX_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN
                                + VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = para = i;
            pBuffCurr++;
            pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE,
                                            VTMSG_ATTR_COMMON_CONFIG_VALUE_LEN);

            memcpy(pBuffCurr, &value, 4);

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(buffReq));
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else if (argCnt == 0) {
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_ZEROX_SETTINGS,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = i;

            rtn = dev_send((uint8_t *)&buffReq,
                           sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);

            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        } else {
            continue;
        }

        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_ZEROX_SETTINGS) {
            pBuffCurr = pBuff;
            int32_t tlvLen0;
            uint8_t *pBuff0;
            if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pBuff0)) {
                uint8_t idx = pBuff0[0];
                if (vtmsg_get_tlv(pBuffCurr, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pBuff0)) {
                    memcpy(&value, pBuff0, 4);
                    if (idx == 6) {
                        printf("[%02d] %s: %d  %s\n", idx, zerox_param[idx].str, value, zerox_param[idx].help);
                    } else {
                        printf("[%02d] %s: 0x%08x  %s\n", idx, zerox_param[idx].str, value, zerox_param[idx].help);
                    }
                } else {
                    rtn = RTN_CODE_TLV_MISSING;
                    break;
                }
            } else {
                rtn = RTN_CODE_TLV_MISSING;
                break;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
        free_dev_resp(pBuff);
    }

    if (argCnt == 2 && para > sizeof(zerox_param) / sizeof(zerox_param[0])) {
        printf("ZEROX para not found!!\n");
        return RTN_CODE_OK;
    }

    return rtn;
}


int32_t packet_cntr(int32_t argCnt)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_COMMON_CONFIG_IDX_LEN] = {};
    uint8_t msgType, hdrFlags;
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    int32_t i, rtn;
    uint32_t *cntr_field = (uint32_t *)malloc(_COUNT_NETWORK_PACKET_CNTR_TABLE * sizeof(uint32_t));

    if (cntr_field == NULL) {
        return RTN_CODE_MEM_FAILED;
    } else {
        memset(cntr_field, 0, _COUNT_NETWORK_PACKET_CNTR_TABLE * sizeof(uint32_t));
    }

    for (i = 0; i < _COUNT_NETWORK_PACKET_CNTR_TABLE; i++) {
        if (argCnt == 1 && strcmp(cli_argv[1], "c") == 0) {
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_NETWORK_CNTR,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = 0xff; //0xff for clean
            rtn = dev_send((uint8_t *)&buffReq, sizeof(buffReq));
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
            return rtn;
        } else {
            vtmsg_build_msghdr((uint8_t *)&buffReq,
                                VTMSG_MSG_TYPE_NETWORK_CNTR,
                                VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                            VTMSG_TLV_TYPE_COMMON_CONFIG_IDX,
                                            VTMSG_ATTR_COMMON_CONFIG_IDX_LEN);
            pBuffCurr[0] = i;
            rtn = dev_send((uint8_t *)&buffReq, sizeof(buffReq));
            if (rtn < 0) {
                rtn = RTN_CODE_MESSAGE_SEND_FAILED;
                return rtn;
            }
        }


        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_NETWORK_CNTR) {
            int32_t tlvLen0;
            uint8_t *pBuff0;
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_IDX, &tlvLen0, &pBuff0)) {
                if (i != pBuff0[0]) {
                    rtn = RTN_CODE_CMD_FAILED;
                }
            } else {
                rtn = RTN_CODE_TLV_MISSING;
            }
            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_COMMON_CONFIG_VALUE, &tlvLen0, &pBuff0)) {
                memcpy(&cntr_field[i], pBuff0, sizeof(uint32_t));
            } else {
                rtn = RTN_CODE_TLV_MISSING;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
        free_dev_resp(pBuff);
    }

    printf("Packet Counter" "\n");
    printf("tx submitted         : %10u" "\n", cntr_field[tx_submitted]);
    printf("tx                   : %10u" "\n", cntr_field[tx]);
    printf("rx                   : %10u" "\n", cntr_field[rx]);
    printf("   data              : %10u(normal)" "\n"
           "                       %10u(bypass)" "\n"
           "                       %10u(raw)" "\n",
           cntr_field[rx_ctrl_normal_rx], cntr_field[rx_ctrl_bypass_rx], cntr_field[rx_ctrl_raw_rx]);
    printf("   data_fcs          : %10u(ok)    ,%10u(fail)" "\n", cntr_field[rx_fcs_ok], cntr_field[rx_fcs_fail]);
    printf("   data_hcs          : %10u(ok)    ,%10u(fail)" "\n", cntr_field[rx_hcs_ok], cntr_field[rx_hcs_fail]);
    printf("   data_mic          : %10u(ok)    ,%10u(fail)" "\n", cntr_field[rx_mic_ok], cntr_field[rx_mic_fail]);
    printf("   ctrl              : %10u(ack)   ,%10u(nack)" "\n", cntr_field[rx_ctrl_ack], cntr_field[rx_ctrl_nack]);

    printf("\n" "IRQ det_pmb/rx_cmp/urgentIn   : %10u / %10u / %10u" "\n", cntr_field[detect_pmb], cntr_field[rx_cmp], cntr_field[urgent_in]);
    printf("    tx_cmp           : %10u" "\n", cntr_field[tx_cmp]);
    printf("    tx ack/nack/ackto: %10u / %10u / %10u(%u)" "\n", cntr_field[tx_ack],
           cntr_field[tx_nack], cntr_field[tx_ack_timeout], cntr_field[tx_ack_timeout_ignore]);
    printf("    tx_csma          : %10u(success),%10u(fail)" "\n", cntr_field[tx_csma_success], cntr_field[tx_csma_fail]);
    printf("    tx_retry_fail    : %10u" "\n", cntr_field[tx_retry_fail]);
    printf("tx_sw_ack_timeout    : %10u" "\n", cntr_field[tx_sw_ack_timeout]);
    printf("tx_sw_tx_cmp_timeout : %10u" "\n", cntr_field[tx_sw_tx_cmp_timeout]);

    printf("\n" "lowpan6 rx           : %10u" "\n", cntr_field[lowpan6_rx]);
    printf("        rx esc       : %10u(route)" "\n"
           "                       %10u(bootstrap)" "\n"
           "                       %10u(vango)" "\n",
           cntr_field[lowpan6_rx_esc_route], cntr_field[lowpan6_rx_esc_bootstrap], cntr_field[lowpan6_rx_esc_vango]);
    printf("        rx drop      : %10u" "\n", cntr_field[lowpan6_rx_drop]);
    printf("        rx drop mcast: %10u" "\n", cntr_field[lowpan6_rx_drop_multicast]);
    printf("        forward ucast: %10u, %10u(fail)" "\n", cntr_field[lowpan6_rx_forward_unicast],
           cntr_field[lowpan6_rx_forward_unicast_fail]);
    printf("        forward bcast: %10u" "\n", cntr_field[lowpan6_rx_forward_broadcast]);

    printf("\n" "ICMP rx echo request : %10u" "\n", cntr_field[icmp_rx_echo_request]);
    printf("        echo reply   : %10u" "\n", cntr_field[icmp_rx_echo_reply]);
    printf("     tx echo request : %10u" "\n", cntr_field[icmp_tx_echo_request]);
    printf("        echo reply   : %10u" "\n", cntr_field[icmp_tx_echo_reply]);

    printf("\n" "SDK UART tx          : %10u" "\n", cntr_field[sdk_uart_tx_packet]);
    printf("SDK UART rx          : %10u" "\n", cntr_field[sdk_uart_rx_packet]);
    printf("SDK PLC tx           : %10u" "\n", cntr_field[sdk_plc_tx_packet]);
    printf("SDK PLC rx           : %10u" "\n", cntr_field[sdk_plc_rx_packet]);

    free(cntr_field);
    return rtn;
}


int32_t get_version(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_GET_VERSION_INFO, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_VERSION_INFO) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_VERSION_INFO, &tlvLen0, &pBuff0)) {
            printf("\nMagpie PLC Platform - Copyright (c) 2015 Vango Technologies, Inc.\n");
            printf("\nversion: v6.%d.%d.%d_%d.%d.%d   ", pBuff0[0],
                   pBuff0[1],
                   pBuff0[2],
                   pBuff0[3],
                   pBuff0[4],
                   pBuff0[5]);
            _ver_time(*(uint32_t *)(pBuff0 + 6));
            printf("\n");
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}


int32_t check_dcu_support(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_DCU_SUPPORT, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_DCU_SUPPORT) {
        int32_t tlvLen0;
        uint8_t *pBuff0;
        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_DCU_SUPPROT_FLAGS, &tlvLen0, &pBuff0)) {
            uint8_t i, count = 1, mapgie_found = 0;
            printf("DCU\nList available MAGPIE under DCU:\n");
            for (i = 0; i < DCU_IF_TOTAL; i++) {
                if (pBuff0[0] & 1 << i) {
                    printf("%d.  MAGPIE [%d]\n", count, i);
                    mapgie_found++;
                    count++;
                }
            }

            if (mapgie_found > 0) {
                char lineBuff[16];
                uint8_t idx;
                printf("Please select index of MAGPIE: (0-2)\nEnter 3 or others is DCU mode\n");
                fgets((char *)lineBuff, 16, stdin);
                sscanf(lineBuff, "%u", &idx);
                if (idx >= 0 && idx < 3) {
                    printf("MAGPIE [%u] selected!\n", idx);
                    g_vtmsg_msg_devid = idx;
                } else {
                    printf("DCU mode!\n", idx);
                    g_vtmsg_msg_devid = 3;
                    g_cmd_level = COMMAND_DCU;
                }
                g_vtmsg_msg_source = VTMSG_IF_SPI;
            } else {
                printf("\tnot found\n");
            }
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}




int32_t send_bbp_sig_req(uint8_t bbp_sig_type, uint16_t pEntryIdx)
{
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_BBP_SIG_TYPE_LEN] = {};
    uint8_t *pBuffCurr = NULL;

    int32_t rtn;

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_BBP_SIG, VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_BBP_SIG_TYPE_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_BBP_SIG_TYPE,
                                    VTMSG_ATTR_BBP_SIG_TYPE_LEN);
    memcpy(pBuffCurr, &bbp_sig_type, sizeof(uint8_t));
    pBuffCurr += sizeof(uint8_t);
    pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, &pEntryIdx, sizeof(uint16_t));
    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN + VTMSG_ATTR_BBP_SIG_TYPE_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }
    return rtn;
}


int32_t print_bbp_sig_data(uint8_t bbp_sig_type, uint32_t data_counter)
{
    int i;
    int32_t rtn;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t pyldLen = 0;
    uint32_t sig_data;
    printf("Capture Start\n");
    for (i = 0; i < data_counter; i++) {
        rtn = send_bbp_sig_req(bbp_sig_type, i);
        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }
        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_BBP_SIG) {
            int32_t tlvLen0;
            uint8_t *pBuff0;

            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BBP_SIG_DATA, &tlvLen0, &pBuff0)) {
                memcpy(&sig_data, pBuff0, sizeof(uint32_t));
                printf("%d\n", sig_data);
            } else {
                rtn = RTN_CODE_TLV_MISSING;
            }
            free_dev_resp(pBuff);
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }
    }
    printf("Capture Done\n");
}


int32_t get_bbp_sig0(void)
{
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;
    uint32_t band_plane, rx_gain, rx_pwr;
    uint8_t bbp_sig_type = BBP_SIG_TYPE_PREAMBLE;
    uint16_t pEntryIdx = 0xffff;

    rtn = send_bbp_sig_req(bbp_sig_type, pEntryIdx);
    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }
    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_BBP_SIG) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BBP_SIG0_BAND, &tlvLen0, &pBuff0)) {
            memcpy(&band_plane, pBuff0, sizeof(uint32_t));
            printf("band_plane = %s \n", band_plane == 1 ? "FCC" : "CENELEC");
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BBP_SIG0_RX_GAIN, &tlvLen0, &pBuff0)) {
            memcpy(&rx_gain, pBuff0, sizeof(uint32_t));
            printf("rx_gain:%08x \n", rx_gain);
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BBP_SIG0_RX_PWR, &tlvLen0, &pBuff0)) {
            memcpy(&rx_pwr, pBuff0, sizeof(uint32_t));
            printf("rx_pwr:%08x \n", rx_pwr);
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }
    free_dev_resp(pBuff);
    rtn = print_bbp_sig_data(bbp_sig_type, 256);

    return rtn;
}


int32_t get_bbp_sig1(void)
{
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;
    uint16_t analog_RX_gain, digital_RX_gain;
    uint8_t bbp_sig_type = BBP_SIG_TYPE_NOISE_FLOOR;
    uint16_t pEntryIdx = 0xffff;

    rtn = send_bbp_sig_req(bbp_sig_type, pEntryIdx);
    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }
    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_BBP_SIG) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_BBP_SIG1_PMB_MISC, &tlvLen0, &pBuff0)) {
            memcpy(&analog_RX_gain, pBuff0, sizeof(uint16_t));
            memcpy(&digital_RX_gain, pBuff0 + sizeof(uint16_t), sizeof(uint16_t));
            printf("a_rx_gain:%08x d_rx_gain:%08x\n", analog_RX_gain, digital_RX_gain);
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    rtn = print_bbp_sig_data(bbp_sig_type, 12800);
    return rtn;
}


int32_t get_bbp_sig(int32_t argCnt)
{
    uint8_t bbp_sig_type = BBP_SIG_TYPE_PREAMBLE;
    int32_t rtn;
    if (argCnt == 1) {
        sscanf(cli_argv[1], "%x", &bbp_sig_type);
    } else {
        printf("Usage:", DB_PRINT);
        printf("bbp_sig <opetion> 0:PMB 1:Noise");
        return RTN_CODE_OK;
    }
    switch (bbp_sig_type) {
    case BBP_SIG_TYPE_PREAMBLE:
        rtn = get_bbp_sig0();
        break;
    case BBP_SIG_TYPE_NOISE_FLOOR:
        rtn = get_bbp_sig1();
        break;
    default:
        printf("Usage:", DB_PRINT);
        printf("bbp_sig <opetion> 0:PMB 1:Noise");
        return RTN_CODE_OK;
    }
}


int32_t get_nei_eui_entry_print(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
    int32_t tlvLen0;
    uint8_t *pBuff0;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        memcpy(pEntryIdx, pBuff0, sizeof(uint16_t));
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_NEI_EUI_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            struct NEI_EUI64 entry;
            int j;
            memcpy(&entry, pBuff0, tlvLen0);
            printf("[%02d]", *pEntryIdx);
            if (entry.valid == 1) {
                for (j = 0; j < 8; j++)
                    printf("0x%02x ", entry.eui64[j]);
                printf("|  %04d", entry.rx_eui_cnt);
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }

    return rtn;
}

int32_t get_nei_eui_Table_entry(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_NEI_EUI_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);

    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        printf("Neighbor EUI64 Table \n");
        printf("                  EUI64                     |  Rx Cnt\n");
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_NEI_EUI_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_nei_eui_entry_print(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t get_eni_eui_Table(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;

    do {
        rtn = get_nei_eui_Table_entry(&entryIdx);
        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
    } while (rtn == 0 && entryIdx < 0xFFFF);
    return rtn;
}

int32_t get_joined_entry_print_clou_failed(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
    int32_t tlvLen0;
    uint8_t *pBuff0;
    FILE *fp = NULL;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        *pEntryIdx = (uint16_t)*pBuff0;
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    fp = fopen("/var/www/html/joinedlist.html", "a+");
    if (fp == NULL) {
        return RTN_CODE_MEM_FAILED;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_S2E_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            struct lbs_alloc_short_addr_table entry;
            memcpy(&entry, pBuff0, tlvLen0);
            if (entry.valid == 1) {
                fprintf(fp, "<br><input type=\"checkbox\">%02x%02x%02x%02x%02x%02x\n",
                        entry.eui_64_bit_addr[7],
                        entry.eui_64_bit_addr[6],
                        entry.eui_64_bit_addr[5],
                        entry.eui_64_bit_addr[2],
                        entry.eui_64_bit_addr[1],
                        entry.eui_64_bit_addr[0]);
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }

    fclose(fp);

    return rtn;
}


int32_t get_joined_table_entry_clou_failed(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_JOINED_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);

    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        printf("write to failed list...\n");
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_JOINED_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_joined_entry_print_clou_failed(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t get_joined_table_clou_failed(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;
    memset(joined_dev_table, 0, sizeof(joined_dev_table));

    do {
        rtn = get_joined_table_entry_clou_failed(&entryIdx);
        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
    } while (rtn == 0 && entryIdx < 0xFFFF);

    system("sort /var/www/html/joinedlist.html -o /var/www/html/joinedlist.html");
    return rtn;
}



int32_t get_joined_entry_print_clou(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
    int32_t tlvLen0;
    uint8_t *pBuff0;
    FILE *fp = NULL;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        *pEntryIdx = (uint16_t)*pBuff0;
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    fp = fopen("/var/www/html/meter.html", "a+");
    if (fp == NULL) {
        return RTN_CODE_MEM_FAILED;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_S2E_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            struct lbs_alloc_short_addr_table entry;
            memcpy(&entry, pBuff0, tlvLen0);
            if (entry.valid == 1) {
                fprintf(fp, "%02x%02x%02x%02x%02x%02x  <br>\n",
                        entry.eui_64_bit_addr[7],
                        entry.eui_64_bit_addr[6],
                        entry.eui_64_bit_addr[5],
                        entry.eui_64_bit_addr[2],
                        entry.eui_64_bit_addr[1],
                        entry.eui_64_bit_addr[0]);
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }

    fclose(fp);

    return rtn;
}


int32_t get_joined_table_entry_clou(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_JOINED_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);

    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        printf("write to upgrade list...\n");
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_JOINED_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_joined_entry_print_clou(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t get_joined_table_clou(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;
    memset(joined_dev_table, 0, sizeof(joined_dev_table));

    do {
        rtn = get_joined_table_entry_clou(&entryIdx);
        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
    } while (rtn == 0 && entryIdx < 0xFFFF);
    return rtn;
}



int32_t get_joined_entry_print(uint8_t *pBuff, uint32_t len, uint16_t *pEntryIdx)
{
    int32_t rtn = RTN_CODE_OK;
    int32_t tlvLen0;
    uint8_t *pBuff0;

    *pEntryIdx = 0xFFFF;

    if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_CMD_INDEX, &tlvLen0, &pBuff0)) {
        memcpy(pEntryIdx, pBuff0, sizeof(uint16_t));
    } else {
        return RTN_CODE_TLV_MISSING;
    }

    if (len > 0) {
        if (vtmsg_get_tlv(pBuff, len, VTMSG_TLV_TYPE_S2E_TBL_ENTRY, &tlvLen0, &pBuff0)) {
            struct lbs_alloc_short_addr_table entry;
            int j;
            memcpy(&entry, pBuff0, tlvLen0);
            printf("[%02d]", *pEntryIdx);
            if (entry.valid == 1) {
                int i;
                printf("0x%02x%02x    ", entry.alloc_short_addr[0], entry.alloc_short_addr[1]);
                for (j = 0; j < 8; j++)
                    printf("0x%02x ", entry.eui_64_bit_addr[j]);
                printf("    %d", entry.valid);

                printf("\n");
                for (i = 0; i < MAX_POLLING_TABLE_SIZE; i++) {
                    if (joined_dev_table[i].valid != 1) {
                        memcpy(&joined_dev_table[i], &entry, tlvLen0);
                        joined_dev_table[i].valid = 1;
                        break;
                    }
                }
            }
        } else {
            rtn = RTN_CODE_TLV_MISSING;
        }
    }

    return rtn;
}


int32_t get_joined_table_entry(uint16_t *pEntryIdx)
{
    uint8_t msgType, hdrFlags;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_GET_JOINED_TABLE, VTMSG_ATTR_CMD_INDEX_LEN);

    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_CMD_INDEX,
                                    VTMSG_ATTR_CMD_INDEX_LEN);

    memcpy(pBuffCurr, pEntryIdx, sizeof(uint16_t));

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_INDEX_LEN);

    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);

    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (*pEntryIdx == 0) {
        printf("LBS Short Address to EUI64 Next hop address Table        valid \n");
        printf("================================\n");
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_JOINED_TABLE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_SUCCESS) {
            rtn = get_joined_entry_print(pBuff, pyldLen, pEntryIdx);
        } else if (vtmsg_tlv_sts_handler(pBuff, pyldLen) == VTMSG_STS_END_OF_ENTRY) {
            printf("\n\n");
            *pEntryIdx = 0xffff;
            rtn = RTN_CODE_OK;
        } else {
            rtn = RTN_CODE_CMD_FAILED;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}


int32_t get_joined_table(int32_t argCnt)
{
    uint16_t entryIdx = 0;
    int32_t rtn = 0;
    memset(joined_dev_table, 0, sizeof(joined_dev_table));

    do {
        rtn = get_joined_table_entry(&entryIdx);
        if (entryIdx < 0xFFFF) {
            entryIdx++;
        }
    } while (rtn == 0 && entryIdx < 0xFFFF && entryIdx < MAX_POLLING_TABLE_SIZE);
    return rtn;
}


enum settings_configuration {
    BOOTSTRAP_ONOFF = (1 << 0),
};
int32_t do_configuration(int32_t argCnt)
{
    int32_t rtn = 0, para1 = 0, current_argCnt = 1;
    uint32_t settings = 0;
    if (argCnt >= 2) {
        while (current_argCnt <= argCnt) {
            char *cmd = (char *)cli_argv[current_argCnt];
            if (strncmp(cmd, "bsoff", 2) == 0) {
                settings |= BOOTSTRAP_ONOFF;
                current_argCnt++;
                if (current_argCnt <= argCnt) sscanf(cli_argv[current_argCnt], "%u", &para1);
            }
            current_argCnt++;
        }
    } else {
        printf("device configuration\n");
        printf("Usage: config [action] [para...]\n");
        printf("\tAction:\n");
        printf("\tbsoff [1/0], disable/enable bootstrapping\n");
        return RTN_CODE_OK;
    }

    if (settings & BOOTSTRAP_ONOFF) {
        uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN] = {};
        uint8_t msgType, hdrFlags;
        int32_t tlvLen0;
        uint8_t *pBuff0;
        uint8_t *pBuffCurr = NULL;
        uint8_t *pBuff = NULL;
        int32_t pyldLen = 0;

        pBuffCurr = vtmsg_build_msghdr(buffReq,
                                        VTMSG_MSG_TYPE_SYS_CONFIGURATION,
                                        VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr = vtmsg_build_tlvhdr(pBuffCurr,
                                        VTMSG_TLV_TYPE_BOOTSTRAP_ONOFF,
                                        VTMSG_ATTR_CMD_RESULT_LEN);
        pBuffCurr[0] = para1;

        rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_CMD_RESULT_LEN);

        if (rtn < 0) {
            rtn = RTN_CODE_MESSAGE_SEND_FAILED;
            return rtn;
        }

        rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
        if (rtn < 0) {
            rtn = RTN_CODE_NO_RESPONSE;
            return rtn;
        }

        if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_SYS_CONFIGURATION) {
            int32_t tlvLen0;
            uint8_t *pBuff0;

            if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_CMD_RESULT, &tlvLen0, &pBuff0)) {
            } else {
                rtn = RTN_CODE_TLV_MISSING;
            }
        } else {
            rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
        }

        free_dev_resp(pBuff);
    }

    return rtn;
}

int32_t lbslist(int32_t argCnt)
{
    vtmsg_msgHdr_s msgHdr;
    uint8_t msgType, hdrFlags;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;
    uint8_t i = 0;

    vtmsg_build_msghdr((uint8_t *)&msgHdr, VTMSG_MSG_TYPE_GET_LBS_INFO, 0x0);

    rtn = dev_send((uint8_t *)&msgHdr, sizeof(vtmsg_msgHdr_s));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_GET_LBS_INFO) {
        int32_t tlvLen0;
        uint8_t *pBuff0;

        if (vtmsg_get_tlv(pBuff, pyldLen, VTMSG_TLV_TYPE_LBS_TBL_ENTRY, &tlvLen0, &pBuff0))
        {
            printf("\nlbs table\n");
            printf("\nIndex    PAN ID    Short Addr    Route Cost\n");	
            for(i= 0; i< 5; i++)
            {
                printf(" [%d]     ", i);
                printf("0x%2X%2X      ", pBuff0[1 + i*6], pBuff0[0+ i*6] );
                printf("0x%2X%2X     ", pBuff0[3 + i*6], pBuff0[2+ i*6] );
                printf("%5d      ", ((pBuff0[5 + i*6]<< 8) + pBuff0[4+ i*6]));
                printf("\n");
            }                      
        } 
        else 
        {
            rtn = RTN_CODE_TLV_MISSING;
        }
    } 
    else 
    {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);

    return rtn;
}


int32_t joinpan(int32_t argCnt)
{
    uint8_t msgType, hdrFlags, idx;
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_SEND_JOIN_PAN_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 1) {
        sscanf(cli_argv[1], "%hhu", &idx);
    } else {
        printf("Please input index\n");
        return RTN_CODE_OK;
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_SEND_JOIN_NETWORK_CMD, VTMSG_SEND_JOIN_PAN_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
            VTMSG_TLV_TYPE_SEND_JOIN_PAN,
            VTMSG_SEND_JOIN_PAN_LEN);

    pBuffCurr[0] = idx;

    rtn = dev_send(buffReq, sizeof(vtmsg_msgHdr_s) + VTMSG_SEND_JOIN_PAN_LEN);
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_SEND_JOIN_NETWORK_CMD) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } 
        else 
        {			
            printf("join pan sucess\n");
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}

int32_t route_request(int32_t argCnt)
{
    uint8_t msgType, hdrFlags;
    uint8_t addr[2];
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_SHORT_ADDR_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 0){
        printf("Route discovery requset usage:\n");
        printf("route a [short addr 0x??]\n");
        return get_routingTable(0);
    } else if (argCnt == 2) {
        if (cli_argv[1][0]== 'a' && (cli_argv[2][1]== 'x' || cli_argv[2][1]== 'X')) {
            uint32_t input;
            sscanf((char *)&cli_argv[1][2], "%x", &input);
            addr[0] = (uint8_t)((input & 0x0000ff00) >> 8);
            addr[1] = (uint8_t)input & 0x000000ff;
        }
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_REQUEST_ROUTE, VTMSG_ATTR_SHORT_ADDR_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_SHORT_ADDR,
                                    VTMSG_ATTR_SHORT_ADDR_LEN);

    memcpy(pBuffCurr, addr, sizeof(addr));

    rtn = dev_send(buffReq, sizeof(buffReq));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_REQUEST_ROUTE) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } else {
            rtn = RTN_CODE_OK;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}

int32_t set_pan_id(int32_t argCnt)
{
    uint8_t msgType, hdrFlags;
    uint8_t addr[2];
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_PAN_ID_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 0){
        printf("Panid usage:\n");
        printf("panid [panid 0x??]\n");
        return RTN_CODE_OK;
    } else if (argCnt == 1) {
        if ((cli_argv[1][1]== 'x' || cli_argv[1][1]== 'X')) {
            uint32_t input;
            sscanf((char *)&cli_argv[1][2], "%x", &input);
            addr[1] = (uint8_t)((input & 0x0000ff00) >> 8);
            addr[0] = (uint8_t)input & 0x000000ff;
        } else {
        	printf("Panid usage:\n");
        	printf("panid [panid 0x??]\n");
        	return RTN_CODE_OK;
		}
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_SET_PAN_ID, VTMSG_ATTR_PAN_ID_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_PAN_ID,
                                    VTMSG_ATTR_PAN_ID_LEN);

    memcpy(pBuffCurr, addr, sizeof(addr));

    rtn = dev_send(buffReq, sizeof(buffReq));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_SET_PAN_ID) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } else {
            rtn = RTN_CODE_OK;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;	
}

int32_t path_request(int32_t argCnt)
{
    uint8_t msgType, hdrFlags;
    uint8_t addr[2];
    uint8_t buffReq[sizeof(vtmsg_msgHdr_s) + VTMSG_ATTR_SHORT_ADDR_LEN] = {};
    uint8_t *pBuffCurr = NULL;
    uint8_t *pBuff = NULL;
    int32_t rtn;
    int32_t pyldLen = 0;


    if (argCnt == 0){
        printf("Path discovery requset usage:\n");
        printf("route a [short addr 0x??]\n");
        return RTN_CODE_OK;
    } else if (argCnt == 1) {
        if (cli_argv[1][1]== 'x' || cli_argv[1][1]== 'X') {
            uint32_t input;
            sscanf((char *)&cli_argv[1][2], "%x", &input);
            addr[0] = (uint8_t)((input & 0x0000ff00) >> 8);
            addr[1] = (uint8_t)input & 0x000000ff;
        }
    }

    vtmsg_build_msghdr(buffReq, VTMSG_MSG_TYPE_REQUEST_PATH, VTMSG_ATTR_SHORT_ADDR_LEN);
    pBuffCurr = vtmsg_build_tlvhdr(buffReq + sizeof(vtmsg_msgHdr_s),
                                    VTMSG_TLV_TYPE_SHORT_ADDR,
                                    VTMSG_ATTR_SHORT_ADDR_LEN);

    memcpy(pBuffCurr, addr, sizeof(addr));

    rtn = dev_send(buffReq, sizeof(buffReq));
    if (rtn < 0) {
        rtn = RTN_CODE_MESSAGE_SEND_FAILED;
        return rtn;
    }

    rtn = dev_get_resp(&msgType, &pBuff, &pyldLen, &hdrFlags);
    if (rtn < 0) {
        rtn = RTN_CODE_NO_RESPONSE;
        return rtn;
    }

    if (pyldLen > 0 && msgType == VTMSG_MSG_TYPE_REQUEST_PATH) {
        if (vtmsg_tlv_sts_handler(pBuff, pyldLen) != VTMSG_STS_SUCCESS) {
            rtn = RTN_CODE_CMD_FAILED;
        } else {
            rtn = RTN_CODE_OK;
        }
    } else {
        rtn = RTN_CODE_UNEXPECTED_MESSAGE_TYPE;
    }

    free_dev_resp(pBuff);
    return rtn;
}

