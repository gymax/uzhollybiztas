#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "main.h"
#include "mbox.h"
#include "slip_dev.h"
#include "app.h"
#include "utility.h"
#include "vtmsg.h"
#include "log.h"

#if OS_WIN32
#include <windows.h>
#include <tchar.h>
#define __func__ __FUNCTION__
#else
#include <unistd.h> /*  misc. UNIX functions      */
#include <syslog.h>
#endif



uint8_t dev_rxBuff[BUF_LEN_DEV_RX];
int32_t devBuff_idx = 0;


uint8_t lineBuff[256];
uint8_t lastLineBuff[256] = "\0";
uint32_t cli_argc;
uint8_t *cli_argv[CONFIG_CONSOLE_ARGC_MAX];
vtmsg_msgHdr_s *pHifHdr;

uint32_t g_cmd_level = COMMAND_NORMAL;

struct slip_dev *slipdev = NULL;

#if OS_WIN32
HANDLE cli_threadInfo;
#else
pthread_t cli_threadInfo;
#endif
extern uint8_t upg_running;
mbox_t mboxCb;

int debug_on = 0;

#if 0
typedef struct {
    uint8_t sts;
    uint8_t msgType;
    uint8_t hdrFlags;
    int32_t pyldLen;
    uint8_t *pBuff;
} vtmsg_msg_s;
#endif

vtmsg_msg_s gMboxMsgBuff;


typedef int32_t (*cliCmdHndlr_t) (int32_t);


typedef struct {
    char *cmdStr;
    cliCmdHndlr_t cmdFn;
    uint8_t cmd_level;
} cliCmd;


int32_t reset_dev(int32_t argCnt)
{
    uint8_t magiccode[3] = {0x0d, 0xc0, 0xAA};
    uint32_t i;
    for (i = 0; i < 300; i++) {
        slipdev->write((char *)&magiccode[2], 1);
    }

    dev_send(NULL, 0);
    dev_send(NULL, 0);
    dev_send(NULL, 0);
    dev_send(NULL, 0);
    printf("reset uart...");
}


int32_t set_target_dest(int32_t argCnt)
{
    if (argCnt == 0) {
        printf("Current vtmsg destination is ");

        if (!g_vtmsg_msg_outgoing) 
            printf("local");
        else if (msg_outgoing_addr_len == 2)
            printf("0x%02x%02x\n", msg_outgoing_addr[0], msg_outgoing_addr[1]);
        else if (msg_outgoing_addr_len == 8)
            printf("%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
                    msg_outgoing_addr[7], msg_outgoing_addr[6],
                    msg_outgoing_addr[5], msg_outgoing_addr[4],
                    msg_outgoing_addr[3], msg_outgoing_addr[2],
                    msg_outgoing_addr[1], msg_outgoing_addr[0]);

        printf("\nUsage: target [short addr 0x??]/\"local\"\n");
        return RTN_CODE_OK;
    } else if (argCnt == 1) {
        if (strstr((char *)cli_argv[1], "local")) {
            g_vtmsg_msg_outgoing = 0;
            printf("\nChange to local\n");
        } else {
            char buff[128] = "";
            strcpy(buff, cli_argv[1]);
            strcpy(cli_argv[1], buff);
            g_vtmsg_msg_outgoing = 1;
            if (cli_argv[1][1] == 'x' || cli_argv[1][1] == 'X') {
                uint32_t input;
                msg_outgoing_addr_len = 2;
                sscanf(cli_argv[1], "%x", &input);
                msg_outgoing_addr[0] = (uint8_t)((input & 0x0000ff00) >> 8);
                msg_outgoing_addr[1] = (uint8_t)input & 0x000000ff;
            } else if (cli_argv[1][2] == ':') {
                msg_outgoing_addr_len = 8;
                sscanf(cli_argv[1], "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
                       (uint8_t *)&msg_outgoing_addr[7], (uint8_t *)&msg_outgoing_addr[6],
                       (uint8_t *)&msg_outgoing_addr[5], (uint8_t *)&msg_outgoing_addr[4],
                       (uint8_t *)&msg_outgoing_addr[3], (uint8_t *)&msg_outgoing_addr[2],
                       (uint8_t *)&msg_outgoing_addr[1], (uint8_t *)&msg_outgoing_addr[0]);
            } else {
                msg_outgoing_addr_len = 8;
                sscanf(cli_argv[1], "%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx",
                       (uint8_t *)&msg_outgoing_addr[7], (uint8_t *)&msg_outgoing_addr[6],
                       (uint8_t *)&msg_outgoing_addr[5], (uint8_t *)&msg_outgoing_addr[4],
                       (uint8_t *)&msg_outgoing_addr[3], (uint8_t *)&msg_outgoing_addr[2],
                       (uint8_t *)&msg_outgoing_addr[1], (uint8_t *)&msg_outgoing_addr[0]);
            }
        }
    }
    return RTN_CODE_OK;
}

int32_t set_debug(int32_t argCnt)
{
    if (argCnt == 1) {
        if (strstr((char *)cli_argv[1], "on")) {
            debug_on = 1;
            printf("\ndebug on\n");
        } else if (strstr((char *)cli_argv[1], "off")) {
            debug_on = 0;
            printf("\ndebug off\n");
        }
    } else {
        printf("\nUsage: debug on/off\n");
    }
    return RTN_CODE_OK;
}


uint8_t without_statement = 0;

cliCmd cliCmdsList[] = {
    { (char *)"ver", get_version, COMMAND_NORMAL },
    { (char *)"rev", get_version_internal, COMMAND_SUPER },
    { (char *)"ifconfig", get_networkInfo, COMMAND_NORMAL },
    { (char *)"route", route_request, COMMAND_NORMAL },
    { (char *)"path", path_request, COMMAND_NORMAL },
    { (char *)"dw", mem_dumpword, COMMAND_SUPER },
    { (char *)"ww", mem_writeword, COMMAND_SUPER },
    { (char *)"nwkstart", start_network, COMMAND_NORMAL },
    { (char *)"upgrade", start_upgrade, COMMAND_LOCAL | COMMAND_DCU },
    { (char *)"upg-config", config_upgConfig, COMMAND_LOCAL | COMMAND_DCU },
    { (char *)"reboot", target_reboot, COMMAND_NORMAL },
    { (char *)"bbp_nv", bbp_nv_config, COMMAND_SUPER },
    { (char *)"app_nv", app_nv_config, COMMAND_SUPER },
    { (char *)"csma", csma_config, COMMAND_SUPER },
    { (char *)"zerox", zerox_config, COMMAND_SUPER },
    { (char *)"pktcntr", packet_cntr, COMMAND_NORMAL },
    { (char *)"hping", host_ping, COMMAND_LOCAL | COMMAND_DCU },
    { (char *)"bbpr", bbp_raw_command, COMMAND_NORMAL },
    { (char *)"log", get_log, COMMAND_NORMAL },
    { (char *)"syslog", get_runtime_sys_log, COMMAND_NORMAL },
    { (char *)"bbp_sig", get_bbp_sig, COMMAND_SUPER },
    { (char *)"nei_eui", get_eni_eui_Table, COMMAND_SUPER },
    { (char *)"s2e", get_joined_table, COMMAND_NORMAL | COMMAND_DCU },
    { (char *)"diagno", network_diagnosis, COMMAND_NORMAL | COMMAND_DCU },
    { (char *)"settimer", tools_settimer, COMMAND_NORMAL | COMMAND_DCU },
    { (char *)"all", traverse_send_command, COMMAND_NORMAL | COMMAND_DCU },
    { (char *)"config", do_configuration, COMMAND_NORMAL },
    { (char *)"target", set_target_dest, COMMAND_NORMAL },
    { (char *)"reset", reset_dev, COMMAND_NORMAL },
    { (char *)"debug", set_debug, COMMAND_NORMAL },
    { (char *)"allver", network_check_ver, COMMAND_NORMAL | COMMAND_DCU },
    { (char *)"panid", set_pan_id, COMMAND_NORMAL | COMMAND_DCU },
    //{(char*)"utest", uart_test},
    //{"ver", get_version},
    //{"ifconfig", get_ifconfig},
    //{"get-route", get_route},
    //{"set-route", get_route},
    //{"nwkstart", nwkstart_req},
    { NULL, NULL }
};


char stack_print_str[DEBUG_STRING_MAX] = { 0 };
int stack_is_print_turned_on[5] = { 0, 0, 0, 0, 0 };
void hc_stack_print(int len, char *str)
{
    printf("%s", str);
}

int32_t dev_get_resp(uint8_t *pType, uint8_t **ppBuff, uint32_t *pLen, uint8_t *pFlags)
{
    int32_t rtn;

    rtn = mbox_take(&mboxCb, (uint8_t *)&gMboxMsgBuff);

    if (rtn) {
        if (gMboxMsgBuff.sts == 0) {
            DEBUG_ERROR("Failed to read data!!\n");
            rtn = -1;
        } else {
            *pType = gMboxMsgBuff.msgType;
            *ppBuff = gMboxMsgBuff.pBuff;
            *pLen = gMboxMsgBuff.pyldLen;
            *pFlags = gMboxMsgBuff.hdrFlags;
            rtn = 0;
        }
    } else {
        DEBUG_ERROR("Response timeout\n");
        rtn = -1;
    }

    return rtn;
}


uint32_t msg_outgoing_addr[8] = {};
uint32_t msg_outgoing_addr_len = 0;
int32_t dev_send(const uint8_t *pMsg, const uint32_t msgLen)
{
    uint32_t i, j;
    uint8_t c;
    uint32_t o = 0;
    uint8_t slipcode[6] = { SLIP_END, SLIP_ESC, SLIP_ESC_END, SLIP_ESC_ESC };
    uint8_t remoteMsg[VTMSG_ATTR_MSG_OUTGOING_LEN] = {};

    if (g_vtmsg_msg_outgoing) {
        build_remote_header(remoteMsg);
    }

    STACK_PRINT("devSendOut!!", DB_PRINT);

    /* Send pbuf out on the serial I/O device. */
    /* Start with packet delimiter. */
    slipdev->write((char *)&slipcode[0], 1);
    DEBUG_MORE("\n", c);

    //if it's outgoing msg adding additional remote header

    for (i = 0, j = 0; i < msgLen; ) {
        //if it's outgoing msg adding additional remote header first
        if (g_vtmsg_msg_outgoing == 1 && j < VTMSG_ATTR_MSG_OUTGOING_LEN) {
            c = (remoteMsg[j]);
            j++;
        } else {
            c = (pMsg[i]);
            i++;
        }

        switch (c) {
        case SLIP_END:
            /* need to escape this byte (0xC0 -> 0xDB, 0xDC) */
            slipdev->write((char *)&slipcode[1], 1);
            slipdev->write((char *)&slipcode[2], 1);
            o += 2;
            break;
        case SLIP_ESC:
            /* need to escape this byte (0xDB -> 0xDB, 0xDD) */
            slipdev->write((char *)&slipcode[1], 1);
            slipdev->write((char *)&slipcode[3], 1);
            o += 2;
            break;
        default:
            /* normal byte - no need for escaping */
            slipdev->write((char *)&c, 1);
            o += 1;
            break;
        }
        DEBUG_MORE(" %02x ", c);
    }
    DEBUG_MORE("\n", c);
    /* End with packet delimiter. */
    slipdev->write((char *)&slipcode[0], 1);
    DEBUG_MORE("send out %d\n", o);
    //usleep(100000);
    return RTN_CODE_OK;
}

int32_t dev_getmsg(uint8_t *pbuf, int32_t len)
{
    int32_t rtn = 0;
    int32_t pyldLen;
    vtmsg_msg_s vtmsgMsg;
    uint8_t *pPyldBuff = NULL;
    int i;

    memset(&vtmsgMsg, 0, sizeof(vtmsgMsg));

    pyldLen = vtmsg_get_msghdr_len(pbuf);

    if (pyldLen == (len - sizeof(vtmsg_msgHdr_s))) {
        pPyldBuff = ( uint8_t *)malloc(pyldLen);
        memcpy(pPyldBuff, pbuf + sizeof(vtmsg_msgHdr_s), pyldLen);
        //Msg type here show out directly
        switch (pHifHdr->type) {
        case VTMSG_MSG_TYPE_UPGRADE_EVENT:
        {
            check_upgrade_event(pPyldBuff, pyldLen);
            free(pPyldBuff);
            break;
        }
        case VTMSG_MSG_TYPE_NETWORK_EVENT:
        {
            check_network_event(pPyldBuff, pyldLen);
            free(pPyldBuff);
            break;
        }
        default:

            rtn = 1;
            break;
        }
    } else {
        //packet format error?
        printf("packet format error?\n");
        rtn = -1;
    }

    if (rtn < 0) {
        vtmsgMsg.sts = 0;
    } else if (rtn > 0) {
        vtmsgMsg.sts = 1;
        vtmsgMsg.pBuff = pPyldBuff;
        vtmsgMsg.pyldLen = pyldLen;
        vtmsgMsg.hdrFlags = pHifHdr->flags;
        vtmsgMsg.msgType = pHifHdr->type;
    } else if (rtn == 0) {
        return rtn;
    }
    rtn = mbox_send(&mboxCb, (uint8_t *)&vtmsgMsg);
}


#if OS_WIN32
uint32_t __stdcall dev_rxslip_handler(PVOID pM)
#else
void *dev_rxslip_handler(void *param)
#endif
{
    char c, cc, slip_start = 0;
    if (without_statement == 0)
        printf("Rx start...\n");

    while (1) {
        if (slipdev->read(&c, 1) == 1) {
            DEBUG_BASIC("%02hhx", (uint8_t)c);
            if (c == (char)0xC0) {
                if (!slip_start) {
                    slip_start = 1;
                }
                if ((devBuff_idx) > 0) {
                    DEBUG_BASIC("\ngot msg: %d Bytes\n", devBuff_idx);
                    if (devBuff_idx >= sizeof(vtmsg_msgHdr_s)) {
                        if (dev_getmsg(dev_rxBuff, devBuff_idx) < 0) {
                            DEBUG_BASIC("Rx mbox full!!\n");
                        }
                    } else {
                        DEBUG_BASIC("size too small!?\n");
                    }
                    /* reset buf idx */
                    devBuff_idx = 0;
                    slip_start = 0;
                }
            } else if (c == (char)0xDB && slip_start) {
                /* read more */
                if (slipdev->read(&c, 1) == 1) {
                    DEBUG_BASIC("%02hhx ", (uint8_t)c);
                    if (c == (char)0xDC) {
                        cc = 0xC0;
                        DEBUG_BASIC("(%02hhx) ", (uint8_t)cc);
                        *(dev_rxBuff + devBuff_idx) = cc;
                        devBuff_idx++;
                    } else if (c == (char)0xDD) {
                        DEBUG_BASIC("-> DB ");
                        cc = 0xDB;
                        DEBUG_BASIC("(%02hhx) ", (uint8_t)cc);
                        *(dev_rxBuff + devBuff_idx) = cc;
                        devBuff_idx++;
                    } else {
                        DEBUG_ERROR("error: decode error\n");
                        /* drop */
                    }
                }
            } else if (slip_start) {
                /* normal byte - no need for escaping */
                DEBUG_BASIC("(%02hhx) ", (uint8_t)c);
                *(dev_rxBuff + devBuff_idx) = c;
                devBuff_idx++;
            }
            /* force flush */
            if (devBuff_idx == (BUF_LEN_DEV_RX - 1)) {
                devBuff_idx = 0;
            }
        } else {
            //DEBUG_BASIC("read tun fail\n");
            #if OS_WIN32
            Sleep(10);
            #else
            usleep(1000);
            #endif
        }
    }
}
static void statement(void)
{
    printf("/------------------------------------------------------------------------------------\n");
    printf("/ Host interface of Magpie PLC Platform - Copyright (c) 2016 Vango Technologies, Inc.\n");
    printf("/ Vanilla V%d.%d (%s)\n", HOSTCLI_VERSION_MAJOR, HOSTCLI_VERSION_MINOR, HOSTCLI_VERSION_HASH);
    printf("/------------------------------------------------------------------------------------\n");
    printf("\n\nSee '?' for more information \n'q' for safety exit\n\n\n\n");
}

static void help(void)
{
#if OS_WIN32
    printf("Usage:\n-I COMx [-C \"command\"] [-R \"short addr\"|\"EUI-64\"]\n\n");
#else
    printf("Usage:\n-I /dev/ttyUSBx [-C \"command\"] [-R \"short addr\"|\"EUI-64\"]\n\n");
#endif
}

int32_t cli_processCmd(int32_t argCnt)
{
    int32_t idx = 0, rtn;

    DEBUG_MORE("%s\n", __func__);

    while (cliCmdsList[idx].cmdStr != NULL) {
        if (strcmp((const char *)cli_argv[0], (const char *)cliCmdsList[idx].cmdStr) == 0
            && (g_cmd_level & cliCmdsList[idx].cmd_level)) {
            rtn = (*cliCmdsList[idx].cmdFn)(argCnt - 1);
            break;
        } else {
            idx++;
        }
    }

    cli_argc = 0;

    print_rtn_error(rtn, __func__);


    if (cliCmdsList[idx].cmdStr == NULL) {
        DEBUG_ERROR("Unknown Command !!\n");
        rtn = -1;
    }

    return rtn;
}


int32_t cli_parseInput(char *inBuff)
{
    int32_t rtn, argStart = 1, idx = 0;

    DEBUG_MORE("%s\n", __func__);
    while (inBuff[idx] != '\0') {
        if (inBuff[idx] != 0x20 && inBuff[idx] != '\t' && inBuff[idx] != '\n') {
            if (argStart) {
                if (cli_argc >= CONFIG_CONSOLE_ARGC_MAX)
                    return -2;
                cli_argv[cli_argc] = (uint8_t *)&inBuff[idx];
                cli_argc++;
                argStart = 0;
            }
        } else {
            argStart = 1;
            inBuff[idx] = '\0';
        }
        idx++;
    }

    if (cli_argc == 0)
        return -1;

    rtn = cli_processCmd(cli_argc);

    printf("\n");

    return rtn;
}

#if OS_WIN32
int32_t opterr = 1,                 /* if error message should be printed */
        optind = 1,                 /* index into parent argv vector */
        optopt,                     /* character checked for validity */
        optreset;                   /* reset getopt */
char *optarg;                       /* argument associated with option */

#define BADCH   (int32_t)'?'
#define BADARG  (int32_t)':'
#define EMSG    (char *)""

/*
 * getopt --
 *      Parse argc/argv argument vector.
 */
int32_t getopt(int32_t nargc, char *const nargv[], const char *ostr)
{
    static char *place = EMSG;              /* option letter processing */
    const char *oli;                        /* option letter list index */

    if (optreset || !*place) {              /* update scanning pointer */
        optreset = 0;
        if (optind >= nargc || *(place = nargv[optind]) != '-') {
            place = EMSG;
            return(-1);
        }
        if (place[1] && *++place == '-') {      /* found "--" */
            ++optind;
            place = EMSG;
            return(-1);
        }
    }                                       /* option letter okay? */
    if ((optopt = (int32_t)*place++) == (int32_t)':' ||
        !(oli = strchr(ostr, optopt))) {
        /*
         * if the user didn't specify '-' as an option,
         * assume it means -1.
         */
        if (optopt == (int32_t)'-')
            return(-1);
        if (!*place)
            ++optind;
        if (opterr && *ostr != ':')
            (void)printf("illegal option -- %c\n", optopt);
        return(BADCH);
    }
    if (*++oli != ':') {                    /* don't need argument */
        optarg = NULL;
        if (!*place)
            ++optind;
    } else {                                 /* need an argument */
        if (*place)                     /* no white space */
            optarg = place;
        else if (nargc <= ++optind) {   /* no arg */
            place = EMSG;
            if (*ostr == ':')
                return(BADARG);
            if (opterr)
                (void)printf("option requires an argument -- %c\n", optopt);
            return(BADCH);
        } else {                          /* white space */
            optarg = nargv[optind];
        }
        place = EMSG;
        ++optind;
    }
    return(optopt);                         /* dump back option letter */
}
#endif





int32_t main(int32_t argc, char **argv)
{
    uint8_t ch;
    int32_t rtn;
    char main_cmd[256] = {};
#if OS_WIN32
    char dev_name[128];
    size_t converted = 0;
    TCHAR w_tmp[128];
#endif
    while ((ch = getopt(argc, argv, "h?WC:I:R:")) != -1) {
        if (ch == 0xFF) {
            DEBUG_BASIC("ch=%x\n", ch);
            goto skip;
        }
        DEBUG_BASIC("ch=%x\n", ch);
        switch (ch) {
        case 'I':
            #if OS_WIN32
            if (without_statement == 0) {
                printf("Connecting port %s\n", optarg);
            }
            //mbstowcs_s(&converted, w_tmp, strlen(optarg) + 1, optarg, _TRUNCATE);
            sprintf((char *)dev_name, "\\\\.\\%s", (const char *)optarg);
            slipdev = slip_dev_init((CHAR *)dev_name);
            if (slipdev == NULL) {
                system("PAUSE");
                return 0;
            }
            #else
            slipdev = slip_dev_init(optarg);
            #endif
            DEBUG_BASIC("fd: 0x%x\n", slipdev);
            DEBUG_BASIC("fd: 0x%d\n", slipdev->fd);
            break;
        case 'C':
            sprintf((char *)main_cmd, "%s", (const char *)optarg);
            //printf("%s",(const char*)optarg);
            break;
        case 'R':
            g_vtmsg_msg_outgoing = 1;
            if (optarg[1] == 'x' || optarg[1] == 'X') {
                uint32_t input;
                msg_outgoing_addr_len = 2;
                sscanf(&optarg[2], "%x", &input);
                msg_outgoing_addr[0] = (uint8_t)((input & 0x0000ff00) >> 8);
                msg_outgoing_addr[1] = (uint8_t)input & 0x000000ff;
            } else if (optarg[2] == ':') {
                msg_outgoing_addr_len = 8;
                sscanf(optarg, "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
                       (uint8_t *)&msg_outgoing_addr[7], (uint8_t *)&msg_outgoing_addr[6],
                       (uint8_t *)&msg_outgoing_addr[5], (uint8_t *)&msg_outgoing_addr[4],
                       (uint8_t *)&msg_outgoing_addr[3], (uint8_t *)&msg_outgoing_addr[2],
                       (uint8_t *)&msg_outgoing_addr[1], (uint8_t *)&msg_outgoing_addr[0]);
            } else {
                msg_outgoing_addr_len = 8;
                sscanf(optarg, "%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx",
                       (uint8_t *)&msg_outgoing_addr[7], (uint8_t *)&msg_outgoing_addr[6],
                       (uint8_t *)&msg_outgoing_addr[5], (uint8_t *)&msg_outgoing_addr[4],
                       (uint8_t *)&msg_outgoing_addr[3], (uint8_t *)&msg_outgoing_addr[2],
                       (uint8_t *)&msg_outgoing_addr[1], (uint8_t *)&msg_outgoing_addr[0]);
            }
            break;
        case 'W':
            without_statement = 1;
            break;
        case 'h':
        case '?':
        default:
            statement();
            help();
            return -1;
            break;
        }
    }
 skip:
    if (slipdev == NULL) {
        help();
        return -1;
    }
    if (without_statement == 0) {
        statement();
        DEBUG_BASIC("param parsing done!!!\n");
        printf("init ...\n");
    }
    if (g_vtmsg_msg_outgoing == 0) {
        g_cmd_level |= COMMAND_LOCAL;
    }
    if (mbox_init(&mboxCb, sizeof(vtmsg_msg_s), 1) < 0) {
        DEBUG_ERROR("failed to create mailbox !! \n");
        return -1;
    }
#if OS_WIN32
    cli_threadInfo = CreateThread(
        NULL,       // default security attributes
        0,          // use default stack size
        (LPTHREAD_START_ROUTINE)dev_rxslip_handler,         // thread function name
        NULL,       // argument to thread function
        0,          // use default creation flags
        NULL);      // returns the th
#else
    rtn = pthread_create(&cli_threadInfo, NULL, dev_rxslip_handler, NULL);
    if (rtn != 0) {
        assert(0);
    }
#endif
    if (strlen(main_cmd) == 0) {
#if OS_WIN32
        Sleep(100);
#else
        usleep(100000);
#endif
        check_connection();
        printf("CLI Ready...\n");
    }
    init_app_config();

    //cli
    while (1) {
        uint8_t *pInput = lineBuff;
        if (strlen(main_cmd) != 0) {
            //g_cmd_level |= COMMAND_SUPER;
            pInput = main_cmd;
        } else {
            printf("vt# ");
            fgets((char *)lineBuff, 256, stdin);
        }
        while (*pInput == ' ' || *pInput == '\t') {
            pInput++;
        }
        if (pInput[0] == 0x5b || pInput[0] == '\n') {
            continue;
        }
#if 0
        if (pInput[0] == '\0' || pInput[0] == '\033' || pInput[0] == '\n') {
            printf("vt# %s", lastLineBuff);
            strcpy((char *)pInput, (const char *)lastLineBuff);
        } else {
            strcpy((char *)lastLineBuff, (const char *)pInput);
        }
#endif
        if (pInput[0] == 'q' || pInput[0] == 'Q') {
            app_safety_exit();
            break;
        }

        if (pInput[0] == '?') {
            int32_t idx = 0;
            int32_t printed = 0;

            while (cliCmdsList[idx].cmdStr != NULL) {
                if (g_cmd_level & cliCmdsList[idx].cmd_level) {
                    printf("%-12s", cliCmdsList[idx].cmdStr);
                    printed++;
                    if (printed % 4 == 0) {
                        printf("\n", printed);
                    }
                }
                idx++;
            }

            printf("\n");
            continue;
        }

        if (pInput[0] == 'v' && pInput[1] == 't') {
            int32_t idx = 0;
            printf("password?\n");
            fgets((char *)lineBuff, 256, stdin);
            if (strstr(lineBuff, "8161")) {
                g_cmd_level |= COMMAND_SUPER;
            }
            continue;
        }

        if ((rtn = cli_parseInput((char *)pInput)) < 0) {
            if (rtn == -2) {
                printf("Bad Input !!\n");
            } else {
                printf("\n");
                //break;
            }
        }

        if (strlen(main_cmd) != 0 && strcmp(cli_argv[0], "upgrade") == 0) {
            upg_running = 1;
            while (upg_running) {
                #if OS_WIN32
                Sleep(10);
                #else
                usleep(1000);
                #endif
            }
            ;
            app_safety_exit();
            break;
        } else if (strlen(main_cmd) != 0) {
            app_safety_exit();
            break;
        }
    }

    return 0;
}
