## 1014
- 在辦公室分析華立的資料，發現有很多串台區的問題，當下提出能不能重新調配檔案，現場回覆:要總部提出調配的解決方案才可以進行調配

## 1013
- 2194 : 到現場同一個變電箱內有兩台集中器，另一台通訊很好，但這台很難與底下的表進行通訊
- 2151 : 線拉好後開始下大雨就先撤了，明天再去
- 在辦公室分析華立的數據有發現集中器有異常現象[1537]
## 1012
- 在辦公室分析華立的資料來判斷抄表率下降的可能原因，可以參考華立資料內的檔案

## 1011
- 早上分析集中器的日誌，發現到集中器可能有異常會進行復位模塊，以及超時幀的問題:客戶抄表超時等60秒就直接下發下一筆抄表幀，模塊如果慢一點回應超時幀就會發生同時收到超時幀以及下一本抄表的回覆，以上情形絕對是會影響抄表的，但客戶目前似乎無法接受?
- 1655 : 再次前往此台區進行測試及分析，表大部分都組上了，但依然很難通訊，外掛表與集中器組網後，不管是與集中器還是其他表取得資料都有困難，常常timeout，級數也很高

## 1010
- 1655 : 前往此台區發現進行測試及分析，發現通訊不好通，測試完之後有重啟模塊，組網滿順利的
- 因感冒身體不適所以沒再去其他台區

## 1009
- 早上討論要去哪個台區
- 1. 1551 : 9/29掉0 : 前往現場通訊測試ok, 詢問才知道稍早有其他工程時重啟了.... 
- 2. 2654 : 抄表率一直都不太好,只有40只表, 此台區較特殊:有兩個集中器,2654的表會一直往另一個集中器組網,且2654因找不到集中器的位置,所以我將外掛表裝設於表A旁邊進行簡易通訊發現路由不是很好通,有豬noise
- 3. 1514 : 抄表率100% : 抓noise

## 1008
- 早上因處理飯店的事情所以較晚進公司
- 中午前往台區2485及1012
- 2485 透過主站得知日凍結0%,至現場進行分析發現通訊良好, 回公司後華立的工程師才跟我們說任務沒有配,配上任務後抄表正常
- 1012 至現場檢查升級情況,還是剩下1百多台沒辦法升級成功,於現場進行通訊測試,後面一百多台很難通訊
### Next to do
- 掉0的台區幾乎都已經跑得差不多了,接下來我會開始進行分析客戶提到有跳幀重複幀的情況

## 事件整理

- 透過客戶的主站系統可以得知哪個台區抄表率降為0
- 透過實際到掉0的台區初步可以劃分出以下狀況
* 無法通訊無法組網(重開集中器無法解決)
* 可以通訊但有一直加黑名單的情況(重開集中器會暫時好)
* 可以通訊但抄表抄不到(目前透過TAB當初的log 有觀察到表端的log有異常的現象)(重開集中器會好)

## 目前實際上做了
* 製作特殊版本的程序方便抓取premble
* 更新可以設定PAN ID的操控寶至測試台區(1012) 目前: 278/398
* 現場:
    1. 1012 :遠程升級
    2. 1020 :premble 測試
    3. 2485 :到現場來不及接上sniffer客戶就下電了...
    4. 2457 :到現場DCU端完全接收不到任何資料，但噪聲不高
    5. 2778 :到現場變電箱在居民的庭院中，庭院門被鎖起來無法進入...
* `客戶一直在強調分析結果以及處理狀況：目前我分析的結果就是，我實際看到得情況不管是SA還是SW行為都是正常的，並沒有異常的行為或是BUG，情況看來都是通訊就很難成功了`

## Next to do
* 觀察1012升級後抄表是否有好轉
* 前往2898確認一些資訊
* 前往其他掉0台區觀察現象
* 目前現場都還是使用1155版本的STM32，但很久以前就更新至1157也提供給客戶了，若有更新應該會減少抄表失敗的狀況，將會提供STM32不會對還沒組上網的表進行廣播抄表，抄表將會更有效率，因為很多台區檔案內有很多表是無法組上的(1158)。

## 掉0通訊排查步驟
* 觀察燈號：請華立人員幫忙進行點抄此台區的電表觀察路由模塊上的T/R燈號有無閃爍
* 集中器不要下電：接上sniffer後，再請華立人員進行點抄，並觀察sniffer掃到的數據
* 機中器不要下電，接上操控器，透過樹梅派或是PC使hostcli target至路由模塊(abcdabcdabcdabcd)觀察s2e
* 量噪聲

## 排查小技巧
### 外掛表通訊
* 設定組網
` ifconfig join`
* 設定PAN ID
` route p id`
* 設定short address (如有開啟白名單:必須要用使用過的short address)
` route s addr`
* 設定KEY
`g3test gmk 0 0000000000484f4c4c4559474d4b0000`
* 設定完成即可開始進行通訊排查以及取得dcu_6306或是meter_6306的資料

### 現場法寶
![screen](tool.jpg)
* 可以夾電源的延長線 * 1
* 操控寶 * 1
* controller/meter 模塊 * 1
* Preamble 模塊 * 2
* sniffer 模塊 * 1
* 操控器底板(工裝)(插頭) * 2
* 操控器底板(工裝)(夾式) * 1
* USB延長線 * N
* USB HUB * N
* RS232 Cable * 1
* uart2usb cable * N
* 開孔集中器模塊 * 1