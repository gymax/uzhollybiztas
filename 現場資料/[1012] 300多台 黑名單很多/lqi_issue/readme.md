# 現場直連DCU Tx問題


## 1004總結

從現場圖可以看到我們有空接一個變壓器, 辦公室沒有  
變壓器的存在會影響DCU的Tx

- 跟現場取電or辦公室取電無關
- 只影響DCU
- 因为12V适配器上的电容跟集中器模块上的10uH电感, 产生线路上的附载效应

回到昨天現場將變壓器拔掉之後meter收到DCU來的lqi回到100


## 1003總結

> 因為要現場升級模塊, 我們從集中器底下取電, 接出了DCU和meter

發現**現場直連**的情形底下有通訊問題, DCU和meter互加黑名單, 觀察lqi之後發現meter收到DCU的封包lqi非常低

在同樣的接線條件下, 我們又做了以下改變

1. 加入meter
2. 更換DCU
3. 更換供電環境

目前認為是只有現場才發生, DCU換更換問題仍在, TxRx問題與電表無關

## 版本

## 環境

現場接線圖
![screen](20191003210401.jpg)

辦公室接線圖
![screen](20191003213142.jpg)

## 問題描述

### 原本台區上的DCU和meter組網後通訊

> 原始檔案在 field_ori_dcu_1_meter

一開始ping不通
```
 vt# pinge 0
 
 
 Ping Test : size 7, loops 1
 Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
 Robust retry success(2), Update Neighor table 0x0000(Robust)
 T seq=0001
 Total Send Tx/Rx : 1/0, AvgTime 99999 msec
 PER : 100.0 %
```

meter看到來自dcu的封包lqi很低
```
Rx Packet : (type 1, size 48)
****************************************************************
  30006070 | 28 00 0C 10 00 23 38 00 - 09 00 14 49 88 AD ED 50
  30006080 | 02 00 00 00 0D 6C 00 00 - 00 00 01 42 1D BB 7D D6
  30006090 | FA 73 F8 15 74 77 89 A9 - AA 60 B9 D2 23 45 E3 B2
****************************************************************

Decrypt Rx Packet :
    30001070 | 28 00 0C 10 00 23 38 00 - 09 00 14 49 88 AD ED 50
    30001080 | 02 00 00 00 0D 6C 00 00 - 00 00 BE 00 00 00 02 40
    30001090 | 56 50 07 54 2F 00 E0 05 - 00 30

****PHY Parameter(FCC)******************************************
  phase = 3
  lqi = 35(0x23)
  tone_map = 0xFFFFFF(expect 0xFF80FC)
  modulation = D-0(expect D-3)
  det_scale = 20
  noise_thr = 30
****************************************************************
```

DCU端收得很好

```
 Rx Packet : (type 1, size 48)
 ****************************************************************
   300059B0 | 28 00 0C 10 00 6D 38 00 - 09 00 14 49 88 82 ED 50
   300059C0 | 00 00 02 00 0D 2D 00 00 - 00 00 23 8A D9 CB AC E6
   300059D0 | 0B B5 0F E0 37 EA BC 7E - 66 36 11 9D 7D 79 77 BE
 ****************************************************************
 
 Decrypt Rx Packet :
     30001370 | 28 00 0C 10 00 6D 38 00 - 09 00 14 49 88 82 ED 50
     30001380 | 00 00 02 00 0D 2D 00 00 - 00 00 BE 00 02 00 00 40
     30001390 | 56 50 07 54 17 00 A2 46 - 09 00
 
 ****PHY Parameter(FCC)******************************************
   phase = 3
   lqi = 109(0x6D)
   tone_map = 0xFFFFFF(expect 0xFFFFFF)
   modulation = D-0(expect D-3)
   det_scale = 20
   noise_thr = 30
 ****************************************************************
```

互加黑名單
```
Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[001](65534m46s):  0x0002   0x0002     0x0019        1             0

BlackList Table : [000]-[127]
                    Dest
[000](00009m48s):  0x0002
```

```
 Routing Table : [000]-[511]
                     Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
 [000](65533m29s):  0x0000   0x0000     0x0021        1             0
 
 BlackList Table : [000]-[127]
                     Dest
 [000](00008m24s):  0x0000
```

### 換我們帶來的的DCU和meter組網後通訊

> 原始檔案在 field_my_dcu_1_meter


meter一樣收得很差, 同上dcu看到meter很好
```
Rx Packet : (type 1, size 48)
****************************************************************
  300049F0 | 28 00 0C 10 00 29 38 20 - 01 00 14 69 88 5D 3A DC
  30004A00 | 01 00 00 00 0D 31 00 00 - 00 00 57 52 03 8F F1 D9
  30004A10 | E7 0C 91 78 DD 87 06 5D - 9B 37 7F 1D 3C 83 BC 0A
****************************************************************

Decrypt Rx Packet :
    30000E30 | 28 00 0C 10 00 29 38 20 - 01 00 14 69 88 5D 3A DC
    30000E40 | 01 00 00 00 0D 31 00 00 - 00 00 BE 00 00 00 01 40
    30000E50 | 56 50 07 54 1B 00 A2 C2 - 03 00

****PHY Parameter(FCC)******************************************
  phase = 3
  lqi = 41(0x29)
  tone_map = 0xFFFFFF(expect 0xFF85F8)
  modulation = D-0(expect D-3)
  det_scale = 20
  noise_thr = 30
****************************************************************
```

不是互加黑名單而是互認為對方weak
```
Device     : LBD (FFD) Pan 0xDC3A  Joined
Short Addr : 0x0001 (RC_COORD 0x002A)
Extended Addr : 0x5387 38FF FE02 0000

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65534m52s):  0x0000   0x0000     0x002A        1             1

BlackList Table : [000]-[127]
                    Dest
```

```
Device     : LBS (FFD) Pan 0xDC3A  Joined
Short Addr : 0x0000 (RC_COORD 0x0000)
Extended Addr : 0xABCD ABCD ABCD ABCD

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[001](65534m55s):  0x0001   0x0001     0x002A        1             1

BlackList Table : [000]-[127]
                    Dest
```



### 加入另一個meter測試meter互通

> field_my_dcu_2_meter

基本上一樣, meter之間lqi很高, 但看到從DCU來的都很低

meter到DCU還選擇轉送...
```
Device     : LBD (FFD) Pan 0xDC3A  Joined
Short Addr : 0x0001 (RC_COORD 0x0043)
Extended Addr : 0x5387 38FF FE02 0000

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65534m55s):  0x0000   0x0002     0x0043        2             1
[001](65534m55s):  0x0002   0x0002     0x0019        1             0

BlackList Table : [000]-[127]
                    Dest

Link Cost Penalty Table(Size 512)
No. Dest    State  Stable  TryRun  Dead  Penalty
000 0x0002      1       0       0     0        0

RREQ History : [00]-[31]
         Dest    Ori     Seq

```

### 同樣組合回到辦公室
> office_my_dcu_2_meter

現象消失, 所有對傳的lqi都在100左右

## TODO

討論和行動移至下面討論

https://gitlab.com/gymax/uzhollybiztas/issues/15