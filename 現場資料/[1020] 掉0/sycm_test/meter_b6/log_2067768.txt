Send Beacon (RC_COORD 0xFFFF)

bbp a

TDP_FIR3_SCALE
    fir3_scale               : 0x00000086(134)
    fir3_scale_max           : 0x00000086(134)
    fir3_scale_min           : 0x0000000A(10)
    fir3_tx_success_cnt      : 0x00000008(8)
    fir3_add_3db_thr         : 0x0000000F(15)
    urgent_in_cnt            : 0x00000000(0)
    ot_event_cnt             : 0x00000000(0)
    start_specific_tx_scale  : 0x00000086(134)
    last_abnormal_tx_scale   : 0x00000086(134)
PHY_RX_MIB_CNT
    preamb_det                  : 208
    syncm_det_success           : 126 (Fail 82)
    fch_crc_correct             : 2
    ack_nack_fch_correct        : 2
    ACK/NACK/Unmatch_correct    : 0 / 0 / 0
    data_fch_correct            : 0
    data_correct                : 0 (Fail 0)
    apdt_basic_time_block(btb)  : 105
    apdt_preamb_det_scale       : 20 (Max 20, Min 20)
    apdt_preamb_noise_thr       : 30 (Max 30, Min1)
    apdt_latched_fa_cnt_per_btb : 1 (AVG 0.78)
    apdt_scale_event            : 0(Up), 0(Dn)
    apdt_huge_fa_in_rx_event    : 0
    syncm_location              :    0     1     2     3     4     5
                                     0     0     0     0     0   126 (counter)
    FCH Error                   : 0, (format 00 00 00 00  00 00 00 00)
PHY_TX_MIB_CNT               Data     ACK    NACK
    mac_send_cnt         :      8       0       0
    mac_abort_cnt        :      0       0       0
    mac_drop_cnt         :      0       0
    retransmit_cnt       : 0
    data_param_err_cnt   : 0

vt# Send Beacon (RC_COORD 0xFFFF)
Send Beacon (RC_COORD 0xFFFF)
Send Beacon (RC_COORD 0xFFFF)

VCS wait 594 ms(14)
Send Beacon (RC_COORD 0xFFFF)
Send Beacon (RC_COORD 0xFFFF)
Send Beacon (RC_COORD 0xFFFF)
Send Beacon (RC_COORD 0xFFFF)

VCS wait 730 ms(11)

vt#
vt#
vt#
vt# Send Beacon (RC_COORD 0xFFFF)
igt

IDLE_GAIN_STATISTIC(curr 36)
    analog_gain         : 6
    digital_gain        : 30
    igt_mib_process_cnt : 758
    igt_mib_drop_cnt    : 5
    igt ( >=90dB)      : 0
    igt (   84dB)      : 0
    igt (   78dB)      : 0
    igt (   72dB)      : 0
    igt (   66dB)      : 2
    igt (   60dB)      : 10
    igt (   54dB)      : 8
    igt (   48dB)      : 8
    igt (   42dB)      : 32
    igt (   36dB)      : 693
    igt (   30dB)      : 0
IGT_FORCE
    freeze              : 0
    analog_gain         : 6
    digital_gain        : 30
    cic1_pwr_max        : 14113 (expect 11400 ~ 22800)
    cic1_pwr_tgt        : 7600
    rx_filt_out_pwr_max : 42370 (expect 5700 ~ 11400)
    rx_filt_out_pwr_tgt : 3800

vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt# lSend Beacon (RC_COORD 0xFFFF)
qi

AVG LQI = 0.0 (SNR 1073741814.0)
STD LQI = 0.1
SNR_MIB    Preamble  Data
current  :        0     6
   0 dB  :      272     0
   1 dB  :        8     0
   2 dB  :       13     0
   3 dB  :        6     0
   4 dB  :        9     0
   5 dB  :        5     0
   6 dB  :        3     0
   7 dB  :        2     0
   8 dB  :        6     0
   9 dB  :        7     2
  10 dB  :       10     0
  11 dB  :        8     0
  12 dB  :        2     0
  13 dB  :        0     0
  14 dB  :        1     0
  15 dB  :        0     0
  16 dB  :        0     0
  17 dB  :        0     0
  18 dB  :        0     0
  19 dB  :        0     0
  20 dB  :        0     0