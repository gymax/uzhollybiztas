vt# bbp a

TDP_FIR3_SCALE
    fir3_scale               : 0x00000086(134)
    fir3_scale_max           : 0x00000086(134)
    fir3_scale_min           : 0x0000000A(10)
    fir3_tx_success_cnt      : 0x0000000C(12)
    fir3_add_3db_thr         : 0x0000000F(15)
    urgent_in_cnt            : 0x00000000(0)
    ot_event_cnt             : 0x00000000(0)
    start_specific_tx_scale  : 0x00000086(134)
    last_abnormal_tx_scale   : 0x00000086(134)
PHY_RX_MIB_CNT
    preamb_det                  : 604
    syncm_det_success           : 268 (Fail 336)
    fch_crc_correct             : 3 (fch cnt error)
    ack_nack_fch_correct        : 1
    ACK/NACK/Unmatch_correct    : 0 / 0 / 0
    data_fch_correct            : 0
    data_correct                : 0 (Fail 0)
    apdt_basic_time_block(btb)  : 132
    apdt_preamb_det_scale       : 32 (Max 32, Min 20)
    apdt_preamb_noise_thr       : 30 (Max 30, Min1)
    apdt_latched_fa_cnt_per_btb : 1 (AVG 2.54)
    apdt_scale_event            : 2(Up), 0(Dn)
    apdt_huge_fa_in_rx_event    : 4
    syncm_location              :    0     1     2     3     4     5
                                     0     0     0     0     0   268 (counter)
    FCH Error                   : 8, (format 95 61 04 1F  AD 9E 0B CB)
PHY_TX_MIB_CNT               Data     ACK    NACK
    mac_send_cnt         :     12       0       0
    mac_abort_cnt        :      0       0       0
    mac_drop_cnt         :      0       0
    retransmit_cnt       : 0
    data_param_err_cnt   : 0

vt# Send Beacon (RC_COORD 0xFFFF)
Send Beacon (RC_COORD 0xFFFF)

vt#



IDLE_GAIN_STATISTIC(curr 36)
    analog_gain         : 12
    digital_gain        : 24
    igt_mib_process_cnt : 1109
    igt_mib_drop_cnt    : 11
    igt ( >=90dB)      : 0
    igt (   84dB)      : 0
    igt (   78dB)      : 0
    igt (   72dB)      : 0
    igt (   66dB)      : 16
    igt (   60dB)      : 52
    igt (   54dB)      : 36
    igt (   48dB)      : 32
    igt (   42dB)      : 377
    igt (   36dB)      : 585
    igt (   30dB)      : 0
IGT_FORCE
    freeze              : 0
    analog_gain         : 12
    digital_gain        : 24
    cic1_pwr_max        : 24066 (expect 11400 ~ 22800)
    cic1_pwr_tgt        : 7600
    rx_filt_out_pwr_max : 40908 (expect 5700 ~ 11400)
    rx_filt_out_pwr_tgt : 3800
SNR_MIB    Preamble  Data
current  :        8     7
   0 dB  :      137     0
   1 dB  :       37     0
   2 dB  :       58     0
   3 dB  :       61     0
   4 dB  :       55     0
   5 dB  :       18     0
   6 dB  :       13     0
   7 dB  :       17     0
   8 dB  :       37     0
   9 dB  :       54     0
  10 dB  :       36     1
  11 dB  :       19     0
  12 dB  :        8     0
  13 dB  :        3     0
  14 dB  :        2     0
  15 dB  :        0     0
  16 dB  :        0     0
  17 dB  :        0     0
  18 dB  :        0     0
  19 dB  :        0     0
  20 dB  :        0     0
  21 dB  :        0     0