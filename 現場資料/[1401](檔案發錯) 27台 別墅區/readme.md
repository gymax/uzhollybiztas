## 1401

- pand 94ad
- 額外表很快組上, pinge DCU 100次 ok
- 看到很多beacon requset
- 這個額外表接上去有很多鄰居
- 回來詢問華立後發現那些鄰居都是沒登記的表, 可能這個台區表沒被完整登記, 導致抄0, 後面的表也連不上

ifconfig
```
Bandplan   : FCC
Device     : LBD (FFD) (start time 1121 sec) AC:50 Hz Pan 0x94AD  Joined
Short Addr : 0x0027 (RC_COORD 0x002A)
Extended Addr : 0x8311 39FF FE02 0000

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65519m35s):  0x0000   0x0000     0x002A        1             1
[001](65530m03s):  0x0026   0x0026     0x001A        1             0

BlackList Table : [000]-[127]
                    Dest

BlackList History : [00]-[127]
         Dest

RREQ History : [00]-[31]
         Dest    Ori     Seq
[00]     0002    0000     5122

Neighbor Table : [000]-[64] TX Use Neighbor table
      TMR/Neighgor   Dest    LQI  Mod(expect) Scheme    ToneMap    TXRES TXGAINe
[000](000/237 min): 0x013E  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[001](000/238 min): 0x00D0  00/5B   0/0(3)     0/0   FFFFFF/FE6FC0  0/0    0/ 0C
[002](000/237 min): 0x012F  00/2B   0/0(1)     0/0   FFFFFF/8E0000  0/0    0/ 0C
[003](000/237 min): 0x0151  00/66   0/0(3)     0/0   FFFFFF/FFFFF0  0/0    0/ 0C
[004](000/237 min): 0x00EC  00/46   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[005](000/237 min): 0x008D  00/2B   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[006](000/237 min): 0x0055  00/2E   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[007](000/237 min): 0x00DD  00/33   0/0(2)     0/0   FFFFFF/FF0000  0/0    0/ 0C
[008](000/237 min): 0x0145  00/40   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[009](000/237 min): 0x0168  00/5A   0/0(3)     0/0   FFFFFF/F1E780  0/0    0/ 0C
[010](000/237 min): 0x016D  00/6B   0/0(3)     0/0   FFFFFF/FFFFE0  0/0    0/ 0C
[011](000/237 min): 0x00E9  00/2A   0/0(1)     0/0   FFFFFF/0F0000  0/0    0/ 0C
[012](000/237 min): 0x0119  00/27   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[013](001/255 min): 0x0000  00/66   3/3(3)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[014](000/237 min): 0x016E  00/54   0/0(2)     0/0   FFFFFF/87FE00  0/0    0/ 0C
[015](000/237 min): 0x00AF  00/46   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[016](000/237 min): 0x0090  00/51   0/0(1)     0/0   FFFFFF/007C20  0/0    0/ 0C
[017](000/252 min): 0x0149  00/48   0/0(1)     0/0   FFFFFF/0363C0  0/0    0/ 0C
[018](000/237 min): 0x011D  00/53   0/0(2)     0/0   FFFFFF/CC0000  0/0    0/ 0C
[019](000/237 min): 0x003C  00/3C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[020](000/237 min): 0x0110  00/4F   0/0(2)     0/0   FFFFFF/400FF8  0/0    0/ 0C
[021](000/237 min): 0x000C  00/63   0/0(3)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[022](000/237 min): 0x0161  00/63   0/0(3)     0/0   FFFFFF/FFF01B  0/0    0/ 0C
[023](000/237 min): 0x0156  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[024](000/253 min): 0x012D  00/4A   0/0(2)     0/0   FFFFFF/FD3E00  0/0    0/ 0C
[025](000/237 min): 0x002E  00/62   0/0(3)     0/0   FFFFFF/E787FF  0/0    0/ 0C
[026](000/237 min): 0x0003  00/30   0/0(1)     0/0   FFFFFF/078000  0/0    0/ 0C
[027](000/237 min): 0x0115  00/76   0/0(3)     0/0   FFFFFF/00F1F8  0/0    0/ 0C
[028](000/239 min): 0x014D  00/3A   0/0(1)     0/0   FFFFFF/000060  0/0    0/ 0C
[029](000/238 min): 0x00DC  00/29   0/0(2)     0/0   FFFFFF/060000  0/0    0/ 0C
[030](000/238 min): 0x00E5  00/1D   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[031](000/238 min): 0x012B  00/45   0/0(2)     0/0   FFFFFF/073000  0/0    0/ 0C
[032](000/238 min): 0x0163  00/2F   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[033](000/238 min): 0x00BF  00/60   0/0(2)     0/0   FFFFFF/3FFFBB  0/0    0/ 0C
[034](000/238 min): 0x009D  00/51   0/0(2)     0/0   FFFFFF/006642  0/0    0/ 0C
[035](000/238 min): 0x0140  00/42   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[036](000/238 min): 0x015D  00/70   0/0(3)     0/0   FFFFFF/07FFFF  0/0    0/ 0C
[037](000/255 min): 0x0026  00/6A   0/0(3)     0/0   FFFFFF/948FFF  0/0    0/ 0C
[038](000/238 min): 0x016C  00/33   0/0(1)     0/0   FFFFFF/000440  0/0    0/ 0C
[039](000/238 min): 0x0095  00/43   0/0(2)     0/0   FFFFFF/003800  0/0    0/ 0C
[040](000/238 min): 0x011F  00/62   0/0(3)     0/0   FFFFFF/0FFFC0  0/0    0/ 0C
[041](000/238 min): 0x0142  00/62   0/0(3)     0/0   FFFFFF/00F800  0/0    0/ 0C
[042](000/238 min): 0x0133  00/76   0/0(3)     0/0   FFFFFF/01FDFA  0/0    0/ 0C
[043](000/238 min): 0x012C  00/63   0/0(2)     0/0   FFFFFF/1FF180  0/0    0/ 0C
[044](000/238 min): 0x00CD  00/6B   0/0(2)     0/0   FFFFFF/760640  0/0    0/ 0C
[045](000/238 min): 0x012E  00/45   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[046](000/238 min): 0x0017  00/2E   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[047](000/238 min): 0x016B  00/46   0/0(2)     0/0   FFFFFF/0003E0  0/0    0/ 0C
[048](000/238 min): 0x002B  00/6B   0/0(3)     0/0   FFFFFF/07FFFF  0/0    0/ 0C
[049](000/238 min): 0x013A  00/24   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[050](000/238 min): 0x0030  00/6D   0/0(3)     0/0   FFFFFF/007FFA  0/0    0/ 0C
[051](000/238 min): 0x0157  00/5D   0/0(2)     0/0   FFFFFF/3F4800  0/0    0/ 0C
[052](000/246 min): 0x00DB  00/64   0/0(3)     0/0   FFFFFF/FE27C0  0/0    0/ 0C
[053](000/250 min): 0x0007  00/34   0/0(1)     0/0   FFFFFF/0F0000  0/0    0/ 0C
[064](002/255 min): 0xFFFF  00/00   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C

MHR SEQ History : [00]-[15]
         Src     Seq
[00]     0000     103
[01]     000002feff180912     194
[02]     000002feff207864     204
[03]     000002feff101709     106
[04]     000002feff149037     148

Bandplan   : FCC
Device     : LBD (FFD) (start time 68 sec) AC:50 Hz Pan 0x94AD  Joined
Short Addr : 0x0001 (RC_COORD 0x001A)
Extended Addr : 0x8311 39FF FE02 0000

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65534m47s):  0x0000   0x0000     0x001A        1             0

BlackList Table : [000]-[127]
                    Dest

BlackList History : [00]-[127]
         Dest

RREQ History : [00]-[31]
         Dest    Ori     Seq

Neighbor Table : [000]-[64] TX Use Neighbor table
      TMR/Neighgor   Dest    LQI  Mod(expect) Scheme    ToneMap    TXRES TXGAINe
[000](000/255 min): 0x0170  00/4D   0/0(2)     0/0   FFFFFF/00C20E  0/0    0/ 0C
[001](000/255 min): 0x016E  00/5B   0/0(2)     0/0   FFFFFF/83F700  0/0    0/ 0C
[002](000/255 min): 0x009D  00/7F   0/0(3)     0/0   FFFFFF/03FFEE  0/0    0/ 0C
[003](000/255 min): 0x0000  00/5E   0/0(3)     0/0   FFFFFF/003FFF  0/0    0/ 0C
[004](000/255 min): 0x016C  00/51   0/0(3)     0/0   FFFFFF/07FC0C  0/0    0/ 0C
[005](000/255 min): 0x012E  00/4E   0/0(2)     0/0   FFFFFF/007CC0  0/0    0/ 0C
[006](000/255 min): 0x013E  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[007](000/255 min): 0x0055  00/57   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[008](000/255 min): 0x00E9  00/2B   0/0(1)     0/0   FFFFFF/0E0000  0/0    0/ 0C
[009](000/255 min): 0x012B  00/44   0/0(1)     0/0   FFFFFF/070002  0/0    0/ 0C
[010](000/255 min): 0x0157  00/72   0/0(2)     0/0   FFFFFF/FFFFFE  0/0    0/ 0C
[011](000/255 min): 0x00E5  00/4E   0/0(1)     0/0   FFFFFF/0601C0  0/0    0/ 0C
[012](000/255 min): 0x00DB  00/6A   0/0(3)     0/0   FFFFFF/3FFFE0  0/0    0/ 0C
[013](000/255 min): 0x016B  00/70   0/0(2)     0/0   FFFFFF/F80FFE  0/0    0/ 0C
[014](000/255 min): 0x00D0  00/6A   0/0(1)     0/0   FFFFFF/000FC0  0/0    0/ 0C
[015](000/255 min): 0x011F  00/6A   0/0(3)     0/0   FFFFFF/0FE7C0  0/0    0/ 0C
[016](000/255 min): 0x0115  00/7F   0/0(2)     0/0   FFFFFF/01E7F8  0/0    0/ 0C
[017](000/255 min): 0x012D  00/52   0/0(1)     0/0   FFFFFF/782600  0/0    0/ 0C
[018](000/255 min): 0x012F  00/30   0/0(1)     0/0   FFFFFF/860000  0/0    0/ 0C
[019](000/255 min): 0x0119  00/2B   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[020](000/255 min): 0x0133  00/76   0/0(3)     0/0   FFFFFF/0164EE  0/0    0/ 0C
[021](000/255 min): 0x0168  00/62   0/0(2)     0/0   FFFFFF/FFE7C0  0/0    0/ 0C
[022](000/255 min): 0x0163  00/34   0/0(1)     0/0   FFFFFF/060000  0/0    0/ 0C
[023](000/255 min): 0x00AF  00/4C   0/0(1)     0/0   FFFFFF/DFA006  0/0    0/ 0C
[024](000/255 min): 0x016D  00/66   0/0(3)     0/0   FFFFFF/FFFFE8  0/0    0/ 0C
[025](000/255 min): 0x015F  00/51   0/0(3)     0/0   FFFFFF/003800  0/0    0/ 0C
[026](000/255 min): 0x00EC  00/3C   0/0(2)     0/0   FFFFFF/3FE400  0/0    0/ 0C
[027](000/255 min): 0x0140  00/49   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[028](000/255 min): 0x000C  00/62   0/0(3)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[029](000/255 min): 0x0126  00/2B   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[030](000/255 min): 0x015D  00/76   0/0(3)     0/0   FFFFFF/0FFFFF  0/0    0/ 0C
[031](000/255 min): 0x0142  00/5B   0/0(3)     0/0   FFFFFF/01FC00  0/0    0/ 0C
[032](000/255 min): 0x002B  00/72   0/0(3)     0/0   FFFFFF/07FFFF  0/0    0/ 0C
[033](000/255 min): 0x014D  00/6A   0/0(2)     0/0   FFFFFF/0033C0  0/0    0/ 0C
[034](000
Receive beacon_request
    ->form: 091710FFFE020000
/255 min): 0x00B6  00/22   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0   0 FCC
[035](000/255 min): 0x011D  00/52   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[036](000/255 min): 0x00C7  00/5C   0/0(2)     0/0   FFFFFF/0FF006  0/0    0/ 0C
[037](000/255 min): 0x0008  00/3D   0/0(2)     0/0   FFFFFF/1F0000  0/0    0/ 0C
[038](000/255 min): 0x0017  00/39   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[039](000/255 min): 0x0002  00/1D   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[040](000/255 min): 0x002E  00/66   0/0(3)     0/0   FFFFFF/805FFF  0/0    0/ 0C
[041](000/255 min): 0x00CD  00/62   0/0(2)     0/0   FFFFFF/02FFE0  0/0    0/ 0C
[042](000/255 min): 0x013F  00/41   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[043](000/255 min): 0x0030  00/6B   0/0(3)     0/0   FFFFFF/01FFFE  0/0    0/ 0C
[044](000/255 min): 0x0072  00/45   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[045](000/255 min): 0x00CB  00/1F   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[046](000/255 min): 0x0025  00/26   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[047](000/255 min): 0x0110  00/70   0/0(2)     0/0   FFFFFF/3CFFFF  0/0    0/ 0C
[048](000/255 min): 0x00BF  00/53   0/0(2)     0/0   FFFFFF/3FF6FF  0/0    0/ 0C
[049](000/255 min): 0x0151  00/68   0/0(3)     0/0   FFFFFF/FFFFF0  0/0    0/ 0C
[050](000/255 min): 0x00DD  00/41   0/0(1)     0/0   FFFFFF/870000  0/0    0/ 0C
[051](000/255 min): 0x0161  00/6A   0/0(3)     0/0   FFFFFF/FFF00F  0/0    0/ 0C
[052](000/255 min): 0x012C  00/55   0/0(2)     0/0   FFFFFF/07E1C8  0/0    0/ 0C
[064](002/255 min): 0xFFFF  00/00   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C

```

拿dcu route
```
Routing Table : [000]-[099]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[001](64662m06s):  0x0025   0x0025     0x0017        1             0
[002](65534m16s):  0x0002   0x0002     0x00FF        1             1
[003](53278m24s):  0x000A   0x0012     0x0054        2             2
[004](53279m05s):  0x0009   0x0012     0x0064        3             1
[005](65199m11s):  0x0026   0x0026     0x002A        1             2
[006](63232m43s):  0x0023   0x0023     0x0018        1             0
[007](65527m32s):  0x0027   0x0027     0x002A        1             1
[008](53276m47s):  0x000C   0x000C     0x002A        1             1
[010](53276m47s):  0x000E   0x000C     0x0044        2             1
[011](53277m12s):  0x0007   0x0012     0x0034        2             0
[012](53283m14s):  0x0012   0x0012     0x002A        1             1
```
s2e
```
vt# s2e
LBS Short Address to EUI64 address Table
=====================================================
[00]0x0002    0x33 0x17 0x10 0xff 0xfe 0x02 0x00 0x00
[01]0x0027    0x83 0x11 0x39 0xff 0xfe 0x02 0x00 0x00
[02]0x0026    0x12 0x44 0x16 0xff 0xfe 0x02 0x00 0x0

```

找到一個好通的點
```
Bandplan   : FCC
EUI-64      : 1244:16ff:fe02:0000
Device      : LBD (FFD)  (uptime 52498 sec)
Already joined a pan
PAN ID      :  0x94ad
Short Addr  :  0x0026
IPv6 addr   :  fe800000 00000000 020000ff fe000000

Route discovery requset usage:
route a [short addr 0x??]
Routing Table : [000]-[099]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65185m42s):  0x0000   0x0000     0x0019        1             0
[001](65185m42s):  0x0002   0x0000     0x0118        2             1
[002](65533m54s):  0x0027   0x0027     0x001B        1             0

Magpie PLC Platform - Copyright (c) 2015 Vango Technologies, Inc.

SW version: v6.0.4.102 @e7bb268d
   2018/11/22 13:56:40

FW version: v6.0.0.0 @e8de071e - 80003733
   2018/11/02 13:21:11

Chip ver
LD MP B01
Band support: CEN-A FCC Vango
Others: Data coherent mode, Two RS blocks,

Reboot Counter 140
Reboot flag 0012
CLI|SDK|DMA| SQ|APP|UPG|CMD|CNF|RXQ|TFA|SEC|
  0|  1|  0|  0|  1|  0|  0|  0|  0|  0|  0|
DMA : 1
RX  : 20
TCIP: 31
Current_idx = 62
Index--[0]--
REG A0000080: 00000000
REG A0000084: 00040000
REG A00000C8: 00002007
REG A00000CC: 01000314
REG A00100B0: 20001001
REG A00100C8: 00000000
REG A0249C00: 00000010
REG A0249C10: 00000012
[0]:(  0D 23:31:52)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[1]:(  0D 23:46:42)SYSTEM_REBOOT (APP)
[2]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[3]:(  0D 23:45:43)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[4]:(  1D 00:00:19)SYSTEM_REBOOT (APP)
[5]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[6]:(  0D 23:44:27)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[7]:(  0D 23:59:30)SYSTEM_REBOOT (APP)
[8]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[9]:(  0D 23:45:29)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[10]:(  1D 00:00:26)SYSTEM_REBOOT (APP)
[11]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[12]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[13]:(  0D 20:53:30)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[14]:(  0D 21:08:05)SYSTEM_REBOOT (APP)
[15]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[16]:(  0D 00:00:36)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[17]:(  0D 00:10:00)SYSTEM_REBOOT (APP)
[18]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[19]:(  0D 23:36:09)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[20]:(  0D 23:50:40)SYSTEM_REBOOT (APP)
[21]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[22]:(  0D 23:45:50)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[23]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[24]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[25]:(  0D 00:01:26)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[26]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[27]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[28]:(  0D 00:01:09)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[29]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[30]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[31]:(  0D 00:01:56)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[32]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[33]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[34]:(  0D 00:02:27)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[35]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[36]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[37]:(  0D 00:00:39)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[38]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[39]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[40]:(  0D 00:00:39)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[41]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[42]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[43]:(  0D 00:00:49)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[44]:(  0D 20:39:52)SYSTEM_REBOOT (APP)
[45]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[46]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[47]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[48]:(  0D 05:57:42)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[49]:(  0D 06:13:27)SYSTEM_REBOOT (APP)
[50]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[51]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[52]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[53]:(  0D 14:44:25)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[54]:(  0D 15:00:18)SYSTEM_REBOOT (APP)
[55]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[56]:(  0D 23:44:09)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[57]:(  1D 00:00:05)SYSTEM_REBOOT (APP)
[58]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[59]:(  0D 23:43:59)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[60]:(  0D 23:58:50)SYSTEM_REBOOT (APP)
[61]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[62]:(  0D 00:00:44)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[63]:(  0D 00:02:10)SYSTEM_REBOOT (APP)
[64]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[65]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[66]:(  0D 09:54:58)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[67]:(  0D 10:09:37)SYSTEM_REBOOT (APP)
[68]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[69]:(  0D 23:45:06)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[70]:(  0D 23:59:53)SYSTEM_REBOOT (APP)
[71]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[72]:(  0D 23:45:14)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[73]:(  1D 00:00:08)SYSTEM_REBOOT (APP)
[74]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[75]:(  0D 00:02:45)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[76]:(  0D 00:08:47)SYSTEM_REBOOT (APP)
[77]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[78]:(  0D 23:37:39)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[79]:(  0D 23:53:01)SYSTEM_REBOOT (APP)
[80]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[81]:(  0D 23:44:16)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[82]:(  0D 23:59:00)SYSTEM_REBOOT (APP)
[83]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[84]:(  0D 23:44:46)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[85]:(  1D 00:00:08)SYSTEM_REBOOT (APP)
[86]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[87]:(  0D 23:46:29)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[88]:(  0D 23:58:56)SYSTEM_REBOOT (APP)
[89]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[90]:(  0D 06:42:45)BOOTSTRAP_JOIN_SUCCESS  (PANID 0xBCD0)
[91]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[92]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[93]:(  0D 07:27:48)BOOTSTRAP_JOIN_SUCCESS  (PANID 0xBCD0)
[94]:(  0D 23:56:09)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[95]:(  1D 00:00:09)SYSTEM_REBOOT (SDK_WD)
[96]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[97]:(  0D 00:00:50)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[98]:(  0D 00:17:44)SYSTEM_REBOOT (APP)
[99]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[100]:(  0D 00:01:01)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[101]:(  0D 00:07:05)SYSTEM_REBOOT (APP)
[102]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[103]:(  0D 00:27:03)BOOTSTRAP_JOIN_SUCCESS  (PANID 0xBCD0)
[104]:(  0D 23:20:27)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[105]:(  0D 23:38:41)SYSTEM_REBOOT (APP)
[106]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[107]:(  0D 23:40:52)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[108]:(  1D 00:18:45)SYSTEM_REBOOT (APP)
[109]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[110]:(  0D 23:22:03)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[111]:(  0D 23:37:27)SYSTEM_REBOOT (APP)
[112]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[113]:(  0D 23:45:11)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[114]:(  1D 00:00:34)SYSTEM_REBOOT (APP)
[115]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[116]:(  0D 23:45:01)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[117]:(  1D 00:01:21)SYSTEM_REBOOT (APP)
[118]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[119]:(  0D 23:42:40)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[120]:(  0D 23:57:37)SYSTEM_REBOOT (APP)
[121]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[122]:(  0D 23:45:14)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x00AE)
[123]:(  0D 23:59:57)SYSTEM_REBOOT (APP)
[124]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
[125]:(  0D 00:00:44)BOOTSTRAP_JOIN_SUCCESS  (PANID 0x94AD)
[126]:(  0D 00:13:12)SYSTEM_REBOOT (APP)
[127]:(  0D 00:00:00)SYSTEM_BOOT_UP (Power Button Reset) (RTC Alarm)
```


```
Receive beacon_request
    ->form: 091710FFFE020000
nfig

Bandplan   : FCC
Device     : LBD (FFD) (start time 201 sec) AC:50 Hz Pan 0x94AD  Joined
Short Addr : 0x0027 (RC_COORD 0x002A)
Extended Addr : 0x8311 39FF FE02 0000

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65534m49s):  0x0000   0x0000     0x002A        1             1

BlackList Table : [000]-[127]
                    Dest

BlackList History : [00]-[127]
         Dest

RREQ History : [00]-[31]
         Dest    Ori     Seq

Neighbor Table : [000]-[64] TX Use Neighbor table
      TMR/Neighgor   Dest    LQI  Mod(expect) Scheme    ToneMap    TXRES TXGAINe
[000](000/252 min): 0x013E  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[001](000/253 min): 0x00D0  00/5B   0/0(3)     0/0   FFFFFF/FE6FC0  0/0    0/ 0C
[002](000/252 min): 0x012F  00/2B   0/0(1)     0/0   FFFFFF/8E0000  0/0    0/ 0C
[003](000/252 min): 0x0151  00/66   0/0(3)     0/0   FFFFFF/FFFFF0  0/0    0/ 0C
[004](000/252 min): 0x00EC  00/46   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[005](000/252 min): 0x008D  00/2B   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[006](000/252 min): 0x0055  00/2E   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[007](000/252 min): 0x00DD  00/33   0/0(2)     0/0   FFFFFF/FF0000  0/0    0/ 0C
[008](000/252 min): 0x0145  00/40   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[009](000/252 min): 0x0168  00/5A   0/0(3)     0/0   FFFFFF/F1E780  0/0    0/ 0C
[010](000/252 min): 0x016D  00/6B   0/0(3)     0/0   FFFFFF/FFFFE0  0/0    0/ 0C
[011](000/252 min): 0x00E9  00/2A   0/0(1)     0/0   FFFFFF/0F0000  0/0    0/ 0C
[012](000/252 min): 0x0119  00/27   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[013](002/254 min): 0x0000  00/64   3/3(3)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[014](000/252 min): 0x016E  00/54   0/0(2)     0/0   FFFFFF/87FE00  0/0    0/ 0C
[015](000/252 min): 0x00AF  00/46   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[016](000/253 min): 0x0090  00/51   0/0(1)     0/0   FFFFFF/007C20  0/0    0/ 0C
[017](000/253 min): 0x0149  00/51   0/0(2)     0/0   FFFFFF/03E180  0/0    0/ 0C
[018](000/253 min): 0x011D  00/53   0/0(2)     0/0   FFFFFF/CC0000  0/0    0/ 0C
[019](000/253 min): 0x003C  00/3C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[020](000/253 min): 0x0110  00/4F   0/0(2)     0/0   FFFFFF/400FF8  0/0    0/ 0C
[021](000/253 min): 0x000C  00/63   0/0(3)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[022](000/253 min): 0x0161  00/63   0/0(3)     0/0   FFFFFF/FFF01B  0/0    0/ 0C
[023](000/253 min): 0x0156  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[024](000/253 min): 0x012D  00/3E   0/0(2)     0/0   FFFFFF/FFBE00  0/0    0/ 0C
[025](000/253 min): 0x002E  00/62   0/0(3)     0/0   FFFFFF/E787FF  0/0    0/ 0C
[026](000/253 min): 0x0003  00/30   0/0(1)     0/0   FFFFFF/078000  0/0    0/ 0C
[027](000/253 min): 0x0115  00/76   0/0(3)     0/0   FFFFFF/00F1F8  0/0    0/ 0C
[028](000/254 min): 0x014D  00/3A   0/0(1)     0/0   FFFFFF/000060  0/0    0/ 0C
[029](000/253 min): 0x00DC  00/29   0/0(2)     0/0   FFFFFF/060000  0/0    0/ 0C
[030](000/253 min): 0x00E5  00/1D   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[031](000/253 min): 0x012B  00/45   0/0(2)     0/0   FFFFFF/073000  0/0    0/ 0C
[032](000/253 min): 0x0163  00/2F   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[033](000/253 min): 0x00BF  00/60   0/0(2)     0/0   FFFFFF/3FFFBB  0/0    0/ 0C
[034](000/253 min): 0x009D  00/51   0/0(2)     0/0   FFFFFF/006642  0/0    0/ 0C
[035](000/253 min): 0x0140  00/42   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[036](000/253 min): 0x015D  00/70   0/0(3)     0/0   FFFFFF/07FFFF  0/0    0/ 0C
[037](000/255 min): 0x0026  00/62   0/0(3)     0/0   FFFFFF/C307FF  0/0    0/ 0C
[038](000/253 min): 0x016C  00/33   0/0(1)     0/0   FFFFFF/000440  0/0    0/ 0C
[039](000/253 min): 0x0095  00/43   0/0(2)     0/0   FFFFFF/003800  0/0    0/ 0C
[040](000/253 min): 0x011F  00/62   0/0(3)     0/0   FFFFFF/0FFFC0  0/0    0/ 0C
[041](000/253 min): 0x0142  00/62   0/0(3)     0/0   FFFFFF/00F800  0/0    0/ 0C
[042](000/253 min): 0x0133  00/76   0/0(3)     0/0   FFFFFF/01FDFA  0/0    0/ 0C
[043](000/253 min): 0x012C  00/63   0/0(2)     0/0   FFFFFF/1FF180  0/0    0/ 0C
[044](000/253 min): 0x00CD  00/6B   0/0(2)     0/0   FFFFFF/760640  0/0    0/ 0C
[045](000/253 min): 0x012E  00/45   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[046](000/253 min): 0x0017  00/2E   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[047](000/253 min): 0x016B  00/46   0/0(2)     0/0   FFFFFF/0003E0  0/0    0/ 0C
[048](000/253 min): 0x002B  00/6B   0/0(3)     0/0   FFFFFF/07FFFF  0/0    0/ 0C
[049](000/253 min): 0x013A  00/24   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[050](000/253 min): 0x0030  00/6D   0/0(3)     0/0   FFFFFF/007FFA  0/0    0/ 0C
[051](000/253 min): 0x0157  00/5D   0/0(2)     0/0   FFFFFF/3F4800  0/0    0/ 0C
[052](000/255 min): 0x00DB  00/62   0/0(2)     0/0   FFFFFF/7F6FC0  0/0    0/ 0C
[064](002/255 min): 0xFFFF  00/00   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C

MHR SEQ History : [00]-[15]
         Src     Seq
[00]     0000     103
[01]     000002feff180912     141
[02]     000002feff207864     170

vt#
Receive beacon_request
    ->form: 911211FFFE020000
Send Beacon (RC_COORD 0x2A)

Receive beacon_request
    ->form: 120918FFFE020000

vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt#
Receive beacon_request
    ->form: 647820FFFE020000

vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt#
vt# Send Beacon (RC_COORD 0x2A)
route

Device     : LBD (FFD) Pan 0x94AD  Joined
Short Addr : 0x0027 (RC_COORD 0x002A)
Extended Addr : 0x8311 39FF FE02 0000

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65534m14s):  0x0000   0x0000     0x002A        1             1

BlackList Table : [000]-[127]
                    Dest

BlackList History : [00]-[127]
         Dest

RREQ History : [00]-[31]
         Dest    Ori     Seq

vt#
vt#
vt# ping 0

command not found !

vt#
Receive beacon_request
    ->form: 485015FFFE020000

Receive beacon_request
    ->form: 120918FFFE020000
pinge 0


Ping Test : size 7, loops 1
.
Total Send Tx/Rx : 1/1, AvgTime 132 msec
PER : 0.0 %

vt# pinge 0 100


Ping Test : size 7, loops 100
...................
Receive beacon_request
    ->form: 598714FFFE020000
..
Receive beacon_request
    ->form: 911211FFFE020000
...............................C Addr 20006531 | 00 00 02 FE FF 20 78 64
 mhr seq 0xae
 Dup Drop.....
Receive beacon_request
    ->form: 647820FFFE020000
...................
Receive beacon_request
    ->form: 091710FFFE020000
........................
Total Send Tx/Rx : 100/100, AvgTime 156 msec
PER : 0.0 %

vt#
Receive beacon_request
    ->form: 120918FFFE020000

Receive beacon_request
    ->form: 379014FFFE020000

Receive beacon_request
    ->form: 911211FFFE020000
>> proc_rx_from_plc: Rx from SHORT-PLC(26)

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 93 bf 32 7e 39 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 93 bf 32 7e 39 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (32) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](59)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (59)

      56 83 11 39 02 00 00 81 00 00 2d 00 01 06 20 7e
      a0 1e 23 03 73 7b cf 81 80 12 05 01 80 06 01 80
      07 04 00 00 00 01 08 04 00 00 00 01 53 3b 7e 01
      02 03 00 00 68 01 01 01 02 7b 54


Send ToneMap Response(0000), Mod D-3 0xFFFFFF
>> proc_rx_from_plc: Rx from SHORT-PLC(65)

      56 83 11 39 02 00 00 01 00 00 33 00 01 06 30 7e
      a0 2e 03 23 10 1c f2 e6 e6 00 60 20 80 02 07 80
      a1 09 06 07 60 85 74 05 08 01 01 be 0f 04 0d 01
      00 00 00 06 5f 04 00 ff ff ff 28 00 9f 38 7e 00
      54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 33 00 01 06 30 7e
      a0 2e 03 23 10 1c f2 e6 e6 00 60 20 80 02 07 80
      a1 09 06 07 60 85 74 05 08 01 01 be 0f 04 0d 01
      00 00 00 06 5f 04 00 ff ff ff 28 00 9f 38 7e 00
      54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (57) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](84)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (84)

      56 83 11 39 02 00 00 81 00 00 46 00 01 06 39 7e
      a0 37 23 03 30 d4 c9 e6 e7 00 61 29 a1 09 06 07
      60 85 74 05 08 01 01 a2 03 02 01 00 a3 05 a1 03
      02 01 00 be 10 04 0e 08 00 06 5f 1f 04 00 00 1a
      1d 01 90 00 07 62 51 7e 01 02 03 00 00 60 01 01
      01 02 66 54

>> proc_rx_from_plc: Rx from SHORT-PLC(98)

      56 83 11 39 02 00 00 01 00 00 54 00 01 06 51 7e
      a0 4f 03 23 32 53 75 e6 e6 00 c0 01 81 00 07 01
      00 62 01 00 ff 02 01 01 02 04 02 04 12 00 08 09
      06 00 00 01 00 00 ff 0f 02 12 00 00 09 0c 07 e3
      09 17 01 0b 38 00 00 00 00 00 09 0c 07 e3 09 18
      02 0d 2a 00 00 00 00 00 01 00 00 00 00 69 05 7e
      86 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 54 00 01 06 51 7e
      a0 4f 03 23 32 53 75 e6 e6 00 c0 01 81 00 07 01
      00 62 01 00 ff 02 01 01 02 04 02 04 12 00 08 09
      06 00 00 01 00 00 ff 0f 02 12 00 00 09 0c 07 e3
      09 17 01 0b 38 00 00 00 00 00 09 0c 07 e3 09 18
      02 0d 2a 00 00 00 00 00 01 00 00 00 00 69 05 7e
      86 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (20) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](47)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (47)

      56 83 11 39 02 00 00 81 00 00 21 00 01 06 14 7e
      a0 12 23 03 52 c4 68 e6 e7 00 c4 01 81 00 01 00
      3d ce 7e 01 02 03 00 00 67 01 01 01 02 6f 54

>> proc_rx_from_plc: Rx from SHORT-PLC(98)

      56 83 11 39 02 00 00 01 00 00 54 00 01 06 51 7e
      a0 4f 03 23 54 63 73 e6 e6 00 c0 01 81 00 07 01
      00 63 05 00 ff 02 01 01 02 04 02 04 12 00 08 09
      06 00 00 01 00 00 ff 0f 02 12 00 00 09 0c 07 e3
      09 11 02 00 01 00 00 00 00 00 09 0c 07 e3 09 18
      02 0d 2a 00 00 00 00 00 01 00 00 00 00 08 18 7e
      26 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 54 00 01 06 51 7e
      a0 4f 03 23 54 63 73 e6 e6 00 c0 01 81 00 07 01
      00 63 05 00 ff 02 01 01 02 04 02 04 12 00 08 09
      06 00 00 01 00 00 ff 0f 02 12 00 00 09 0c 07 e3
      09 11 02 00 01 00 00 00 00 00 09 0c 07 e3 09 18
      02 0d 2a 00 00 00 00 00 01 00 00 00 00 08 18 7e
      26 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (139) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](166)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (166)

      56 83 11 39 02 00 00 81 00 00 98 00 01 06 8b 7e
      a8 89 23 03 74 0a 58 e6 e7 00 c4 02 81 00 00 00
      00 01 00 82 01 5e 01 04 02 15 09 0c 07 e3 09 15
      06 00 00 00 ff 80 00 02 06 00 00 00 6c 06 00 00
      00 00 06 00 00 00 6c 06 00 00 00 00 06 00 00 00
      00 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00
      06 00 00 00 00 06 00 00 00 00 06 00 00 00 19 06
      00 00 00 00 06 00 00 00 19 06 00 00 00 00 06 00
      00 00 00 06 00 00 00 00 06 00 00 00 00 06 00 00
      00 00 06 00 00 00 00 79 d7 7e 01 02 03 00 00 6d
      01 01 01 02 9d 54

>> proc_rx_from_plc: Rx from SHORT-PLC(26)

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 71 a3 f6 7e bf 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 71 a3 f6 7e bf 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (139) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](166)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (166)

      56 83 11 39 02 00 00 81 00 00 98 00 01 06 8b 7e
      a8 89 23 03 76 18 7b 06 00 00 00 00 02 15 09 0c
      07 e3 09 16 07 00 00 00 ff 80
Receive beacon_request
    ->form: 485015FFFE020000
00 02 06 00 00 00
      6c 06 00 00 00 00 06 00 00 00 6c 06 00 00 00 00
      06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 06
      00 00 00 00 06 00 00 00 00 06 00 00 00 00 06 00
      00 00 19 06 00 00 00 00 06 00 00 00 19 06 00 00
      00 00 06 00 00 00 00 06 00 00 00 00 06 00 00 00
      00 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00
      02 15 09 0c 07 e3 09 60 bd 7e 01 02 03 00 00 66
      01 01 01 02 c8 54


Receive beacon_request
    ->form: 598714FFFE020000
>> proc_rx_from_plc: Rx from SHORT-PLC(26)

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 91 ad 11 7e 04 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 91 ad 11 7e 04 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (120) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](147)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (147)

      56 83 11 39 02 00 00 81 00 00 85 00 01 06 78 7e
      a0 76 23 03 78 94 0d 17 01 00 00 00 ff 80 00 02
      06 00 00 00 6c 06 00 00 00 00 06 00 00 00 6c 06
      00 00 00 00 06 00 00 00 00 06 00 00 00 00 06 00
      00 00 00 06 00 00 00 00 06 00 00 00 00 06 00 00
      00 00 06 00 00 00 19 06 00 00 00 00 06 00 00 00
      19 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00
      06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 06
      00 00 00 00 03 08 7e 01 02 03 00 00 5f 01 01 01
      02 35 54

>> proc_rx_from_plc: Rx from SHORT-PLC(38)

      56 83 11 39 02 00 00 01 00 00 18 00 01 06 15 7e
      a0 13 03 23 b6 5d f5 e6 e6 00 c0 02 81 00 00 00
      01 73 7f 7e e3 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 18 00 01 06 15 7e
      a0 13 03 23 b6 5d f5 e6 e6 00 c0 02 81 00 00 00
      01 73 7f 7e e3 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
Send Beacon (RC_COORD 0x2A)
>> proc_rx_from_uart: (139) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](166)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (166)

      56 83 11 39 02 00 00 81 00 00 98 00 01 06 8b 7e
      a8 89 23 03 9a 7a 56 e6 e7 00 c4 02 81 01 00 00
      00 02 00 74 02 15 09 0c 07 e3 09 18 02 00 00 00
      ff 80 00 02 06 00 00 00 6c 06 00 00 00 00 06 00
      00 00 6c 06 00 00 00 00 06 00 00 00 00 06 00 00
      00 00 06 00 00 00 00 06 00 00 00 00 06 00 00 00
      00 06 00 00 00 00 06 00 00 00 19 06 00 00 00 00
      06 00 00 00 19 06 00 00 00 00 06 00 00 00 00 06
      00 00 00 00 06 00 00 00 00 06 00 00 00 00 06 00
      00 00 00 06 00 00 00 70 91 7e 01 02 03 00 00 68
      01 01 01 02 72 54

>> proc_rx_from_plc: Rx from SHORT-PLC(26)

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 d1 a9 53 7e 82 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 d1 a9 53 7e 82 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (12) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](39)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (39)

      56 83 11 39 02 00 00 81 00 00 19 00 01 06 0c 7e
      a0 0a 23 03 9c cf 61 00 cc c6 7e 01 02 03 00 00
      64 01 01 01 02 15 54

>> proc_rx_from_plc: Rx from SHORT-PLC(98)

      56 83 11 39 02 00 00 01 00 00 54 00 01 06 51 7e
      a0 4f 03 23 f8 05 1c e6 e6 00 c0 01 81 00 07 00
      00 63 62 04 ff 02 01 01 02 04 02 04 12 00 08 09
      06 00 00 01 00 00 ff 0f 02 12 00 00 09 0c 07 e3
      09 17 01 00 01 00 00 00 00 00 09 0c 07 e3 09 18
      02 0d 2a 00 00 00 00 00 01 00 00 00 00 b9 9d 7e
      b0 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 54 00 01 06 51 7e
      a0 4f 03 23 f8 05 1c e6 e6 00 c0 01 81 00 07 00
      00 63 62 04 ff 02 01 01 02 04 02 04 12 00 08 09
      06 00 00 01 00 00 ff 0f 02 12 00 00 09 0c 07 e3
      09 17 01 00 01 00 00 00 00 00 09 0c 07 e3 09 18
      02 0d 2a 00 00 00 00 00 01 00 00 00 00 b9 9d 7e
      b0 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (139) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](166)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (166)

      56 83 11 39 02 00 00 81 00 00 98 00 01 06 8b 7e
      a8 89 23 03 be 5c 31 e6 e7 00 c4 01 81 00 01 14
      02 02 09 0c 07 e3 09 17 01 10 07 29 ff 80 00 02
      11 55 02 02 09 0c 07 e3 09 17 01 10 30 0e ff 80
      00 02 11 56 02 02 09 0c 07 e3 09 17 01 11 30 29
      ff 80 00 02 11 55 02 02 09 0c 07 e3 09 18 02 09
      1c 0d ff 80 00 02 11 56 02 02 09 0c 07 e3 09 18
      02 09 2f 2f ff 80 00 02 11 55 02 02 09 0c 07 e3
      09 18 02 09 31 10 ff 80 00 02 11 56 02 02 09 0c
      07 e3 09 18 02 09 33 4d e9 7e 01 02 03 00 00 63
      01 01 01 02 6c 54

>> proc_rx_from_plc: Rx from SHORT-PLC(26)

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 11 a5 95 7e 00 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 11 a5 95 7e 00 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (139) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](166)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (166)

      56 83 11 39 02 00 00 81 00 00 98 00 01 06 8b 7e
      a8 89 23 03 b0 22 d8 0c ff 80 00 02 11 55 02 02
      09 0c 07 e3 09 18 02 09 34 01 ff 80 00 02 11 56
      02 02 09 0c 07 e3 09 18 02 0a 01 11 ff 80 00 02
      11 55 02 02 09 0c 07 e3 09 18 02 0a 21 1a ff 80
      00 02 11 56 02 02 09 0c 07 e3 09 18 02 0a 2e 2f
      ff 80 00 02 11 55 02 02 09 0c 07 e3 09 18 02 0a
      32 09 ff 80 00 02 11 56 02 02 09 0c 07 e3 09 18
      02 0a 34 34 ff 80 00 02 11 55 02 02 09 0c 07 e3
      09 18 02 0a 37 1e ff b5 38 7e 01 02 03 00 00 68
      01 01 01 02 5c 54

>> proc_rx_from_plc: Rx from SHORT-PLC(26)

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 31 a7 b4 7e 41 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 31 a7 b4 7e 41 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (124) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](151)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (151)

      56 83 11 39 02 00 00 81 00 00 89 00 01 06 7c 7e
      a0 7a 23 03 b2 f6 f3 80 00 02 11 56 02 02 09 0c
      07 e3 09 18 02 0a 38 1d ff 80 00 02 11 37 02 02
      09 0c 07 e3 09 18 02 0b 04 0c ff 80 00 02 11 55
      02 02 09 0c 07 e3 09 18 02 0b 04 36 ff 80 00 02
      11 56 02 02 09 0c 07 e3 09 18 02 0b 0d 0a ff 80
      00 02 11 55 02 02 09 0c 07 e3 09 18 02 0d 24 2f
      ff 80 00 02 11 56 02 02 09 0c 07 e3 09 18 02 0d
      25 2f ff 80 00 02 11 38 52 54 7e 01 02 03 00 00
      66 01 01 01 02 ea 54

>> proc_rx_from_plc: Rx from SHORT-PLC(26)

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 53 b3 f4 7e af 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 01 00 00 0c 00 01 06 09 7e
      a0 07 03 23 53 b3 f4 7e af 54

>> proc_rx_from_plc: vt-seq:(0)
>> query [VT-QUERY] remove VT header
>> proc_rx_from_uart: (32) idx:0
g_plc_msg_hdr.dest_addr = 00 00
>> vt_ptcl_rebuild_buf: Tx to DCU [VT-QUERY](59)
>> proc_rx_from_uart: vt-seq:(0)
>> proc_rx_from_uart: Tx to DCU (59)

      56 83 11 39 02 00 00 81 00 00 2d 00 01 06 20 7e
      a0 1e 23 03 73 7b cf 81 80 12 05 01 80 06 01 80
      07 04 00 00 00 01 08 04 00 00 00 01 53 3b 7e 01
      02 03 00 00 67 01 01 01 02 7a 54


Receive beacon_request
    ->form: 091710FFFE020000

Receive beacon_request
    ->form: 911211FFFE020000

vt#
vt#
vt#
vt#
vt#
Receive beacon_request
    ->form: 120918FFFE020000

Receive beacon_request
    ->form: 598714FFFE020000

Receive beacon_request
    ->form: 647820FFFE020000

Receive beacon_request
    ->form: 120918FFFE020000

Receive beacon_request
    ->form: 911211FFFE020000

Receive beacon_request
    ->form: 598714FFFE020000

Receive beacon_request
    ->form: 091710FFFE020000
Send Beacon (RC_COORD 0x2A)
>> proc_rx_from_plc: Rx from SHORT-PLC(21)

      56 83 11 39 02 00 00 03 00 00 07 00 01 04 04 8b
      fd d7 30 71 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 03 00 00 07 00 01 04 04 8b
      fd d7 30 71 54

>> proc_rx_from_plc: vt-seq:(0)
>> heartbeat: (4)
>> heartbeat: rep VT-HEARTBEAT(35)

      56 83 11 39 02 00 00 83 00 00 15 00 01 04 04 8b
      fd d7 30 01 02 03 00 00 64 01 03 01 06 01 01 01
      02 79 54


Receive beacon_request
    ->form: 120918FFFE020000

Receive beacon_request
    ->form: 598714FFFE020000

Receive beacon_request
    ->form: 647820FFFE020000

Receive beacon_request
    ->form: 581015FFFE020000

Receive beacon_request
    ->form: 647820FFFE020000

Receive beacon_request
    ->form: 598714FFFE020000
Send Beacon (RC_COORD 0x2A)

Receive beacon_request
    ->form: 120918FFFE020000

Receive beacon_request
    ->form: 091710FFFE020000
Add D:0002 O:0000  seq:5117 to RREQ History[0]
C Addr 20006531 | 00 00 02 FE FF 20 78 64
 mhr seq 0xb8
 Dup Drop
Send ToneMap Response(0000), Mod D-3 0xFFFFFF
>> holley_send_heartbeat(20)

      68 0e 0e 68 5a 99 99 06 82 05 55 30 00 aa aa 00
      01 00 77 16


Receive beacon_request
    ->form: 647820FFFE020000

Receive beacon_request
    ->form: 091710FFFE020000

Receive beacon_request
    ->form: 120918FFFE020000
Send Beacon (RC_COORD 0x2A)

Receive beacon_request
    ->form: 598714FFFE020000
>> proc_rx_from_plc: Rx from SHORT-PLC(21)

      56 83 11 39 02 00 00 03 00 00 07 00 01 04 04 8a
      74 d9 30 e9 54

g_plc_msg_hdr.dest_addr = 00 00

      56 83 11 39 02 00 00 03 00 00 07 00 01 04 04 8a
      74 d9 30 e9 54

>> proc_rx_from_plc: vt-seq:(0)
>> heartbeat: (4)
>> heartbeat: rep VT-HEARTBEAT(35)

      56 83 11 39 02 00 00 83 00 00 15 00 01 04 04 8a
      74 d9 30 01 02 03 00 00 63 01 03 01 06 01 01 01
      02 f0 54


Receive beacon_request
    ->form: 485015FFFE020000

Receive beacon_request
    ->form: 485015FFFE020000

Receive beacon_request
    ->form: 911211FFFE020000

Receive beacon_request
    ->form: 911211FFFE020000

Receive beacon_request
    ->form: 091710FFFE020000

Receive beacon_request
    ->form: 647820FFFE020000

CTRL-A Z for help | 115200 8N1 | NOR | Minicom 2.7 | VT102 | Offline | ttyUSB

Receive beacon_request
    ->form: 120918FFFE020000

Receive beacon_request
    ->form: 667820FFFE020000

Receive beacon_request
    ->form: 379014FFFE020000

Receive beacon_request
    ->form: 379014FFFE020000

Receive beacon_request
    ->form: 861211FFFE020000

Receive beacon_request
    ->form: 131710FFFE020000

Receive beacon_request
    ->form: 911211FFFE020000

Receive beacon_request
    ->form: 598714FFFE020000

Receive beacon_request
    ->form: 581015FFFE020000

Receive beacon_request
    ->form: 091710FFFE020000

Receive beacon_request
    ->form: 046016FFFE020000


    IDLE_GAIN_STATISTIC(curr 39)
    analog_gain         : 33
    digital_gain        : 6
    igt_mib_process_cnt : 12548
    igt_mib_drop_cnt    : 312
    igt ( >=90dB)      : 0
    igt (   84dB)      : 0
    igt (   78dB)      : 0
    igt (   72dB)      : 0
    igt (   66dB)      : 0
    igt (   60dB)      : 4
    igt (   54dB)      : 2
    igt (   48dB)      : 359
    igt (   42dB)      : 5281
    igt (   36dB)      : 6590
    igt (   30dB)      : 0
IGT_FORCE
    freeze              : 0
    analog_gain         : 33
    digital_gain        : 6
    cic1_pwr_max        : 36647 (expect 11400 ~ 22800)
    cic1_pwr_tgt        : 7600
    rx_filt_out_pwr_max : 10925 (expect 5700 ~ 11400)
    rx_filt_out_pwr_tgt : 3800

vt#
Receive beacon_request
    ->form: 120918FFFE020000

vt#
vt#
vt#
vt#
Receive beacon_request
    ->form: 799014FFFE020000
snr

command not found !

vt# bbp a

TDP_FIR3_SCALE
    fir3_scale               : 0x00000086(134)
    fir3_scale_max           : 0x00000086(134)
    fir3_scale_min           : 0x0000000A(10)
    fir3_tx_success_cnt      : 0x0000002B(43)
    fir3_add_3db_thr         : 0x0000000F(15)
    urgent_in_cnt            : 0x00000000(0)
    ot_event_cnt             : 0x00000000(0)
    start_specific_tx_scale  : 0x00000086(134)
    last_abnormal_tx_scale   : 0x00000086(134)
PHY_RX_MIB_CNT
    preamb_det                  : 10086
    syncm_det_success           : 9339 (Fail 747)
    fch_crc_correct             : 8561 (fch cnt error)
    ack_nack_fch_correct        : 2367
    ACK/NACK/Unmatch_correct    : 193 / 0 / 2
    data_fch_correct            : 6192
    data_correct                : 5611 (Fail 581)
    apdt_basic_time_block(btb)  : 763
    apdt_preamb_det_scale       : 20 (Max 22, Min 20)
    apdt_preamb_noise_thr       : 30 (Max 30, Min30)
    apdt_latched_fa_cnt_per_btb : 1 (AVG 0.97)
    apdt_scale_event            : 1(Up), 2(Dn)
    apdt_huge_fa_in_rx_event    : 0
    syncm_location              :    0     1     2     3     4     5
                                   400  8567   151    61    30   130 (counter)
    FCH Error                   : 2, (format 24 D1 CE 49  B6 42 A4 60)
PHY_TX_MIB_CNT               Data     ACK    NACK
    mac_send_cnt         :    223     208      65
    mac_abort_cnt        :      0       0       0
    mac_drop_cnt         :      0       0
    retransmit_cnt       : 34
    data_param_err_cnt   : 0

vt#
Send ToneMap Response(0000), Mod D-3 0xFFFFFF

vt#
Receive beacon_request
    ->form: 647820FFFE020000

vt#
Receive beacon_request
    ->form: 091710FFFE020000
i
Receive beacon_request
    ->form: 667820FFFE020000

Receive beacon_request
    ->form: 598714FFFE020000

Receive beacon_request
    ->form: 581015FFFE020000
lqi

LQI[28] = 211
LQI[29] = 21
LQI[30] = 28
LQI[31] = 14
LQI[32] = 15
LQI[33] = 19
LQI[34] = 13
LQI[35] = 23
LQI[36] = 18
LQI[37] = 21
LQI[38] = 16
LQI[39] = 17
LQI[40] = 22
LQI[41] = 36
LQI[42] = 36
LQI[43] = 17
LQI[44] = 17
LQI[45] = 17
LQI[46] = 25
LQI[47] = 25
LQI[48] = 17
LQI[49] = 11
LQI[50] = 16
LQI[51] = 20
LQI[52] = 20
LQI[53] = 25
LQI[54] = 31
LQI[55] = 22
LQI[56] = 16
LQI[57] = 14
LQI[58] = 15
LQI[59] = 11
LQI[60] = 11
LQI[61] = 13
LQI[62] = 7
LQI[63] = 11
LQI[64] = 15
LQI[65] = 28
LQI[66] = 21
LQI[67] = 26
LQI[68] = 25
LQI[69] = 25
LQI[70] = 25
LQI[72] = 21
LQI[73] = 16
LQI[74] = 22
LQI[75] = 17
LQI[76] = 21
LQI[77] = 18
LQI[78] = 33
LQI[79] = 31
LQI[80] = 15
LQI[81] = 34
LQI[82] = 27
LQI[83] = 24
LQI[84] = 27
LQI[85] = 23
LQI[86] = 26
LQI[87] = 14
LQI[88] = 23
LQI[90] = 16
LQI[91] = 44
LQI[92] = 44
LQI[93] = 35
LQI[94] = 52
LQI[95] = 70
LQI[96] = 70
LQI[98] = 72
LQI[99] = 90
LQI[100] = 90
LQI[102] = 120
LQI[103] = 120
LQI[104] = 96
LQI[106] = 145
LQI[107] = 99
LQI[109] = 57
LQI[111] = 59
LQI[112] = 38
LQI[114] = 28
LQI[116] = 27
LQI[118] = 16
LQI[120] = 19
LQI[122] = 12
LQI[124] = 13
LQI[127] = 30
AVG LQI = 79.21 (SNR 9.1577)
STD LQI = 28.6
SNR[-3] = 232
SNR[-2] = 76
SNR[-1] = 75
SNR[0] = 91
SNR[1] = 87
SNR[2] = 78
SNR[3] = 81
SNR[4] = 83
SNR[5] = 50
SNR[6] = 61
SNR[7] = 97
SNR[8] = 62
SNR[9] = 78
SNR[10] = 113
SNR[11] = 101
SNR[12] = 63
SNR[13] = 139
SNR[14] = 192
SNR[15] = 252
SNR[16] = 255
SNR[17] = 255
SNR[18] = 97
SNR[>18] = 145
SNR_MIB    Preamble  Data
current  :        0    11
   0 dB  :     2422     0
   1 dB  :      480     0
   2 dB  :      402     0
   3 dB  :      489     0
   4 dB  :      585     0
   5 dB  :      416     0
   6 dB  :      465     0
   7 dB  :      494    98
   8 dB  :      417   533
   9 dB  :      410   738
  10 dB  :      675  1417
  11 dB  :      490  1017
  12 dB  :      545   827
  13 dB  :      983  1063
  14 dB  :     1086   825
  15 dB  :      710   706
  16 dB  :     1287  1117
  17 dB  :     1287   926
  18 dB  :     1667   759
  19 dB  :     1858  1578
  20 dB  :     1898   937
  21 dB  :     1362  6146

```


## 換DCU

```
[000]:0x23 0x82 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[001]:0x29 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[002]:0x30 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[003]:0x31 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[004]:0x32 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[005]:0x33 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[006]:0x35 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[007]:0x33 0x17 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[008]:0x73 0x85 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[009]:0x06 0x90 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[010]:0x08 0x90 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[011]:0x09 0x90 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[012]:0x79 0x06 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[013]:0x85 0x06 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[014]:0x89 0x06 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[015]:0x09 0x12 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[016]:0x10 0x12 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[017]:0x12 0x49 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[018]:0x21 0x17 0x16 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[019]:0x83 0x27 0x18 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[020]:0x77 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[021]:0x78 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[022]:0x79 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[023]:0x81 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[024]:0x82 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[025]:0x46 0x85 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[026]:0x78 0x92 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[027]:0x83 0x11 0x39 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
Total 28 (Msg1 28, Msg3 0, Accept 0, ReJoin 0)


```


