var hours = 0;

// Script to open and close sidebar
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}


var dropdown = document.getElementsByClassName("dropdown-btn");
var i;
for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}


function pbar_reset() {
  $("#doneBar").hide();
  document.getElementById("pbar").style.width = '0%';
  document.getElementById("pbarStatus").innerHTML = "0%";
  document.getElementById("pbarStatus").className = "";
  document.getElementById("pbarStatus").innerHTML = "";
}



var pbar_force_success = 0;
function pbar_move(timer,type, text) {
	if(text == '' ){
		text = "Success!!!";
	}

  var elem = document.getElementById("pbar");   
  var width = 0;
  var progress = setInterval(frame, timer);
  function frame() {
    if (width >= 100) {
      clearInterval(progress);
      document.getElementById("pbarStatus").className = "w3-text-green w3-animate-opacity";
      document.getElementById("pbarStatus").innerHTML = text;
      $("#doneBar").show();
      if (type == 'dump'){
          $("#dumpImage").show();
      }
    } else {
      if (pbar_force_success == 1) {
          width = 98;
          pbar_force_success = 0;
      }
      width++; 
      elem.style.width = width + '%'; 
      document.getElementById("pbarStatus").innerHTML = width+"%";
    }
  }
}

function pbar_progress_msg(msg) {
  $('#pbarStatus').text(msg);
}


//get browser timezone
function timezone_check(){

  //.var locale = navigator.language;
  var month = Ddate = 0;
  var d = new Date();
  timezone_diff = d.getTimezoneOffset();// / 60;
  sessionStorage.setItem("timezone_diff", timezone_diff);


  year = d.getYear()+1900;

  hour =  prefix_number(d.getHours());
  Ddate = prefix_number(d.getDate());
  min = prefix_number(d.getMinutes());
  sec = prefix_number(d.getSeconds());
 
  if(d.getMonth() < 11 ){

    month = d.getMonth() + 1;
    month = '0'+month;
  }else{
    month = d.getMonth() + 1;
  }
 
  var   localTime   = year + '-' + month + '-' + Ddate + ' ' + hour + ':' + min + ':' + sec;  
  $("#timing").val(localTime); 
}

function prefix_number(value){
  var tmp = 0;
  if(value < 10 ){
    tmp = '0'+value;
  }else{
    tmp = value;
  }
  return tmp;
}


//post or get controller-plus json
function controller_plus_result(from , type, job, arr, pi_time){

  var result = '';
  controller_plus_url = "controllerplus/scheduled/"+job;
    
  $.ajax({
    type: type,
    url: controller_plus_url,
    data: arr, // or JSON.stringify ({name: 'jonas'}),
    contentType: "application/json",
    dataType: 'json',
    success: function(data) { 

      switch(from){
        case 'reboot_detect':
        case 'reboot':
        //case 'nevent-ack':
          if(job == 'setting'){
            //alert("Add setting done");
          }else if ( type == "DELETE"){
            alert("Delete job done");
          }else{
            alert("Add job done");
          }
        break;
        case 'target_remote':
          break;
        case 'bbp_nv':
          break;
        //if status  = 3, it means topology scheduled still work
        case 'nevent-ack':
          if(job == "job"  && json_str == '{}'){
            if(data.job.length == 0){
              result =  "stop";
            }
            $.each(data.job ,  function(key, val){
              if(val.addr == '0xlocal' ){
                if(val.status == 3 ){
                  $("#set_nevent-ack").attr("disabled",true);
                  result =  true;
                  console.log(result);
                }else{
                  $("#set_nevent-ack").attr("disabled",false);
                  result =  false;
                  console.log(result);
                }
              }
            });   
          }
        break;

        default:
          type ='';
      }

      },
      error : function(jqXHR, status, exception) {
        if(from =='reboot' || from == 'reboot_detect'){
            $("#set_reboot").prop("checked","");
        }else if (type == 'topology'){
            $("#set_topology").prop("checked","");
        }
        $("#"+from+"_action").html("");
        console.log(jqXHR); console.log(status);
      }
      
  });
  return result;
}

/*for tabs */
function openTabs(evt, meterType) {
  var i, x, tablinks;
  x = document.getElementsByClassName("tabs");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace("w3-gray", " ");
  }
  document.getElementById(meterType).style.display = "block";
  evt.currentTarget.className += " w3-gray";
}



function query_meter_ver(addr){


var url_target = "controllerplus/app/target ";
var url_rev = "controllerplus/app/rev";
var url_ver = "controllerplus/app/ver";
var interval = 1000;
var promise = Promise.resolve();


//$("#queryBtn_up").prop("disabled", true);
$.get(url_target +"local" , function(data){});

$.each(addr, function(key, eui){
  meter_sw = meter_fw = '';
  if(eui != ''){
    promise = promise.then(function () {
      //console.log(eui);
      $.get(url_target + eui, function(data){
        if(data.error == "false"){
          $.get(url_ver, function(result_ver){
            if(result_ver.error == "false"){
              meter_sw = "SW : "+result_ver.build_time+"<br>";
              $("#ver_"+eui).html(meter_sw+meter_fw);
            }else{
              meter_sw = "SW : timeout <br>";
              $("#ver_"+eui).html(meter_sw+meter_fw);
            }
          });
          $.get(url_rev, function(result_rev){
            if(result_rev.error == "false"){
              meter_fw = " FW : "+result_rev.build_time;
              $("#ver_"+eui).html(meter_sw+meter_fw);
            }else{
              meter_fw = " FW : timeout <br>";
              $("#ver_"+eui).html(meter_sw+meter_fw);
            }
          });

        }
       
      });
      return new Promise(function (resolve) {
        setTimeout(resolve, interval);
      });
    });
  }
});

promise.then(function () {
  //console.log('Loop finished.');
  $("#loadingIMG").hide();
  $(".w3-display-middle").css({"background-color": "rgba(255, 255, 255)"});
  //alert("Query Version Finished !!");
});

}