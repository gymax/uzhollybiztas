    function select_catalog(){
        var val = status_code;		
        $("#status_00").hide();
        $("#status_01").hide();
        $("#status_02").hide();
        $("#status_03").hide();
        $("#status_04").hide();
        $("#status_05").hide();
        $("#status_06").hide();
		$("#status_07").hide();
        $("#status_08").hide();
        $("#status_10").hide();
        $("#status_99").hide();
        
        $("#instru_00").hide();
        $("#instru_01").hide();
        $("#instru_02").hide();
        $("#instru_03").hide();
        $("#instru_04").hide();
        $("#instru_05").hide();
        $("#instru_06").hide();
		$("#instru_07").hide();
        $("#instru_08").hide();
        $("#instru_10").hide();
        $("#instru_99").hide();
        
        if(val == 'ready'){
            $("#status_00").show();    
            $("#instru_00").show();
        } else if (val == 'running'){
            $("#status_01").show();    
            $("#instru_01").show();
        } else if (val == 'success'){
            $("#status_10").show();    
            $("#instru_10").show();
        } else if (val == 'timeout_first_join'){
            $("#status_02").show();
            $("#instru_02").show();
        } else if (val == 'timeout_second_join'){
            $("#status_06").show();
            $("#instru_06").show();
        } else if (val == 'timeout_upg'){
            $("#status_03").show();
            $("#instru_03").show();
        } else if (val == 'upg_fail'){
            $("#status_04").show();
            $("#instru_04").show();
        } else if (val == 'flash_issue'){
            $("#status_07").show();
            $("#instru_07").show();
        } else if (val == 'empty_id'){
            $("#status_08").show();
            $("#instru_08").show();
        } else {
            $("#status_99").show();    
            $("#instru_99").show();           
        }
    }
    function showlog(){
        var display = $("#cmdlog");
        console.log("cmdlog");
        var success_handle = function(response, status)
        {
            console.log("success");
            display.val(response);
        }
        var error_handle = function(response, status)
        {
            console.log("fail");
            //display.val ;
            display.hide();
        }
        $.ajax({
            type : 'get',
            url : 'log.txt',
            dataType : 'text',
            success : success_handle,
            error : error_handle,
            cache : false
        });
    }
    var uptime = 'NA';
    var version = "NA";
    function showinfo(){
        $("#uptime").text('升级已花费时间 : '+ uptime +'  (上一个状态)');
        $("#version").text('升级版本 : '+ version);
    }