TDP_FIR3_SCALE
    fir3_scale               : 0x00000086(134)
    fir3_scale_max           : 0x00000086(134)
    fir3_scale_min           : 0x0000000A(10)
    fir3_tx_success_cnt      : 0x00000017(23)
    fir3_add_3db_thr         : 0x0000000F(15)
    urgent_in_cnt            : 0x00000000(0)
    ot_event_cnt             : 0x00000000(0)
    start_specific_tx_scale  : 0x00000086(134)
    last_abnormal_tx_scale   : 0x00000086(134)
PHY_RX_MIB_CNT
    preamb_det                  : 123
    syncm_det_success           : 48 (Fail 75)
    fch_crc_correct             : 1
    ack_nack_fch_correct        : 0
    ACK/NACK/Unmatch_correct    : 0 / 0 / 0
    data_fch_correct            : 1
    data_correct                : 0 (Fail 1)
    apdt_basic_time_block(btb)  : 218
    apdt_preamb_det_scale       : 20 (Max 20, Min 20)
    apdt_preamb_noise_thr       : 30 (Max 30, Min1)
    apdt_latched_fa_cnt_per_btb : 0 (AVG 0.34)
    apdt_scale_event            : 0(Up), 0(Dn)
    apdt_huge_fa_in_rx_event    : 0
    syncm_location              :    0     1     2     3     4     5
                                     0     0     0     0     1    47 (counter)
    FCH Error                   : 0, (format 00 00 00 00  00 00 00 00)
PHY_TX_MIB_CNT               Data     ACK    NACK
    mac_send_cnt         :     23       0       0
    mac_abort_cnt        :      0       0       0
    mac_drop_cnt         :      0       0
    retransmit_cnt       : 0
    data_param_err_cnt   : 0

vt# Send Beacon (RC_COORD 0xFFFF)
Send Beacon (RC_COORD 0xFFFF)

vt# igt

IDLE_GAIN_STATISTIC(curr 42)
    analog_gain         : 36
    digital_gain        : 6
    igt_mib_process_cnt : 1353
    igt_mib_drop_cnt    : 1
    igt ( >=90dB)      : 0
    igt (   84dB)      : 0
    igt (   78dB)      : 0
    igt (   72dB)      : 0
    igt (   66dB)      : 0
    igt (   60dB)      : 4
    igt (   54dB)      : 7
    igt (   48dB)      : 276
    igt (   42dB)      : 1020
    igt (   36dB)      : 45
    igt (   30dB)      : 0
IGT_FORCE
    freeze              : 0
    analog_gain         : 36
    digital_gain        : 6
    cic1_pwr_max        : 27142 (expect 11400 ~ 22800)
    cic1_pwr_tgt        : 7600
    rx_filt_out_pwr_max : 12996 (expect 5700 ~ 11400)
    rx_filt_out_pwr_tgt : 3800

vt# lqi

LQI[28] = 1
LQI[41] = 1
AVG LQI = 34.50 (SNR 1073741822.2)
STD LQI = 6.5
SNR[-3] = 1
SNR[0] = 1
SNR_MIB    Preamble  Data
current  :       10     5
   0 dB  :       29     0
   1 dB  :        2     0
   2 dB  :        1     0
   3 dB  :        2     0
   4 dB  :        3     0
   5 dB  :        2     0
   6 dB  :        1     0
   7 dB  :        3     1
   8 dB  :        3     1
   9 dB  :        2     0
  10 dB  :       10     1
  11 dB  :        4     0
  12 dB  :        2     0
  13 dB  :        0     0
  14 dB  :        0     0
  15 dB  :        0     0
  16 dB  :        0     0
  17 dB  :        0     0
  18 dB  :        0     0
  19 dB  :        0     0
  20 dB  :        0     0
  21 dB  :        0     0