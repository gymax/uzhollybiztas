前情提要，一处台区为别墅富人区，从外部无法判断，线路和电表归属（只能看见5台表组网，而实际听说有40+台），且该区副部长非常重视，所以电力局请我们帮忙。
    1.1.上午前往了电力局所提供的台区2512，通过外挂表，发现该地区可以看见3台DCU，连进去发现除了黑名单的表一台表都没有。黑名单里面有2台表，下了这两台黑名单后，依然只有这两台组进台区。遂关闭了白名单，显示有14台表开始组网。
    1.2.等待10分钟，前往下一个附近台区2346，相距约20米。使用同样做法，关闭白名单，等待新表组网。
    1.3.等待10分钟，前往下一个台区（忘记记录），因距离约500米，且有8台拉闸表担心会有影响，所以没有继续操作。
    1.4.返回2512，发现组上51台表，有3台未组上。
    1.5.返回2346，发现组上47台表，有1台未组上，打算提供给电力局这两份档案。
	1.6.电力局并没有提供给我们原来40多台的档案，只能提供给组上网的电表请他们自行对比，如果还不够，补上组不上网的电表档案。
	![screen](现场示意图.jpg)